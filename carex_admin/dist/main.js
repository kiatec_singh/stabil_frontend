(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header-page></header-page>\n<!-- <div class=\"container\"> -->\n<div class=\"row\">\n    <div class=\"col-2 row--left-container\">\n        <!--Sidebar content-->\n        <menu-left></menu-left>\n\n    </div>\n    <div class=\"col-8\">\n        <!-- <dashboard></dashboard>  -->\n        <!-- <ganttchart></ganttchart>   -->\n        <router-outlet></router-outlet>\n    </div>\n    <div class=\"col-2 row--right-container\">\n        <menu-right></menu-right>\n        <!-- <widgets></widgets>  -->\n    </div>\n</div>\n<!-- </div> -->\n<footer-page></footer-page>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/analytics/analytics.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/analytics/analytics.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"analytics\">\n    <div [ngClass]=\"(isLoading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n    <h1 class=\"analytics__heading\">Carex Analytics</h1>\n    <div class=\"analytics__container\">\n        <div class=\"analytics__filtercontainer\">\n            <form [formGroup]=\"analyticsForm\" (ngSubmit)=\"getanalyticsData()\">\n                <div class=\"form-row\">\n                    <div class=\"form-group col-md-2\"></div>\n                    <div class=\"form-group col-md-3\">\n                        <label for=\"application\"><i class=\"fas fa-mobile-alt\"></i>Select Application*\n                                                </label>\n                        <select *ngIf=\"filteredapplications\" multiple [(ngModel)]=\"slctapplication\" data-live-search=\"true\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"Multiselect($event,item)\" id=\"exampleFormControlSelect1\">\n                                                        <option *ngFor=\"let item of filteredapplications; index as op;trackBy: trackByFn\"\n                                                                [selected]=\"item.selected===true ? 'selected':null\"\n                                                                data-tokens=\"item\" [ngValue]=\"item\">\n                                                                {{item.appname}}\n                                                        </option>\n                                                </select>\n                        <span *ngIf=\"!slctapplication || slctapplication.length==0\" class=\"analytics__errormsg\">Please select an application</span>\n                    </div>\n                    <div class=\"form-group col-md-3\">\n                        <label for=\"startdate\"><i class=\"fas fa-calendar-alt\"></i>Period</label>\n                        <input [selectMode]=\"'range'\" [(ngModel)]=\"selecteddates\" [readonly]=\"true\" [ngModelOptions]=\"{standalone: true}\" [owlDateTime]=\"i\" type=\"text\" class=\"form-control\" value=\"selecteddates\" [owlDateTimeTrigger]=\"i\" placeholder=\"ubegrænset\" />\n                        <span class=\"analytics__dateicon\" [owlDateTimeTrigger]=\"i\"><i class=\"fal fa-calendar-alt\"></i></span>\n                        <span class=\"analytics__closeicon\" (click)=\"clearstartDate(item)\"><i class=\"fal fa-times\"></i></span>\n                        <owl-date-time (afterPickerClosed)=\"StartDate(item,i)\" [disabled]=\"false\" #i></owl-date-time>\n                    </div>\n                    <div class=\"form-group col-md-2\" *ngIf=\"filteredata && filteredata.length>0\">\n                        <div *ngIf=\"isdownloading===true\" class=\"spinner\"></div>\n                        <div class=\"analytics__info\">\n                            <button (click)=\"download($event)\" class=\"btn\"><i class=\"fas fa-cloud-download\"></i>Download Frontend analytics report</button>\n                        </div>\n                    </div>\n                    <div class=\"form-group col-md-2\"></div>\n\n                </div>\n                <div class=\"form-group row\">\n                    <div class=\"form-group col-md-4\">\n                    </div>\n                    <div class=\"form-group col-md-4\">\n                        <button [disabled]=\"!slctapplication || slctapplication.length==0\" type=\"submit\" (click)=\"fetchdata()\" class=\"btn login__login btn-lg\">Search</button>\n                    </div>\n                    <div class=\"form-group col-md-4\">\n                    </div>\n                </div>\n                <!-- <div class=\"form-group col-md-2\">\n                        <div class=\"analytics__info\">\n                            <div *ngIf=\"isLoading===true\" class=\"spinner\"></div>\n                            <p class=\"analytics__chartnamee\"><i class=\"fas fa-users\"></i> Total users visits </p>\n                            <h2 *ngIf=\"filteredata && filteredata.length\">\n                                {{filteredata.length}}</h2>s\n                        </div>\n                    </div>\n                    <div class=\"form-group col-md-2\">\n                        <div class=\"analytics__info\">\n                            <div *ngIf=\"isLoading===true\" class=\"spinner\"></div>\n                            <p class=\"analytics__chartnamee\"><i class=\"fas fa-users\"></i> Total users</p>\n                            <h2 *ngIf=\"totalusersData && totalusersData.length\">\n                                {{totalusersData.length}}</h2>\n                        </div>\n                    </div> -->\n                <!-- <div class=\"form-group row\" >\n                </div> -->\n            </form>\n        </div>\n\n    </div>\n    <!-- <div class=\"analytics__overview\">\n        <div *ngIf=\"slctapplication && slctapplication.length!=0\" class=\"\">\n            <div class=\"row\">\n                <div class=\"col-sm-4\">\n                    <div *ngIf=\"isLoading===true\" class=\"spinner\"></div>\n                    <div class=\"analytics__dashboardchart\">\n                        <p class=\"analytics__chartname\">Pages by views </p>\n                        <google-chart *ngIf=\"eventData\" [type]=\"piechart\" [data]=\"eventData\" [options]=\"pieoptions\">\n                        </google-chart>\n                    </div>\n                </div>\n                <div class=\"col-sm-4\">\n                    <div *ngIf=\"isLoading===true\" class=\"spinner\"></div>\n                    <div class=\"analytics__dashboardchart\">\n                        <p class=\"analytics__chartname\">Devices</p>\n                        <google-chart *ngIf=\"platformsData\" [type]=\"piechart\" [data]=\"platformsData\" [options]=\"platformspieoptions\">\n                        </google-chart>\n                    </div>\n                </div>\n                <div class=\"col-sm-4\">\n                    <div *ngIf=\"isLoading===true\" class=\"spinner\"></div>\n                    <div class=\"analytics__dashboardchart\">\n                        <p class=\"analytics__chartname\">Versions</p>\n                        <google-chart *ngIf=\"versionsData\" [type]=\"columnchart\" [data]=\"versionsData\" [options]=\"versionscolumnoptions\">\n                        </google-chart>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-sm-6\">\n                    <div *ngIf=\"isLoading===true\" class=\"spinner\"></div>\n                    <div class=\"analytics__dashboardchart\">\n                        <p class=\"analytics__chartname\">Most active users</p>\n                        <google-chart *ngIf=\"activeusersData\" [type]=\"columnchart\" [data]=\"activeusersData\" [options]=\"activeuserscolumnoptions\">\n                        </google-chart>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div> -->\n</div>\n<jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/breadcrum/breadcrum.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/breadcrum/breadcrum.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"\">\n    <ul id=\"breadcrumb\" *ngIf=\"breadcrumelements\" class=\"breadcrum__breadcrumcontainer\">\n        <ng-template ngFor let-item [ngForOf]=\"breadcrumelements\" let-i=\"index\" let-isEven=\"even\" let-isOdd=\"odd\"\n            [ngForTrackBy]=\"trackByFn\">\n            <li>\n                <a href=\"#\">\n                </a>\n            </li>\n            <li>\n                <a href=\"#\">\n                    {{item}}\n                </a>\n            </li>\n        </ng-template>\n        <li>\n            <a href=\"#\">\n            </a>\n        </li>\n        <li>\n            <a href=\"#\">\n\n            </a>\n        </li>\n    </ul>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/changepassword/changepassword.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/changepassword/changepassword.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"changepassword\">\n    <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n\n\n    <p text-center class=\"login__heading\">Vi har sendt dig en engangskode, som du skal bruge til at oprette nyt kodeord. Engangskoden er gyldig i to timer. Der kan gå nogle minutter, før koden når frem til dig. Hvis du ikke modtager koden i din indbakke, så kontrollér eventuelt din folder\n        for uønsket mail</p>\n    <form [formGroup]=\"loginForm\" class=\"changepassword__form\" (ngSubmit)=\"gotologin()\">\n        <div class=\"form-group\">\n            <label for=\"exampleInputPassword1\">Kodeord</label>\n            <input (keyup)=\"inputvalueChange()\" [(ngModel)]=\"loginForm.password\" formControlName=\"password\" #passwordele type=\"password\" class=\"form-control login__textbox\" placeholder=\"Password\">\n            <span toggle=\"#input-pwd\" (click)=\"showpassword()\" [ngClass]=\"(showpwd===true)?'login__eye fa fa-fw fa-eye field-icon': 'login__eye fa fa-fw fa-eye-slash field-icon'\"></span>\n            <div *ngIf=\"loginForm.controls.password.invalid && (loginForm.controls.password.dirty || loginForm.controls.password.touched)\">\n                <p class=\"login__error alert alert-danger\" *ngIf=\"loginForm.controls.password.errors.Numbers\">Skal have mindst et tal</p>\n                <p class=\"login__error alert alert-danger\" *ngIf=\"loginForm.controls.password.errors.Lower\">Skal have mindst et lille bogstav</p>\n                <p class=\"login__error alert alert-danger\" *ngIf=\"loginForm.controls.password.errors.Upper\">Skal have mindst et stort bogstav</p>\n                <p class=\"login__error alert alert-danger\" *ngIf=\"loginForm.controls.password.errors.long\">Dit kodeord skal være mindst 8 tegn langt</p>\n            </div>\n        </div>\n\n\n        <div class=\"form-group\">\n            <label for=\"exampleInputPassword1\">Gentag kodeord</label>\n            <input formControlName=\"confirmPassword\" (keyup)=\"inputvalueChange()\" [(ngModel)]=\"loginForm.confirmPassword\" #repasswordele type=\"password\" class=\"form-control login__textbox\" placeholder=\"Password\">\n            <span toggle=\"#input-pwd\" (click)=\"showrepassword()\" [ngClass]=\"(showrepwd===true)?'login__eye fa fa-fw fa-eye field-icon': 'login__eye fa fa-fw fa-eye-slash field-icon'\"></span>\n            <div *ngIf=\"loginForm.controls.confirmPassword.invalid && (loginForm.controls.confirmPassword.dirty || loginForm.controls.confirmPassword.touched)\">\n                <p class=\"login__error alert alert-danger\" *ngIf=\"loginForm.controls.confirmPassword.errors && loginForm.controls.confirmPassword.errors.MatchPassword\">\n                    Kodeordene skal være ens.</p>\n            </div>\n        </div>\n\n        <div *ngIf=\"serviceerror\">\n            <p class=\"login__error\" *ngIf=\"serviceerror && serviceerrormessage\">{{serviceerrormessage}}</p>\n        </div>\n        <div *ngIf=\"errormessage\">\n            <div class=\"login__error\">{{errormessage}}</div>\n        </div>\n        <button type=\"submit\" [disabled]=\"!((loginForm.value.password  === loginForm.value.confirmPassword) && (loginForm.controls.password.errors === null) &&(loginForm.controls.confirmPassword.errors && !loginForm.controls.confirmPassword.errors.MatchPassword ) )\"\n            class=\"btn login__login btn-lg\">Gem kodeord</button>\n        <button color=\"secondary\" type=\"button\" (click)=\"resetForm()\" class=\"btn login__forgetpwd btn-lg\">Ryd</button>\n    </form>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/chat-container/chat-container.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/chat-container/chat-container.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"chat-conatiner\">\n    <div *ngIf=\"chatenable==true\" class=\"chat-container__chat-box\">\n        <div class=\"chat-container__header\">\n            <!-- \n            <a (click)=\"hide($event)\" href=\"#\"><i class=\"fa fa-angle-down\"></i></a>  <a (click)=\"close($event)\" href=\"#\"><i class=\"fa fa-times\"></i></a> -->\n            <a (click)=\"close($event)\" href=\"#\">okay, got it!</a>\n        </div>\n        <div class=\"chatlist\">\n            <ul class=\"list-group chat-container__list-group\">\n                <li *ngFor=\"let msg of messages\" class=\"list-group-item\">\n                    <div class=\"message\">\n                        {{msg}}\n                    </div>\n                    <div class=\"timeform\">\n                        <i class=\"fa fa-clock-o\" aria-hidden=\"true\"></i>\n                        <span class=\"chat-container__timestamp\"> 12/10/2018 10:21</span>\n                    </div>\n                </li>\n            </ul>\n        </div>\n        <div class=\"chat-container__control\">\n            <form class=\"form-horizontal\">\n                <input [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"chatquestion\" type=\"text\"\n                    class=\"form-control chatinput\" />\n                <button class=\"btn btn-success sendbtn\" (click)=\"sendchatMessage($event)\">Send</button>\n            </form>\n        </div>\n    </div>\n    <div *ngIf=\"chatenable==false\" class=\"chat-container__icon\">\n        <a (click)=\"open($event)\" href=\"#\">Ask me Anything!\n            <i class=\"fa fa-comments chat-container__chat-icon\"></i>\n        </a>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/configuration/configuration.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/configuration/configuration.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\n    <div class=\"configuration\">\n        <div class=\"configuration__header-container\">\n            <div class=\"configuration__header\">\n                <breadcrum-viewer [breadcrumelements]=\"navigationElements\"></breadcrum-viewer>\n                <div class=\"detailspage__operations\">\n                    <div>\n                        <button (click)=\"Cancel($event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\" title=\"Cancel save\">\n                                <i class=\"fa fa-times-circle\"></i>Annuller</button>\n                        <button type=\"submit\" (click)=\"saveConfiguraiton($event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\">\n                                <i class=\"fa fa-save\"></i>Gem\n                            </button>\n                    </div>\n                </div>\n            </div>\n            <div class=\"configuration__form-container\">\n                <div class=\"configuration__nav-container\">\n                    <ul class=\"nav nav-pills nav-justified\" id=\"pills-tab\" role=\"tablist\">\n                        <li class=\"nav-item\">\n                            <a data-toggle=\"pill\" href=\"#pills-i\" id=\"pills-i-tab\" role=\"tab\" class=\"nav-link active\" aria-controls=\"pills-i\" aria-selected=\"true\">Rediger App Brand\n                                </a>\n                        </li>\n                    </ul>\n                </div>\n\n                <div class=\"tab-content configuration__container\" id=\"pills-tabContent\">\n                    <div class=\"tab-pane fade show active\" id=\"pills-i\" role=\"tabpanel\" aria-labelledby=\"pills-i-tab\">\n                        <div class=\"form-group\">\n                            <form *ngIf=\"alldokumentList && alldokumentList.length>0\">\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">\n                                            Dokument</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctdok\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChange($event,item)\" id=\"slctdok\">\n                                                <ng-template ngFor let-item [ngForOf]=\"alldokumentList\" let-i=\"index\" let\n                                                    first=first; [ngForTrackBy]=\"trackByFn\">\n                                                    <option [ngValue]=\"item.uuid\">\n                                                        {{item.brugervendtnoegle}}\n                                                    </option>\n                                                </ng-template>\n                                            </select>\n                                        <span ngxClipboard (cbOnSuccess)=\"copied($event)\" [cbContent]=\"slcOrgname\" (click)=\"Copyorg('orgcopyflag')\" [ngClass]=\"orgcopyflag === true?' configuration__copyicon configuration__copyicon--active':'configuration__copyicon configuration__copyicon--inactive'\"><i\n                                                    class=\"far fa-copy\"></i></span>\n                                    </div>\n\n\n                                </div>\n\n                                <div>\n                                    <div class=\"form-group row\" *ngIf=\"jsoneditor===true\">\n                                        <label id=\"inputGroupFile04\" class=\"col-sm-4 col-form-label\">Brand</label>\n                                        <div *ngIf=\"showupload===false\" class=\"col-sm-8\">\n                                            <a class=\"configuration__download\" (click)=\"DownloadJson($event)\" href=\"#\"> <i class=\"fas fa-cloud-download\"></i> Download Brand</a>\n                                            <a class=\"configuration__delete\" (click)=\"Deleteconfig($event,item,relationoptionitem,in)\" href=\"#\"> <i class=\"fas fa-trash\"></i> Delete Brand</a>\n                                            <json-editor [options]=\"editorOptions\" (change)=\"getData($event)\" [data]=\"body\">\n                                            </json-editor>\n                                        </div>\n                                        <div class=\"col-sm-8\" *ngIf=\"showupload===true\">\n                                            <input type=\"file\" class=\"custom-file-input form-control\" [(ngModel)]=\"fileuploaded\" #fileupload [ngModelOptions]=\"{standalone: true}\" (change)=\"uploadfile($event)\" id=\"inputGroupFile04\" />\n                                            <i class=\"fas fa-cloud-upload\">Upload a new Json configuation</i>\n                                        </div>\n                                    </div>\n\n                                </div>\n\n                            </form>\n\n\n                        </div>\n\n                    </div>\n                </div>\n\n\n                <jqxNotification #successNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"3000\" [template]=\"'success'\">\n                    <div>\n                        Success!! Thank you..\n                    </div>\n                </jqxNotification>\n                <jqxNotification #errorNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"6000\" [template]=\"'error'\">\n                    <div>Error!! pelase try again</div>\n                </jqxNotification>\n\n            </div>\n\n        </div>\n    </div>\n    <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/createpage/createpage.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/createpage/createpage.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"createpage\">\n  <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n  </div>\n  <div class=\"tab-content createpage__container\" id=\"pills-tabContent\">\n    <div *ngIf=\"detailsData\" class=\"form-group\">\n      <customform [detailsData]=\"detailsData\"></customform>\n    </div>\n  </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/customform/customform.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/customform/customform.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"customform\">\n\n    <div class=\"modal-header detailspage__filterctr\">\n        <button type=\"button\" class=\"btn detailspage__filterclose\" (click)=\"cancel($event)\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"\">\n        <div class=\"customform__lastupdated\"> Opdateret\n            <span class=\"customform__date\">{{lastupdatedTime}}</span> af {{brugerName}}\n        </div>\n        <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n        </div>\n        <div *ngIf=\"detailsform\" class=\"customform__form-container\">\n            <div class=\"customform__nav-container\">\n                <ul class=\"nav nav-pills nav-justified\" id=\"pills-tab\" role=\"tablist\">\n                    <li class=\"nav-item\">\n                        <a data-toggle=\"pill\" role=\"tab\" class=\"nav-link  active\">Opret</a>\n                    </li>\n                </ul>\n            </div>\n            <div class=\"tab-content customform__container\" id=\"pills-tabContent\">\n                <div class=\"form-group\">\n                    <form class=\"form-horizontal row\" id=\"detailForm\" #detailForm novalidate [formGroup]=\"detailsForm\">\n                        <ng-template ngFor let-item [ngForOf]=\"detailsform.get('items').controls\" let-i=\"index\" [ngForTrackBy]=\"trackByFn\">\n                            <div class=\"col-sm-10\">\n                                <div class=\"detailspage__accordianscontainer\" *ngIf=\"item.value.accordian!=false && item.value.allaccordians; else noaccordian\">\n                                    <ngb-accordion #acc=\"ngbAccordion\" id=\"{{i}}\" [activeIds]=\"activeIds\">\n                                        <ngb-panel id=\"panel-{{i}}\" title=\"{{item.value.title}}\">\n                                            <ng-template ngbPanelContent>\n                                                <ng-template ngFor let-accordianitem [ngForOf]=\"item.value.allaccordians\" let-k=\"index\" [ngForTrackBy]=\"trackByFn\">\n                                                    <div class=\"form-group row\" id=\"{{accordianitem.name}}\" *ngIf=\"accordianitem.type=='input' && accordianitem.ld_brugervendtnoegle!=='adresser'  && accordianitem.type!=='file' && accordianitem.ld_brugervendtnoegle!=='body'\">\n                                                        <label title=\"{{accordianitem.tooltip}}\" class=\"col-sm-4 col-form-label\">{{accordianitem.name}}</label>\n                                                        <div class=\"col-sm-8\" *ngIf=\"accordianitem.required=='true'\">\n                                                            <input disabled={{accordianitem.readonly}} class=\"form-control\" required [(ngModel)]=\"accordianitem.value\" [ngModelOptions]=\"{standalone: true}\" value={{accordianitem.value}}>\n                                                            <div id=\"{{item.value.ld_brugervendtnoegle}}\" class=\"invalid-feedback {{item.value.ld_brugervendtnoegle}}\">\n                                                                Please fill this field.\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-sm-8\" *ngIf=\"accordianitem.required=='false'\">\n                                                            <input disabled={{accordianitem.readonly}} class=\"form-control\" [(ngModel)]=\"accordianitem.value\" [ngModelOptions]=\"{standalone: true}\" value={{accordianitem.value}}>\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"form-group row\" id=\"{{accordianitem.name}}\" *ngIf=\"accordianitem.type=='editor'\">\n                                                        <label title=\"{{accordianitem.tooltip}} \" class=\"col-sm-4 col-form-label \">{{accordianitem.name}}</label>\n                                                        <div class=\"col-sm-8 \">\n                                                            <span (click)=\"openfilter(filters,$event,item)\" class=\"detailspage__user\"><i class=\"fal fa-user-alt\"></i></span>\n                                                            <ckeditor #editor id=\"editor\" type=\"classic\" [config]=\"config\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"accordianitem.value\" (change)=\"onchngEditordata($event) \"></ckeditor>\n                                                            <div *ngIf=\"accordianitem.required=='true'\" id=\"{{item.value.ld_brugervendtnoegle}}\" class=\"invalid-feedback {{item.value.ld_brugervendtnoegle}}\">\n                                                                Please fill this field.\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </ng-template>\n                                            </ng-template>\n                                        </ngb-panel>\n                                    </ngb-accordion>\n                                </div>\n\n                                <ng-template #noaccordian>\n                                    <div class=\"form-group row \" id=\"{{item.value.name}} \" *ngIf=\"item.value.type=='input' && item.value.ld_brugervendtnoegle!=='adresser' && item.value.hide===false && item.value.ld_brugervendtnoegle.indexOf(\n                                                                'skabelontekst')==-1 && item.value.type!=='file' && item.value.ld_brugervendtnoegle!=='body' \">\n                                        <label title=\"{{item.value.tooltip}} \" class=\"col-sm-4 col-form-label \">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8 \" *ngIf=\"item.value.required=='true' && item.value.hide===false \">\n                                            <input disabled={{item.value.readonly}} class=\"form-control \" required [(ngModel)]=\"item.value.value \" [ngModelOptions]=\"{standalone: true} \" value={{item.value.value}}>\n                                            <div class=\"invalid-feedback \">\n                                                Please fill this field.\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-8 \" *ngIf=\"item.value.required=='false' \">\n                                            <input disabled={{item.value.readonly}} class=\"form-control \" [(ngModel)]=\"item.value.value \" [ngModelOptions]=\"{standalone: true} \" value={{item.value.value}}>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row \" id=\"{{item.value.name}} \" *ngIf=\"item.value.type=='input' && item.value.ld_brugervendtnoegle.indexOf( 'skabelontekst')>-1\">\n                                        <label title=\"{{item.value.tooltip}}\" class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\" *ngIf=\"item.value.required=='false'\">\n                                            <span (click)=\"openfilter(filters,$event,item)\" class=\"detailspage__user\"><i class=\"fal fa-user-alt\"></i></span>\n                                            <ckeditor #editor id=\"editor\" type=\"classic\" [config]=\"config\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"item.value.value\" (change)=\"onchngEditordata($event)\"></ckeditor>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" id=\"{{item.value.name}}\" *ngIf=\"item.value.type=='file' && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\" class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n\n                                        <div class=\"col-sm-4\" *ngIf=\"item.value.value\">\n\n                                            <a class=\"btn btn-link\" (click)=\"downloadFile($event)\" href=\"{{item.value.value}}\">{{item.value.value}}</a>\n                                            <span class=\"customform__downloadFile\"></span>\n                                        </div>\n\n                                        <div [ngClass]=\"(item.value.value) ? 'col-sm-4':'col-sm-8'\" *ngIf=\"item.value.required=='true'\">\n\n                                            <span> upload a new file</span>\n                                            <input *ngIf=\"readonly==false\" (change)=\"selectFile($event,item)\" class=\"form-control\" type=\"file\" required>\n                                            <input *ngIf=\"readonly==true\" disabled (change)=\"selectFile($event,item)\" class=\"form-control\" type=\"file\" required>\n                                            <div class=\"invalid-feedback\">\n                                                Please fill this field.\n                                            </div>\n                                        </div>\n                                        <div [ngClass]=\"(item.value.value) ? 'col-sm-4':'col-sm-8'\" *ngIf=\"item.value.required=='false'\">\n                                            <input *ngIf=\"readonly==false\" (change)=\"selectFile($event,item)\" class=\"form-control\" type=\"file\">\n                                            <input *ngIf=\"readonly==true\" disabled (change)=\"selectFile($event,item)\" class=\"form-control\" type=\"file\">\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" id=\"{{item.value.name}}\" *ngIf=\"item.value.type=='input' && item.value.ld_brugervendtnoegle=='adresser' && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\" class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\" *ngIf=\"item.value.required=='true'\">\n                                            <div class=\"autocomplete-container\">\n                                                <input disabled={{item.value.readonly}} class=\"form-control\" required [(ngModel)]=\"item.value.value\" [ngModelOptions]=\"{standalone: true}\" type=\"text\" autocomplete=\"off\" [value]=\"selectedStreet\" ngxDawaAutocomplete (items$)=\"onItems($event)\" (keyup)=\"ErrorMsg($event)\"\n                                                    (highlighted$)=\"onItemHighlighted($event)\" (select$)=\"onItemSelected($event)\" />\n                                                <div class=\"invalid-feedback\">\n                                                    Please fill this field.\n                                                </div>\n                                                <div *ngIf=\"itemss.length > 0\">\n                                                    <ul class=\"autocomplete-items\">\n                                                        <li class=\"autocomplete-item\" *ngFor=\"let item of itemss; let i = index;\" [class.highlighted]=\"i === highlightedIndex\" (click)=\"onItemSelected(item)\">\n                                                            {{item.text}}\n                                                        </li>\n                                                    </ul>\n                                                </div>\n                                                <div *ngIf=\"enableaddresserrorMsg === true\">\n                                                    <div *ngIf=\"(addresserrorMesg === true)  && itemss.length== 0\" class=\"text-danger\">\n                                                        Adressen kunne ikke verificeres. Prøv at skrive lidt af adressen, og find den via de viste forslag.\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-8\" *ngIf=\"item.value.required=='false' && item.value.hide===false\">\n                                            <div class=\"autocomplete-container\">\n                                                <input disabled={{item.value.readonly}} class=\"form-control\" [(ngModel)]=\"item.value.value\" [ngModelOptions]=\"{standalone: true}\" type=\"text\" [value]=\"selectedStreet\" ngxDawaAutocomplete (items$)=\"onItems($event)\" (keyup)=\"ErrorMsg($event)\" (highlighted$)=\"onItemHighlighted($event)\"\n                                                    (select$)=\"onItemSelected($event)\" />\n                                                <div class=\"invalid-feedback\">\n                                                    Please fill this field.\n                                                </div>\n                                                <div *ngIf=\"itemss.length > 0\">\n                                                    <ul class=\"autocomplete-items\">\n                                                        <li class=\"autocomplete-item\" *ngFor=\"let item of itemss; let i = index;\" [class.highlighted]=\"i === highlightedIndex\" (click)=\"onItemSelected(item)\">\n                                                            {{item.text}}\n                                                        </li>\n                                                    </ul>\n                                                </div>\n                                                <div *ngIf=\"enableaddresserrorMsg === true\">\n                                                    <div *ngIf=\"(addresserrorMesg === true)  && itemss.length== 0\" class=\"text-danger\">\n                                                        Adressen kunne ikke verificeres. Prøv at skrive lidt af adressen, og find den via de viste forslag.\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" id=\"{{item.value.ld_brugervendtnoegle}}\" *ngIf=\"item.value.type=='dropdown' && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\" class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\" *ngIf=\"item.value.required === 'true'\">\n\n\n                                            <!-- multiselect==='false' -->\n                                            <select *ngIf=\"item.value.multiselect==='false' && item.value.relationsvirkning===false\" required (change)=\"Select($event,item)\" [(ngModel)]=\"item.value.selected_name\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" data-live-search=\"true\">\n                                            <option\n                                                *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                [selected]=\"optionitems.selected === true ? 'selected':null\"\n                                                [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                {{optionitems.rel_uri}}\n                                            </option>\n                                        </select>\n\n                                            <!-- multiselect==='true' -->\n                                            <select multiple *ngIf=\"item.value.multiselect==='true' && item.value.relationsvirkning===false\" required class=\"form-control selectpicker show-tick\" data-live-search=\"true\" [(ngModel)]=\"item.value.selected_name\" [ngModelOptions]=\"{standalone: true}\"\n                                                (change)=\"Multiselect($event,item)\">\n                                            <option\n                                                *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                [selected]=\"optionitems.selected=== true ? 'selected':null\"\n                                                [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                {{optionitems.rel_uri}}\n                                            </option>\n                                        </select>\n                                            <div id=\"{{item.value.ld_brugervendtnoegle}}\" class=\"invalid-feedback {{item.value.ld_brugervendtnoegle}}\">\n                                                Please fill this field.\n                                            </div>\n\n                                            <!----------new component-->\n                                            <!-- //todo required -->\n                                            <div *ngIf=\"item.value.multiselect==='true' && item.value.relationsvirkning===true\" [ngClass]=\"(opentoogle === false)? 'customform__multiselect customform__multiselect--close':'customform__multiselect customform__multiselect--open'\">\n                                                <ng-template ngFor let-relationoptionitem [ngForOf]=\"item.value.options\" let-j=\"index\" [ngForTrackBy]=\"trackByFn\">\n                                                    <div class=\"customform__multiselectitem  col-sm-5\">\n\n                                                        <div [ngClass]=\"(relationoptionitem.selected===true)? 'customform__sltd': ' customform__unsltd'\">\n                                                            <label class=\"form-check-label customform__selectItemlabel\">{{relationoptionitem.rel_uri}}\n                                                            <input\n                                                                [checked]=\"relationoptionitem.selected===true ? true:false\"\n                                                                [(ngModel)]=\"relationoptionitem.selected\"\n                                                                [ngModelOptions]=\"{standalone: true}\" type=\"checkbox\"\n                                                                (change)=\"Multiselectrelation($event,item,relationoptionitem)\"\n                                                                class=\"form-check-input customform__customcheck\">\n                                                            <span class=\"customform__checkmark\"></span>\n                                                        </label>\n                                                        </div>\n                                                        <!-- Delete -->\n                                                        <ng-template ngFor let-intervalitem [ngForOf]=\"relationoptionitem.intervals\" let-in=\"index\" [ngForTrackBy]=\"trackByFn\">\n                                                            <div class=\"customform__multiselectelement\">\n                                                                <div class=\"customform__deletecontainer\">\n                                                                    <a title=\"Delete new entry\" (click)=\"Deletedateitem($event,item,relationoptionitem,in)\" class=\"button customform__deleteitem\" href=\"#\">\n                                                                        <i class=\"fa fa-minus-circle\"></i></a>\n                                                                </div>\n                                                                <!-- fra -->\n                                                                <div class=\"customform__multiselectitemdate\">\n                                                                    <label class=\"form-check-label customform__mltsltd-date-lbl\">Fra\n                                                                    :\n                                                                </label>\n                                                                    <div class=\"customform__mltsltd-dateitem\">\n                                                                        <input [(ngModel)]=\"intervalitem.fra\" [min]=\"mindate\" [ngModelOptions]=\"{standalone: true}\" [readonly]=\"true\" [owlDateTime]=\"j\" type=\"text\" class=\"form-control\" value={{intervalitem.fra}} [owlDateTimeTrigger]=\"j\" placeholder=\"\" />\n                                                                        <span class=\"customform__dateicon\" [owlDateTimeTrigger]=\"j\"><i\n                                                                            class=\"fal fa-calendar-alt\"></i></span>\n                                                                        <span class=\"customform__closeicon\" (click)=\"clearDate(item)\"><i\n                                                                            class=\"fal fa-times\"></i></span>\n                                                                        <owl-date-time (afterPickerClosed)=\"dateEditfra(item,intervalitem,relationoptionitem,in,j)\" [disabled]=\"false\" [pickerType]=\"'both'\" #j>\n                                                                        </owl-date-time>\n                                                                    </div>\n                                                                </div>\n                                                                <!-- til -->\n                                                                <div class=\"customform__multiselectitemdate\">\n                                                                    <label class=\"form-check-label customform__mltsltd-date-lbl\">Til\n                                                                    :\n                                                                </label>\n                                                                    <div class=\"customform__mltsltd-dateitem\">\n                                                                        <input [(ngModel)]=\"intervalitem.til\" [min]=\"mindate\" [ngModelOptions]=\"{standalone: true}\" [readonly]=\"true\" [owlDateTime]=\"k\" type=\"text\" class=\"form-control\" value={{intervalitem.til}} [owlDateTimeTrigger]=\"k\" placeholder=\"\" />\n                                                                        <span class=\"customform__dateicon\" [owlDateTimeTrigger]=\"k\"><i\n                                                                            class=\"fal fa-calendar-alt\"></i></span>\n                                                                        <span class=\"customform__closeicon\" (click)=\"clearDate(item)\"><i\n                                                                            class=\"fal fa-times\"></i></span>\n                                                                        <owl-date-time (afterPickerClosed)=\"dateEdittil(item,intervalitem,relationoptionitem,index,k)\" [disabled]=\"false\" [pickerType]=\"'both'\" #k>\n                                                                        </owl-date-time>\n\n                                                                    </div>\n                                                                </div>\n                                                                <div class=\"customform__multiselectitemdate\">\n                                                                    <div class=\"form-group\">\n                                                                        <label class=\"form-check-label customform__mltsltd-date-lbl\">Note:</label>\n                                                                        <textarea class=\"form-control customform__multiselectitemtextarea\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"intervalitem.note\" rows=\"2\"></textarea>\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                        </ng-template>\n\n                                                        <div class=\"customform__addcontainer\">\n                                                            <a title=\"Create new entry\" (click)=\"Createdateitem($event,item,relationoptionitem)\" class=\"button customform__additem\" href=\"#\">\n                                                                <i class=\"fa fa-plus-circle\"></i>Opret</a>\n                                                        </div>\n                                                    </div>\n                                                </ng-template>\n\n                                                <div [ngClass]=\"(opentoogle === false)? 'customform__opentoogle customform__opentoogle--show': 'customform__opentoogle customform__opentoogle--hide'\">\n                                                    <a href=\"#\" (click)=\"tooglepakker($event)\">\n                                                        <i class=\"fas fa-chevron-circle-down\"></i>\n                                                    </a>\n                                                </div>\n                                                <div [ngClass]=\"(opentoogle === false)? 'customform__closetoogle customform__closetoogle--hide': 'customform__closetoogle customform__closetoogle--show'\">\n                                                    <a href=\"#\" (click)=\"tooglepakker($event)\">\n                                                        <i class=\"fas fa-chevron-circle-up\"></i>\n                                                    </a>\n\n                                                </div>\n\n                                            </div>\n                                            <!----------END  new component-->\n                                        </div>\n                                        <div class=\"col-sm-8\" *ngIf=\"item.value.required === 'false'  && item.value.relationsvirkning===false && item.value.hide===false\">\n                                            <select *ngIf=\"item.value.multiselect==='false' && item.value.relationsvirkning===false\" (change)=\"Select($event,item)\" [(ngModel)]=\"item.value.selected_name\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" data-live-search=\"true\">\n                                            <option\n                                                *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                [selected]=\"optionitems.selected===true ? 'selected':null\"\n                                                [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                {{optionitems.rel_uri}}\n                                            </option>\n                                        </select>\n                                            <select multiple *ngIf=\"item.value.multiselect==='true' && item.value.relationsvirkning===false\" class=\"form-control selectpicker show-tick\" data-live-search=\"true\" #multiselect [(ngModel)]=\"item.value.selected_name\" [ngModelOptions]=\"{standalone: true}\"\n                                                (change)=\"Multiselect($event,item)\">\n                                            <option\n                                                *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                [selected]=\"optionitems.selected===true ? 'selected':null\"\n                                                [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                {{optionitems.rel_uri}}\n                                            </option>\n                                        </select>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" id=\"{{item.value.ld_brugervendtnoegle}}\" *ngIf=\"item.value.type=='date' && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\" class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\">\n                                            <input [min]=\"item.value.mindate\" [(ngModel)]=\"item.value.formatdate\" [readonly]=\"true\" [ngModelOptions]=\"{standalone: true}\" [owlDateTime]=\"i\" type=\"text\" [required]=\"item.value.required==='true'? 'required' : null\" class=\"form-control\" value={{item.value.value}}\n                                                [disabled]=\"item.value.readonly===true? true : false\" [owlDateTimeTrigger]=\"i\" placeholder=\"\" />\n                                            <span class=\"customform__dateicon\" [owlDateTimeTrigger]=\"i\"><i\n                                                class=\"fal fa-calendar-alt\"></i></span>\n                                            <span class=\"customform__closeicon\" (click)=\"clearDate(item)\"><i\n                                                class=\"fal fa-times\"></i></span>\n                                            <owl-date-time (afterPickerClosed)=\"dateChange(item,i)\" [disabled]=\"false\" [pickerType]=\"'both'\" #i></owl-date-time>\n                                            <div id=\"{{item.value.ld_brugervendtnoegle}}\" *ngIf=\"item.value.required === 'true'\" class=\"invalid-feedback {{item.value.ld_brugervendtnoegle}}\">\n                                                Please fill this field.\n                                            </div>\n                                        </div>\n\n                                    </div>\n\n                                    <div class=\"form-group row\" id=\"{{item.value.ld_brugervendtnoegle}}\" *ngIf=\"item.value.type=='textarea' && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\" class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\">\n                                            <textarea disabled={{item.value.readonly}} [(ngModel)]=\"item.value.value\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control\" value={{item.value.value}} id=\"exampleFormControlTextarea1\" rows=\"15\"></textarea>\n                                        </div>\n                                    </div>\n                                </ng-template>\n                            </div>\n                            <div *ngIf=\"item.value.type=='file' && item.value.image\">\n                                <ng-container [ngTemplateOutlet]=\"imageTemplate\" [ngTemplateOutletContext]=\"{$implicit: '', image: item.value.image}\"></ng-container>\n                            </div>\n                        </ng-template>\n                    </form>\n                    <div *ngIf=\"buttons && buttons.length>0\" class=\"modal-footer\">\n                        <ng-template ngFor let-clickitem [ngForOf]=\"buttons\" let-i=\"index\" [ngForTrackBy]=\"trackByFn\">\n                            <button type=\"submit\" *ngIf=\"clickitem.click_action==='CALL_update_objekt'\"\n                                class=\"btn btn-lg customform__save\" (click)=\"saveData()\">\n                                <i class=\"fa fa-save\"></i>{{clickitem.value}}</button>\n                        </ng-template>\n\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <ng-template #filters let-c=\"close\" let-d=\"dismiss\">\n        <fletfilters-viewer (selectedfletfilters)=\"setDatatoEditor($event)\" [fletfilters]=\"fletfilters\"></fletfilters-viewer>\n    </ng-template>\n    <ng-template #warning let-c=\"close\" let-d=\"dismiss\">\n        <warning-viewer (confirmreplace)=\"replaceeditorContent($event)\">\n        </warning-viewer>\n    </ng-template>\n\n</div>\n<jqxNotification #successnotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>\n        Success!! Thank you..\n    </div>\n</jqxNotification>\n<jqxNotification #errornotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div>{{errorMessage}}</div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/dashboard/dashboard.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/dashboard/dashboard.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"dashboard\">\n    <div style=\"width: 100%; display: inline-block;\">\n        <div id=\"donutchart\" style=\"width: 50%; height: 500px;float: left;\"></div>\n        <div id=\"barchart_values\" style=\"width: 50%; height: 500px; float:left;\"></div>\n        <div id=\"smilie_chart\">\n                <div id=\"dialog\">Is this a 3 button dialog?</div>\n               <div class=\"dashboard__pagination\"> <a href=\"#\"><i class=\"fa fa-caret-left float-left\"></i></a><p class=\"dashboard__week float-left\"> UGE 32 2017</p><a href=\"#\"><i class=\"fa fa-caret-right float-left\"></i></a></div>\n            <ul class=\"dashboard__chart-container\">\n                <li class=\"dashboard__chart-element-first\">\n                    <div class=\"dashboard__chart-label-container\">\n                        <p class=\"dashboard__chart-element-labels\">Activity1 <span>Ligge på rygge... </span></p>\n                        <p class=\"dashboard__chart-element-labels\">Activity2 <span>Tage trappen...</span></p>\n                        <p class=\"dashboard__chart-element-labels\">Activity3 <span>Gå på tur...</span></p>\n                    </div>\n                </li>\n                <li class=\"dashboard__chart-element\">\n                        <a  data-toggle=\"modal\" id=\"emoji1\" data-target=\"#exampleModal\"  href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--green\"> <i class=\"fa fa-smile-o\" aria-hidden=\"true\"></i></a> \n                        <a data-toggle=\"modal\" data-target=\"#exampleModal\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--yellow\"> <i class=\"fa fa-meh-o\" aria-hidden=\"true\"></i></a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--yellow\"> </a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--label\"> UGE Start</a> \n                </li>\n                <li class=\"dashboard__chart-element\">\n                        <a (click)=\"changeActivity()\"href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--green\"> <i class=\"fa fa-smile-o\" aria-hidden=\"true\"></i></a> \n                </li>\n                <li class=\"dashboard__chart-element\">\n                        <a (click)=\"changeActivity()\" ref=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--red\"> </a>\n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--red\"> <i class=\"fa fa-frown-o\" aria-hidden=\"true\"></i></a> \n                </li>\n                <li class=\"dashboard__chart-element\">\n                        <a (click)=\"changeActivity()\" ref=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--green\"> <i class=\"fa fa-smile-o\" aria-hidden=\"true\"></i></a> \n                </li>\n                <li class=\"dashboard__chart-element\">\n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--green\"></a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--green\"> <i class=\"fa fa-smile-o\" aria-hidden=\"true\"></i></a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--yellow\"> <i class=\"fa fa-meh-o\" aria-hidden=\"true\"></i></a> \n                </li>\n                <li class=\"dashboard__chart-element\">\n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--activity\"> <i class=\"fa fa-adn\"></i></a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--activity\"></a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--activity\"> <i class=\"fa fa-adn\"></i></a> \n                </li>\n                <li class=\"dashboard__chart-element\">\n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--activity\"> <i class=\"fa fa-adn\"></i></a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--activity\"> <i class=\"fa fa-adn\"></i></a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--activity\"> <i class=\"fa fa-adn\"></i></a> \n                        <a (click)=\"changeActivity()\" href=\"#\" class=\"dashboard__chart-emoji dashboard__chart-emoji--label\"> UGE Slut</a> \n                </li>\n            </ul>\n        </div>\n\n          <!-- Modal -->\n          <div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n            <div class=\"modal-dialog\" role=\"document\">\n              <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                  <h5 class=\"modal-title\" id=\"exampleModalLabel\">Change Activity Status</h5>\n                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                  </button>\n                </div> <form>\n                <div class=\"modal-body\">\n                   \n                        <div class=\"row\">\n                                <legend class=\"col-form-label col-sm-2 pt-0\">Activity Status</legend>\n                                <div class=\"col-sm-10\">\n                                  <div class=\"form-check\">\n                                    <input class=\"form-check-input\" type=\"radio\" name=\"gridRadios\" id=\"Done\" value=\"Done\">\n                                    <label class=\"form-check-label\" for=\"Done\">\n                                      Done\n                                    </label>\n                                  </div>\n                                  <div class=\"form-check\">\n                                    <input class=\"form-check-input\" type=\"radio\" name=\"gridRadios\" id=\"Partially\" value=\"Partially\">\n                                    <label class=\"form-check-label\" for=\"Partially\">\n                                      Partially\n                                    </label>\n                                  </div>\n                                  <div class=\"form-check disabled\">\n                                    <input class=\"form-check-input\" type=\"radio\" name=\"gridRadios\" id=\"Not Done\" value=\"Not Done\">\n                                    <label class=\"form-check-label\" for=\"Not Done\">\n                                      Not Done\n                                    </label>\n                                  </div>\n                                </div>\n                              </div>\n                              \n                </div>\n                <div class=\"modal-footer\">\n                  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n                  <button (click)=\"changeActivity($event)\" type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Save changes</button>\n                </div>\n            </form>\n              </div>\n            </div>\n          </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/delete/delete.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/delete/delete.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"delete\">\n\n  <div class=\"modal-header detailspage__filterctr\">\n    <button type=\"button\" class=\"btn detailspage__filterclose\" (click)=\"cancel($event)\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n\n  <h1 mat-dialog-title>Slet {{data.displayheadingName}} med uuid {{data.uuid}}</h1>\n  <div mat-dialog-content>\n    <p>Are you sure you want to delete this entry of?</p>\n  </div>\n  <div mat-dialog-actions>\n    <button type=\"submit\" mat-button [mat-dialog-close]=\"data\" class=\"btn delete__slet btn-lg\"><i\n        class=\"fa fa-trash\"></i> Slet</button>\n  </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/detailspage/detailspage.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/detailspage/detailspage.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"detailspage\">\n\n    <div class=\"modal-header detailspage__filterctr\">\n        <button type=\"button\" class=\"btn detailspage__filterclose\" (click)=\"cancel($event)\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <!--  unused \n    <div class=\"detailspage__header-container\">\n        <div class=\"detailspage__header\">\n            <div *ngIf=\"detailsform\">\n                 <breadcrum-viewer [breadcrumelements]=\"navigationElements\"></breadcrum-viewer> \n                 <operations-viewer [actions]=\"actions\" [objekt_uuid]=\"uuid\" [list]=\"false\" [details]=\"true\" [sharedthis]=\"this\"></operations-viewer>\n            </div>\n        </div>\n    </div> -->\n    <div class=\"\">\n        <div class=\"detailspage__lastupdated\"> Opdateret\n            <span class=\"detailspage__date\">{{lastupdatedTime}}</span> af {{brugerName}}\n        </div>\n        <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n        </div>\n        <div *ngIf=\"detailsform\" class=\"detailspage__form-container\">\n            <div class=\"detailspage__nav-container\">\n                <ul class=\"nav nav-pills nav-justified\" id=\"pills-tab\" role=\"tablist\">\n                    <li class=\"nav-item\">\n                        <a data-toggle=\"pill\" role=\"tab\" class=\"nav-link  active\">Detaljer</a>\n                    </li>\n                </ul>\n            </div>\n            <div class=\"tab-content detailspage__container\" id=\"pills-tabContent\">\n                <div class=\"form-group\">\n                    <form class=\"form-horizontal row\" id=\"detailForm\" #detailForm novalidate [formGroup]=\"detailsForm\">\n                        <ng-template ngFor let-item [ngForOf]=\"detailsform.get('items').controls\" let-i=\"index\"\n                            [ngForTrackBy]=\"trackByFn\">\n                            <div class=\"col-sm-10\">\n\n                                <div class=\"detailspage__accordianscontainer\"\n                                    *ngIf=\"item.value.accordian!=false && item.value.allaccordians; else noaccordian\">\n                                    <ngb-accordion #acc=\"ngbAccordion\" activeIds=\"ngb-panel-0\">\n                                        <ngb-panel title=\"{{item.value.title}}\">\n                                            <ng-template ngbPanelContent>\n                                                <ng-template ngFor let-accordianitem\n                                                    [ngForOf]=\"item.value.allaccordians\" let-k=\"index\"\n                                                    [ngForTrackBy]=\"trackByFn\">\n\n                                                    <div class=\"form-group row\" id=\"{{accordianitem.name}}\"\n                                                        *ngIf=\"accordianitem.type=='input' && accordianitem.ld_brugervendtnoegle!=='adresser'  && accordianitem.type!=='file' && accordianitem.ld_brugervendtnoegle!=='body'\">\n                                                        <label title=\"{{accordianitem.tooltip}}\"\n                                                            class=\"col-sm-4 col-form-label\">{{accordianitem.name}}</label>\n                                                        <div class=\"col-sm-8\" *ngIf=\"accordianitem.required=='true'\">\n                                                            <input disabled={{accordianitem.readonly}}\n                                                                class=\"form-control\" required\n                                                                [(ngModel)]=\"accordianitem.value\"\n                                                                [ngModelOptions]=\"{standalone: true}\"\n                                                                value={{accordianitem.value}}>\n                                                        </div>\n                                                        <div class=\"col-sm-8\" *ngIf=\"accordianitem.required=='false'\">\n                                                            <input disabled={{accordianitem.readonly}}\n                                                                class=\"form-control\" [(ngModel)]=\"accordianitem.value\"\n                                                                [ngModelOptions]=\"{standalone: true}\"\n                                                                value={{accordianitem.value}}>\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"form-group row\" id=\"{{accordianitem.name}}\"\n                                                        *ngIf=\"accordianitem.type=='editor'\">\n                                                        <label title=\"{{accordianitem.tooltip}} \"\n                                                            class=\"col-sm-4 col-form-label \">{{accordianitem.name}}</label>\n                                                        <div class=\"col-sm-8 \">\n                                                            <span (click)=\"openfilter(filters,$event,item) \"\n                                                                class=\"detailspage__user \"><i\n                                                                    class=\"fal fa-user-alt \"></i></span>\n                                                            <ckeditor #editor id=\"editor \" type=\"classic \"\n                                                                [config]=\"config \"\n                                                                [ngModelOptions]=\"{standalone: true} \"\n                                                                [(ngModel)]=\"accordianitem.value \"\n                                                                (change)=\"onchngEditordata($event) \"></ckeditor>\n                                                        </div>\n                                                    </div>\n                                                </ng-template>\n                                            </ng-template>\n                                        </ngb-panel>\n                                    </ngb-accordion>\n                                </div>\n\n\n                                <ng-template #noaccordian>\n                                    <div class=\"form-group row \" id=\"{{item.value.name}} \"\n                                        *ngIf=\"item.value.type=='input' && item.value.ld_brugervendtnoegle!=='adresser' && item.value.type!=='file' && item.value.ld_brugervendtnoegle!=='body' && item.value.hide===false \">\n                                        <label title=\"{{item.value.tooltip}} \"\n                                            class=\"col-sm-4 col-form-label \">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8 \" *ngIf=\"item.value.required=='true' \">\n                                            <input disabled={{item.value.readonly}} class=\"form-control \" required\n                                                [(ngModel)]=\"item.value.value \" [ngModelOptions]=\"{standalone: true} \"\n                                                value={{item.value.value}}>\n                                            <div class=\"invalid-feedback \">\n                                                Please fill this field.\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-8 \" *ngIf=\"item.value.required=='false' \">\n                                            <input disabled={{item.value.readonly}} class=\"form-control \"\n                                                [(ngModel)]=\"item.value.value \" [ngModelOptions]=\"{standalone: true} \"\n                                                value={{item.value.value}}>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row \" id=\"{{item.value.name}} \"\n                                        *ngIf=\"item.value.type==='editor' && item.value.hide===false \">\n                                        <label title=\"{{item.value.tooltip}} \"\n                                            class=\"col-sm-4 col-form-label \">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8 \" *ngIf=\"item.value.required=='false' \">\n                                            <!-- <input disabled={{item.value.readonly}} class=\"form-control \" required [(ngModel)]=\"item.value.value \" [ngModelOptions]=\"{standalone: true} \" value={{item.value.value}}> -->\n                                            <span (click)=\"openfilter(filters,$event,item) \"\n                                                class=\"detailspage__user \"><i class=\"fal fa-user-alt \"></i></span>\n                                            <ckeditor #editor id=\"editor \" type=\"classic \" [config]=\"config \"\n                                                [ngModelOptions]=\"{standalone: true} \" [(ngModel)]=\"item.value.value \"\n                                                (change)=\"onchngEditordata($event) \"></ckeditor>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row \" id=\"{{item.value.name}} \"\n                                        *ngIf=\"item.value.type=='file' \">\n                                        <label title=\"{{item.value.tooltip}} \"\n                                            class=\"col-sm-4 col-form-label \">{{item.value.name}}</label>\n\n                                        <div class=\"col-sm-4 \" *ngIf=\"item.value.value \">\n\n                                            <a class=\"btn btn-link \" (click)=\"downloadFile($event) \"\n                                                href=\"{{item.value.value}} \">{{item.value.value}}</a>\n                                            <span class=\"detailspage__downloadFile \"></span>\n                                        </div>\n\n                                        <div [ngClass]=\"(item.value.value) ? 'col-sm-4': 'col-sm-8' \"\n                                            *ngIf=\"item.value.required=='true' \">\n\n                                            <span> upload a new file</span>\n                                            <input *ngIf=\"readonly==false \" (change)=\"selectFile($event,item) \"\n                                                class=\"form-control \" type=\"file \" required>\n                                            <input *ngIf=\"readonly==true \" disabled (change)=\"selectFile($event,item) \"\n                                                class=\"form-control \" type=\"file \" required>\n                                            <div class=\"invalid-feedback \">\n                                                Please fill this field.\n                                            </div>\n                                        </div>\n                                        <div [ngClass]=\"(item.value.value) ? 'col-sm-4': 'col-sm-8' \"\n                                            *ngIf=\"item.value.required=='false' \">\n                                            <input *ngIf=\"readonly==false \" (change)=\"selectFile($event,item) \"\n                                                class=\"form-control \" type=\"file \">\n                                            <input *ngIf=\"readonly==true \" disabled (change)=\"selectFile($event,item) \"\n                                                class=\"form-control \" type=\"file \">\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row \" id=\"{{item.value.name}} \"\n                                        *ngIf=\"item.value.type=='input' && item.value.ld_brugervendtnoegle=='adresser' && item.value.hide===false \">\n                                        <label title=\"{{item.value.tooltip}} \"\n                                            class=\"col-sm-4 col-form-label \">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8 \" *ngIf=\"item.value.required=='true' \">\n                                            <div class=\"autocomplete-container \">\n                                                <input disabled={{item.value.readonly}} class=\"form-control \" required\n                                                    [(ngModel)]=\"item.value.value \"\n                                                    [ngModelOptions]=\"{standalone: true} \" type=\"text \" [value]=\"selectedStreet\n                                                    \" ngxDawaAutocomplete (items$)=\"onItems($event) \"\n                                                    (keyup)=\"ErrorMsg($event) \"\n                                                    (highlighted$)=\"onItemHighlighted($event) \"\n                                                    (select$)=\"onItemSelected($event) \" />\n                                                <div class=\"invalid-feedback \">\n                                                    Please fill this field.\n                                                </div>\n                                                <div *ngIf=\"itemss.length> 0\">\n                                                    <ul class=\"autocomplete-items\">\n                                                        <li class=\"autocomplete-item\"\n                                                            *ngFor=\"let item of itemss; let i = index;\"\n                                                            [class.highlighted]=\"i === highlightedIndex\"\n                                                            (click)=\"onItemSelected(item)\">\n                                                            {{item.text}}\n                                                        </li>\n                                                    </ul>\n                                                </div>\n                                                <div *ngIf=\"enableaddresserrorMsg === true\">\n                                                    <div *ngIf=\"(addresserrorMesg === true)  && itemss.length== 0\"\n                                                        class=\"text-danger\">\n                                                        Adressen kunne ikke verificeres. Prøv at skrive lidt af\n                                                        adressen, og find den via de viste forslag.\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-8\" *ngIf=\"item.value.required=='false'\">\n                                            <div class=\"autocomplete-container\">\n                                                <input disabled={{item.value.readonly}} class=\"form-control\"\n                                                    [(ngModel)]=\"item.value.value\" [ngModelOptions]=\"{standalone: true}\"\n                                                    type=\"text\" [value]=\"selectedStreet\" ngxDawaAutocomplete\n                                                    (items$)=\"onItems($event)\" (keyup)=\"ErrorMsg($event)\"\n                                                    (highlighted$)=\"onItemHighlighted($event)\"\n                                                    (select$)=\"onItemSelected($event)\" />\n                                                <div class=\"invalid-feedback\">\n                                                    Please fill this field.\n                                                </div>\n                                                <div *ngIf=\"itemss.length > 0\">\n                                                    <ul class=\"autocomplete-items\">\n                                                        <li class=\"autocomplete-item\"\n                                                            *ngFor=\"let item of itemss; let i = index;\"\n                                                            [class.highlighted]=\"i === highlightedIndex\"\n                                                            (click)=\"onItemSelected(item)\">\n                                                            {{item.text}}\n                                                        </li>\n                                                    </ul>\n                                                </div>\n                                                <div *ngIf=\"enableaddresserrorMsg === true\">\n                                                    <div *ngIf=\"(addresserrorMesg === true)  && itemss.length== 0\"\n                                                        class=\"text-danger\">\n                                                        Adressen kunne ikke verificeres. Prøv at skrive lidt af\n                                                        adressen, og find den via de viste forslag.\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" id=\"{{item.value.ld_brugervendtnoegle}}\"\n                                        *ngIf=\"item.value.type=='dropdown' && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\"\n                                            class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\">\n\n\n                                            <!-- multiselect==='false' -->\n                                            <select [disabled]=\"item.value.readonly\"\n                                                *ngIf=\"item.value.multiselect==='false' && item.value.relationsvirkning===false && item.value.required === 'true'\"\n                                                required (change)=\"Select($event,item)\"\n                                                [(ngModel)]=\"item.value.selected_name\"\n                                                [ngModelOptions]=\"{standalone: true}\"\n                                                class=\"form-control selectpicker show-tick\" data-live-search=\"true\">\n                                                <option\n                                                    *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                    [selected]=\"optionitems.selected === true ? 'selected':null\"\n                                                    [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                    {{optionitems.rel_uri}}\n                                                </option>\n                                            </select>\n\n                                            <!-- multiselect==='true' -->\n                                            <select [disabled]=\"item.value.readonly\" multiple\n                                                *ngIf=\"item.value.multiselect==='true' && item.value.relationsvirkning===false && item.value.required === 'true'\"\n                                                required class=\"form-control selectpicker show-tick\"\n                                                data-live-search=\"true\" [(ngModel)]=\"item.value.selected_name\"\n                                                [ngModelOptions]=\"{standalone: true}\"\n                                                (change)=\"Multiselect($event,item)\">\n                                                <option\n                                                    *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                    [selected]=\"optionitems.selected=== true ? 'selected':null\"\n                                                    [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                    {{optionitems.rel_uri}}\n                                                </option>\n                                            </select>\n                                            <div id=\"{{item.value.ld_brugervendtnoegle}}\"\n                                                class=\"invalid-feedback {{item.value.ld_brugervendtnoegle}}\">\n                                                Please fill this field.\n                                            </div>\n\n                                            <!----------new component-->\n                                            <!-- //todo required -->\n                                            <div *ngIf=\"item.value.multiselect=='true' && item.value.relationsvirkning==true\"\n                                                [ngClass]=\"(opentoogle === false)? 'detailspage__multiselect detailspage__multiselect--close':'detailspage__multiselect detailspage__multiselect--open'\">\n                                                <ng-template ngFor let-relationoptionitem [ngForOf]=\"item.value.options\"\n                                                    let-j=\"index\" [ngForTrackBy]=\"trackByFn\">\n                                                    <div class=\"detailspage__multiselectitem  col-sm-5\">\n\n                                                        <div\n                                                            [ngClass]=\"(relationoptionitem.selected===true)? 'detailspage__sltd': ' detailspage__unsltd'\">\n                                                            <label\n                                                                class=\"form-check-label detailspage__selectItemlabel\">{{relationoptionitem.rel_uri}}\n                                                                <input\n                                                                    [checked]=\"relationoptionitem.selected===true ? true:false\"\n                                                                    [(ngModel)]=\"relationoptionitem.selected\"\n                                                                    [ngModelOptions]=\"{standalone: true}\"\n                                                                    type=\"checkbox\"\n                                                                    (change)=\"Multiselectrelation($event,item,relationoptionitem)\"\n                                                                    class=\"form-check-input detailspage__customcheck\">\n                                                                <span class=\"detailspage__checkmark\"></span>\n                                                            </label>\n                                                        </div>\n                                                        <!-- Delete -->\n                                                        <ng-template ngFor let-intervalitem\n                                                            [ngForOf]=\"relationoptionitem.intervals\" let-in=\"index\"\n                                                            [ngForTrackBy]=\"trackByFn\">\n                                                            <div class=\"detailspage__multiselectelement\">\n                                                                <div class=\"detailspage__deletecontainer\">\n                                                                    <a title=\"Delete new entry\"\n                                                                        (click)=\"Deletedateitem($event,item,relationoptionitem,in)\"\n                                                                        class=\"button detailspage__deleteitem\" href=\"#\">\n                                                                        <i class=\"fa fa-minus-circle\"></i></a>\n                                                                </div>\n                                                                <!-- fra -->\n                                                                <div class=\"detailspage__multiselectitemdate\">\n                                                                    <label\n                                                                        class=\"form-check-label detailspage__mltsltd-date-lbl\">Fra\n                                                                        :\n                                                                    </label>\n                                                                    <div class=\"detailspage__mltsltd-dateitem\">\n                                                                        <input [min]=\"mindate\"\n                                                                            [(ngModel)]=\"intervalitem.fra\"\n                                                                            [ngModelOptions]=\"{standalone: true}\"\n                                                                            [readonly]=\"true\" [owlDateTime]=\"j\"\n                                                                            type=\"text\" class=\"form-control\"\n                                                                            value={{intervalitem.fra}}\n                                                                            [owlDateTimeTrigger]=\"j\" placeholder=\"\" />\n                                                                        <span class=\"detailspage__dateicon\"\n                                                                            [owlDateTimeTrigger]=\"j\"><i\n                                                                                class=\"fal fa-calendar-alt\"></i></span>\n                                                                        <span class=\"detailspage__closeicon\"\n                                                                            *ngIf=\"item.value.readonly===false\"\n                                                                            (click)=\"clearDate(item)\"><i\n                                                                                class=\"fal fa-times\"></i></span>\n                                                                        <owl-date-time\n                                                                            (afterPickerClosed)=\"dateEditfra(item,intervalitem,relationoptionitem,in,j)\"\n                                                                            [disabled]=\"item.value.readonly\"\n                                                                            [pickerType]=\"'both'\" #j>\n                                                                        </owl-date-time>\n                                                                    </div>\n                                                                </div>\n                                                                <!-- til -->\n                                                                <div class=\"detailspage__multiselectitemdate\">\n                                                                    <label\n                                                                        class=\"form-check-label detailspage__mltsltd-date-lbl\">Til\n                                                                        :\n                                                                    </label>\n                                                                    <div class=\"detailspage__mltsltd-dateitem\">\n                                                                        <input [min]=\"mindate\"\n                                                                            [(ngModel)]=\"intervalitem.til\"\n                                                                            [ngModelOptions]=\"{standalone: true}\"\n                                                                            [readonly]=\"true\" [owlDateTime]=\"k\"\n                                                                            type=\"text\" class=\"form-control\"\n                                                                            value={{intervalitem.til}}\n                                                                            [owlDateTimeTrigger]=\"k\" placeholder=\"\" />\n                                                                        <span class=\"detailspage__dateicon\"\n                                                                            [owlDateTimeTrigger]=\"k\"><i\n                                                                                class=\"fal fa-calendar-alt\"></i></span>\n                                                                        <span class=\"detailspage__closeicon\"\n                                                                            *ngIf=\"item.value.readonly===false\"\n                                                                            (click)=\"clearDate(item)\"><i\n                                                                                class=\"fal fa-times\"></i></span>\n                                                                        <owl-date-time\n                                                                            (afterPickerClosed)=\"dateEdittil(item,intervalitem,relationoptionitem,index,k)\"\n                                                                            [disabled]=\"item.value.readonly\"\n                                                                            [pickerType]=\"'both'\" #k>\n                                                                        </owl-date-time>\n\n                                                                    </div>\n                                                                </div>\n                                                                <div class=\"detailspage__multiselectitemdate\">\n                                                                    <div class=\"form-group\">\n                                                                        <label\n                                                                            class=\"form-check-label detailspage__mltsltd-date-lbl\">Note:</label>\n                                                                        <textarea\n                                                                            class=\"form-control detailspage__multiselectitemtextarea\"\n                                                                            [ngModelOptions]=\"{standalone: true}\"\n                                                                            [(ngModel)]=\"intervalitem.note\"\n                                                                            rows=\"2\"></textarea>\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                        </ng-template>\n\n                                                        <div class=\"detailspage__addcontainer\">\n                                                            <a title=\"Create new entry\"\n                                                                (click)=\"Createdateitem($event,item,relationoptionitem)\"\n                                                                class=\"button detailspage__additem\" href=\"#\">\n                                                                <i class=\"fa fa-plus-circle\"></i>Opret</a>\n                                                        </div>\n                                                    </div>\n                                                </ng-template>\n\n                                                <div\n                                                    [ngClass]=\"(opentoogle === false)? 'detailspage__opentoogle detailspage__opentoogle--show': 'detailspage__opentoogle detailspage__opentoogle--hide'\">\n                                                    <a href=\"#\" (click)=\"tooglepakker($event)\">\n                                                        <i class=\"fas fa-chevron-circle-down\"></i>\n                                                    </a>\n                                                </div>\n                                                <div\n                                                    [ngClass]=\"(opentoogle === false)? 'detailspage__closetoogle detailspage__closetoogle--hide': 'detailspage__closetoogle detailspage__closetoogle--show'\">\n                                                    <a href=\"#\" (click)=\"tooglepakker($event)\">\n                                                        <i class=\"fas fa-chevron-circle-up\"></i>\n                                                    </a>\n\n                                                </div>\n\n                                            </div>\n                                            <!----------END  new component-->\n\n                                            <!-- <div class=\"col-sm-8\"\n            *ngIf=\"item.value.required === 'false'  && item.value.relationsvirkning===false \"> -->\n                                            <select [disabled]=\"item.value.readonly\"\n                                                *ngIf=\"item.value.multiselect==='false' && item.value.relationsvirkning===false && item.value.required === 'false'\"\n                                                (change)=\"Select($event,item)\" [(ngModel)]=\"item.value.selected_name\"\n                                                [ngModelOptions]=\"{standalone: true}\"\n                                                class=\"form-control selectpicker show-tick\" data-live-search=\"true\">\n                                                <option\n                                                    *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                    [selected]=\"optionitems.selected===true ? 'selected':null\"\n                                                    [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                    {{optionitems.rel_uri}}\n                                                </option>\n                                            </select>\n                                            <select [disabled]=\"item.value.readonly\" multiple\n                                                *ngIf=\"item.value.multiselect==='true' && item.value.relationsvirkning===false && item.value.required === 'false'\"\n                                                class=\"form-control selectpicker show-tick\" data-live-search=\"true\"\n                                                #multiselect [(ngModel)]=\"item.value.selected_name\"\n                                                [ngModelOptions]=\"{standalone: true}\"\n                                                (change)=\"Multiselect($event,item)\">\n                                                <option\n                                                    *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                    [selected]=\"optionitems.selected===true ? 'selected':null\"\n                                                    [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                    {{optionitems.rel_uri}}\n                                                </option>\n                                            </select>\n                                            <!-- </div> -->\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" id=\"{{item.value.ld_brugervendtnoegle}}\"\n                                        *ngIf=\"item.value.type=='date' && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\"\n                                            class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\" *ngIf=\"item.value.readonly===false\">\n                                            <input [min]=\"item.value.mindate\" [(ngModel)]=\"item.value.formatdate\"\n                                                [readonly]=\"true\" [ngModelOptions]=\"{standalone: true}\"\n                                                [owlDateTime]=\"i\" type=\"text\"\n                                                [required]=\"item.value.required==='true'? 'required' : null\"\n                                                class=\"form-control\" value={{item.value.value}}\n                                                [disabled]=\"item.value.readonly===true? true : false\"\n                                                [owlDateTimeTrigger]=\"i\" placeholder=\"\" />\n                                            <span class=\"detailspage__dateicon\" [owlDateTimeTrigger]=\"i\"><i\n                                                    class=\"fal fa-calendar-alt\"></i></span>\n                                            <span class=\"detailspage__closeicon\" *ngIf=\"item.value.readonly===false\"\n                                                (click)=\"clearDate(item)\"><i class=\"fal fa-times\"></i></span>\n                                            <owl-date-time (afterPickerClosed)=\"dateChange(item,i)\"\n                                                [disabled]=\"item.value.readonly\" [pickerType]=\"'both'\" #i>\n                                            </owl-date-time>\n                                            <div id=\"{{item.value.ld_brugervendtnoegle}}\"\n                                                *ngIf=\"item.value.required === 'true'\"\n                                                class=\"invalid-feedback {{item.value.ld_brugervendtnoegle}}\">\n                                                Please fill this field.\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-8\" *ngIf=\"item.value.readonly===true\">\n                                            <input [(ngModel)]=\"item.value.selected_list\" [readonly]=\"true\"\n                                                [ngModelOptions]=\"{standalone: true}\" type=\"text\"\n                                                [required]=\"item.value.required==='true'? 'required' : null\"\n                                                class=\"form-control\" value={{item.value.value}}\n                                                [disabled]=\"item.value.readonly===true? true : false\" placeholder=\"\" />\n                                            <span class=\"detailspage__dateicon\"><i\n                                                    class=\"fal fa-calendar-alt\"></i></span>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" id=\"{{item.value.ld_brugervendtnoegle}}\"\n                                        *ngIf=\"item.value.type=='textarea'  && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\"\n                                            class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\">\n                                            <textarea disabled={{item.value.readonly}} [(ngModel)]=\"item.value.value\"\n                                                [ngModelOptions]=\"{standalone: true}\" class=\"form-control\"\n                                                value={{item.value.value}} id=\"exampleFormControlTextarea1\"\n                                                rows=\"15\"></textarea>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" id=\"{{item.value.ld_brugervendtnoegle}}\"\n                                        *ngIf=\"item.value.type=='html'  && item.value.hide===false\">\n                                        <label title=\"{{item.value.tooltip}}\"\n                                            class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\">\n                                            <div [innerHTML]=\"item.value.value\"></div>\n                                        </div>\n                                    </div>\n                                </ng-template>\n                            </div>\n\n                            <div *ngIf=\"item.value.type=='file' && item.value.image\">\n                                <ng-container [ngTemplateOutlet]=\"imageTemplate\"\n                                    [ngTemplateOutletContext]=\"{$implicit: '', image: item.value.image}\"></ng-container>\n                            </div>\n                        </ng-template>\n\n\n                    </form>\n\n                    <div *ngIf=\"buttons && buttons.length>0\" class=\"modal-footer\">\n                        <ng-template ngFor let-clickitem [ngForOf]=\"buttons\" let-i=\"index\" [ngForTrackBy]=\"trackByFn\">\n                            <button type=\"submit\" *ngIf=\"clickitem.click_action==='CALL_update_objekt'\"\n                                class=\"btn btn-lg detailspage__save\" (click)=\"saveData()\">\n                                <i class=\"fa fa-save\"></i>{{clickitem.value}}</button>\n                            <button *ngIf=\"clickitem.click_action==='delete'\" type=\"submit\"\n                                class=\"btn btn-lg detailspage__slet\" (click)=\"Delete()\">\n                                <i class=\"fa fa-trash\"></i>{{clickitem.value}}</button>\n                        </ng-template>\n\n                    </div>\n                </div>\n\n\n            </div>\n            <jqxNotification #successNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\"\n                [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"3000\"\n                [template]=\"'success'\">\n                <div>\n                    Success!! Thank you..\n                </div>\n            </jqxNotification>\n            <jqxNotification #errorNotification [width]=\"3000\" [position]=\"'bottom-right'\" [opacity]=\"0.9\"\n                [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"15000\"\n                [template]=\"'error'\">\n                <div>{{errorMessage}}</div>\n            </jqxNotification>\n\n        </div>\n    </div>\n    <ng-template #filters let-c=\"close\" let-d=\"dismiss\">\n        <fletfilters-viewer (selectedfletfilters)=\"setDatatoEditor($event)\" [fletfilters]=\"fletfilters\">\n        </fletfilters-viewer>\n    </ng-template>\n    <ng-template #warning let-c=\"close\" let-d=\"dismiss\">\n        <warning-viewer (confirmreplace)=\"replaceeditorContent($event)\">\n        </warning-viewer>\n    </ng-template>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/filter/filter.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/filter/filter.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"loading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"loading\"></i>\n</div>\n\n\n<div class=\"filter\" *ngIf=\"filtereditorOptions\">\n\n    <div class=\"modal-header filter__filterctr\">\n        <button type=\"button\" class=\"btn filter__filterclose\" (click)=\"cancel($event)\">\n                        <span aria-hidden=\"true\">&times;</span>\n                    </button>\n    </div>\n\n\n    <div class=\"form-group\">\n        <form>\n            <div class=\"form-group row\">\n                <div class=\"col-sm-2 filter__radio\">\n                    <input (change)=\"createnew($event)\" class=\"form-check-input filter__radiosize col-sm-12\" id=\"opret\" name=\"opret\" value=\"createnewmodel\" type=\"radio\" /></div>\n                <div class=\"col-sm-4\"><label class=\"col-form-label\">Opret Ny</label></div>\n            </div>\n            <div [ngClass]=\"(createnewmodel===true)? 'form-group row': 'form-group row filter__disable'\" class=\"form-group row\">\n                <label class=\"col-sm-4 col-form-label\">Filter Navn </label>\n                <div class=\"col-sm-8\">\n                    <input class=\"form-control\" [(ngModel)]=\"filternavnmodel\" [disabled]=\"filternavnenable\" [value]=\"filternavnmodel\" [ngModelOptions]=\"{standalone: true}\" type=\"text\" name=\"oprettext\" #filternavn id=\"filternavn\">\n                    <div *ngIf=\"!filternavnmodel && createnewmodel===true\">\n                        <p class=\"text-danger\">\n                            Please fill this field.\n                        </p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <div class=\"col-sm-2 filter__radio\">\n                    <input (change)=\"overwrite($event)\" class=\"form-check-input filter__radiosize col-sm-12\" id=\"opret\" name=\"opret\" value=\"overwritemodel\" type=\"radio\" /></div>\n                <div class=\"col-sm-4\"><label class=\"col-form-label\">Overskriv eksist</label></div>\n            </div>\n            <div [ngClass]=\"(overwritemodel===true)? 'form-group row': 'form-group row filter__disable'\">\n                <label class=\"col-sm-4 col-form-label\">Vælg Filter</label>\n                <div class=\"col-sm-8\">\n                    <select [(ngModel)]=\"filteruuid\" data-live-search=\"true\" title=\"Intet valgt\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"getFilterData($event,item)\" #choosefilter id=\"choosefilter\">\n                        <ng-template ngFor let-item [ngForOf]=\"allfilterList\" let-i=\"index\" let first=first;\n                            [ngForTrackBy]=\"trackByFn\">\n                            <option [ngValue]=\"item.uuid\">\n                                {{item.titel}}\n                            </option>\n                        </ng-template>\n                    </select>\n                    <div *ngIf=\"overwritemodel === true && !filteruuid\">\n                        <p class=\"text-danger\">\n                            Please fill this field.\n                        </p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-4 col-form-label\">Tilgængelig for</label>\n                <div class=\"col-sm-8\">\n                    <select multiple [(ngModel)]=\"allredaktoerslctd\" data-live-search=\"true\" title=\"Intet valgt\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" id=\"availablefor\">\n                        <ng-template ngFor let-item [ngForOf]=\"allredaktoerList\" let-i=\"index\" let first=first;\n                            [ngForTrackBy]=\"trackByFn\">\n                            <option [ngValue]=\"item.key\">\n                                {{item.value}}\n                            </option>\n                        </ng-template>\n                    </select>\n                    <div *ngIf=\"!allredaktoerslctd\">\n                        <p class=\"text-danger\">\n                            Please fill this field.\n                        </p>\n                    </div>\n                </div>\n            </div>\n        </form>\n    </div>\n    Filter data preview\n    <json-editor [options]=\"filtereditorOptions\" [data]=\"filterdata\">\n    </json-editor>\n    <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-outline-dark filter__cancel\" (click)=\"cancel($event)\">\n                <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n        <button type=\"submit\" class=\"btn btn-outline-dark filter__save\" (click)=\"saveFilter($event)\">\n                <i class=\"fa fa-save\"></i>Gem</button>\n    </div>\n</div>\n\n<jqxNotification #successNotification [width]=\"400\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>\n        Success!! Thank you..\n    </div>\n</jqxNotification>\n<jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/fletfilters/fletfilters.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/fletfilters/fletfilters.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"loading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"loading\"></i>\n</div>\n\n\n<div class=\"fletfilters\">\n    <div class=\"modal-header notifyviewer__filterctr\">\n        <button type=\"button\" class=\"btn notifyviewer__filterclose\" (click)=\"cancel($event)\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n\n    <div class=\"form-group\">\n        <form>\n            <div class=\"form-group row\">\n                <label class=\"col-sm-4 col-form-label\">Indsæt Flettefelter</label>\n                <div *ngIf=\"fletfilters\" class=\"col-sm-8\">\n                    <select [(ngModel)]=\"rel_uuid\" data-live-search=\"true\" title=\"Intet valgt\"\n                        [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\"\n                        (change)=\"setFilter($event,item)\" #choosefilter id=\"choosefilter\">\n                        <ng-template ngFor let-item [ngForOf]=\"fletfilters.options\" let-i=\"index\" let first=first;\n                            [ngForTrackBy]=\"trackByFn\">\n                            <option [ngValue]=\"item.rel_uuid\">\n                                {{item.rel_uri}}\n                            </option>\n                        </ng-template>\n                    </select>\n                    <div *ngIf=\"!rel_uuid\">\n                        <p class=\"text-danger\">\n                            Please fill this field.\n                        </p>\n                    </div>\n                </div>\n            </div>\n        </form>\n    </div>\n\n    <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-outline-dark sendemail__cancel\" (click)=\"cancel($event)\">\n            <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n        <button type=\"submit\" class=\"btn btn-outline-dark sendemail__save\" (click)=\"apply($event)\">\n            <i class=\"fa fa-save\"></i>Udfør</button>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/footer/footer-page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/footer/footer-page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<footer class=\"footer\">\n  <div class=\"container text-center\">\n    <span class=\"footer__text\"> powered by carex. all rights reserved</span>\n  </div>\n</footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/ganttchart/ganttchart.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/ganttchart/ganttchart.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"ganttchart\">\n  <div class=\"ganttchart__header-container\">\n    <div class=\"ganttchart__header\">\n      <div class=\"\">\n        <breadcrum-viewer [breadcrumelements]=\"navigationElements\"></breadcrum-viewer>\n      </div>\n    </div>\n  </div>\n  <div>\n    <div id=\"chartdiv\" class=\"ganttchart__chartdiv\" #chartdiv [style.width.%]=\"100\" [style.height.px]=\"500\"></div>\n  </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/header/header-page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/header/header-page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header-page\">\n\n    <span class=\"header-page__version\">version  {{version}}</span>\n    <nav class=\"navbar navbar-expand-md navbar-dark\">\n\n        <a (click)=\"home($event)\" class=\"navbar-brand header__logo-link\" href=\"#\">\n            <img class=\"header-page__logo\" src=\"assets/img/logo-carex.png\">\n            <span class=\"header-page__envi\">- {{envi}}</span>\n        </a>\n        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n\n        <div *ngIf=\"user && user.username\" class=\"header-page__user-container\">\n            <div class=\"header-page__notification-container\">\n                <a href=\"#\" class=\"header-page__dropdown-item header-page__notifications\">\n                    <i class=\"fa fa-bell\"></i>\n                    <span class=\"header-page__notify\">1</span>\n                </a>\n            </div>\n            <div *ngIf=\"user\" class=\"dropdown  header-page__dropdown\">\n                <button class=\"btn btn-secondary dropdown-toggle header-page__user-btn\" type=\"button\" id=\"dropdownMenu2\" data-toggle=\"dropdown\" aria-haspopup=\"false\" aria-expanded=\"false\">\n          <i class=\"fa fa-user-circle-o\" aria-hidden=\"true\"></i> {{user.username}}\n        </button>\n                <div class=\"dropdown-menu  header-page__dropdown-menu\" aria-labelledby=\"dropdownMenu2\">\n                    <button class=\"dropdown-item header-page__dropdown-item\" type=\"button\">\n            <i class=\"fa fa-user-circle-o\" aria-hidden=\"true\"></i>Profile</button>\n                    <button (click)=\"logout()\" class=\"dropdown-item header-page__dropdown-item\" type=\"button\">\n                <i class=\"fa fa-sign-out\"></i>Logout</button>\n                </div>\n            </div>\n\n        </div>\n    </nav>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/itsystems/itsystems.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/itsystems/itsystems.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"itsystems\">\n    <div class=\"\">\n        <div class=\"itsystems__header-container\">\n            <div class=\"itsystems__header\">\n                <div *ngIf=\"actions\"> \n                    <!-- <breadcrum-viewer [breadcrumelements]=\"navigationElements\"></breadcrum-viewer> -->\n                    <div class=\"itsystems__operations\">\n                        <operations-viewer [list]=\"true\" [details]=\"false\" (newlistdata)=\"RefreshData($event)\" (filtereddata)=applyfilters($event); [actions]=\"actions\" [filterList]=\"filterList\" [sharedthis]=\"this\"></operations-viewer>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"\">\n            <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\"></div>\n            <div [ngClass]=\"(loading==true) ? 'd-none':' '\">\n            </div>\n        </div>\n        <div class=\"\">\n            <div [ngClass]=\"(fetchingfilter==true) ? 'spinner':'spinner d-none'\"></div>\n            <div [ngClass]=\"(fetchingfilter==true) ? 'd-none':' '\">\n            </div>\n        </div>\n    </div>\n    <div class=\"itsystems__emptycontainer\">\n    </div>\n    <search-viewer *ngIf=\"click_action==='getObjektfilter'\" (filteredGriddata)=\"filteredGrid($event)\"  #searchcomponent  [actions]=\"actions\" [sharedthis]=\"this\"></search-viewer>\n    <div [ngClass]=\"(loading==true) ? 'invisible':' itsystems__gridContainer'\">\n        <jqxGrid *ngIf=\"columns\" [height]=\"'80%'\" [filterbarmode]=\"'simple'\" [groups]=\"groupableValue\" [localization]=\"localization\" (onColumnreordered)=\"onColumnreordered($event)\" [selectionmode]=\"'multiplecellsextended'\" [width]=\"'100%'\" [pagermode]=\"simple\" [altrows]=\"true\"\n            [pagesize]=\"20\" [theme]=\"'ui-redmond_new'\" (onSort)=\"Sort($event)\" (onFilter)=\"Filter($event)\" [pageable]=\"true\" [autoloadstate]=\"true\" [showfilterrow]=\"(click_action==='getObjektfilter')?false:true\" [filterable]=\"(click_action==='getObjektfilter')?false:true\" [autosavestate]=\"true\" [rendered]=\"rendered\" #gridReference [enablehover]=\"true\" [enabletooltips]=\"true\"\n            [autoshowloadelement]=\"true\" [columnsreorder]=\"true\" [columnsresize]=\"true\" [autorowheight]=\"true\" [enableellipsis]=\"true\" (onRowclick)=\"Rowclick($event)\" [source]=\"dataAdapter\" [columns]=\"columns\" [sortable]=\"true\" [ready]=\"ready\" \n            [autoshowfiltericon]=\"true\">\n        </jqxGrid>\n    </div>\n    <jqxNotification #successNotification [width]=\"400\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n        <div>\n            Success!! Thank you..\n        </div>\n    </jqxNotification>\n    <jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n        <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n    </jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/jsonviewer/jsonviewer.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/jsonviewer/jsonviewer.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"isLoading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"isLoading\"></i>\n</div>\n\n<div class=\"jsonviewer\" *ngIf=\"filtereditorOptions\">\n    <div class=\"modal-header jsonviewer__filterctr\">\n        <button type=\"button\" class=\"btn jsonviewer__filterclose\" (click)=\"cancel($event)\">\n                        <span aria-hidden=\"true\">&times;</span>\n                    </button>\n    </div>\n\n    Filter data preview\n    <json-editor [options]=\"filtereditorOptions\" [data]=\"body\">\n    </json-editor>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/login/login.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/login/login.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"login\">\n    <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n    <div [ngClass]=\"(fetching==true) ? 'spinner':'spinner d-none'\">\n    </div>\n\n    <h4 text-center class=\"login__heading\">Indtast brugernavn og kodeord</h4>\n    <form>\n        <div class=\"form-group\">\n\n            <label>Bruger Navn</label>\n            <input type=\"email\" [(ngModel)]=\"username\" (keyup)=\"reset()\" autocomplete=\"on\"\n                [ngModelOptions]=\"{standalone: true}\" class=\"form-control login__textbox\" placeholder=\"Bruger Navn\">\n\n        </div>\n        <div class=\"form-group\">\n            <label>Adgangskode</label>\n            <input [(ngModel)]=\"passwordkey\" autocomplete=\"on\" (keyup)=\"reset()\" [ngModelOptions]=\"{standalone: true}\"\n                #password type=\"password\" class=\"form-control login__textbox\" placeholder=\"Adgangskode\">\n            <span (click)=\"showpassword()\"\n                [ngClass]=\"(showpwd===true)?'login__eye fa fa-fw fa-eye field-icon': 'login__eye fa fa-fw fa-eye-slash field-icon'\"></span>\n        </div>\n\n        <div *ngIf=\"error\">\n            <div class=\"login__error\">Forkert brugernavn eller adgangskode</div>\n        </div>\n        <div *ngIf=\"errormessage\">\n            <div class=\"login__error\">{{errormessage}}</div>\n        </div>\n        <button type=\"submit\" (click)=\"login()\" class=\"btn login__login btn-lg\">Login</button>\n        <button type=\"submit\" (click)=\"resetpassword()\" class=\"btn login__forgetpwd btn-lg\">Ny Bruger / glemt\n            adgangskode</button>\n    </form>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/menu-left/menu-left.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/menu-left/menu-left.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"menu-left\">\n    <nav>\n        <ul class=\"list-group\">\n            <ng-template ngFor let-item *ngIf=\"menulist\" [ngForOf]=\"menulist\" let-i=\"index\" [ngForTrackBy]=\"trackByFn\">\n                <div>\n                    <li *ngIf=\"item.brugervendtnoegle && (item.click_action === 'list_objekt_data' || item.click_action === 'getObjektfilter')\" [routerLinkActive]=\"\" [ngClass]=\"[rla.isActive?'list-group-item active':'list-group-item', (item.submenu && rla.isActive) ?'list-group-item submenu':'list-group-item']\" #rla=\"routerLinkActive\"\n                        (click)=\"active($event,item,item.uuid,item.brugervendtnoegle)\">\n                        <a routerLink=\"/{{item.brugervendtnoegle}}\" routerLinkActive>{{item.funktionsnavn}}</a>\n                    </li>\n                    <li *ngIf=\"item.brugervendtnoegle && item.click_action === 'configuration'\" [routerLinkActive]=\"\" [ngClass]=\"[rla.isActive?'list-group-item active':'list-group-item', (item.submenu && rla.isActive) ?'list-group-item submenu':'list-group-item']\" #rla=\"routerLinkActive\"\n                        (click)=\"active($event,item,item.uuid,item.brugervendtnoegle)\">\n                        <a routerLink=\"/{{item.click_action}}\" routerLinkActive>{{item.funktionsnavn}}</a>\n                    </li>\n                    <li *ngIf=\"item.brugervendtnoegle && item.click_action === 'templates'\" [routerLinkActive]=\"\" [ngClass]=\"[rla.isActive?'list-group-item active':'list-group-item', (item.submenu && rla.isActive) ?'list-group-item submenu':'list-group-item']\" #rla=\"routerLinkActive\"\n                        (click)=\"active($event,item,item.uuid,item.brugervendtnoegle)\">\n                        <a routerLink=\"/{{item.click_action}}\" routerLinkActive>{{item.funktionsnavn}}</a>\n                    </li>\n                    <li *ngIf=\"item.brugervendtnoegle && item.click_action === 'translation'\" [routerLinkActive]=\"\" [ngClass]=\"[rla.isActive?'list-group-item active':'list-group-item', (item.submenu && rla.isActive) ?'list-group-item submenu':'list-group-item']\" #rla=\"routerLinkActive\"\n                        (click)=\"active($event,item,item.uuid,item.brugervendtnoegle)\">\n                        <a routerLink=\"/{{item.click_action}}\" routerLinkActive>{{item.funktionsnavn}}</a>\n                    </li>\n                    <li *ngIf=\"item.brugervendtnoegle && item.click_action === 'getuseranalytics'\" [routerLinkActive]=\"\" [ngClass]=\"[rla.isActive?'list-group-item active':'list-group-item', (item.submenu && rla.isActive) ?'list-group-item submenu':'list-group-item']\" #rla=\"routerLinkActive\"\n                        (click)=\"active($event,item,item.uuid,item.brugervendtnoegle)\">\n                        <a routerLink=\"/{{item.click_action}}\" routerLinkActive>{{item.funktionsnavn}}</a>\n                    </li>\n                    <li *ngIf=\"item.brugervendtnoegle && item.click_action === 'uploads'\" [routerLinkActive]=\"\" [ngClass]=\"[rla.isActive?'list-group-item active':'list-group-item', (item.submenu && rla.isActive) ?'list-group-item submenu':'list-group-item']\" #rla=\"routerLinkActive\"\n                        (click)=\"active($event,item,item.uuid,item.brugervendtnoegle)\">\n                        <a routerLink=\"/{{item.click_action}}\" routerLinkActive>{{item.funktionsnavn}}</a>\n                    </li>\n                    <li *ngIf=\"item.brugervendtnoegle && item.click_action === 'skemaeditor'\" [routerLinkActive]=\"\" [ngClass]=\"[rla.isActive?'list-group-item active':'list-group-item', (item.submenu && rla.isActive) ?'list-group-item submenu':'list-group-item']\" #rla=\"routerLinkActive\"\n                    (click)=\"active($event,item,item.uuid,item.brugervendtnoegle)\">\n                    <a routerLink=\"/{{item.click_action}}\" routerLinkActive>{{item.funktionsnavn}}</a>\n                </li>\n                </div>\n            </ng-template>\n\n        </ul>\n    </nav>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/menu-right/menu-right.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/menu-right/menu-right.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"menu-right\">\n\n    <!-- <div *ngIf=\"state=='inactive'\" class=\"menu-right__hamburger-close\">\n                <a href=\"#\" (click)=\"toogleMenu($event)\">\n                        <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n                </a>\n        </div>\n        <div *ngIf=\"state=='active'\" class=\"menu-right__hamburger-open\">\n                <a href=\"#\" (click)=\"toogleMenuOpen($event)\">\n                        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>\n                </a>\n        </div> -->\n    <div class=\"menu-right__list-container\" [@menuState]=\"state\">\n        <!--    <nav>\n                        <ul class=\"list-group\">\n                                \n                                <li class=\"list-group-item\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{ exact: true }\">\n                                        <i class=\"fa fa-info\" aria-hidden=\"true\"></i>\n\n                                </li>\n                                <li routerLinkActive=\"active\" c class=\"list-group-item\">\n                                        <i class=\"fa fa-cloud-download\" aria-hidden=\"true\"></i>\n                                </li>\n                                <li routerLinkActive=\"active\" c class=\"list-group-item\">\n                                        <i class=\"fa fa-user-circle\" aria-hidden=\"true\"></i>\n                                </li>\n                                <li routerLinkActive=\"active\" c class=\"list-group-item\">\n                                        <i class=\"fa fa-eur\" aria-hidden=\"true\"></i>\n                                </li>\n                                <li routerLinkActive=\"active\" class=\"list-group-item\">\n                                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                </li>\n                                <li class=\"list-group-item\" routerLinkActive=\"active\">\n                                        <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>\n                                </li>\n                                <li routerLinkActive=\"active\" class=\"list-group-item\">\n                                        <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>\n                                </li>\n                                <li routerLinkActive=\"active\" class=\"list-group-item\">\n                                        <i class=\"fa fa-cloud-upload\" aria-hidden=\"true\"></i>\n                                </li>\n                                <li routerLinkActive=\"active\" class=\"list-group-item\">\n                                                <i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i>\n                                        </li> -->\n        <!-- <li routerLinkActive=\"active\" class=\"list-group-item\">\n                                                <a  routerLink=\"/statistics\">Statistics</a>\n                                            </li> \n                        </ul>\n                </nav>-->\n        <nav *ngIf=\"authorisation\">\n            <ul class=\"list-group\" *ngIf=\"userrelations && userrelations.length\">\n                <!-- <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"getStatistics($event)\"\n                                [routerLinkActiveOptions]=\"{exact:true}\">\n                                        <a routerLink=\"/statistics\">Statistics</a>\n                                </li> -->\n                <!-- <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"getAnalytics($event)\" [routerLinkActiveOptions]=\"{exact:true}\">\n                    <a routerLink=\"/analytics\">Analytics</a>\n                </li>\n                <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"serviceAnalytics($event)\" [routerLinkActiveOptions]=\"{exact:true}\">\n                    <a routerLink=\"/serviceanalytics\">Service log analytics</a>\n                </li> -->\n                <!-- <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"sendpushNotification($event)\">\n                    <a routerLink=\"/sendpushNotification\">Send Notification</a>\n                </li> -->\n                <!-- <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"translation($event)\">\n                    <a routerLink=\"/translation\">Oversættelse</a>\n                </li>\n                <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"templates($event)\">\n                    <a routerLink=\"/templates\">Skabelon Editor</a>\n                </li>\n                <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"configuration($event)\">\n                    <a routerLink=\"/configuration\">Brand Editor</a>\n                </li>\n                <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"uploads($event)\">\n                    <a routerLink=\"/uploads\">Uploads</a>\n                </li> -->\n                <!-- <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" *ngIf=\"userrelations && userrelations.length\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"sendmessages($event)\">\n                    <a routerLink=\"/sendmessages\">Besked til medarbejdere</a>\n                </li> -->\n               <!-- <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"newgrid($event)\">\n                    <a routerLink=\"/newgrid\">newgrid</a>\n                </li> -->\n                <!-- <li [routerLinkActive]=\"['active']\" #rla=\"routerLinkActive\" [ngClass]=\"[active?'list-group-item active':'list-group-item']\" (click)=\"skemaeditor($event)\">\n                    <a routerLink=\"/skemaeditor\">Skema Editor</a>\n                </li> -->\n\n\n                <!-- <li routerLinkActive=\"active\" class=\"list-group-item\">\n                <a  routerLink=\"/sendpushNotification\">Push Notification</a>\n            </li> -->\n            </ul>\n        </nav>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/message-form/message-form.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/message-form/message-form.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"chatcontrol\"> \n        <input type=\"text\" class=\"form-control chatinput\" [(ngModel)]=\"message.content\"/>\n        <button class=\"btn btn-success sendbtn\" (click)=\"sendMessage()\">Send</button>\n      </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/message-item/message-item.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/message-item/message-item.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<li class=\"list-group-item\">\n    <img [src]=\"message.avatar\" class=\"avatar\" />\n    <div class=\"message\">\n        {{message.content}}\n    </div>\n    <div class=\"timeform\">\n        <i class=\"fa fa-clock-o\" aria-hidden=\"true\"></i>\n        <span class=\"timestamp\">at {{message.timestamp | date : 'dd/MM/yyyy' }}</span>\n    </div>\n</li>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/message-list/message-list.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/message-list/message-list.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"chatlist\">\n    <ul class=\"list-group\">\n      <message-item *ngFor=\"let msg of messages\" [message]=\"msg\"></message-item>\n    </ul>\n  </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/modal/modal.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/modal/modal.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ng-template #modal let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Upload file</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body itsystems--modal-body\" (drop)=\"onDrop($event)\">\n        <div class=\"itsystems__modal-container\">\n            <!-- (dragenter)=\"ondragEnter($event)\"\n                                                                                (dragleave)=\"ondragLeave($event)\"\n                                                                                (dragover)=\"ondragOver($event)\" -->\n            <!-- <p>One fine body&hellip;</p> -->\n            <form #uploadfileForm (ngSubmit)=\"onfileSubmit($event)\">\n                <label class=\"cursor-pointer itsystems__upload\">\n                    <input type=\"file\" ngf-select ng-model=\"new_files\" id=\"inputFile\" #inputFile class=\"\"\n                        (change)=\"onfileChange($event)\">\n                    <!--(click)=\"onfileClick($event)\"  -->\n\n                </label>\n                <div class=\"itsystems__dropContainer\">\n                    <span class=\"itsystems__dropContainer itsystems__dropContainer--droptext\"></span>\n                    <i class=\"fa fa-download\"></i>\n\n                </div>\n            </form>\n            <div class=\"uploadcontainer\">\n                <div [ngClass]=\"(uploadStatus==true) ? '':'d-none'\">\n                    <div class=\"alert alert-success\">\n                        <strong>Success!</strong> All done file uploaded successfully.\n                    </div>\n                </div>\n                <div [ngClass]=\"(fileStatus==true) ? '':'d-none'\">\n                    <div class=\"alert alert-danger \">\n                        <strong>Error!</strong> Allowed file types csv, json, xls, xlsx, pdf. Maximum 10MB allowed.\n                    </div>\n                </div>\n                <div [ngClass]=\"(uploadContainer==true) ? '':'d-none'\">\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-outline-dark\" (click)=\"onfileSubmit($event)\">\n            <i class=\"fa fa-save\"></i> Gem</button>\n        <button type=\"submit\" class=\"itsystems__operations-icon itsystems__buttons-icon\">\n            <i class=\"fa fa-save\"></i>Gem </button>\n        <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"c('Close click')\">\n            <i class=\"fa fa-times-circle\"></i> Annuler</button>\n    </div>\n\n</ng-template>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/newgrid/newgrid.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/newgrid/newgrid.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"loading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"loading\"></i>\n</div>\n\n<div class=\"newgrid\">\n    <div *ngIf=\"columns && columnsdata\">\n        <p-table [style]=\"{width:'100%'}\" [scrollable]=\"true\" #dt [columns]=\"columns\" [rows]=\"10\" [value]=\"columnsdata\" [paginator]=\"true\" currentPageReportTemplate=\"Showing {first} to {last} of {totalRecords} entries\" [filterDelay]=\"0\" [showCurrentPageReport]=\"true\"\n            [rowsPerPageOptions]=\"[10,25,50]\" [loading]=\"loading\" styleClass=\"p-datatable-customers\" [filterDelay]=\"0\" [globalFilterFields]=\"['levering','organisation_uuid','representative.name','status']\">\n            <!-- <ng-template pTemplate=\"caption\">\n                Gird Data\n            </ng-template> -->\n            <ng-template pTemplate=\"header\" let-columns>\n                <tr>\n                    <th *ngFor=\"let col of columns\" class=\"newgrid__colheading\" field=\"representative.name\" [pSortableColumn]=\"col.datafield\">\n                        {{col.text}}\n                    </th>\n                    <th pSortableColumn=\"representative.name\">Representative\n                        <p-sortIcon field=\"representative.name\"></p-sortIcon>\n                    </th>\n                </tr>\n\n\n                <tr>\n                    <th>\n                        <p-calendar (onSelect)=\"onDateSelect($event)\" (onClearClick)=\"dt.filter('', 'levering', 'equals')\" [showButtonBar]=\"true\" styleClass=\"p-column-filter\" placeholder=\"Levering Date\" [readonlyInput]=\"true\" dateFormat=\"yy-mm-dd\"></p-calendar>\n                    </th>\n                    <th>\n                        <input pInputText type=\"text\" (input)=\"dt.filter($event.target.value, 'organisation_uuid', 'startsWith')\" placeholder=\"Search by Platforms\" class=\"p-column-filter\">\n                    </th>\n                    <th>\n                        <input pInputText type=\"text\" (input)=\"dt.filter($event.target.value, 'bruger_uuid', 'startsWith')\" placeholder=\"Search by Platforms\" class=\"p-column-filter\">\n                    </th>\n                    <th>\n                        <!-- <input pInputText type=\"text\" (input)=\"dt.filter($event.target.value, 'navn', 'startsWith')\" placeholder=\"Search by Platforms\" class=\"p-column-filter\"> -->\n                        <p-multiSelect [options]=\"representatives\" placeholder=\"All\" (onChange)=\"onRepresentativeChange($event)\" styleClass=\"p-column-filter\" optionLabel=\"name\">\n                            <ng-template let-option pTemplate=\"item\">\n                                <div class=\"p-multiselect-representative-option\">\n                                    <span class=\"p-ml-1\">{{option.label}}</span>\n                                </div>\n                            </ng-template>\n                        </p-multiSelect>\n                    </th>\n                    <!-- <th>\n                        <p-multiSelect [options]=\"representatives\" placeholder=\"All\" (onChange)=\"onRepresentativeChange($event)\" styleClass=\"p-column-filter\" optionLabel=\"name\">\n                            <ng-template let-option pTemplate=\"item\">\n                                <div class=\"p-multiselect-representative-option\">\n                                    <span class=\"p-ml-1\">{{option.label}}</span>\n                                </div>\n                            </ng-template>\n                        </p-multiSelect>\n                    </th> -->\n\n\n                    <!-- <th>\n                        <input pInputText type=\"text\" (input)=\"dt.filter($event.target.value, 'country.name', 'contains')\" placeholder=\"Search by Country\" class=\"p-column-filter\">\n                    </th>\n                    <th>\n                        <p-multiSelect [options]=\"representatives\" placeholder=\"All\" (onChange)=\"onRepresentativeChange($event)\" styleClass=\"p-column-filter\" optionLabel=\"name\">\n                            <ng-template let-option pTemplate=\"item\">\n                                <div class=\"p-multiselect-representative-option\">\n                                    <span class=\"p-ml-1\">{{option.label}}</span>\n                                </div>\n                            </ng-template>\n                        </p-multiSelect>\n                    </th>\n                    <th>\n                        <p-calendar (onSelect)=\"onDateSelect($event)\" (onClearClick)=\"dt.filter('', 'date', 'equals')\" [showButtonBar]=\"true\" styleClass=\"p-column-filter\" placeholder=\"Registration Date\" [readonlyInput]=\"true\" dateFormat=\"yy-mm-dd\"></p-calendar>\n                    </th>\n                    <th>\n                        <p-dropdown [options]=\"statuses\" (onChange)=\"dt.filter($event.value, 'status', 'equals')\" styleClass=\"p-column-filter\" placeholder=\"Select a Status\" [showClear]=\"true\">\n                            <ng-template let-option pTemplate=\"item\">\n                                <span [class]=\"'customer-badge status-' + option.value\">{{option.label}}</span>\n                            </ng-template>\n                        </p-dropdown>\n                    </th>\n                    <th>\n                        <input pInputText type=\"text\" (input)=\"onActivityChange($event)\" placeholder=\"Minimum\" class=\"p-column-filter\" >\n                    </th> -->\n                </tr>\n\n\n\n\n            </ng-template>\n\n            <ng-template pTemplate=\"body\" let-user let-columns=\"columns\">\n                <tr>\n                    <td *ngFor=\"let col of columns\">\n                        {{user[col.datafield]}}\n                    </td>\n                </tr>\n            </ng-template>\n        </p-table>\n\n\n\n        <!-- <p-table #dt [value]=\"customers\" dataKey=\"id\"\n        [rows]=\"10\" [showCurrentPageReport]=\"true\" [rowsPerPageOptions]=\"[10,25,50]\" [loading]=\"loading\" styleClass=\"p-datatable-customers\"\n        [paginator]=\"true\" currentPageReportTemplate=\"Showing {first} to {last} of {totalRecords} entries\"\n        [filterDelay]=\"0\" [globalFilterFields]=\"['name','country.name','representative.name','status']\">\n        <ng-template pTemplate=\"caption\"> -->\n\n        <!-- <p-table #dt [value]=\"customers\" dataKey=\"id\" [rows]=\"10\" [showCurrentPageReport]=\"true\" [rowsPerPageOptions]=\"[10,25,50]\" [loading]=\"loading\" styleClass=\"p-datatable-customers\" [paginator]=\"true\" currentPageReportTemplate=\"Showing {first} to {last} of {totalRecords} entries\"\n        [filterDelay]=\"0\" [globalFilterFields]=\"['name','country.name','representative.name','status']\">\n        <ng-template pTemplate=\"caption\">\n            <div class=\"table-header\">\n                List of Customers\n                <span class=\"p-input-icon-left\">\n                <i class=\"pi pi-search\"></i>\n                <input pInputText type=\"text\" (input)=\"dt.filterGlobal($event.target.value, 'contains')\" placeholder=\"Global Search\" />\n            </span>\n            </div>\n        </ng-template>\n        <ng-template pTemplate=\"header\">\n            <tr>\n                <th>Name</th>\n                <th>Country</th>\n                <th>Representative</th>\n                <th>Date</th>\n                <th>Status</th>\n                <th>Activity</th>\n            </tr>\n            <tr>\n                <th>\n                    <input pInputText type=\"text\" (input)=\"dt.filter($event.target.value, 'name', 'startsWith')\" placeholder=\"Search by Name\" class=\"p-column-filter\">\n                </th>\n                <th>\n                    <input pInputText type=\"text\" (input)=\"dt.filter($event.target.value, 'country.name', 'contains')\" placeholder=\"Search by Country\" class=\"p-column-filter\">\n                </th>\n                <th>\n                    <p-multiSelect [options]=\"representatives\" placeholder=\"All\" (onChange)=\"onRepresentativeChange($event)\" styleClass=\"p-column-filter\" optionLabel=\"name\">\n                        <ng-template let-option pTemplate=\"item\">\n                            <div class=\"p-multiselect-representative-option\">\n                                <span class=\"p-ml-1\">{{option.label}}</span>\n                            </div>\n                        </ng-template>\n                    </p-multiSelect>\n                </th>\n                <th>\n                    <p-calendar (onSelect)=\"onDateSelect($event)\" (onClearClick)=\"dt.filter('', 'date', 'equals')\" [showButtonBar]=\"true\" styleClass=\"p-column-filter\" placeholder=\"Registration Date\" [readonlyInput]=\"true\" dateFormat=\"yy-mm-dd\"></p-calendar>\n                </th>\n                <th>\n                    <p-dropdown [options]=\"statuses\" (onChange)=\"dt.filter($event.value, 'status', 'equals')\" styleClass=\"p-column-filter\" placeholder=\"Select a Status\" [showClear]=\"true\">\n                        <ng-template let-option pTemplate=\"item\">\n                            <span [class]=\"'customer-badge status-' + option.value\">{{option.label}}</span>\n                        </ng-template>\n                    </p-dropdown>\n                </th>\n                <th>\n                    <input pInputText type=\"text\" (input)=\"onActivityChange($event)\" placeholder=\"Minimum\" class=\"p-column-filter\">\n                </th>\n            </tr>\n        </ng-template>\n        <ng-template pTemplate=\"body\" let-customer>\n            <tr>\n                <td>\n                    {{customer.name}}\n                </td>\n                <td>\n                    <span class=\"image-text\">{{customer.country.name}}</span>\n                </td>\n                <td>\n                    <span class=\"image-text\">{{customer.representative.name}}</span>\n                </td>\n                <td>\n                    {{customer.date}}\n                </td>\n                <td>\n                    <span [class]=\"'customer-badge status-' + customer.status\">{{customer.status}}</span>\n                </td>\n                <td>\n                    <p-progressBar [value]=\"customer.activity\" [showValue]=\"false\"></p-progressBar>\n                </td>\n            </tr>\n        </ng-template>\n        <ng-template pTemplate=\"emptymessage\">\n            <tr>\n                <td colspan=\"6\">No customers found.</td>\n            </tr>\n        </ng-template>\n    </p-table> -->\n\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/notifications/notifications.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/notifications/notifications.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- FUTURE COMPONENT -->\n<div class=\"notifications\">\n    <div *ngIf=\"success ===true\" class=\"notifications__success\">\n        <p class=\"notifications__message\">\n            {{message}}\n        </p>\n    </div>\n    <div *ngIf=\"warning ===true\" class=\"notifications__warning\">\n        <p class=\"notifications__message\">\n            {{message}}\n        </p>\n    </div>\n    <div *ngIf=\"error ===true\" class=\"notifications__error\">\n        <p class=\"notifications__message\">\n            {{message}}\n        </p>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/notifyviewer/notifyviewer.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/notifyviewer/notifyviewer.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"loading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"loading\"></i>\n</div>\n\n\n<div class=\"notifyviewer\">\n    <div class=\"modal-header notifyviewer__filterctr\">\n\n        <button type=\"button\" class=\"btn notifyviewer__filterclose\" (click)=\"cancel($event)\">\n                        <span aria-hidden=\"true\">&times;</span>\n                </button>\n    </div>\n    <div class=\"form-group\">\n        <form>\n            <div class=\"form-group row\">\n                <h6 class=\"notifyviewer__heading\">{{selectedmenuItem}}: {{selectedItem.value}}</h6>\n                <p class=\"notifyviewer__subheading\" *ngIf=\"selectedItem.key==='notificationBatchUpdate' \">Ret udsendelsestidspunkt for filtrerede notifikationer</p>\n                <p class=\"notifyviewer__subheading\" *ngIf=\"selectedItem.key!='notificationBatchUpdate' \">Aflys udsendelse af filterede notifikationer</p>\n                <label *ngIf=\"selectedItem.key==='notificationBatchUpdate'\" class=\"col-sm-4 col-form-label\">Tidspunkt</label>\n                <div *ngIf=\"selectedItem.key==='notificationBatchUpdate'\" class=\"col-sm-8\">\n                    <div class=\"notifyviewer__mltsltd-dateitem\">\n                        <input [ngModel]=\"selecteddate\" [min]=\"mindate\" [ngModelOptions]=\"{standalone: true}\" value={{selecteddate}} [readonly]=\"true\" (ngModelChange)=\"DateChanged($event)\" [owlDateTime]=\"k\" type=\"text\" class=\"form-control\" [owlDateTimeTrigger]=\"k\" placeholder=\"Vælg dato/tidspunkt\"\n                        />\n                        <span class=\"notifyviewer__dateicon\" [owlDateTimeTrigger]=\"k\"><i\n                                                                class=\"fal fa-calendar-alt\"></i></span>\n                        <span class=\"notifyviewer__closeicon\" (click)=\"clearDate(item)\"><i\n                                                                class=\"fal fa-times\"></i></span>\n                        <owl-date-time [startAt]=\"mindate\" (dateSelected)=\"selectedDate(k)\" [pickerType]=\"'both'\" [disabled]=\"false\" #k>\n                        </owl-date-time>\n                        <!-- [showSecondsTimer]='true' -->\n                    </div>\n                    <div *ngIf=\"!selectedtime\">\n                        <p class=\"text-danger\">\n                            Udfyld venligst dette felt.\n                        </p>\n                    </div>\n                </div>\n            </div>\n        </form>\n    </div>\n\n    <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-outline-dark notifyviewer__cancel\" (click)=\"cancel($event)\">\n                        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n        <button type=\"submit\" class=\"btn btn-outline-dark notifyviewer__save\" [disabled]=\"!selectedtime && selectedItem.key==='notificationBatchUpdate'\" (click)=\"saveFilter($event)\">\n                        <i class=\"fa fa-save\"></i>Udfør</button>\n    </div>\n</div>\n\n<jqxNotification #successNotification [width]=\"400\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>\n        Success!! Thank you..\n    </div>\n</jqxNotification>\n<jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/operations/operations.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/operations/operations.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"operations\" *ngIf=\"buttons  && buttons.length\">\n    <div class=\"dropdown\">\n        <ng-template ngFor let-item [ngForOf]=\"buttons\" let-i=\"index\" [ngForTrackBy]=\"trackByFn\">\n            <button title=\"Eksporter\" class=\"btn  dropdown-toggle operations__operations-exportbtn\" *ngIf=\"item.click_action==='export'\" type=\"button\" data-toggle=\"dropdown\"> \n            <i class=\"fas fa-cloud-download\"></i>Eksporter\n        <span class=\"caret\"></span>\n    </button>\n        </ng-template>\n        <ul class=\"dropdown-menu itsystems__operations-exportul\">\n            <li class=\"operations__operations-exportli\" id=\"csv\" (click)=\"download($event)\"><a href=\"#\" id=\"csv\" class=\"operations__operations-exporta\">csv</a>\n            </li>\n            <li class=\"operations__operations-exportli\" id=\"xlsx\" (click)=\"download($event)\"><a href=\"#\" id=\"xlsx\" class=\"operations__operations-exporta\">xlsx</a>\n            </li>\n        </ul>\n    </div>\n\n    <ng-template ngFor let-clickitem [ngForOf]=\"buttons\" let-i=\"index\" [ngForTrackBy]=\"trackByFn\">\n        <a title=\"{{clickitem.value}}\" (click)=\"open(modal,$event)\" *ngIf=\"clickitem.click_action==='CALL_importer_objektdata'\" class=\"button operations__operations-icon\" href=\"#\">\n            <i class=\"fas fa-cloud-upload\"></i>{{clickitem.value}}</a>\n        <a title=\"{{clickitem.value}}\" *ngIf=\"clickitem.click_action==='CALL_update_objekt'\" (click)=\"saveData($event)\" class=\"button operations__operations-icon\" href=\"#\">\n            <i class=\"fa fa-plus-circle\"></i> {{clickitem.value}}</a>\n        <a title=\"{{clickitem.value}}\" *ngIf=\"clickitem.click_action==='CALL_objekt_detaljer'\" (click)=\"CreatePage($event)\" class=\"button operations__operations-icon\" href=\"#\">\n            <i class=\"fa fa-plus-circle\"></i> {{clickitem.value}}</a>\n        <a title=\"{{clickitem.value}}\" class=\"button operations__operations-icon\" *ngIf=\"clickitem.click_action==='delete'\" (click)=\"Delete($event)\" href=\"#\">\n            <i class=\"fa fa-trash\"></i>{{clickitem.value}}</a>\n        <a title=\"{{clickitem.value}}\" (click)=\"Cancel($event)\" *ngIf=\"clickitem.click_action==='annuller'\" class=\"button operations__operations-icon\">\n            <i class=\"fa fa-times-circle\"></i>{{clickitem.value}}</a>\n        <!-- <a title=\"{{clickitem.value}}\" *ngIf=\"clickitem.key==='gem'\" (click)=\"saveData($event)\" class=\"button operations__operations-icon\">\n            <i class=\"fa fa-save\"></i>{{clickitem.value}}\n        </a> -->\n        <a title=\"{{clickitem.value}}\" *ngIf=\"clickitem.click_action==='refresh'\" (click)=\"getValueFromObservable()\" class=\"button operations__operations-icon\">\n            <i class=\"fa fa-refresh\"></i>{{clickitem.value}}\n        </a>\n        <a title=\"{{clickitem.value}}\" (click)=\"open(filterviewer,$event)\" *ngIf=\"clickitem.click_action==='filterAjour'\" class=\"button operations__operations-icon\" href=\"#\">\n            <i class=\"fas fa-save\"></i>{{clickitem.value}}</a>\n        <!-- <a title=\"Refresh\" *ngIf=\"alloperations.includes('Opdater listen')\" (click)=\"getValueFromObservable()\" class=\"button operations__operations-icon\">\n        <i class=\"fa fa-refresh\"></i>Refresh Data\n    </a>  -->\n    </ng-template>\n    <div class=\"dropdown\" *ngIf=\"Handlinger && Handlinger.dropdown\">\n        <button title=\"Handlinger\" class=\"btn dropdown-toggle operations__operations-exportbtn\" id=\"handling\" type=\"button\" data-toggle=\"dropdown\"> <i class=\"fas fa-bell\"></i>Handlinger\n            <span class=\"caret\"></span>\n        </button>\n        <ul class=\"dropdown-menu itsystems__operations-exportul\" aria-labelledby=\"handling\">\n            <ng-template ngFor let-item [ngForOf]=\"Handlinger.dropdown\" let-i=\"index\" let first=first; [ngForTrackBy]=\"trackByFn\">\n                <li class=\"operations__operations-exportli\" id=\"item.key\" (click)=\"openNotification(notifyviewer,$event,item)\"><a href=\"#\" id=\"item.key\" class=\"operations__operations-exporta\">{{item.value}}</a>\n                </li>\n            </ng-template>\n        </ul>\n    </div>\n    <div class=\"dropdown\" *ngIf=\"Udsend && Udsend.length\">\n        <ng-template ngFor let-udsenditem [ngForOf]=\"Udsend\" let-i=\"index\" let first=first; [ngForTrackBy]=\"trackByFn\">\n            <button (click)=\"openSendEmail(sendEmail,$event,Udsend)\" title=\"Udsend\" class=\"btn operations__operations-exportbtn\" id=\"handling\" type=\"button\"> <i class=\"fas fa-envelope\"></i>{{udsenditem.value}}\n            <span class=\"caret\"></span>\n        </button>\n        </ng-template>\n    </div>\n    <ng-template #modal let-c=\"close\" let-d=\"dismiss\">\n        <div class=\"modal-header\">\n            <h4 class=\"modal-title\">Upload file</h4>\n            <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n                <span aria-hidden=\"true\">&times;</span>\n            </button>\n        </div>\n        <div class=\"modal-body itsystems--modal-body\" (drop)=\"onDrop($event)\">\n            <div class=\"itsystems__modal-container\">\n                <form #uploadfileForm (ngSubmit)=\"onfileSubmit($event)\">\n                    <label class=\"cursor-pointer itsystems__upload\">\n                        <input type=\"file\" ngf-select ng-model=\"new_files\" id=\"inputFile\" #inputFile\n                            class=\"\" (change)=\"onfileChange($event)\">\n                    </label>\n                    <div class=\"itsystems__dropContainer\">\n                        <span class=\"itsystems__dropContainer itsystems__dropContainer--droptext\"></span>\n                        <i class=\"fa fa-download\"></i>\n                    </div>\n                </form>\n                <div class=\"uploadcontainer\">\n                    <div [ngClass]=\"(uploadStatus==true) ? '':'d-none'\">\n                        <div class=\"alert alert-success\">\n                            <strong>Success!</strong> All done file uploaded successfully.\n                        </div>\n                    </div>\n                    <div [ngClass]=\"(fileStatus==true) ? '':'d-none'\">\n                        <div class=\"alert alert-danger \">\n                            <strong>Error!</strong> Only csv,xlsx,json filetypes are allowed. Maximum file size 10MB allowed.\n                        </div>\n                    </div>\n                    <div [ngClass]=\"(uploadContainer==true) ? '':'d-none'\">\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-outline-dark operations__cancel\" (click)=\"d('Cross click')\">\n                        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n            <button type=\"submit\" class=\"btn btn-outline-dark operations__save\" (click)=\"onfileSubmit($event)\">\n                        <i class=\"fa fa-save\"></i>Gem</button>\n\n        </div>\n    </ng-template>\n    <ng-template #filterviewer let-c=\"close\" let-d=\"dismiss\">\n        <filter-viewer [allfilterList]=\"filterList\" [filtersdata]=\"filtersdata\"></filter-viewer>\n    </ng-template>\n\n    <ng-template ngFor let-clickitem *ngIf=\"buttons\" [ngForOf]=\"buttons\" let-i=\"index\" [ngForTrackBy]=\"trackByFn\">\n        <div *ngIf=\"clickitem.click_action==='gemt_filtre'\">\n            <div class=\"col-sm-4 itsystems__filter\">\n                <select [(ngModel)]=\"filteruuid\" data-live-search=\"true\" title=\"Intet valgt\" [ngModelOptions]=\"{standalone: true}\" (change)=\"getFilterData($event,item)\" class=\"form-control selectpicker show-tick\" #choosefilter id=\"choosefilter\">\n            <ng-template ngFor let-item [ngForOf]=\"filterList\" let-i=\"index\" let first=first;\n                [ngForTrackBy]=\"trackByFn\">\n                <option [ngValue]=\"item.uuid\">\n                    {{item.titel}}\n                </option>\n            </ng-template>\n        </select>\n                <span *ngIf=\"filterList && filterList.length>0\" class=\"operations__showfltrdata\" (click)=\"showfilterData(jsonviewer,$event)\"><i class=\"fa fa-info-circle\"></i>\n        </span>\n                <ng-template #jsonviewer let-c=\"close\" let-d=\"dismiss\">\n                    <jsonviewer-viewer [filtersdata]=\"filtersdata\"></jsonviewer-viewer>\n                </ng-template>\n            </div>\n            <label *ngIf=\"list===true\" class=\"col-sm-2 col-form-label operations__filterlabel\">{{clickitem.value}}</label>\n        </div>\n\n    </ng-template>\n\n\n    \n    <ng-template #notifyviewer let-c=\"close\" let-d=\"dismiss\">\n        <notifyviewer-viewer [selectedmenuItem]=\"sharedthis.bredcrum\" [selectedItem]=\"selectedItem\" [displayedrows]=\"displayedrows\"></notifyviewer-viewer>\n    </ng-template>\n    <ng-template #sendEmail let-c=\"close\" let-d=\"dismiss\">\n        <sendemail-viewer [action]=\"action\"></sendemail-viewer>\n    </ng-template>\n</div>\n\n<jqxNotification #successnotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>\n        Success!! Thank you..\n    </div>\n</jqxNotification>\n<jqxNotification #errornotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div>{{errorMessage}}</div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/otp/otp.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/otp/otp.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"otp\">\n    <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n    <p text-center class=\"login__heading\">Vi har sendt dig en engangskode, som du skal bruge til at oprette nyt kodeord. Engangskoden er gyldig i to timer. Der kan gå nogle minutter, før koden når frem til dig. Hvis du ikke modtager koden i din indbakke, så kontrollér eventuelt din folder\n        for uønsket mail</p>\n    <form class=\"otp__form\" [formGroup]=\"loginForm\" (ngSubmit)=\"gotochangePasword()\">\n\n        <div class=\"form-group\">\n            <label for=\"exampleInputEmail1\">Indtast engangskode</label>\n        </div>\n        <div class=\"form-group\">\n            <input type=\"tel\" [(ngModel)]=\"otpval1\" tabindex=\"1\" formControlName=\"otp1\" #otp1 id=\"otp1\" maxlength=\"1\" class=\"otp__text-box\" max=\"9\" (keyup)=\"next1($event)\" />\n            <input type=\"tel\" [(ngModel)]=\"otpval2\" tabindex=\"1\" formControlName=\"otp2\" #otp2 id=\"otp2\" maxlength=\"1\" class=\"otp__text-box\" max=\"9\" (keyup)=\"next2($event)\" />\n            <input type=\"tel\" [(ngModel)]=\"otpval3\" tabindex=\"1\" formControlName=\"otp3\" #otp3 id=\"otp3\" maxlength=\"1\" class=\"otp__text-box\" max=\"9\" (keyup)=\"next3($event)\" />\n            <input type=\"tel\" [(ngModel)]=\"otpval4\" tabindex=\"1\" formControlName=\"otp4\" #otp4 id=\"otp4\" maxlength=\"1\" class=\"otp__text-box\" max=\"9\" (keyup)=\"next4($event)\" />\n        </div>\n        <div *ngIf=\"noauthorisation\">\n            <div class=\"login__error\">autorisation bruger findes ikke i systemet</div>\n        </div>\n        <div *ngIf=\"error\">\n            <div class=\"login__error\">{{errormessage}}</div>\n        </div>\n        <button [disabled]=\"!username\" type=\"submit\" (click)=\"otp()\" class=\"btn login__login btn-lg\">Fortsæt</button>\n    </form>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/search/search.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/search/search.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"search\">\n\n    <div class=\"search__container\">\n        <div *ngIf=\"loading===true\" class=\"spinner\">\n            <i class=\"fa fa-spinner fa-spin\"></i>\n        </div>\n\n        <div *ngIf=\"searchform && filterList\">\n            <ngb-accordion [destroyOnHide]=\"false\" (panelChange)=\"panelExpanded($event)\"  #acc=\"ngbAccordion\"\n                [activeIds]=\"activeIds\">\n                <ngb-panel id=\"search_panel\">\n                    <ng-template ngbPanelTitle>\n                        <i class=\"fas fa-search\"></i> Filtre muligheder\n                    </ng-template>\n                    <ng-template ngbPanelContent>\n                        <form class=\"form-group\" id=\"searchform\" #searchForm novalidate [formGroup]=\"searchform\">\n                            <div class=\"col-sm-10\">\n                                <ng-template ngFor let-item [ngForOf]=\"searchform.get('items').controls\" let-i=\"index\"\n                                    [ngForTrackBy]=\"trackByFn\">\n                                    <div class=\"form-group row\" *ngIf=\"item.value.type=='input' && item.value.property!='hidden'\">\n                                        <label title=\"{{item.value.name}}\"\n                                            class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\">\n                                            <input class=\"form-control\" [(ngModel)]=\"item.value.value\"\n                                                [ngModelOptions]=\"{standalone: true}\" value={{item.value.value}}>\n                                        </div>\n                                    </div>\n\n                                    <div class=\"form-group row\" *ngIf=\"item.value.type=='datepicker'\">\n                                        <label title=\"{{item.value.name}} \"\n                                            class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\">\n                                            <div *ngIf=\"item.value.type=='datepicker'\" class=\"search__radio\">\n                                                <input [selectMode]=\"'range'\" [ngModelOptions]=\"{standalone: true}\"\n                                                    [(ngModel)]=\"item.value.formatdate\" [readonly]=\"true\"\n                                                     [owlDateTime]=\"j\" type=\"text\"\n                                                    class=\"form-control\" value={{item.value.formatdate}}\n                                                    [disabled]=\"item.value.readonly===true? true : false\"\n                                                    [owlDateTimeTrigger]=\"j\" placeholder=\"\" />\n                                                <span class=\"search__dateicon\" [owlDateTimeTrigger]=\"j\"><i\n                                                        class=\"fal fa-calendar-alt\"></i></span>\n                                                <span class=\"search__closeicon\" (click)=\"clearDate(item)\"><i\n                                                        class=\"fal fa-times\"></i></span>\n                                                <owl-date-time (afterPickerClosed)=\"dateChange(item,i)\"\n                                                    [pickerType]=\"'both'\" #j>\n                                                </owl-date-time>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" *ngIf=\"item.value.type=='dropdown'\">\n                                        <label title=\"{{item.value.name}} \"\n                                            class=\"col-sm-4 col-form-label\">{{item.value.name}}</label>\n                                        <div class=\"col-sm-8\">\n                                            <select multiple (change)=\"Multiselect($event,item)\"\n                                                [(ngModel)]=\"item.value.selected_name\"\n                                                [ngModelOptions]=\"{standalone: true}\"\n                                                class=\"form-control selectpicker show-tick\" data-live-search=\"true\">\n                                                <option\n                                                    *ngFor=\"let optionitems of item.value.options; index as op;trackBy: trackByFn\"\n                                                    [selected]=\"optionitems.selected === true ? 'selected':null\"\n                                                    [ngValue]=\"optionitems.rel_uri\" data-tokens=\"optionitems\">\n                                                    {{optionitems.rel_uri}}\n                                                </option>\n                                            </select>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group col-sm-8 offset-md-4\" id=\"{{item.value.name}} \"\n                                        *ngIf=\"item.value.type=='button'\">\n                                        <button type=\"submit\" (click)=\"Search()\"\n                                            class=\"btn search__fetch btn-lg\">{{item.value.name}}</button>\n                               \n                                    <button type=\"submit\" (click)=\"resetSearch(item)\"\n                                        class=\"btn search__reset btn-lg\">Nulstil</button>\n                                </div>\n                                    \n                                </ng-template>\n                            </div>\n                        </form>\n                    </ng-template>\n                </ngb-panel>\n                <!-- <ngb-panel  id=\"ngb-panel-2\">\n                    <ng-template ngbPanelTitle>\n                        <i class=\"fas fa-filter\"></i> Gemt Filtre\n                    </ng-template>\n                    <ng-template ngbPanelContent>\n                        <form class=\"form-group search__filterform\" id=\"searchform\"  novalidate >\n                            <div class=\"col-sm-10\">\n                            <div class=\"form-group row\">  \n                           \n                            <label class=\"col-sm-4 col-form-label\">Gemte Filtre</label>\n                            <div *ngIf=\"filterList && filterList.length\" class=\"col-sm-8\">\n                                <select [(ngModel)]=\"filteruuid\" data-live-search=\"true\" title=\"Intet valgt\"\n                                    [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\"\n                                    #choosefilter id=\"choosefilter\">\n                                    <ng-template ngFor let-item [ngForOf]=\"filterList\" let-i=\"index\" let first=first;\n                                        [ngForTrackBy]=\"trackByFn\">\n                                        <option [ngValue]=\"item.uuid\">\n                                            {{item.titel}}\n                                        </option>\n                                    </ng-template>\n                                </select>\n                            </div>\n</div>\n                            <div class=\"form-group col-sm-6 offset-md-4 \">\n                            <button [disabled]='!filteruuid' type=\"submit\" (click)=\"Fetchfilter()\" class=\"btn search__fetch btn-lg\">Hent Filtre Data</button>\n                        </div>\n\n                        \n                    </div>\n                        </form>\n                        </ng-template>\n                </ngb-panel> -->\n            </ngb-accordion>\n        </div>\n    </div>\n</div>\n\n<jqxNotification #successNotification [width]=\"400\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"0.9\"\n    [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>\n        Success!! Thank you..\n    </div>\n</jqxNotification>\n<jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\"\n    [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/sendemail/sendemail.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/sendemail/sendemail.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"loading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"loading\"></i>\n</div>\n\n\n<div class=\"sendemail\">\n    <div class=\"modal-header notifyviewer__filterctr\">\n        <button type=\"button\" class=\"btn notifyviewer__filterclose\" (click)=\"cancel($event)\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"form-group\">\n        <form>\n            <div class=\"form-group row\">\n                <h6 class=\"sendemail__heading\">Er du sikker? Ved klik på Udfør sendes alle notifikationer med status\n                    new, hvor Levering er overskredet, uden hensyn til den valgte filtrering. Klik på Udfør for at\n                    bekræfte, eller på Annuller for at afbryde.</h6>\n            </div>\n        </form>\n    </div>\n\n    <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-outline-dark sendemail__cancel\" (click)=\"cancel($event)\">\n            <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n        <button type=\"submit\" class=\"btn btn-outline-dark sendemail__save\" (click)=\"sendEmail($event)\">\n            <i class=\"fa fa-save\"></i>Udfør</button>\n    </div>\n</div>\n\n<jqxNotification #successNotification [width]=\"400\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"0.9\"\n    [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>\n        Success!! Thank you..\n    </div>\n</jqxNotification>\n<jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\"\n    [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/sendmessages/sendmessages.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/sendmessages/sendmessages.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\n    <div class=\"sendmessages\">\n        <div class=\"sendmessages__header-container\">\n            <div class=\"sendmessages__header\">\n                <!-- <operations-viewer [list]=\"true\" [details]=\"false\" (newlistdata)=\"RefreshData($event)\" (filtereddata)=applyfilters($event); [filterList]=\"filterList\" [sharedthis]=\"this\">\n                </operations-viewer> -->\n            </div>\n            <div class=\"sendmessages__form-container\">\n                <div class=\"sendmessages__nav-container\">\n                    <ul class=\"nav nav-pills nav-justified\" id=\"pills-tab\" role=\"tablist\">\n                        <li class=\"nav-item\">\n                            <a data-toggle=\"pill\" href=\"#pills-i\" id=\"pills-i-tab\" role=\"tab\" class=\"nav-link active\" aria-controls=\"pills-i\" aria-selected=\"true\">Besked til Medarbejdere\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n\n                <div class=\"tab-content sendmessages__container\" id=\"pills-tabContent\">\n                    <div class=\"tab-pane fade show active\" id=\"pills-i\" role=\"tabpanel\" aria-labelledby=\"pills-i-tab\">\n                        <div class=\"form-group\">\n                            <form>\n\n                                <div *ngIf=\"allreceivers && allreceivers.length>0\" class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">\n                                        Modtagere</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctreceiver\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChange($event,item)\" id=\"slctdok\">\n                                            <ng-template ngFor let-item [ngForOf]=\"allreceivers\" let-i=\"index\" let\n                                                first=first; [ngForTrackBy]=\"trackByFn\">\n                                                <option [ngValue]=\"item.uuid\">\n                                                    {{item.titel}}\n                                                </option>\n                                            </ng-template>\n                                        </select>\n                                    </div>\n                                </div>\n\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Udsendelsestidspunkt</label>\n                                    <div *ngIf=\"mindate\" class=\"col-sm-8\">\n                                        <input [ngModel]=\"selecteddate\" [min]=\"mindate\" [ngModelOptions]=\"{standalone: true}\" value={{selecteddate}} [readonly]=\"true\" (ngModelChange)=\"DateChanged($event)\" [owlDateTime]=\"k\" type=\"text\" class=\"form-control\" [owlDateTimeTrigger]=\"k\" placeholder=\"Vælg dato/tidspunkt\"\n                                        />\n                                        <span class=\"notifyviewer__dateicon\" [owlDateTimeTrigger]=\"k\"><i\n                                            class=\"fal fa-calendar-alt\"></i></span>\n                                        <span class=\"notifyviewer__closeicon\" (click)=\"clearDate(item)\"><i\n                                            class=\"fal fa-times\"></i></span>\n                                        <owl-date-time [startAt]=\"mindate\" (dateSelected)=\"selectedDate(k)\" [pickerType]=\"'both'\" [disabled]=\"false\" #k>\n                                        </owl-date-time>\n                                        <!-- [showSecondsTimer]='true' -->\n\n                                        <div *ngIf=\"!selecteddate\">\n                                            <p class=\"text-danger\">\n                                                Udfyld venligst dette felt.\n                                            </p>\n                                        </div>\n                                    </div>\n                                </div>\n\n\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Skabelon</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctskabelon\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChange($event,item)\" id=\"slctdok\">\n                                            <ng-template ngFor let-item [ngForOf]=\"allreceivers\" let-i=\"index\" let\n                                                first=first; [ngForTrackBy]=\"trackByFn\">\n                                                <option [ngValue]=\"item.uuid\">\n                                                    {{item.titel}}\n                                                </option>\n                                            </ng-template>\n                                        </select>\n                                    </div>\n                                </div>\n\n\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Overskrift</label>\n                                    <div class=\"col-sm-8\">\n                                        <input class=\"form-control\" required [(ngModel)]=\"overskrift\" [ngModelOptions]=\"{standalone: true}\" value={{overskrift}}>\n                                        <div *ngIf=\"!overskrift\">\n                                            <p class=\"text-danger\">\n                                                Udfyld venligst dette felt.\n                                            </p>\n                                        </div>\n                                    </div>\n                                </div>\n\n\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Indsæt flettefelt</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctskabelon\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChange($event,item)\" id=\"slctdok\">\n                                            <ng-template ngFor let-item [ngForOf]=\"allreceivers\" let-i=\"index\" let\n                                                first=first; [ngForTrackBy]=\"trackByFn\">\n                                                <option [ngValue]=\"item.uuid\">\n                                                    {{item.titel}}\n                                                </option>\n                                            </ng-template>\n                                        </select>\n                                    </div>\n                                </div>\n\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Besked</label>\n                                    <div class=\"col-sm-8\">\n                                        <!-- <div id=\"editor\">How are you!</div> -->\n                                        <!-- <span (click)=\"openfilter()\" class=\"sendmessages__user\"><i class=\"fal fa-user-alt\"></i></span> -->\n                                        <!-- <ckeditor #editor id=\"editor\" type=\"classic\" [config]=\"config\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"editorData\" (change)=\"onchngEditordata($event)\"></ckeditor>\n -->\n                                        <button type=\"button\" (click)=\"insertdata()\" class=\"btn btn-primary\">Insert</button>\n                                        <ckeditor #editor id=\"editor\" type=\"classic\" [config]=\"config\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"accordianitem\" (change)=\"onchngEditordata($event)\"></ckeditor>\n                                    </div>\n                                </div>\n\n\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Status</label>\n                                    <div class=\"col-sm-8\">\n                                        <input class=\"form-control\" required [(ngModel)]=\"status\" [ngModelOptions]=\"{standalone: true}\" value={{status}}>\n                                        <div *ngIf=\"!status\">\n                                            <p class=\"text-danger\">\n                                                Udfyld venligst dette felt.\n                                            </p>\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Skabelon navn</label>\n                                    <div class=\"col-sm-8\">\n                                        <input class=\"form-control\" required [(ngModel)]=\"skabelonname\" [ngModelOptions]=\"{standalone: true}\" value={{skabelonname}}>\n                                        <div *ngIf=\"!selectedtime\">\n                                            <p class=\"text-danger\">\n                                                Udfyld venligst dette felt.\n                                            </p>\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">\n                                        Tilgængelig for</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctdok\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChange($event,item)\" id=\"slctdok\">\n                                            <ng-template ngFor let-item [ngForOf]=\"allreceivers\" let-i=\"index\" let\n                                                first=first; [ngForTrackBy]=\"trackByFn\">\n                                                <option [ngValue]=\"item.uuid\">\n                                                    {{item.titel}}\n                                                </option>\n                                            </ng-template>\n                                        </select>\n                                    </div>\n                                </div>\n\n                            </form>\n\n\n                        </div>\n\n                    </div>\n                </div>\n\n\n\n                <!-- <jqxNotification #successNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"3000\" [template]=\"'success'\">\n                    <div>\n                        Success!! Thank you..\n                    </div>\n                </jqxNotification>\n                <jqxNotification #errorNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"6000\" [template]=\"'error'\">\n                    <div>Error!! pelase try again</div>\n                </jqxNotification> -->\n\n            </div>\n\n        </div>\n    </div>\n    <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/skema/skema.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/skema/skema.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\n    <div class=\"skemapage\">\n        <div class=\"skemapage__header-container\">\n            <div class=\"skemapage__header\">\n                <breadcrum-viewer [breadcrumelements]=\"navigationElements\"></breadcrum-viewer>\n                <div class=\"detailspage__operations\">\n                    <div>\n                        <button (click)=\"Cancel($event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\" title=\"Cancel save\">\n                                                        <i class=\"fa fa-times-circle\"></i>Annuller</button>\n                        <!-- <button type=\"submit\" (click)=\"deleteSkema($event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\">\n                                                            <i class=\"fa fa-trash\"></i>Slet\n                                                    </button> -->\n                        <button type=\"submit\" (click)=\"saveSkema(skemadelete,$event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\">\n                                                        <i class=\"fa fa-save\"></i>Gem\n                                                </button>\n                        <button type=\"submit\" (click)=\"createSkema(skemacreate,$event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\">\n                                                    <i class=\"fa fa-plus-circle\"></i>Opret\n                                            </button>\n                    </div>\n                </div>\n            </div>\n            <div class=\"skemapage__form-container\">\n                <div class=\"skemapage__nav-container\">\n                    <ul class=\"nav nav-pills nav-justified\" id=\"pills-tab\" role=\"tablist\">\n                        <li class=\"nav-item\">\n                            <a data-toggle=\"pill\" href=\"#pills-i\" id=\"pills-i-tab\" role=\"tab\" class=\"nav-link active\" aria-controls=\"pills-i\" aria-selected=\"true\">Rediger\n                                                                Skema</a>\n                        </li>\n                    </ul>\n                </div>\n\n                <div class=\"tab-content skemapage__container\" id=\"pills-tabContent\">\n                    <div class=\"tab-pane fade show active\" id=\"pills-i\" role=\"tabpanel\" aria-labelledby=\"pills-i-tab\">\n                        <div class=\"form-group\">\n                            <form *ngIf=\"sorteduserrelations && sorteduserrelations.length>0\">\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Organisation</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctorg\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChange($event,item)\" id=\"slctorg\">\n                                                                                        <ng-template ngFor let-item\n                                                                                                [ngForOf]=\"sorteduserrelations\"\n                                                                                                let-i=\"index\" let\n                                                                                                first=first;\n                                                                                                [ngForTrackBy]=\"trackByFn\">\n                                                                                                <option [ngValue]=\"item.organisation_uuid\"\n                                                                                                        *ngIf=\"item.org && item.organisation_uuid\">\n                                                                                                        {{item.org}}\n                                                                                                </option>\n                                                                                        </ng-template>\n                                                                                </select>\n                                        <span ngxClipboard (cbOnSuccess)=\"copied($event)\" [cbContent]=\"slcOrgname\" (click)=\"Copyorg('orgcopyflag')\" [ngClass]=\"orgcopyflag === true?' skemapage__copyicon skemapage__copyicon--active':'skemapage__copyicon skemapage__copyicon--inactive'\"><i\n                                                                                                class=\"far fa-copy\"></i></span>\n                                    </div>\n\n                                </div>\n                                <div class=\"form-group row\" *ngIf=\"skemalist && skemalist.length>0\">\n                                    <label class=\"col-sm-4 col-form-label\">Skema's</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"skema\" data-live-search=\"true\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onchangeSkema($event,item)\" id=\"exampleFormControlSelect1\">\n                                                                                        <ng-template ngFor let-item\n                                                                                                [ngForOf]=\"skemalist\"\n                                                                                                let-i=\"index\" let\n                                                                                                first=first;\n                                                                                                [ngForTrackBy]=\"trackByFn\">\n                                                                                                <option [ngValue]=\"item.uuid\"\n                                                                                                        *ngIf=\"item.brugervendtnoegle && item.uuid\">\n                                                                                                        {{item.brugervendtnoegle}}\n                                                                                                </option>\n                                                                                        </ng-template>\n                                                                                </select>\n                                        <span ngxClipboard (cbOnSuccess)=\"copied($event)\" [cbContent]=\"slctbrugervendt\" (click)=\"Copyorg('brugervendcopyflag')\" [ngClass]=\"brugervendcopyflag === true?' skemapage__copyicon skemapage__copyicon--active':'skemapage__copyicon skemapage__copyicon--inactive'\"><i\n                                                                                                class=\"far fa-copy\"></i></span>\n                                    </div>\n\n                                </div>\n                                <div>\n                                    <div class=\"form-group row\" *ngIf=\"slctdSkema && slctdSkema!=null\">\n                                        <label id=\"jsonskema\" class=\"col-sm-4 col-form-label\">Skema</label>\n                                        <div *ngIf=\"showupload===false\" class=\"col-sm-8\">\n                                            <a class=\"skemapage__download\" (click)=\"DownloadJson($event)\" href=\"#\"> <i class=\"fas fa-cloud-download\"></i>Download Skema</a>\n                                            <a class=\"skemapage__delete\" (click)=\"Deleteconfig($event,item,relationoptionitem,in)\" href=\"#\"> <i class=\"fas fa-trash\"></i>Delete Skema</a>\n                                            <json-editor [options]=\"editorOptions\" (change)=\"getData($event)\" [data]=\"body\">\n                                            </json-editor>\n                                        </div>\n                                        <div class=\"col-sm-8\" *ngIf=\"showupload===true\">\n                                            <input type=\"file\" class=\"custom-file-input form-control\" [(ngModel)]=\"fileuploaded\" #fileupload [ngModelOptions]=\"{standalone: true}\" (change)=\"uploadfile($event)\" id=\"jsonskema\" />\n                                            <i class=\"fas fa-cloud-upload\">Upload a new json skema</i>\n                                        </div>\n                                    </div>\n                                </div>\n\n                            </form>\n\n\n                        </div>\n\n                    </div>\n                </div>\n\n\n                <jqxNotification #successNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"3000\" [template]=\"'success'\">\n                    <div>\n                        Success!! Thank you..\n                    </div>\n                </jqxNotification>\n                <jqxNotification #errorNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"6000\" [template]=\"'error'\">\n                    <div>Error!! pelase try again</div>\n                </jqxNotification>\n                <ng-template #skemadelete let-c=\"close\" let-d=\"dismiss\">\n                    <skemadelete-viewer [slctdSkema]=\"slctdSkema\"></skemadelete-viewer>\n                </ng-template>\n                <ng-template #skemacreate let-c=\"close\" let-d=\"dismiss\">\n                    <skemacreate-viewer></skemacreate-viewer>\n                </ng-template>\n            </div>\n\n        </div>\n    </div>\n    <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/skemacreate/skemacreate.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/skemacreate/skemacreate.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"loading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"loading\"></i>\n</div>\n\n\n<div class=\"skemacreate\">\n    <div class=\"modal-header notifyviewer__filterctr\">\n\n        <button type=\"button\" class=\"btn notifyviewer__filterclose\" (click)=\"cancel($event)\">\n                        <span aria-hidden=\"true\">&times;</span>\n                </button>\n    </div>\n    <div class=\"form-group\">\n\n        <form>\n            <div class=\"form-group row\">\n\n                <h6 class=\"skemacreate__heading\">Opret Skema</h6>\n                <div *ngIf=\"showupload===false\" class=\"col-sm-12\">\n                    <a class=\"skemapage__delete\" (click)=\"uploadNew($event)\" href=\"#\"> <i class=\"fas fa-trash\"></i>Upload a new</a>\n                    <json-editor [options]=\"editorOptions\" (change)=\"getDataa($event)\" [data]=\"bodydata\">\n                    </json-editor>\n                </div>\n                <div class=\"col-sm-12\" *ngIf=\"showupload===true\">\n                    <input type=\"file\" class=\"custom-file-input form-control\" [(ngModel)]=\"fileuploaded\" #fileupload [ngModelOptions]=\"{standalone: true}\" (change)=\"uploadfilee($event)\" id=\"jsonskemaa\" />\n                    <i class=\"fas fa-cloud-upload\">Upload a new json skema</i>\n                </div>\n            </div>\n        </form>\n    </div>\n\n\n\n    <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-outline-dark skemacreate__cancel\" (click)=\"cancel($event)\">\n                        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n        <button [disabled]=\"!changedjsondata\" type=\"submit\" class=\"btn btn-outline-dark skemacreate__save\" (click)=\"skemacreate($event)\">\n                        <i class=\"fa fa-save\"></i>Gem</button>\n    </div>\n</div>\n\n<jqxNotification #successNotification [width]=\"400\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>Success!! Thank you..</div>\n</jqxNotification>\n<jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/skemadelete/skemadelete.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/skemadelete/skemadelete.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"loading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"loading\"></i>\n</div>\n\n\n<div class=\"skemadelete\">\n    <div class=\"modal-header notifyviewer__filterctr\">\n\n        <button type=\"button\" class=\"btn notifyviewer__filterclose\" (click)=\"cancel($event)\">\n                        <span aria-hidden=\"true\">&times;</span>\n                </button>\n    </div>\n    <div class=\"form-group\">\n        <form>\n            <div class=\"form-group row\">\n                <h6 class=\"skemadelete__heading\">Er du sikker? Du vil slette {{slctdSkema.brugervendtnoegle}}.</h6>\n            </div>\n        </form>\n    </div>\n\n    <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-outline-dark skemadelete__cancel\" (click)=\"cancel($event)\">\n                        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n        <button type=\"submit\" class=\"btn btn-outline-dark skemadelete__save\" (click)=\"skemadelete($event)\">\n                        <i class=\"fa fa-save\"></i>Slet</button>\n    </div>\n</div>\n\n<jqxNotification #successNotification [width]=\"400\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>\n        Success!! Thank you..\n    </div>\n</jqxNotification>\n<jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n</jqxNotification>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/templates/templates.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/templates/templates.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\n    <div class=\"templatepage\">\n        <div class=\"templatepage__header-container\">\n            <div class=\"templatepage__header\">\n                <breadcrum-viewer [breadcrumelements]=\"navigationElements\"></breadcrum-viewer>\n                <div class=\"detailspage__operations\">\n                    <div>\n                        <button (click)=\"Cancel($event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\" title=\"Cancel save\">\n                                                        <i class=\"fa fa-times-circle\"></i>Annuller</button>\n                        <button type=\"submit\" (click)=\"saveSkabelon($event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\">\n                                                        <i class=\"fa fa-save\"></i>Gem\n                                                </button>\n                    </div>\n                </div>\n            </div>\n            <div class=\"templatepage__form-container\">\n                <div class=\"templatepage__nav-container\">\n                    <ul class=\"nav nav-pills nav-justified\" id=\"pills-tab\" role=\"tablist\">\n                        <li class=\"nav-item\">\n                            <a data-toggle=\"pill\" href=\"#pills-i\" id=\"pills-i-tab\" role=\"tab\" class=\"nav-link active\" aria-controls=\"pills-i\" aria-selected=\"true\">Rediger\n                                                                Skabelon</a>\n                        </li>\n                    </ul>\n                </div>\n\n                <div class=\"tab-content templatepage__container\" id=\"pills-tabContent\">\n                    <div class=\"tab-pane fade show active\" id=\"pills-i\" role=\"tabpanel\" aria-labelledby=\"pills-i-tab\">\n                        <div class=\"form-group\">\n                            <form *ngIf=\"sorteduserrelations && sorteduserrelations.length>0\">\n                                <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Organisation</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctorg\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChange($event,item)\" id=\"slctorg\">\n                                                                                        <ng-template ngFor let-item\n                                                                                                [ngForOf]=\"sorteduserrelations\"\n                                                                                                let-i=\"index\" let\n                                                                                                first=first;\n                                                                                                [ngForTrackBy]=\"trackByFn\">\n                                                                                                <option [ngValue]=\"item.organisation_uuid\"\n                                                                                                        *ngIf=\"item.org && item.organisation_uuid\">\n                                                                                                        {{item.org}}\n                                                                                                </option>\n                                                                                        </ng-template>\n                                                                                </select>\n                                        <span ngxClipboard (cbOnSuccess)=\"copied($event)\" [cbContent]=\"slcOrgname\" (click)=\"Copyorg('orgcopyflag')\" [ngClass]=\"orgcopyflag === true?' templatepage__copyicon templatepage__copyicon--active':'templatepage__copyicon templatepage__copyicon--inactive'\"><i\n                                                                                                class=\"far fa-copy\"></i></span>\n                                    </div>\n\n                                </div>\n                                <div class=\"form-group row\" *ngIf=\"skemaList && skemaList.length>0\">\n                                    <label class=\"col-sm-4 col-form-label\">Brugervendtnøgle</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"skebelon\" data-live-search=\"true\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onchangeSkabelon($event,item)\">\n                                                                                        <ng-template ngFor let-item\n                                                                                                [ngForOf]=\"skemaList\"\n                                                                                                let-i=\"index\" let\n                                                                                                first=first;\n                                                                                                [ngForTrackBy]=\"trackByFn\">\n                                                                                                <option [ngValue]=\"item.uuid\"\n                                                                                                        *ngIf=\"item.navn && item.uuid\">\n                                                                                                        {{item.brugervendtnoegle}}\n                                                                                                </option>\n                                                                                        </ng-template>\n                                                                                </select>\n                                        <span ngxClipboard (cbOnSuccess)=\"copied($event)\" [cbContent]=\"slctbrugervendt\" (click)=\"Copyorg('brugervendcopyflag')\" [ngClass]=\"brugervendcopyflag === true?' templatepage__copyicon templatepage__copyicon--active':'templatepage__copyicon templatepage__copyicon--inactive'\"><i\n                                                                                                class=\"far fa-copy\"></i></span>\n                                    </div>\n\n                                </div>\n                                <div class=\"form-group row\" *ngIf=\"skemaList && skemaList.length>0\">\n                                    <label class=\"col-sm-4 col-form-label\">Sprog</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctlang\" data-live-search=\"true\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChangelang($event)\">\n                                                                                        <option value=\"da\">\n                                                                                                Dansk\n                                                                                        </option>\n                                                                                        <option value=\"en\">\n                                                                                                Engelsk\n                                                                                        </option>\n                                                                                </select>\n                                    </div>\n\n                                </div>\n                                <div *ngIf=\"skabeloncontent\">\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-sm-4 col-form-label\">Title</label>\n                                        <div class=\"col-sm-8\">\n                                            <input class=\"form-control\" [(ngModel)]=\"title\" [ngModelOptions]=\"{standalone: true}\" value=\"{{skabeloncontent.title}}\" id=\"title\" />\n                                            <div *ngIf=\"((!title || title.length===0) && issubmitted===true)\">\n                                                <p class=\"text-danger\">\n                                                    Udfyld venligst dette felt.\n                                                </p>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-sm-4 col-form-label\">Teaser</label>\n                                        <div class=\"col-sm-8\">\n                                            <input class=\"form-control\" [(ngModel)]=\"teaser\" [ngModelOptions]=\"{standalone: true}\" value=\"{{skabeloncontent.teaser}}\" id=\"teaser\" />\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-sm-4 col-form-label\">Beskrivelse</label>\n                                        <div class=\"col-sm-8\">\n                                            <input class=\"form-control\" [(ngModel)]=\"beskrivelse\" [ngModelOptions]=\"{standalone: true}\" value=\"{{skabeloncontent.beskrivelse}}\" id=\"beskrivelse\" />\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-sm-4 col-form-label\">Ikon</label>\n                                        <div class=\"col-sm-8\">\n                                            <input class=\"form-control\" [(ngModel)]=\"ikon\" [ngModelOptions]=\"{standalone: true}\" value=\"{{skabeloncontent.ikon}}\" id=\"ikon\" />\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\">\n                                        <label class=\"col-sm-4 col-form-label\">Flettefelter</label>\n                                        <div class=\"col-sm-8\">\n                                            <input [(ngModel)]=\"flettefelter\" readonly=\"true\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control\" value=\"{{skabeloncontent.flettefelter}}\" id=\"exampleFormControlSelect1\" />\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" *ngIf=\"jsoneditor === false\">\n                                        <label class=\"col-sm-4 col-form-label\">Tekst</label>\n                                        <div class=\"col-sm-8\">\n                                            <textarea [(ngModel)]=\"body\" #copytargettext [ngModelOptions]=\"{standalone: true}\" class=\"form-control\" value=\"{{skabeloncontent.body}}\" id=\"exampleFormControlTextarea1\" rows=\"15\"></textarea>\n                                            <span ngxClipboard (cbOnSuccess)=\"copied($event)\" [ngxClipboard]=\"copytargettext\" [cbContent]=\"slctbrugervendt\" (click)=\"Copyorg('textcopyflag')\" [ngClass]=\"textcopyflag === true?' templatepage__copyicon templatepage__copyicon--active':'templatepage__copyicon templatepage__copyicon--inactive'\"><i\n                                                                                                        class=\"far fa-copy\"></i></span>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group row\" *ngIf=\"jsoneditor === true\">\n                                        <label class=\"col-sm-4 col-form-label\">Configuration</label>\n                                        <div class=\"col-sm-8\">\n                                            <json-editor [options]=\"editorOptions\" (change)=\"getData($event)\" [data]=\"body\">\n                                            </json-editor>\n                                        </div>\n                                    </div>\n\n                                </div>\n\n                            </form>\n\n\n                        </div>\n\n                    </div>\n                </div>\n\n\n                <jqxNotification #successNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"3000\" [template]=\"'success'\">\n                    <div>\n                        Success!! Thank you..\n                    </div>\n                </jqxNotification>\n                <jqxNotification #errorNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"6000\" [template]=\"'error'\">\n                    <div>Error!! pelase try again</div>\n                </jqxNotification>\n\n            </div>\n\n        </div>\n    </div>\n    <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/translation/translation.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/translation/translation.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"translation\">\n        <div class=\"translationpage\">\n                <div class=\"translationpage__header-container\">\n                        <div class=\"translationpage__header\">\n                                <breadcrum-viewer [breadcrumelements]=\"navigationElements\"></breadcrum-viewer>\n                                <div class=\"translationpage__operations\">\n                                        <div>\n                                                <a title=\"Importer\" (click)=\"export(modal,$event)\"\n                                                        class=\"button itsystems__operations-icon\" href=\"#\">\n                                                        <i class=\"fas fa-cloud-upload\"></i>Eksporter</a>\n                                                <a title=\"Importer\" (click)=\"open(modal,$event)\"\n                                                        class=\"button itsystems__operations-icon\" href=\"#\">\n                                                        <i class=\"fas fa-cloud-download\"></i>Importer</a>\n                                        </div>\n                                </div>\n                                <ng-template #modal let-c=\"close\" let-d=\"dismiss\">\n                                        <div class=\"modal-header\">\n                                                <h4 class=\"modal-title\">Upload file</h4>\n                                                <button type=\"button\" class=\"close\" aria-label=\"Close\"\n                                                        (click)=\"d('Cross click')\">\n                                                        <span aria-hidden=\"true\">&times;</span>\n                                                </button>\n                                        </div>\n                                        <div class=\"modal-body itsystems--modal-body\" (drop)=\"onDrop($event)\">\n                                                <div class=\"itsystems__modal-container\">\n                                                        <form #uploadfileForm (ngSubmit)=\"onfileSubmit($event)\">\n                                                                <label class=\"cursor-pointer itsystems__upload\">\n                                                                        <input type=\"file\" ngf-select\n                                                                                ng-model=\"new_files\" id=\"inputFile\"\n                                                                                #inputFile class=\"\"\n                                                                                (change)=\"onfileChange($event)\">\n                                                                </label>\n                                                                <div class=\"itsystems__dropContainer\">\n                                                                        <span\n                                                                                class=\"itsystems__dropContainer itsystems__dropContainer--droptext\"></span>\n                                                                        <i class=\"fa fa-download\"></i>\n\n                                                                </div>\n                                                        </form>\n                                                        <div class=\"uploadcontainer\">\n                                                                <div [ngClass]=\"(uploadStatus==true) ? '':'d-none'\">\n                                                                        <div class=\"alert alert-success\">\n                                                                                <strong>Success!</strong>\n                                                                                All done\n                                                                                file uploaded\n                                                                                successfully.\n                                                                        </div>\n                                                                </div>\n                                                                <div [ngClass]=\"(fileStatus==true) ? '':'d-none'\">\n                                                                        <div class=\"alert alert-danger \">\n                                                                                <strong>Error!</strong>\n                                                                                Only csv,xlsx,json\n                                                                                filetypes are allowed.\n                                                                                Maximum file size 10MB\n                                                                                allowed.\n                                                                        </div>\n                                                                </div>\n                                                                <div [ngClass]=\"(uploadContainer==true) ? '':'d-none'\">\n                                                                </div>\n                                                        </div>\n\n                                                </div>\n                                        </div>\n                                        <div class=\"modal-footer\">\n                                                <button type=\"submit\" class=\"btn btn-outline-dark\"\n                                                        (click)=\"onfileSubmit($event)\">\n                                                        <i class=\"fa fa-save\"></i> Gem</button>\n                                                <button type=\"submit\"\n                                                        class=\"itsystems__operations-icon itsystems__buttons-icon\">\n                                                        <i class=\"fa fa-save\"></i>Gem </button>\n                                                <button type=\"button\" class=\"btn btn-outline-dark\"\n                                                        (click)=\"d('Cross click')\">\n                                                        <i class=\"fa fa-times-circle\"></i> Annuller</button>\n                                        </div>\n                                </ng-template>\n\n                        </div>\n                </div>\n                <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n                </div>\n                <div class=\"translationpage__form-container\">\n\n                        <div class=\"translationpage__nav-container\">\n                                <ul class=\"nav nav-pills nav-justified\" id=\"pills-tab\" role=\"tablist\">\n                                        <li class=\"nav-item\">\n                                                <a data-toggle=\"pill\" href=\"#pills-i\" id=\"pills-i-tab\" role=\"tab\"\n                                                        class=\"nav-link active\" aria-controls=\"pills-i\"\n                                                        aria-selected=\"true\">Oversættelse</a>\n                                        </li>\n                                </ul>\n                        </div>\n                        <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n                        </div>\n                        <div class=\"tab-content translationpage__container\" id=\"pills-tabContent\">\n                                <div class=\"tab-pane fade show active\" id=\"pills-i\" role=\"tabpanel\"\n                                        aria-labelledby=\"pills-i-tab\">\n                                        <div class=\"form-group\">\n                                                <form *ngIf=\"userrelations && userrelations.length>0\">\n                                                        <div class=\"form-group row\">\n                                                                <label class=\"col-sm-4 col-form-label\">Vælge\n                                                                        Organisation</label>\n                                                                <div class=\"col-sm-8\">\n                                                                        <select [(ngModel)]=\"slctorg\"\n                                                                                data-live-search=\"true\"\n                                                                                [ngModelOptions]=\"{standalone: true}\"\n                                                                                class=\"form-control selectpicker show-tick\"\n                                                                                (change)=\"onChange($event,item)\"\n                                                                                id=\"exampleFormControlSelect1\">\n                                                                                <ng-template ngFor let-item\n                                                                                        [ngForOf]=\"userrelations\"\n                                                                                        let-i=\"index\" let first=first;\n                                                                                        [ngForTrackBy]=\"trackByFn\">\n                                                                                        <option [ngValue]=\"item.organisation_uuid\"\n                                                                                                data-tokens=\"item.org\"\n                                                                                                *ngIf=\"item.org && item.organisation_uuid\">\n                                                                                                {{item.org}}\n                                                                                        </option>\n                                                                                </ng-template>\n                                                                        </select>\n                                                                </div>\n                                                        </div>\n                                                        <div *ngIf=\"slctorg\" class=\"form-group row\">\n                                                                <label class=\"col-sm-4 col-form-label\">Objekt\n                                                                        type</label>\n                                                                <div class=\"col-sm-8\">\n                                                                        <select [(ngModel)]=\"Objekttype\"\n                                                                                data-live-search=\"true\"\n                                                                                [ngModelOptions]=\"{standalone: true}\"\n                                                                                class=\"form-control selectpicker show-tick\"\n                                                                                id=\"exampleFormControlSelect1\">\n                                                                                <option value=\"applikationsfunktion\">\n                                                                                        Applikations funktion</option>\n                                                                                <option value=\"skema\">Skema</option>\n                                                                                <option value=\"survey\">Survey</option>\n                                                                        </select>\n                                                                </div>\n                                                        </div>\n                                                </form>\n                                        </div>\n                                </div>\n                        </div>\n                        <jqxNotification #successNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\"\n                                [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"3000\"\n                                [template]=\"'success'\">\n                                <div>\n                                        Success!! Thank you..\n                                </div>\n                        </jqxNotification>\n                        <jqxNotification #errorNotification [width]=\"1000\" [position]=\"'bottom-right'\" [opacity]=\"1\"\n                                [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"100\"\n                                [autoCloseDelay]=\"40000000\" [template]=\"'error'\">\n                                <div>{{serviceErrorMessage}} </div>\n                        </jqxNotification>\n                        <jqxNotification #infoNotification [width]=\"300\" [position]=\"'bottom-right'\" [opacity]=\"0.9\"\n                                [autoOpen]=\"false\" [autoClose]=\"true\" [animationOpenDelay]=\"800\" [autoCloseDelay]=\"3000\"\n                                [template]=\"'info'\">\n                                <div>\n                                        No records found!!\n                                </div>\n                        </jqxNotification>\n\n                </div>\n        </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/uploads/uploads.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/uploads/uploads.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\n    <div class=\"uploadspage\">\n        <div class=\"uploadspage__header-container\">\n            <div class=\"uploadspage__header\">\n                <breadcrum-viewer [breadcrumelements]=\"navigationElements\"></breadcrum-viewer>\n                <div class=\"detailspage__operations\">\n                    <div>\n                        <button (click)=\"Cancel($event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\" title=\"Cancel save\">\n                            <i class=\"fa fa-times-circle\"></i>Annuller</button>\n                        <button type=\"submit\" (click)=\"savefiles($event)\" class=\"btn detailspage__operations-icon detailspage__buttons-icon\">\n                            <i class=\"fa fa-save\"></i>Gem\n                        </button>\n                    </div>\n                </div>\n            </div>\n            <div class=\"uploadspage__form-container\">\n                <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n                </div>\n                <div class=\"uploadspage__nav-container\">\n                    <ul class=\"nav nav-pills nav-justified\" id=\"pills-tab\" role=\"tablist\">\n                        <li class=\"nav-item\">\n                            <a data-toggle=\"pill\" href=\"#pills-i\" id=\"pills-i-tab\" role=\"tab\" class=\"nav-link active\" aria-controls=\"pills-i\" aria-selected=\"true\">Upload pictures and pdf files\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n\n                <div class=\"tab-content uploadspage__container\" id=\"pills-tabContent\">\n                    <div class=\"tab-pane fade show active\" id=\"pills-i\" role=\"tabpanel\" aria-labelledby=\"pills-i-tab\">\n                        <div class=\"form-group\">\n                            <form>\n                                <div *ngIf=\"paths\" class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\"> Select path</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctpath\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onChange($event,item)\" id=\"slctpath\">\n                                            <ng-template ngFor let-item [ngForOf]=\"paths\" let-i=\"index\" let first=first;\n                                                [ngForTrackBy]=\"trackByFn\">\n                                                <option [ngValue]=\"item.uuid\" *ngIf=\"item.uuid\">\n                                                    {{item.path}}\n                                                </option>\n                                            </ng-template>\n                                        </select>\n                                    </div>\n                                </div>\n\n                                <div class=\"form-group row\" *ngIf=\"slctpath && slctpath.indexOf('gjensidige')!=-1\">\n                                    <label class=\"col-sm-4 col-form-label\"> Select Organisation</label>\n                                    <div class=\"col-sm-8\">\n                                        <select [(ngModel)]=\"slctOrg\" data-live-search=\"true\" #selectorg [ngModelOptions]=\"{standalone: true}\" class=\"form-control selectpicker show-tick\" (change)=\"onchangeOrg($event,item)\" id=\"slctpath\">\n                                            <ng-template ngFor let-item [ngForOf]=\"sorteduserrelations\" let-i=\"index\"\n                                                let first=first; [ngForTrackBy]=\"trackByFn\">\n                                                <option [ngValue]=\"item.organisation_uuid\"\n                                                    *ngIf=\"item.org && item.organisation_uuid\">\n                                                    {{item.org}}\n                                                </option>\n                                            </ng-template>\n                                        </select>\n                                    </div>\n                                </div>\n                                <div *ngIf=\"slctpath && slctpath!=null\" class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Select files</label>\n                                    <div class=\"col-sm-8\">\n                                        <angular-file-uploader #fileUpload [config]=\"picconfig\">\n                                        </angular-file-uploader>\n                                        <!-- <button (click)=\"DocUpload($event)\" class=\"btn btn-success uploadspage__save\" type=\"button\">Save\n                        picture</button> -->\n                                    </div>\n                                </div>\n                                <!-- <div class=\"form-group row\">\n                                    <label class=\"col-sm-4 col-form-label\">Overwrite files if existing</label>\n                                    <div class=\"col-sm-8\">\n                                        <input class=\"form-control\" [checked]=\"overwrite===true ? true:false\" [(ngModel)]=\"overwrite\" [ngModelOptions]=\"{standalone: true}\" type=\"checkbox\" (change)=\"Changeoverwrite($event)\">\n\n                                    </div>\n                                </div> -->\n\n\n                                <div class=\"uploadspage__customcheckbox\">\n                                    <div class=\"custom-checkbox\">\n                                        <input [checked]=\"overwrite===true ? true:false\" [(ngModel)]=\"overwrite\" [ngModelOptions]=\"{standalone: true}\" type=\"checkbox\" class=\"custom-control-input\" id=\"customCheck\" name=\"example1\">\n                                        <label class=\"custom-control-label\" for=\"customCheck\">Overwrite files if existing</label>\n                                    </div>\n                                </div>\n\n                            </form>\n\n\n                        </div>\n\n                    </div>\n                </div>\n\n\n\n\n            </div>\n\n        </div>\n    </div>\n\n    <ng-template #uploadhandler let-c=\"close\" let-d=\"dismiss\">\n        <div class=\"modal-header uploadspage__filterctr\">\n            <button type=\"button\" class=\"btn uploadspage__filterclose\" (click)=\"cancel($event)\">\n                <span aria-hidden=\"true\">&times;</span>\n            </button>\n        </div>\n        <div class=\"modal-body itsystems--modal-body\">\n\n            <div class=\"form-group\">\n                <form>\n                    <div class=\"uploadspage__overwritectr\">\n                        <h6 class=\"uploadspage__heading\">Files already exists</h6>\n                        <ng-template ngFor let-file [ngForOf]=\"failedfiles\" let-in=\"index\" [ngForTrackBy]=\"trackByFn\">\n                            <!-- <div class=\"form-group row uploadspage__customcheck\">\n                                <label class=\"col-sm-10 col-form-label\">{{file.file}}</label>\n                                <input [checked]=\"file.exists===true ?false:true\" [(ngModel)]=\"file.exists\" [ngModelOptions]=\"{standalone: true}\" type=\"checkbox\" (change)=\"Changeoverwriteall($event)\" class=\"col-sm-2 form-check-input uploadspage__customcheck \">\n\n                            </div> -->\n\n                            <div class=\"uploadspage__customcheckbox\">\n                                <div class=\"custom-checkbox\">\n                                    <input [checked]=\"!file.exists\" type=\"checkbox\" type=\"checkbox\" class=\"custom-control-input\" id=\"{{in}}\" (change)=\"Changeoverwritefile($event,file)\" name=\"{{in}}\">\n                                    <label class=\"custom-control-label custom-control-label--modal\" for=\"{{in}}\">{{file.file}}</label>\n                                </div>\n                            </div>\n\n\n                        </ng-template>\n                        <div class=\"uploadspage__customcheckbox\">\n                            <div class=\"custom-checkbox\">\n                                <input [checked]=\"alloverwrite\" [(ngModel)]=\"alloverwrite\" [ngModelOptions]=\"{standalone: true}\" (change)=\"Changeoverwriteall($event)\" type=\"checkbox\" class=\"custom-control-input\" name=\"customCheckmodal\" id=\"customCheckmodal\">\n                                <label class=\"custom-control-label custom-control-label--modal\" for=\"customCheckmodal\">Overwrite all above files</label>\n                            </div>\n                        </div>\n                        <!-- <div class=\"form-group row\">\n                            <label class=\"col-sm-10 col-form-label\">Overwrite all above files</label>\n                            <input class=\"col-sm-2 form-check-input\" [checked]=\"alloverwrite===true ? false:true\" [(ngModel)]=\"alloverwrite\" [ngModelOptions]=\"{standalone: true}\" type=\"checkbox\" (change)=\"Changeoverwriteall($event)\">\n                            <span class=\"uploadspage__checkmark\"></span>\n                        </div> -->\n                    </div>\n\n                </form>\n            </div>\n\n        </div>\n        <div class=\"modal-footer\">\n            <button type=\"submit\" class=\"btn btn-outline-dark notifyviewer__cancel\" (click)=\"cancel($event)\">\n                <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n            <button type=\"submit\" class=\"btn btn-outline-dark notifyviewer__save\" (click)=\"Saveagain($event)\">\n                <i class=\"fa fa-save\"></i>Overwrite</button>\n        </div>\n\n    </ng-template>\n\n\n</div>\n\n<jqxNotification #successNotification [width]=\"400\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"0.9\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'success'\">\n    <div>\n        Success!! Thank you..\n    </div>\n</jqxNotification>\n<jqxNotification #errorNotification [width]=\"1000\" [height]=\"120\" [position]=\"'bottom-right'\" [opacity]=\"1\" [autoClose]=\"true\" [animationOpenDelay]=\"100\" [autoCloseDelay]=\"4000\" [template]=\"'error'\">\n    <div class=\"errormessage\">{{serviceErrorMessage}} </div>\n</jqxNotification>\n\n<!-- <notifications-viewer [error]=\"error\" [success]=\"success\" [message]=\"message\"></notifications-viewer> -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/username/username.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/username/username.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"username\">\n    <div [ngClass]=\"(loading==true) ? 'spinner':'spinner d-none'\">\n    </div>\n\n    <p text-center class=\"login__heading\">Indtast dit brugernavn nedenfor. Du modtager en engangskode, som du skal bruge for at ændre dit kodeord.</p>\n    <form>\n        <div class=\"form-group\">\n            <label for=\"exampleInputEmail1\">Bruger Navn</label>\n            <input type=\"email\" [(ngModel)]=\"username\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control login__textbox\" placeholder=\"Bruger Navn\">\n        </div>\n        <div *ngIf=\"noauthorisation\">\n            <div class=\"login__error\">autorisation bruger findes ikke i systemet</div>\n        </div>\n        <div *ngIf=\"error\">\n            <div class=\"login__error\">{{errormessage}}</div>\n        </div>\n        <button [disabled]=\"!username\" type=\"submit\" (click)=\"otp()\" class=\"btn login__login btn-lg\">Fortsæt</button>\n    </form>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/warning/warning.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/warning/warning.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"loading\" class=\"spinner\">\n    <i class=\"fa fa-spinner fa-spin\" *ngIf=\"loading\"></i>\n</div>\n\n\n<div class=\"warning\">\n    <div class=\"modal-header notifyviewer__filterctr\">\n\n        <button type=\"button\" class=\"btn notifyviewer__filterclose\" (click)=\"cancel($event)\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n\n    <div class=\"form-group\">\n        <form>\n            <h6 class=\"sendemail__heading\"><i class=\"fas fa-exclamation warning__icon\"></i> Er du sikker ? på, at du vil\n                erstatte indholdet Klik på Udfør for at bekræfte, eller på Annuller for at afbryde.</h6>\n        </form>\n    </div>\n\n    <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-outline-dark sendemail__cancel\" (click)=\"cancel($event)\">\n            <i class=\"fa fa-times\" aria-hidden=\"true\"></i>Annuller</button>\n        <button type=\"submit\" class=\"btn btn-outline-dark sendemail__save\" (click)=\"apply($event)\">\n            <i class=\"fa fa-save\"></i>Udfør</button>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/components/widgets/widgets.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/widgets/widgets.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"widgets\">\n  <div class=\"calender\"><i class=\"fa fa-calendar\"></i>\n    <!--      <ng-datepicker [(ngModel)]=\"date\"></ng-datepicker> -->\n    <jqxCalendar #widgetCalendar [width]=\"250\" [height]=\"220\" [theme]=\"'ui-start'\" [enableTooltips]=\"true\">\n    </jqxCalendar>\n  </div>\n\n\n  <div class=\"events\">\n\n    <div class=\"events__event\">\n      <h5> Monday, February 5 2018</h5>\n      <span class=\"events__time\">13:30</span>\n      <div class=\"events__agenda\">\n        <p class=\"events__message\">Hi Doctor you have an appointment today with patient from Herlev.</p>\n      </div>\n    </div>\n\n    <div class=\"events__event\">\n      <h5> Monday, February 6 2018</h5>\n      <span class=\"events__time\">2:00 pm</span>\n      <div class=\"events__agenda\">\n        <p class=\"alert alert-success\">Hi Doctor you have a patient visitng from Alborg.</p>\n      </div>\n    </div>\n\n    <div class=\"events__event\">\n      <h5> Today</h5>\n      <span class=\"events__time\">11:00 am</span>\n      <div class=\"events__agenda\">\n        <p class=\"events__message\">Hi Doctor you have unit meeting today with all doctors</p>\n      </div>\n    </div>\n\n    <div class=\"events__event\">\n      <h5> Yesterday</h5>\n      <span class=\"events__time\">10:00 am</span>\n      <div class=\"events__agenda\">\n        <p class=\"alert alert-warning\">Hi Doctor you have unit meeting today with all doctors</p>\n      </div>\n    </div>\n\n  </div>\n\n\n  <chat-container></chat-container>\n\n  <!--      <div  class=\"chatform\">\n               <message-list ></message-list>\n                <message-form [message]=\"message\" [messages]=\"messages\"></message-form>\n              </div> -->\n</div>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __createBinding, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__createBinding", function() { return __createBinding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __createBinding(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}

function __exportStar(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./package.json":
/*!**********************!*\
  !*** ./package.json ***!
  \**********************/
/*! exports provided: name, version, scripts, private, dependencies, devDependencies, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"name\":\"carex_amin\",\"version\":\"1.0.32\",\"scripts\":{\"ng\":\"ng\",\"start\":\"ng serve\",\"build\":\"ng build\",\"test\":\"ng test\",\"lint\":\"ng lint\",\"e2e\":\"ng e2e\"},\"private\":true,\"dependencies\":{\"@amcharts/amcharts3-angular\":\"^2.2.4\",\"@angular/animations\":\"^8.2.13\",\"@angular/cdk\":\"^8.2.3\",\"@angular/common\":\"~8.2.13\",\"@angular/compiler\":\"~8.2.13\",\"@angular/core\":\"^8.2.13\",\"@angular/forms\":\"~8.2.13\",\"@angular/http\":\"~7.2.15\",\"@angular/material\":\"^6.4.7\",\"@angular/platform-browser\":\"^8.2.13\",\"@angular/platform-browser-dynamic\":\"^8.2.13\",\"@angular/router\":\"~8.2.13\",\"@ckeditor/ckeditor5-build-classic\":\"^23.1.0\",\"@ng-bootstrap/ng-bootstrap\":\"^5.1.2\",\"@types/file-saver\":\"^2.0.1\",\"ang-jsoneditor\":\"1.7.8\",\"angular-file-uploader\":\"^5.0.2\",\"angular-google-charts\":\"^0.1.6\",\"bootstrap\":\"^4.3.1\",\"ckeditor4-angular\":\"^2.0.0\",\"classlist.js\":\"1.1.20150312\",\"core-js\":\"^2.5.4\",\"dateformat\":\"^3.0.3\",\"export-from-json\":\"^1.1.4\",\"file-saver\":\"^2.0.2\",\"font-awesome\":\"^4.7.0\",\"insert-text-at-cursor\":\"^0.3.0\",\"jqwidgets-framework\":\"^10.1.5\",\"jqwidgets-ng\":\"^10.1.5\",\"jqwidgets-scripts\":\"^10.1.5\",\"json-as-xlsx\":\"^1.0.4\",\"jsoneditor\":\"^7.4.0\",\"moment\":\"^2.24.0\",\"moment-timezone\":\"^0.5.31\",\"ng-pick-datetime\":\"^7.0.0\",\"ng-pick-datetime-moment\":\"^1.0.8\",\"ng2-datepicker\":\"^3.1.1\",\"ngx-clipboard\":\"^12.2.1\",\"ngx-dawa-autocomplete\":\"^2.1.1\",\"primeicons\":\"^4.0.0\",\"primeng\":\"^10.0.3\",\"rxjs\":\"^6.5.3\",\"rxjs-compat\":\"^6.5.3\",\"tslib\":\"^1.14.1\",\"web-animations-js\":\"^2.3.2\",\"zone.js\":\"~0.9.1\"},\"devDependencies\":{\"@angular-devkit/build-angular\":\"~0.803.17\",\"@angular/cli\":\"~8.3.17\",\"@angular/compiler-cli\":\"~8.2.13\",\"@angular/language-service\":\"~8.2.13\",\"@types/jasmine\":\"~2.8.8\",\"@types/jasminewd2\":\"~2.0.3\",\"@types/node\":\"^8.10.62\",\"codelyzer\":\"~4.5.0\",\"jasmine-core\":\"~2.99.1\",\"jasmine-spec-reporter\":\"~4.2.1\",\"karma\":\"~4.0.0\",\"karma-chrome-launcher\":\"~2.2.0\",\"karma-coverage-istanbul-reporter\":\"~2.0.1\",\"karma-jasmine\":\"~1.1.2\",\"karma-jasmine-html-reporter\":\"^0.2.2\",\"protractor\":\"~5.4.0\",\"ts-node\":\"~7.0.0\",\"tslint\":\"~5.11.0\",\"typescript\":\"~3.5.3\"}}");

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".row {\n  height: 100%;\n}\n.row--left-container {\n  border-right: 1px solid rgba(0, 0, 0, 0.1);\n  padding-right: 0;\n  background: #f8f9fa;\n  height: 100%;\n}\n.row--right-container {\n  border-left: 1px solid rgba(0, 0, 0, 0.1);\n  padding-left: 0;\n  background: #f8f9fa;\n  height: 100%;\n}\n.row .ace_editor.ace-jsoneditor {\n  min-height: 1000px;\n}\n.row .col-8 {\n  overflow: scroll;\n}\n.col-8 {\n  height: 100%;\n  overflow: scroll;\n}\n/* The customcheck */\n.customcheck {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n/* Hide the browser's default checkbox */\n.customcheck input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n}\n/* Create a custom checkbox */\n.checkmark {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 25px;\n  width: 25px;\n  background-color: #eee;\n  border-radius: 5px;\n}\n/* On mouse-over, add a grey background color */\n.customcheck:hover input ~ .checkmark {\n  background-color: #ccc;\n}\n/* When the checkbox is checked, add a blue background */\n.customcheck input:checked ~ .checkmark {\n  background-color: #02cf32;\n  border-radius: 5px;\n}\n/* Create the checkmark/indicator (hidden when not checked) */\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n/* Show the checkmark when checked */\n.customcheck input:checked ~ .checkmark:after {\n  display: block;\n}\n/* Style the checkmark/indicator */\n.customcheck .checkmark:after {\n  left: 9px;\n  top: 5px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  transform: rotate(45deg);\n}\n.custom-control-label::before,\n.custom-control-label::after {\n  width: 2.25rem !important;\n  height: 2.25rem !important;\n  margin: 1rem;\n}\n.custom-control-label {\n  padding: 1.5rem 2rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9raWF0ZWMvRG9jdW1lbnRzL0tpYXRlYy9jYXJleF9hZG1pbi9jYXJleF9hZG1pbi9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwiL1VzZXJzL2tpYXRlYy9Eb2N1bWVudHMvS2lhdGVjL2NhcmV4X2FkbWluL2NhcmV4X2FkbWluL3NyYy9hc3NldHMvc2Fzcy9fdmFyaWFibGVzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxZQUFBO0FDQUo7QURDSTtFQUNJLDBDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkVvQnFCO0VGbkJyQixZQUFBO0FDQ1I7QURDSTtFQUNJLHlDQUFBO0VBQ0EsZUFBQTtFQUNBLG1CRWNxQjtFRmJyQixZQUFBO0FDQ1I7QURDSTtFQUNJLGtCQUFBO0FDQ1I7QURDSTtFQUNJLGdCQUFBO0FDQ1I7QURHQTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtBQ0FKO0FESUEsb0JBQUE7QUFDQTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0FDREo7QURLQSx3Q0FBQTtBQUNBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBQ0ZKO0FETUEsNkJBQUE7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUNISjtBRE9BLCtDQUFBO0FBRUE7RUFDSSxzQkFBQTtBQ0xKO0FEU0Esd0RBQUE7QUFFQTtFQUNJLHlCQUFBO0VBQ0Esa0JBQUE7QUNQSjtBRFdBLDZEQUFBO0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0FDVEo7QURhQSxvQ0FBQTtBQUVBO0VBQ0ksY0FBQTtBQ1hKO0FEZUEsa0NBQUE7QUFFQTtFQUNJLFNBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBR0Esd0JBQUE7QUNiSjtBRGdCQTs7RUFFSSx5QkFBQTtFQUNBLDBCQUFBO0VBQ0EsWUFBQTtBQ2JKO0FEZ0JBO0VBQ0ksb0JBQUE7QUNiSiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJy4uL2Fzc2V0cy9zYXNzL3ZhcmlhYmxlcyc7XG4ucm93IHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgJi0tbGVmdC1jb250YWluZXIge1xuICAgICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAkc2hhZG93LWNvbG9yO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xuICAgICAgICBiYWNrZ3JvdW5kOiAkY29sb3ItbGlnaHRncmF5LWJhY2tncm91bmQ7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgJi0tcmlnaHQtY29udGFpbmVyIHtcbiAgICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAkc2hhZG93LWNvbG9yO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci1saWdodGdyYXktYmFja2dyb3VuZDtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgICAuYWNlX2VkaXRvci5hY2UtanNvbmVkaXRvciB7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDEwMDBweDtcbiAgICB9XG4gICAgLmNvbC04IHtcbiAgICAgICAgb3ZlcmZsb3c6IHNjcm9sbDtcbiAgICB9XG59XG5cbi5jb2wtOCB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93OiBzY3JvbGw7XG59XG5cblxuLyogVGhlIGN1c3RvbWNoZWNrICovXG4uY3VzdG9tY2hlY2sge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBwYWRkaW5nLWxlZnQ6IDM1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7XG59XG5cblxuLyogSGlkZSB0aGUgYnJvd3NlcidzIGRlZmF1bHQgY2hlY2tib3ggKi9cbi5jdXN0b21jaGVjayBpbnB1dCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG9wYWNpdHk6IDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5cbi8qIENyZWF0ZSBhIGN1c3RvbSBjaGVja2JveCAqL1xuLmNoZWNrbWFyayB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIGhlaWdodDogMjVweDtcbiAgICB3aWR0aDogMjVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuXG4vKiBPbiBtb3VzZS1vdmVyLCBhZGQgYSBncmV5IGJhY2tncm91bmQgY29sb3IgKi9cblxuLmN1c3RvbWNoZWNrOmhvdmVyIGlucHV0fi5jaGVja21hcmsge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNjY2M7XG59XG5cblxuLyogV2hlbiB0aGUgY2hlY2tib3ggaXMgY2hlY2tlZCwgYWRkIGEgYmx1ZSBiYWNrZ3JvdW5kICovXG5cbi5jdXN0b21jaGVjayBpbnB1dDpjaGVja2Vkfi5jaGVja21hcmsge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMmNmMzI7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG5cbi8qIENyZWF0ZSB0aGUgY2hlY2ttYXJrL2luZGljYXRvciAoaGlkZGVuIHdoZW4gbm90IGNoZWNrZWQpICovXG5cbi5jaGVja21hcms6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cblxuLyogU2hvdyB0aGUgY2hlY2ttYXJrIHdoZW4gY2hlY2tlZCAqL1xuXG4uY3VzdG9tY2hlY2sgaW5wdXQ6Y2hlY2tlZH4uY2hlY2ttYXJrOmFmdGVyIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cblxuXG4vKiBTdHlsZSB0aGUgY2hlY2ttYXJrL2luZGljYXRvciAqL1xuXG4uY3VzdG9tY2hlY2sgLmNoZWNrbWFyazphZnRlciB7XG4gICAgbGVmdDogOXB4O1xuICAgIHRvcDogNXB4O1xuICAgIHdpZHRoOiA1cHg7XG4gICAgaGVpZ2h0OiAxMHB4O1xuICAgIGJvcmRlcjogc29saWQgd2hpdGU7XG4gICAgYm9yZGVyLXdpZHRoOiAwIDNweCAzcHggMDtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbn1cblxuLmN1c3RvbS1jb250cm9sLWxhYmVsOjpiZWZvcmUsXG4uY3VzdG9tLWNvbnRyb2wtbGFiZWw6OmFmdGVyIHtcbiAgICB3aWR0aDogMi4yNXJlbSAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMi4yNXJlbSAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMXJlbTtcbn1cblxuLmN1c3RvbS1jb250cm9sLWxhYmVsIHtcbiAgICBwYWRkaW5nOiAxLjVyZW0gMnJlbTtcbn0iLCIucm93IHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLnJvdy0tbGVmdC1jb250YWluZXIge1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6ICNmOGY5ZmE7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5yb3ctLXJpZ2h0LWNvbnRhaW5lciB7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBwYWRkaW5nLWxlZnQ6IDA7XG4gIGJhY2tncm91bmQ6ICNmOGY5ZmE7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5yb3cgLmFjZV9lZGl0b3IuYWNlLWpzb25lZGl0b3Ige1xuICBtaW4taGVpZ2h0OiAxMDAwcHg7XG59XG4ucm93IC5jb2wtOCB7XG4gIG92ZXJmbG93OiBzY3JvbGw7XG59XG5cbi5jb2wtOCB7XG4gIGhlaWdodDogMTAwJTtcbiAgb3ZlcmZsb3c6IHNjcm9sbDtcbn1cblxuLyogVGhlIGN1c3RvbWNoZWNrICovXG4uY3VzdG9tY2hlY2sge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBwYWRkaW5nLWxlZnQ6IDM1cHg7XG4gIG1hcmdpbi1ib3R0b206IDEycHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZm9udC1zaXplOiAyMnB4O1xuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xuICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xuICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIHVzZXItc2VsZWN0OiBub25lO1xufVxuXG4vKiBIaWRlIHRoZSBicm93c2VyJ3MgZGVmYXVsdCBjaGVja2JveCAqL1xuLmN1c3RvbWNoZWNrIGlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi8qIENyZWF0ZSBhIGN1c3RvbSBjaGVja2JveCAqL1xuLmNoZWNrbWFyayB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAyNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi8qIE9uIG1vdXNlLW92ZXIsIGFkZCBhIGdyZXkgYmFja2dyb3VuZCBjb2xvciAqL1xuLmN1c3RvbWNoZWNrOmhvdmVyIGlucHV0IH4gLmNoZWNrbWFyayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjY2M7XG59XG5cbi8qIFdoZW4gdGhlIGNoZWNrYm94IGlzIGNoZWNrZWQsIGFkZCBhIGJsdWUgYmFja2dyb3VuZCAqL1xuLmN1c3RvbWNoZWNrIGlucHV0OmNoZWNrZWQgfiAuY2hlY2ttYXJrIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAyY2YzMjtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4vKiBDcmVhdGUgdGhlIGNoZWNrbWFyay9pbmRpY2F0b3IgKGhpZGRlbiB3aGVuIG5vdCBjaGVja2VkKSAqL1xuLmNoZWNrbWFyazphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLyogU2hvdyB0aGUgY2hlY2ttYXJrIHdoZW4gY2hlY2tlZCAqL1xuLmN1c3RvbWNoZWNrIGlucHV0OmNoZWNrZWQgfiAuY2hlY2ttYXJrOmFmdGVyIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi8qIFN0eWxlIHRoZSBjaGVja21hcmsvaW5kaWNhdG9yICovXG4uY3VzdG9tY2hlY2sgLmNoZWNrbWFyazphZnRlciB7XG4gIGxlZnQ6IDlweDtcbiAgdG9wOiA1cHg7XG4gIHdpZHRoOiA1cHg7XG4gIGhlaWdodDogMTBweDtcbiAgYm9yZGVyOiBzb2xpZCB3aGl0ZTtcbiAgYm9yZGVyLXdpZHRoOiAwIDNweCAzcHggMDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbn1cblxuLmN1c3RvbS1jb250cm9sLWxhYmVsOjpiZWZvcmUsXG4uY3VzdG9tLWNvbnRyb2wtbGFiZWw6OmFmdGVyIHtcbiAgd2lkdGg6IDIuMjVyZW0gIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAyLjI1cmVtICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMXJlbTtcbn1cblxuLmN1c3RvbS1jb250cm9sLWxhYmVsIHtcbiAgcGFkZGluZzogMS41cmVtIDJyZW07XG59IiwiJGZvbnQtc3RhY2s6IEhlbHZldGljYSxcbnNhbnMtc2VyaWY7XG4vLyB0aGVtZVxuLy9DYXJleCBUaGVtZVxuJHByaW1hcnktY29sb3ItdGhlbWU6ICMwZDMzNDg7IC8vICMwMDdiZmY7Ly8gIzIxMzE4Qztcbi8vIFRyeWcgVGhlbWVcbi8vICRwcmltYXJ5LWNvbG9yLXRoZW1lOiAjREMwMDAwO1xuJGNvbG9yLXdoaXRlOiAjRkZGO1xuJGNvbG9yLWhpZ2hsaWdodC1ncmVlbjogIzc2QjgzMjtcbiRwcmltYXJ5LWNvbG9yLXRoZW1lLXVuaGlnaGxpZ2h0OiByZ2JhKDEzLCA1MSwgNzIsIDAuNyk7XG4kY29sb3ItZGFyay1ibGFjazogIzAwMDtcbiRjb2xvci1saWdodC1ibHVlOiAjODdDRUZBO1xuJGNvbG9yLXRyeWctZ3JheTogIzY5Njk2OTtcbiRjb2xvci1zZWNvbmRhcnktZ3JheTojRDNEM0QzO1xuJGNvbG9yLWxpZ2h0Z3JheTogI2U5ZWNlZjtcbiRjb2xvci13YXJuaW5nOiNmZmNjMDA7XG4kY29sb3ItZXJyb3ItcmVkOiAjRkYwMDMzO1xuJGZ1bGwtaGVpZ2h0OiAxMDAlO1xuJGZvbnQtd2VpZ2h0LW5vcm1hbDogMzAwO1xuJGZ1bGwtd2lkdGg6IDEwMCU7XG4kYmFja2dyb3VuZC1ncmF5OiAjZTllY2VmO1xuJHNoYWRvdy1jb2xvcjogcmdiYSgwLCAwLCAwLCAuMSk7XG4kYmFja2dyb3VuZC1kYXJrLWdyYXk6IzM1MzQzNDtcbiRtaW5pbXVtLXByaW9yaXR5OiA1MDtcbiRjaGF0LWdyZWVuLWNvbG9yOiM1QkMyMzY7XG4kZmEtZm9udC1wYXRoOiBcIi4uL2ZvbnRzXCIgIWRlZmF1bHQ7IC8vIHRoaXMgaXMgZm9yIGZvbnQgYXdlc29tZVxuJGNvbG9yLWxpZ2h0Z3JheS1iYWNrZ3JvdW5kOiAjZjhmOWZhOyJdfQ== */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../providers/storageservice/storageservice */ "./src/providers/storageservice/storageservice.ts");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var AppComponent = /** @class */ (function () {
    function AppComponent(location, router, storageService, baserestService, authService) {
        this.location = location;
        this.router = router;
        this.storageService = storageService;
        this.baserestService = baserestService;
        this.authService = authService;
        this.title = 'Carex';
        this.userinfo = null;
        this.userToken = null;
    }
    AppComponent.prototype.ngOnInit = function () {
        var loc = this.location;
        this.hostname = loc._platformStrategy._platformLocation.location.hostname;
    };
    AppComponent.prototype.unloadHandler = function (event) {
        var loc = this.location;
        if (loc._platformLocation.hostname != 'localhost') {
            this.storageService.clear();
        }
    };
    AppComponent.prototype.beforeUnloadHander = function (event) {
        var loc = this.location;
        if (loc._platformLocation.hostname != 'localhost') {
            this.storageService.clear();
        }
    };
    AppComponent.prototype.validateAuthorisation = function () {
        var _this = this;
        this.baserestService.getAllMenuList(this.userinfo.id).then(function (menulist) {
            _this.userToken = menulist.userToken;
            _this.authService.setuserToken(menulist.userToken);
            _this.authService.setmenulist(menulist);
            if (menulist.relationer) {
                _this.authService.setuserrelations(menulist.relationer);
            }
            _this.authService.setuserobj(_this.userinfo);
            _this.router.navigateByUrl('');
            //this.loading=false;
        }, function (error) {
            // this.loading=false;
            // this.errormessage= error.error.besked;    
        });
    };
    AppComponent.ctorParameters = function () { return [
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_4__["StorageService"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_5__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:unload', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], AppComponent.prototype, "unloadHandler", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:beforeunload', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], AppComponent.prototype, "beforeUnloadHander", null);
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_4__["StorageService"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_5__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: DefaultIntl, MY_CUSTOM_FORMATS, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DefaultIntl", function() { return DefaultIntl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MY_CUSTOM_FORMATS", function() { return MY_CUSTOM_FORMATS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ng2_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-datepicker */ "./node_modules/ng2-datepicker/dist/bundles/ng2-datepicker.umd.js");
/* harmony import */ var ng2_datepicker__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ng2_datepicker__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxgrid__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxrangeselector__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxrangeselector */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxrangeselector.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxtabs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxfileupload__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxdropdownlist__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxcalendar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxtimepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxtimepicker */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxtimepicker.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxpanel__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxpanel */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxpanel.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxdatetimeinput__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_dawa_autocomplete__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ngx-dawa-autocomplete */ "./node_modules/ngx-dawa-autocomplete/fesm5/ngx-dawa-autocomplete.js");
/* harmony import */ var _amcharts_amcharts3_angular__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @amcharts/amcharts3-angular */ "./node_modules/@amcharts/amcharts3-angular/es2015/index.js");
/* harmony import */ var angular_google_charts__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! angular-google-charts */ "./node_modules/angular-google-charts/fesm5/angular-google-charts.js");
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var ang_jsoneditor__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ang-jsoneditor */ "./node_modules/ang-jsoneditor/fesm5/ang-jsoneditor.js");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-clipboard */ "./node_modules/ngx-clipboard/fesm5/ngx-clipboard.js");
/* harmony import */ var ng_pick_datetime_date_time_adapter_moment_adapter_moment_date_time_adapter_class__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class */ "./node_modules/ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/fesm2015/primeng-table.js");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! primeng/toast */ "./node_modules/primeng/fesm2015/primeng-toast.js");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! primeng/calendar */ "./node_modules/primeng/fesm2015/primeng-calendar.js");
/* harmony import */ var primeng_slider__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! primeng/slider */ "./node_modules/primeng/fesm2015/primeng-slider.js");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! primeng/multiselect */ "./node_modules/primeng/fesm2015/primeng-multiselect.js");
/* harmony import */ var primeng_contextmenu__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! primeng/contextmenu */ "./node_modules/primeng/fesm2015/primeng-contextmenu.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/fesm2015/primeng-dialog.js");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! primeng/button */ "./node_modules/primeng/fesm2015/primeng-button.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm2015/primeng-dropdown.js");
/* harmony import */ var primeng_progressbar__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! primeng/progressbar */ "./node_modules/primeng/fesm2015/primeng-progressbar.js");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! primeng/inputtext */ "./node_modules/primeng/fesm2015/primeng-inputtext.js");
/* harmony import */ var ckeditor4_angular__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ckeditor4-angular */ "./node_modules/ckeditor4-angular/fesm2015/ckeditor4-angular.js");
/* harmony import */ var _components_header_header_page__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ../components/header/header-page */ "./src/components/header/header-page.ts");
/* harmony import */ var _components_footer_footer_page__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ../components/footer/footer-page */ "./src/components/footer/footer-page.ts");
/* harmony import */ var _components_menu_right_menu_right__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ../components/menu-right/menu-right */ "./src/components/menu-right/menu-right.ts");
/* harmony import */ var _components_menu_left_menu_left__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ../components/menu-left/menu-left */ "./src/components/menu-left/menu-left.ts");
/* harmony import */ var _components_itsystems_itsystems__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ../components/itsystems/itsystems */ "./src/components/itsystems/itsystems.ts");
/* harmony import */ var _components_detailspage_detailspage__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ../components/detailspage/detailspage */ "./src/components/detailspage/detailspage.ts");
/* harmony import */ var _components_createpage_createpage__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ../components/createpage/createpage */ "./src/components/createpage/createpage.ts");
/* harmony import */ var _components_login_login__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ../components/login/login */ "./src/components/login/login.ts");
/* harmony import */ var _components_widgets_widgets__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ../components/widgets/widgets */ "./src/components/widgets/widgets.ts");
/* harmony import */ var _components_message_list_message_list__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ../components/message-list/message-list */ "./src/components/message-list/message-list.ts");
/* harmony import */ var _components_message_item_message_item__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ../components/message-item/message-item */ "./src/components/message-item/message-item.ts");
/* harmony import */ var _components_message_form_message_form__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ../components/message-form/message-form */ "./src/components/message-form/message-form.ts");
/* harmony import */ var _components_chat_container_chat_container__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ../components/chat-container/chat-container */ "./src/components/chat-container/chat-container.ts");
/* harmony import */ var _components_dashboard_dashboard__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ../components/dashboard/dashboard */ "./src/components/dashboard/dashboard.ts");
/* harmony import */ var _components_notifications_notifications__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ../components/notifications/notifications */ "./src/components/notifications/notifications.ts");
/* harmony import */ var _components_modal_modal__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ../components/modal/modal */ "./src/components/modal/modal.ts");
/* harmony import */ var _components_ganttchart_ganttchart__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ../components/ganttchart/ganttchart */ "./src/components/ganttchart/ganttchart.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./app.routing.module */ "./src/app/app.routing.module.ts");
/* harmony import */ var _components_breadcrum_breadcrum__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ../components/breadcrum/breadcrum */ "./src/components/breadcrum/breadcrum.ts");
/* harmony import */ var _components_translation_translation__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ../components/translation/translation */ "./src/components/translation/translation.ts");
/* harmony import */ var _components_templates_templates__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ../components/templates/templates */ "./src/components/templates/templates.ts");
/* harmony import */ var _components_configuration_configuration__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ../components/configuration/configuration */ "./src/components/configuration/configuration.ts");
/* harmony import */ var _components_analytics_analytics__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ../components/analytics/analytics */ "./src/components/analytics/analytics.ts");
/* harmony import */ var _components_uploads_uploads__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ../components/uploads/uploads */ "./src/components/uploads/uploads.ts");
/* harmony import */ var _components_customform_customform__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ../components/customform/customform */ "./src/components/customform/customform.ts");
/* harmony import */ var angular_file_uploader__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! angular-file-uploader */ "./node_modules/angular-file-uploader/fesm5/angular-file-uploader.js");
/* harmony import */ var _components_filter_filter__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ../components/filter/filter */ "./src/components/filter/filter.ts");
/* harmony import */ var _components_jsonviewer_jsonviewer__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ../components/jsonviewer/jsonviewer */ "./src/components/jsonviewer/jsonviewer.ts");
/* harmony import */ var _components_notifyviewer_notifyviewer__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ../components/notifyviewer/notifyviewer */ "./src/components/notifyviewer/notifyviewer.ts");
/* harmony import */ var _components_operations_operations__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ../components/operations/operations */ "./src/components/operations/operations.ts");
/* harmony import */ var _components_sendemail_sendemail__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ../components/sendemail/sendemail */ "./src/components/sendemail/sendemail.ts");
/* harmony import */ var _components_sendmessages_sendmessages__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ../components/sendmessages/sendmessages */ "./src/components/sendmessages/sendmessages.ts");
/* harmony import */ var _components_fletfilters_fletfilters__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ../components/fletfilters/fletfilters */ "./src/components/fletfilters/fletfilters.ts");
/* harmony import */ var _components_newgrid_newgrid__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! ../components/newgrid/newgrid */ "./src/components/newgrid/newgrid.ts");
/* harmony import */ var _components_warning_warning__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ../components/warning/warning */ "./src/components/warning/warning.ts");
/* harmony import */ var _components_username_username__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ../components/username/username */ "./src/components/username/username.ts");
/* harmony import */ var _components_otp_otp__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ../components/otp/otp */ "./src/components/otp/otp.ts");
/* harmony import */ var _components_changepassword_changepassword__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ../components/changepassword/changepassword */ "./src/components/changepassword/changepassword.ts");
/* harmony import */ var _components_skema_skema__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ../components/skema/skema */ "./src/components/skema/skema.ts");
/* harmony import */ var _components_skemadelete_skemadelete__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ../components/skemadelete/skemadelete */ "./src/components/skemadelete/skemadelete.ts");
/* harmony import */ var _components_skemacreate_skemacreate__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ../components/skemacreate/skemacreate */ "./src/components/skemacreate/skemacreate.ts");
/* harmony import */ var _components_search_search__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! ../components/search/search */ "./src/components/search/search.ts");
/* harmony import */ var _components_delete_delete__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ../components/delete/delete */ "./src/components/delete/delete.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_83__ = __webpack_require__(/*! ../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_84__ = __webpack_require__(/*! ../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_85__ = __webpack_require__(/*! ../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _providers_services_dataflow__WEBPACK_IMPORTED_MODULE_86__ = __webpack_require__(/*! ../providers/services/dataflow */ "./src/providers/services/dataflow.ts");
/* harmony import */ var _providers_services_router_service__WEBPACK_IMPORTED_MODULE_87__ = __webpack_require__(/*! ../providers/services/router.service */ "./src/providers/services/router.service.ts");
/* harmony import */ var _providers_window_ref__WEBPACK_IMPORTED_MODULE_88__ = __webpack_require__(/*! ../providers/window.ref */ "./src/providers/window.ref.ts");
/* harmony import */ var _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_89__ = __webpack_require__(/*! ../providers/storageservice/storageservice */ "./src/providers/storageservice/storageservice.ts");
/* harmony import */ var _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_90__ = __webpack_require__(/*! ../providers/utils/helperService */ "./src/providers/utils/helperService.ts");
/* harmony import */ var _providers_validators_password_validator__WEBPACK_IMPORTED_MODULE_91__ = __webpack_require__(/*! ../providers/validators/password-validator */ "./src/providers/validators/password-validator.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_92__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");

//modules


























//GRID













// here is the default text string - just adjust the strings to reflect your preferred language
var DefaultIntl = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](DefaultIntl, _super);
    function DefaultIntl() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /** A label for the cancel button */
        _this.cancelBtnLabel = 'Annuller';
        /** A label for the set button */
        _this.setBtnLabel = 'Sæt';
        return _this;
    }
    return DefaultIntl;
}(ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__["OwlDateTimeIntl"]));

var MY_CUSTOM_FORMATS = {
    fullPickerInput: 'DD-MM-YYYY HH:mm',
    parseInput: 'DD-MM-YYYY HH:mm',
    datePickerInput: 'DD-MM-YYYY HH:mm',
    timePickerInput: 'DD-MM-YYYY',
    monthYearLabel: 'DD-MM-YYYY',
    dateA11yLabel: 'DD-MM-YYYY',
    monthYearA11yLabel: 'DD-MM-YYYY',
};
//pages











































//components

//models
//providers









//directives

var componentsDeclarations = [
    _components_header_header_page__WEBPACK_IMPORTED_MODULE_39__["HeaderPage"],
    _components_footer_footer_page__WEBPACK_IMPORTED_MODULE_40__["FooterPage"],
    _components_menu_left_menu_left__WEBPACK_IMPORTED_MODULE_42__["MenuLeft"],
    _components_menu_right_menu_right__WEBPACK_IMPORTED_MODULE_41__["MenuRight"],
    _components_ganttchart_ganttchart__WEBPACK_IMPORTED_MODULE_55__["GanttChart"],
    _components_itsystems_itsystems__WEBPACK_IMPORTED_MODULE_43__["Itsystems"],
    _components_detailspage_detailspage__WEBPACK_IMPORTED_MODULE_44__["DetailsPage"],
    _components_createpage_createpage__WEBPACK_IMPORTED_MODULE_45__["CreatePage"],
    _components_login_login__WEBPACK_IMPORTED_MODULE_46__["Login"],
    _components_widgets_widgets__WEBPACK_IMPORTED_MODULE_47__["Widgets"],
    _components_message_list_message_list__WEBPACK_IMPORTED_MODULE_48__["MessageList"],
    _components_message_item_message_item__WEBPACK_IMPORTED_MODULE_49__["MessageItem"],
    _components_message_form_message_form__WEBPACK_IMPORTED_MODULE_50__["MessageForm"],
    _components_chat_container_chat_container__WEBPACK_IMPORTED_MODULE_51__["ChatContainer"],
    _components_dashboard_dashboard__WEBPACK_IMPORTED_MODULE_52__["Dashboard"],
    _components_notifications_notifications__WEBPACK_IMPORTED_MODULE_53__["Notifications"],
    _components_modal_modal__WEBPACK_IMPORTED_MODULE_54__["modalComponent"],
    _components_breadcrum_breadcrum__WEBPACK_IMPORTED_MODULE_57__["BreadcrumComponent"],
    _components_customform_customform__WEBPACK_IMPORTED_MODULE_63__["customForm"],
    _components_translation_translation__WEBPACK_IMPORTED_MODULE_58__["Translation"],
    _components_templates_templates__WEBPACK_IMPORTED_MODULE_59__["Templates"],
    _components_configuration_configuration__WEBPACK_IMPORTED_MODULE_60__["Configuration"],
    _components_analytics_analytics__WEBPACK_IMPORTED_MODULE_61__["analyticsComponent"],
    _components_uploads_uploads__WEBPACK_IMPORTED_MODULE_62__["uploadsComponent"],
    _components_filter_filter__WEBPACK_IMPORTED_MODULE_65__["filterComponent"],
    _components_jsonviewer_jsonviewer__WEBPACK_IMPORTED_MODULE_66__["jsonviewerComponent"],
    _components_notifyviewer_notifyviewer__WEBPACK_IMPORTED_MODULE_67__["notifyviewerComponent"],
    _components_operations_operations__WEBPACK_IMPORTED_MODULE_68__["OperationsComponent"],
    _components_sendemail_sendemail__WEBPACK_IMPORTED_MODULE_69__["sendemailComponent"],
    _components_sendmessages_sendmessages__WEBPACK_IMPORTED_MODULE_70__["sendmessagesComponent"],
    _components_fletfilters_fletfilters__WEBPACK_IMPORTED_MODULE_71__["fletfiltersComponent"],
    _components_newgrid_newgrid__WEBPACK_IMPORTED_MODULE_72__["newGridComponent"],
    _components_otp_otp__WEBPACK_IMPORTED_MODULE_75__["OtpComponent"],
    _components_changepassword_changepassword__WEBPACK_IMPORTED_MODULE_76__["changepasswordComponent"],
    _components_username_username__WEBPACK_IMPORTED_MODULE_74__["UsernameComponent"],
    _components_skema_skema__WEBPACK_IMPORTED_MODULE_77__["Skema"],
    _components_skemadelete_skemadelete__WEBPACK_IMPORTED_MODULE_78__["skemadeleteComponent"],
    _components_skemacreate_skemacreate__WEBPACK_IMPORTED_MODULE_79__["skemacreateComponent"],
    _components_search_search__WEBPACK_IMPORTED_MODULE_80__["searchComponent"],
    _components_delete_delete__WEBPACK_IMPORTED_MODULE_81__["DeleteComponent"],
    _components_warning_warning__WEBPACK_IMPORTED_MODULE_73__["warningComponent"]
];
var providersDeclarations = [
    _providers_services_services__WEBPACK_IMPORTED_MODULE_83__["Services"],
    _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_84__["BaseRestService"],
    _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_85__["AuthRestService"],
    _providers_services_dataflow__WEBPACK_IMPORTED_MODULE_86__["DialogflowService"],
    _providers_services_router_service__WEBPACK_IMPORTED_MODULE_87__["RouterExtService"],
    _providers_window_ref__WEBPACK_IMPORTED_MODULE_88__["WindowRefService"],
    _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_89__["StorageService"],
    _angular_common__WEBPACK_IMPORTED_MODULE_18__["DatePipe"],
    _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_90__["HelperSerice"],
    _providers_validators_password_validator__WEBPACK_IMPORTED_MODULE_91__["PasswordValidation"]
];
var pipeDeclarations = [];
var directivesDeclarations = [];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_82__["AppComponent"], jqwidgets_scripts_jqwidgets_ts_angular_jqxgrid__WEBPACK_IMPORTED_MODULE_7__["jqxGridComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxrangeselector__WEBPACK_IMPORTED_MODULE_8__["jqxRangeSelectorComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxtabs__WEBPACK_IMPORTED_MODULE_9__["jqxTabsComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxfileupload__WEBPACK_IMPORTED_MODULE_10__["jqxFileUploadComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxdropdownlist__WEBPACK_IMPORTED_MODULE_11__["jqxDropDownListComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxcalendar__WEBPACK_IMPORTED_MODULE_12__["jqxCalendarComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxpanel__WEBPACK_IMPORTED_MODULE_14__["jqxPanelComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxtimepicker__WEBPACK_IMPORTED_MODULE_13__["jqxTimePickerComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxdatetimeinput__WEBPACK_IMPORTED_MODULE_15__["jqxDateTimeInputComponent"],
                jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_16__["jqxNotificationComponent"],
                // OwlDateTimeModule, 
                // OwlMomentDateTimeModule,
                //globalization,
                //globalize,
                // Models
                // Components
                componentsDeclarations,
                // Directives
                directivesDeclarations,
                // Pipes
                pipeDeclarations
            ],
            entryComponents: [
                _components_itsystems_itsystems__WEBPACK_IMPORTED_MODULE_43__["Itsystems"],
                _components_detailspage_detailspage__WEBPACK_IMPORTED_MODULE_44__["DetailsPage"],
                _components_customform_customform__WEBPACK_IMPORTED_MODULE_63__["customForm"],
                _components_createpage_createpage__WEBPACK_IMPORTED_MODULE_45__["CreatePage"],
                _components_modal_modal__WEBPACK_IMPORTED_MODULE_54__["modalComponent"],
                _components_ganttchart_ganttchart__WEBPACK_IMPORTED_MODULE_55__["GanttChart"],
                _components_breadcrum_breadcrum__WEBPACK_IMPORTED_MODULE_57__["BreadcrumComponent"],
                _components_customform_customform__WEBPACK_IMPORTED_MODULE_63__["customForm"],
                _components_translation_translation__WEBPACK_IMPORTED_MODULE_58__["Translation"],
                _components_templates_templates__WEBPACK_IMPORTED_MODULE_59__["Templates"],
                _components_analytics_analytics__WEBPACK_IMPORTED_MODULE_61__["analyticsComponent"],
                _components_uploads_uploads__WEBPACK_IMPORTED_MODULE_62__["uploadsComponent"],
                _components_fletfilters_fletfilters__WEBPACK_IMPORTED_MODULE_71__["fletfiltersComponent"],
                _components_filter_filter__WEBPACK_IMPORTED_MODULE_65__["filterComponent"],
                _components_skemacreate_skemacreate__WEBPACK_IMPORTED_MODULE_79__["skemacreateComponent"],
                _components_skemadelete_skemadelete__WEBPACK_IMPORTED_MODULE_78__["skemadeleteComponent"],
                _components_delete_delete__WEBPACK_IMPORTED_MODULE_81__["DeleteComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_56__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                ng2_datepicker__WEBPACK_IMPORTED_MODULE_4__["NgDatepickerModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__["OwlDateTimeModule"],
                ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__["OwlNativeDateTimeModule"],
                _amcharts_amcharts3_angular__WEBPACK_IMPORTED_MODULE_20__["AmChartsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                primeng_contextmenu__WEBPACK_IMPORTED_MODULE_32__["ContextMenuModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_33__["DialogModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_33__["DialogModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_34__["ButtonModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_35__["DropdownModule"],
                primeng_progressbar__WEBPACK_IMPORTED_MODULE_36__["ProgressBarModule"],
                primeng_inputtext__WEBPACK_IMPORTED_MODULE_37__["InputTextModule"],
                primeng_calendar__WEBPACK_IMPORTED_MODULE_29__["CalendarModule"],
                primeng_multiselect__WEBPACK_IMPORTED_MODULE_31__["MultiSelectModule"],
                ngx_clipboard__WEBPACK_IMPORTED_MODULE_24__["ClipboardModule"],
                primeng_slider__WEBPACK_IMPORTED_MODULE_30__["SliderModule"],
                angular_file_uploader__WEBPACK_IMPORTED_MODULE_64__["AngularFileUploaderModule"],
                ang_jsoneditor__WEBPACK_IMPORTED_MODULE_23__["NgJsonEditorModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_26__["MatDialogModule"],
                ckeditor4_angular__WEBPACK_IMPORTED_MODULE_38__["CKEditorModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_27__["TableModule"],
                primeng_toast__WEBPACK_IMPORTED_MODULE_28__["ToastModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_35__["DropdownModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_92__["RouterModule"].forRoot([]),
                angular_google_charts__WEBPACK_IMPORTED_MODULE_21__["GoogleChartsModule"].forRoot(),
                ngx_dawa_autocomplete__WEBPACK_IMPORTED_MODULE_19__["DawaAutocompleteModule"].forRoot()
            ],
            providers: [
                providersDeclarations,
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_18__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_18__["HashLocationStrategy"] },
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_18__["APP_BASE_HREF"], useValue: '/' },
                { provide: ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__["DateTimeAdapter"], useClass: ng_pick_datetime_date_time_adapter_moment_adapter_moment_date_time_adapter_class__WEBPACK_IMPORTED_MODULE_25__["MomentDateTimeAdapter"], deps: [ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__["OWL_DATE_TIME_LOCALE"]] },
                { provide: ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__["OWL_DATE_TIME_LOCALE"], useValue: 'da' },
                { provide: ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__["OwlDateTimeIntl"], useClass: DefaultIntl },
                { provide: ng_pick_datetime_date_time_adapter_moment_adapter_moment_date_time_adapter_class__WEBPACK_IMPORTED_MODULE_25__["OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS"], useValue: { useUtc: true } },
                { provide: ng_pick_datetime__WEBPACK_IMPORTED_MODULE_22__["OWL_DATE_TIME_FORMATS"], useValue: MY_CUSTOM_FORMATS }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_82__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app.routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_itsystems_itsystems__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/itsystems/itsystems */ "./src/components/itsystems/itsystems.ts");
/* harmony import */ var _components_detailspage_detailspage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/detailspage/detailspage */ "./src/components/detailspage/detailspage.ts");
/* harmony import */ var _components_translation_translation__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/translation/translation */ "./src/components/translation/translation.ts");
/* harmony import */ var _components_templates_templates__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/templates/templates */ "./src/components/templates/templates.ts");
/* harmony import */ var _components_analytics_analytics__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/analytics/analytics */ "./src/components/analytics/analytics.ts");
/* harmony import */ var _components_uploads_uploads__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/uploads/uploads */ "./src/components/uploads/uploads.ts");
/* harmony import */ var _components_configuration_configuration__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/configuration/configuration */ "./src/components/configuration/configuration.ts");
/* harmony import */ var _components_sendmessages_sendmessages__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/sendmessages/sendmessages */ "./src/components/sendmessages/sendmessages.ts");
/* harmony import */ var _components_newgrid_newgrid__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/newgrid/newgrid */ "./src/components/newgrid/newgrid.ts");
/* harmony import */ var _components_login_login__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/login/login */ "./src/components/login/login.ts");
/* harmony import */ var _components_username_username__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/username/username */ "./src/components/username/username.ts");
/* harmony import */ var _components_otp_otp__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../components/otp/otp */ "./src/components/otp/otp.ts");
/* harmony import */ var _components_changepassword_changepassword__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../components/changepassword/changepassword */ "./src/components/changepassword/changepassword.ts");
/* harmony import */ var _components_skema_skema__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/skema/skema */ "./src/components/skema/skema.ts");

















var routes = [
    // {path:'Organisationer/:listId', component:Itsystems},  future reference
    { path: 'Katalog TRYG', component: _components_itsystems_itsystems__WEBPACK_IMPORTED_MODULE_3__["Itsystems"] },
    { path: 'tilstandliste/DetailsPage', component: _components_detailspage_detailspage__WEBPACK_IMPORTED_MODULE_4__["DetailsPage"] },
    { path: 'translation', component: _components_translation_translation__WEBPACK_IMPORTED_MODULE_5__["Translation"] },
    { path: 'configuration', component: _components_configuration_configuration__WEBPACK_IMPORTED_MODULE_9__["Configuration"] },
    { path: 'getuseranalytics', component: _components_analytics_analytics__WEBPACK_IMPORTED_MODULE_7__["analyticsComponent"] },
    { path: 'templates', component: _components_templates_templates__WEBPACK_IMPORTED_MODULE_6__["Templates"] },
    { path: 'uploads', component: _components_uploads_uploads__WEBPACK_IMPORTED_MODULE_8__["uploadsComponent"] },
    { path: 'sendmessages', component: _components_sendmessages_sendmessages__WEBPACK_IMPORTED_MODULE_10__["sendmessagesComponent"] },
    { path: 'newgrid', component: _components_newgrid_newgrid__WEBPACK_IMPORTED_MODULE_11__["newGridComponent"] },
    { path: 'login', component: _components_login_login__WEBPACK_IMPORTED_MODULE_12__["Login"] },
    { path: 'username', component: _components_username_username__WEBPACK_IMPORTED_MODULE_13__["UsernameComponent"] },
    { path: 'otp', component: _components_otp_otp__WEBPACK_IMPORTED_MODULE_14__["OtpComponent"] },
    { path: 'changepassword', component: _components_changepassword_changepassword__WEBPACK_IMPORTED_MODULE_15__["changepasswordComponent"] },
    { path: 'skemaeditor', component: _components_skema_skema__WEBPACK_IMPORTED_MODULE_16__["Skema"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule.prototype.ngOnInit = function () {
    };
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());

;


/***/ }),

/***/ "./src/components/analytics/analytics.ts":
/*!***********************************************!*\
  !*** ./src/components/analytics/analytics.ts ***!
  \***********************************************/
/*! exports provided: analyticsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "analyticsComponent", function() { return analyticsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../providers/utils/helperService */ "./src/providers/utils/helperService.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);









var xlsx = __webpack_require__(/*! json-as-xlsx */ "./node_modules/json-as-xlsx/index.js");
var analyticsComponent = /** @class */ (function () {
    function analyticsComponent(fb, helperService, auth, ref, baserestService) {
        var _this = this;
        this.fb = fb;
        this.helperService = helperService;
        this.auth = auth;
        this.ref = ref;
        this.baserestService = baserestService;
        this.isLoading = false;
        this.serviceErrorMessage = '';
        this.columnchart = "ColumnChart";
        this.piechart = "PieChart";
        this.donutchart = "donutchart";
        this.barchart = "BarChart";
        this.chartdata = [];
        this.Dashboard = "Dashboard";
        this.activeusers = [];
        this.totalusers = [];
        this.versions = [];
        this.slctapplication = '';
        this.enddate = null;
        this.startdate = null;
        this.isdownloading = false;
        this.allapplications = [
            { applikation_uuid: 'ae9055e5-210e-4a0b-9d39-b334c12cf744', organisation_uuid: "7fb031ae-0cb9-4a04-8407-1156d3d52108", appname: "Tryg", selected: false },
            { applikation_uuid: 'dc252eac-7759-4de1-a371-25da7a76761b', organisation_uuid: "bef7e721-29cb-4eec-b0a9-af70fdc7b05d", appname: "Molholm", selected: false },
            //{ applikation_uuid: 'ae9055e5-210e-4a0b-9d39-b334c12cf744', organisation_uuid: "4f4645fc-7c9a-4de7-9592-145edafdfc4c", appname: "Willis", selected: false },
            { applikation_uuid: '86d383ea-644c-49df-9d8c-0fdde0811a35', organisation_uuid: "8e7fcffe-444b-4468-ab59-9f63b00d15c5", appname: "Sundhed via AP Pension", selected: false },
            { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "59aa2f87-d09e-4fd7-9837-d3342082b35a", appname: "Carex Workplace", selected: false },
            { applikation_uuid: 'e1bdaed0-9f7a-4288-ac95-a6b00c41857a', organisation_uuid: "38507c00-97bf-43f1-9c76-1d2f1159de31", appname: "Sundhedsunivers", selected: false }
            // { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "aa67c9ab-04a1-43fd-9656-e3f3d742ec9c", appname: "Aalborg", selected: false }
            // { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "aa67c9ab-04a1-43fd-9656-e3f3d742ec9c", appname: "Aalborg", selected: false },
        ];
        this.onChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.analyticsForm = this.fb.group({
            application: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            startdate: [''],
            enddate: ['']
        });
        this.auth.userrelations.subscribe(function (userrelations) {
            _this.userrelations = userrelations;
        });
    }
    analyticsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.userrelations);
        if (this.userrelations) {
            var temp = [];
            temp.push(this.userrelations.map(function (item) { return _this.allapplications.filter(function (filteritem) { return (filteritem.organisation_uuid === item.organisation_uuid); })[0]; }).filter(function (notUndefined) { return notUndefined !== undefined; }));
            this.filteredapplications = temp[0];
        }
        //  this.selecteddates = this.helperService.analticsformatDate(null);//[new Date(2018, 1, 12, 10, 30), new Date(2018, 3, 21, 20, 30)];
        console.log(this.selecteddates);
    };
    analyticsComponent.prototype.ngAfterViewInit = function () {
        $('select').selectpicker();
    };
    analyticsComponent.prototype.createChart = function () {
        this.allevents = [];
        this.platforms = [];
        this.versions = [];
        this.versionsData = [];
        this.activeusers = [];
        this.totalusers = [];
        // this.userrelations = [];
        if (this.filteredata && this.filteredata.length > 0) {
            for (var i in this.filteredata) {
                if (this.filteredata[i].request) {
                    this.allevents.push(this.filteredata[i].request.eventname);
                    this.platforms.push(this.filteredata[i].request.platform);
                    this.versions.push(this.filteredata[i].request.version);
                    this.activeusers.push(this.filteredata[i].request.bruger_uuid);
                    // this.userrelations.push(this.filteredata[i].request.applikation_uuid);
                    if (this.totalusers.indexOf(this.filteredata[i].request.userToken) === -1) {
                        this.totalusers.push(this.filteredata[i].request.userToken);
                    }
                }
            }
            var eventscount = {};
            var platformscount = {};
            var versionscount = {};
            var activeuserscount = {};
            // var userrelationsscount = {};
            this.allevents.forEach(function (i) { eventscount[i] = (eventscount[i] || 0) + 1; });
            this.platforms.forEach(function (i) { platformscount[i] = (platformscount[i] || 0) + 1; });
            this.versions.forEach(function (i) { versionscount[i] = (versionscount[i] || 0) + 1; });
            this.activeusers.forEach(function (i) { activeuserscount[i] = (activeuserscount[i] || 0) + 1; });
            // this.userrelations.forEach(function (i) { userrelationsscount[i] = (userrelationsscount[i] || 0) + 1; });
            this.makeeventchart(eventscount);
            this.makeplatformschart(platformscount);
            this.makeversionschart(versionscount);
            this.makeactiveusersschart(activeuserscount);
            // this.makeuserbyrelationschart(userrelationsscount);
            this.totalusersData = this.totalusers;
        }
        else {
            this.platformsData = null;
            this.activeusersData = null;
            this.activeusersData = null;
            this.versionsData = null;
            this.userrelationsData = null;
            this.eventData = null;
        }
        this.isLoading = false;
    };
    analyticsComponent.prototype.makeeventchart = function (count) {
        var allformatedevent = [];
        var allcolors = [];
        var j;
        for (var i in count) {
            j = [i, count[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        // this.options = {
        //     is3D: true,
        //     width: 600,
        //     height: 600,
        //     bar: { groupWidth: "100%" },
        //     legend: { position: "none" },
        //     colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        // };
        this.pieoptions = {
            is3D: true,
            width: 400,
            height: 400,
            chartArea: {
                left: 50,
                right: 0,
                bottom: 60,
                top: 60,
                width: "100%",
                height: "100%"
            },
        };
        this.eventData = allformatedevent;
        //  this.isLoading=false;
    };
    analyticsComponent.prototype.makeplatformschart = function (platformscount) {
        var allformatedevent = [];
        var allcolors = [];
        var j;
        for (var i in platformscount) {
            j = [i, platformscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.platformspieoptions = {
            pieHole: 0.5,
            width: 400,
            height: 400,
            chartArea: {
                left: 50,
                right: 0,
                bottom: 60,
                top: 60,
                width: "100%",
                height: "100%"
            },
        };
        this.platformsData = allformatedevent;
    };
    analyticsComponent.prototype.makeversionschart = function (versionscount) {
        var allformatedevent = [];
        var allcolors = [];
        var j;
        for (var i in versionscount) {
            j = [i, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.versionscolumnoptions = {
            is3D: true,
            width: 600,
            height: 400,
            bar: { groupWidth: "100%" },
            legend: { position: "none" },
            colors: allcolors,
        };
        this.versionsData = allformatedevent;
    };
    analyticsComponent.prototype.makeactiveusersschart = function (versionscount) {
        var allformatedevent = [];
        var allcolors = [];
        var j;
        for (var i in versionscount) {
            j = [i, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.activeuserscolumnoptions = {
            is3D: true,
            width: 600,
            height: 400,
            bar: { groupWidth: "100%" },
            legend: { position: "none" },
            colors: allcolors,
        };
        this.activeusersData = allformatedevent;
    };
    analyticsComponent.prototype.makeuserbyrelationschart = function (versionscount) {
        var data = [];
        var allformatedevent = [];
        var allcolors = [];
        var j;
        var appname = 'undefined';
        var appcolor = '#0d3348';
        for (var i in versionscount) {
            if (i === "7fb031ae-0cb9-4a04-8407-1156d3d52108") {
                appname = 'Tryg';
                appcolor = '#dc0000';
            }
            if (i === "bef7e721-29cb-4eec-b0a9-af70fdc7b05d") {
                appname = 'Mølholm Sundhedsunivers';
                appcolor = '#595959';
            }
            if (i === "8e7fcffe-444b-4468-ab59-9f63b00d15c5") {
                appname = 'Sundhed via AP';
                appcolor = '#ff4d1d';
            }
            if (i === "59aa2f87-d09e-4fd7-9837-d3342082b35a") {
                appname = 'Connect';
                appcolor = '#ff4d1d';
            }
            if (i === "aa67c9ab-04a1-43fd-9656-e3f3d742ec9c") {
                appname = 'Aalborg';
                appcolor = '#ff4d1d';
            }
            if (i === "4f4645fc-7c9a-4de7-9592-145edafdfc4c") {
                appname = 'Willis';
                appcolor = '#ff4d1d';
            }
            if (i === "undefined") {
                appname = 'undefined';
                appcolor = '#0d3348';
            }
            j = [appname, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.userrelationscolumnoptions = {
            chartType: 'BarChart',
            type: 'BarChart',
            is3D: true,
            explorer: { axis: 'horizontal', keepInBounds: true },
            dataTable: allformatedevent,
            options: {
                is3D: true,
                width: 600,
                height: 400,
                bar: { groupWidth: "100%" },
                legend: { position: "none" },
            }
        };
        this.userrelationsData = allformatedevent;
    };
    analyticsComponent.prototype.getRandomColor = function () {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };
    analyticsComponent.prototype.handleChange = function (name) {
        console.log(name);
    };
    analyticsComponent.prototype.getanalyticsData = function () {
        console.log("oin submit");
    };
    analyticsComponent.prototype.StartDate = function (i, e) {
        console.log(i);
        console.log(e);
    };
    analyticsComponent.prototype.clearstartDate = function (item) {
        console.log(item);
        this.selecteddates = null;
    };
    analyticsComponent.prototype.Multiselect = function (e, item) {
        // this.fetchdata();
        console.log(e);
        console.log(item);
        console.log(this.slctapplication);
    };
    analyticsComponent.prototype.fetchdata = function () {
        var _this = this;
        this.isLoading = true;
        var formatedstd = null;
        var formatedend = null;
        this.filteredata = [];
        var totalusersData = 0;
        if (this.slctapplication && this.slctapplication.length > 0) {
            var organisation_uuid = [];
            var applikation_uuid = [];
            var _loop_1 = function (i) {
                organisation_uuid.push(this_1.slctapplication[i].organisation_uuid);
                applikation_uuid.push(this_1.allapplications.filter(function (item) { return item.organisation_uuid === _this.slctapplication[i].organisation_uuid; })[0].applikation_uuid);
            };
            var this_1 = this;
            for (var i in this.slctapplication) {
                _loop_1(i);
            }
            if (this.selecteddates && this.selecteddates[0]) {
                var dateformat = this.selecteddates[0]._d ? this.selecteddates[0]._d : this.selecteddates[0];
                formatedstd = moment__WEBPACK_IMPORTED_MODULE_7__(dateformat).format("YYYY-MM-DD HH:mm") + ':00';
            }
            if (this.selecteddates && this.selecteddates[1]) {
                var dateformat = this.selecteddates[1]._d ? this.selecteddates[1]._d : this.selecteddates[1];
                formatedend = moment__WEBPACK_IMPORTED_MODULE_7__(dateformat).format("YYYY-MM-DD HH:mm") + ':59';
            }
            if (applikation_uuid && applikation_uuid.length > 0) {
                this.baserestService.getuseranalytics(applikation_uuid, formatedstd, formatedend).then(function (alldomainsdata) {
                    _this.alldomainsdata = alldomainsdata;
                    _this.filteredata = alldomainsdata;
                    _this.createChart();
                }),
                    function (error) {
                        _this.errorNotification.refresh();
                        _this.errorNotification.open();
                        _this.serviceErrorMessage = error.status.besked ? error.status.besked : "Error";
                        _this.isLoading = false;
                    };
            }
        }
        else {
            this.filteredata = null;
            this.platformsData = null;
            this.activeusersData = null;
            this.activeusersData = null;
            this.versionsData = null;
            this.userrelationsData = null;
            this.totalusersData = null;
            this.eventData = null;
            this.isLoading = false;
        }
    };
    analyticsComponent.prototype.download = function () {
        this.isdownloading = true;
        var excelcolumns = [];
        var exceldata = [];
        this.filteredata.map(function (item) {
            if (item && item.request) {
                var requestdata = item.request;
                requestdata.log_timestamp = item.log_timestamp ? item.log_timestamp : '';
                requestdata.action = item.service_action ? item.service_action : '';
                requestdata.cvr = requestdata.cvr ? requestdata.cvr : '';
                requestdata.log_id = item.log_id ? item.log_id : '';
                exceldata.push(requestdata);
            }
        });
        excelcolumns.push({ label: 'log_id', value: 'log_id' });
        excelcolumns.push({ label: 'action', value: 'action' });
        excelcolumns.push({ label: 'platform', value: 'platform' });
        excelcolumns.push({ label: 'userToken', value: 'userToken' });
        excelcolumns.push({ label: 'bruger_uuid', value: 'bruger_uuid' });
        excelcolumns.push({ label: 'eventname', value: 'eventname' });
        excelcolumns.push({ label: 'cvr', value: 'cvr' });
        excelcolumns.push({ label: 'itsystem_uuid', value: 'itsystem_uuid' });
        excelcolumns.push({ label: 'applikation_uuid', value: 'applikation_uuid' });
        excelcolumns.push({ label: 'version', value: 'version' });
        excelcolumns.push({ label: 'relation_organizationid', value: 'relation_organizationid' });
        excelcolumns.push({ label: 'relation_organizationname', value: 'relation_organizationname' });
        excelcolumns.push({ label: 'log_timestamp', value: 'log_timestamp' });
        var settings = {
            sheetName: "Analytics",
            fileName: "Analytics",
        };
        this.isdownloading = false;
        xlsx(excelcolumns, exceldata, settings);
    };
    analyticsComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_6__["HelperSerice"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__["AuthRestService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], analyticsComponent.prototype, "domainname", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], analyticsComponent.prototype, "onChange", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], analyticsComponent.prototype, "errorNotification", void 0);
    analyticsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'analytics-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./analytics.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/analytics/analytics.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_6__["HelperSerice"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__["AuthRestService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"]])
    ], analyticsComponent);
    return analyticsComponent;
}());



/***/ }),

/***/ "./src/components/breadcrum/breadcrum.ts":
/*!***********************************************!*\
  !*** ./src/components/breadcrum/breadcrum.ts ***!
  \***********************************************/
/*! exports provided: BreadcrumComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BreadcrumComponent", function() { return BreadcrumComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BreadcrumComponent = /** @class */ (function () {
    function BreadcrumComponent() {
    }
    BreadcrumComponent.prototype.ngOnInit = function () {
        console.log(this.breadcrumelements);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], BreadcrumComponent.prototype, "breadcrumelements", void 0);
    BreadcrumComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'breadcrum-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./breadcrum.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/breadcrum/breadcrum.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BreadcrumComponent);
    return BreadcrumComponent;
}());



/***/ }),

/***/ "./src/components/changepassword/changepassword.ts":
/*!*********************************************************!*\
  !*** ./src/components/changepassword/changepassword.ts ***!
  \*********************************************************/
/*! exports provided: changepasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changepasswordComponent", function() { return changepasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/storageservice/storageservice */ "./src/providers/storageservice/storageservice.ts");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _providers_validators_password_validator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../providers/validators/password-validator */ "./src/providers/validators/password-validator.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var changepasswordComponent = /** @class */ (function () {
    function changepasswordComponent(fb, ref, authService, baserestService, storageService, router) {
        var _this = this;
        this.fb = fb;
        this.ref = ref;
        this.authService = authService;
        this.baserestService = baserestService;
        this.storageService = storageService;
        this.router = router;
        this.loading = false;
        this.serviceerror = false;
        this.showrepwd = false;
        this.showpwd = false;
        this.userid = this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.userid ? this.router.getCurrentNavigation().extras.state.userid : '';
        this.loginForm = this.fb.group({
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        }, {
            validator: _providers_validators_password_validator__WEBPACK_IMPORTED_MODULE_6__["PasswordValidation"].MatchPassword
        });
        this.authService.userToken.subscribe(function (userToken) {
            _this.userToken = userToken;
        });
    }
    changepasswordComponent.prototype.ngOnInit = function () {
    };
    changepasswordComponent.prototype.gotologin = function () {
        var _this = this;
        this.loading = true;
        this.serviceerror = false;
        this.password = this.loginForm.value.password;
        this.confirmPassword = this.loginForm.value.confirmPassword;
        this.baserestService.changePassword(this.userid.id, this.password).then(function (userinfo) {
            _this.userinfo = userinfo;
            _this.validateAuthorisation();
        }, function (error) {
            _this.serviceerror = true;
            _this.serviceerrormessage = error && error.error ? error.error : error.statusText;
            console.log(error.error);
        });
    };
    changepasswordComponent.prototype.validateAuthorisation = function () {
        var _this = this;
        this.baserestService.getAllMenuList(this.userinfo.id).then(function (menulist) {
            _this.userToken = menulist.userToken;
            _this.authService.setuserToken(menulist.userToken);
            _this.authService.setmenulist(menulist);
            if (menulist.relationer) {
                _this.authService.setuserrelations(menulist.relationer);
            }
            _this.storageService.set('carexadmin', _this.userinfo);
            _this.authService.setuserobj(_this.userinfo);
            _this.router.navigateByUrl('');
            _this.loading = false;
        }, function (error) {
            _this.loading = false;
            _this.serviceerror = true;
            _this.serviceerrormessage = error.error.besked;
        });
    };
    changepasswordComponent.prototype.ngOnDestroy = function () {
        this.ref.detach();
    };
    changepasswordComponent.prototype.showrepassword = function () {
        this.showrepwd = this.showrepwd === true ? false : true;
        if (this.showrepwd === true) {
            this.repasswordele.nativeElement.type = "text";
            this.ref.detectChanges();
        }
        else {
            this.repasswordele.nativeElement.type = "password";
            this.ref.detectChanges();
        }
    };
    changepasswordComponent.prototype.showpassword = function () {
        this.showpwd = this.showpwd === true ? false : true;
        if (this.showpwd === true) {
            this.passwordele.nativeElement.type = "text";
            this.ref.detectChanges();
        }
        else {
            this.passwordele.nativeElement.type = "password";
            this.ref.detectChanges();
        }
    };
    changepasswordComponent.prototype.resetForm = function () {
        this.serviceerror = false;
        this.loginForm.reset();
    };
    changepasswordComponent.prototype.inputvalueChange = function () {
        this.serviceerror = false;
        this.ref.reattach();
        this.ref.detectChanges();
    };
    changepasswordComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__["AuthRestService"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__["BaseRestService"] },
        { type: _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_3__["StorageService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('passwordele', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], changepasswordComponent.prototype, "passwordele", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('repasswordele', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], changepasswordComponent.prototype, "repasswordele", void 0);
    changepasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'changepassword-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./changepassword.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/changepassword/changepassword.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__["AuthRestService"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__["BaseRestService"], _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_3__["StorageService"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], changepasswordComponent);
    return changepasswordComponent;
}());



/***/ }),

/***/ "./src/components/chat-container/chat-container.ts":
/*!*********************************************************!*\
  !*** ./src/components/chat-container/chat-container.ts ***!
  \*********************************************************/
/*! exports provided: ChatContainer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatContainer", function() { return ChatContainer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_services_dataflow__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/services/dataflow */ "./src/providers/services/dataflow.ts");
/* harmony import */ var _models_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/messages */ "./src/models/messages.ts");




var ChatContainer = /** @class */ (function () {
    function ChatContainer(dialogflowservice) {
        this.dialogflowservice = dialogflowservice;
        this.messages = [];
        this.chatenable = false;
        this.chatquestion = '';
    }
    ChatContainer.prototype.onInit = function () {
        this.messages.push(new _models_messages__WEBPACK_IMPORTED_MODULE_3__["Messages"](''));
        console.log(this.message);
    };
    ChatContainer.prototype.hide = function (event) {
        event.preventDefault();
        console.log("in hided");
    };
    ChatContainer.prototype.close = function (event) {
        event.preventDefault();
        //this.state = this.state === 'active' ? 'inactive' : 'active';
        this.chatenable = this.chatenable == true ? false : true;
        console.log("in close");
    };
    ChatContainer.prototype.open = function (event) {
        event.preventDefault();
        //this.state = this.state === 'active' ? 'inactive' : 'active';
        this.chatenable = this.chatenable == true ? false : true;
        console.log("in close");
    };
    ChatContainer.prototype.sendchatMessage = function (event) {
        var _this = this;
        console.log(this.chatquestion);
        this.messages.push(this.chatquestion);
        var cq = this.chatquestion;
        this.chatquestion = '';
        this.dialogflowservice.sendChatMessage(cq).subscribe(function (Message) { return _this.message = Message; });
        setTimeout(function () {
            console.log("Async Task Calling Callback");
            _this.setMessages();
            // this.generate();
        }, 2500);
    };
    ChatContainer.prototype.setMessages = function () {
        var chat = this.message;
        this.messages.push(chat.result.fulfillment.speech);
        console.log(this.message);
    };
    ChatContainer.ctorParameters = function () { return [
        { type: _providers_services_dataflow__WEBPACK_IMPORTED_MODULE_2__["DialogflowService"] }
    ]; };
    ChatContainer = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'chat-container',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chat-container.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/chat-container/chat-container.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_services_dataflow__WEBPACK_IMPORTED_MODULE_2__["DialogflowService"]])
    ], ChatContainer);
    return ChatContainer;
}());



/***/ }),

/***/ "./src/components/configuration/configuration.ts":
/*!*******************************************************!*\
  !*** ./src/components/configuration/configuration.ts ***!
  \*******************************************************/
/*! exports provided: Configuration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Configuration", function() { return Configuration; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-clipboard */ "./node_modules/ngx-clipboard/fesm5/ngx-clipboard.js");
/* harmony import */ var ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ang-jsoneditor */ "./node_modules/ang-jsoneditor/fesm5/ang-jsoneditor.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_7__);








var Configuration = /** @class */ (function () {
    function Configuration(baserestService, auth, clipboardService, ref) {
        var _this = this;
        this.baserestService = baserestService;
        this.auth = auth;
        this.clipboardService = clipboardService;
        this.ref = ref;
        this.slctdok = "vaælge";
        this.brand = "vaælge";
        this.title = '';
        this.teaser = '';
        this.beskrivelse = '';
        this.flettefelter = "[]";
        this.slctlang = 'da';
        this.loading = false;
        this.orgcopyflag = false;
        this.brugervendcopyflag = false;
        this.textcopyflag = false;
        this.allbrandsList = [];
        this.allbrands = [];
        this.alldokumentList = [];
        this.showupload = false;
        this.fileuploaded = '';
        this.nullbrand = {
            "configuration": "not available"
        };
        this.defaultbrandconf = {
            "environment": {},
            "configuration": {}
        };
        //
        this.allapplications = [
            { applikation_uuid: 'dc252eac-7759-4de1-a371-25da7a76761b', organisation_uuid: "bef7e721-29cb-4eec-b0a9-af70fdc7b05d", appname: "Molholm", selected: false },
            { applikation_uuid: '86d383ea-644c-49df-9d8c-0fdde0811a35', organisation_uuid: "8e7fcffe-444b-4468-ab59-9f63b00d15c5", appname: "Ap", selected: false },
            { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "59aa2f87-d09e-4fd7-9837-d3342082b35a", appname: "Connect", selected: false },
            { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "aa67c9ab-04a1-43fd-9656-e3f3d742ec9c", appname: "Aalborg", selected: false },
            { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "2a60966b-fd54-47dd-8478-92a120a54276", appname: "Zeppeline", selected: false },
            { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "80227aae-d26c-45a7-b9b4-e28210dca374", appname: "BKMedical", selected: false },
            { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "4cf2e4ad-3daf-4139-93d3-d3891beefaa2", appname: "AON", selected: false }
        ];
        this.editorOptions = new ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorOptions"]();
        this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
        this.auth.userToken.subscribe(function (userToken) {
            _this.userToken = userToken;
        });
        this.auth.userrelations.subscribe(function (userrelations) {
            _this.userrelations = userrelations;
        });
    }
    Configuration.prototype.ngOnInit = function () {
        var _this = this;
        this.baserestService.getDokumentList().then(function (alldokumentList) {
            _this.alldokumentList = alldokumentList.data;
            _this.setDok();
        }, function (error) { console.log(error); _this.loading = false; });
    };
    Configuration.prototype.setDok = function () {
        setTimeout(function () {
            $('select').selectpicker();
        }, 100);
    };
    Configuration.prototype.onChange = function (e, item) {
        var _this = this;
        this.loading = true;
        this.showupload = false;
        this.allbrands.length = 0;
        this.baserestService.getBrands(this.slctdok, this.userToken).then(function (brandconfig) { _this.setbrandConfig(brandconfig); }, function (error) { console.log(error); _this.loading = false; });
    };
    Configuration.prototype.setbrandConfig = function (brandconfig) {
        this.jsoneditor = true;
        // this.showupload = brandconfig === null ? true : false;
        if (brandconfig === null) {
            brandconfig = this.nullbrand;
        }
        this.brandconfig = brandconfig; //? brandconfig : this.defaultbrandconf;
        this.body = brandconfig; //? brandconfig : this.defaultbrandconf;
        this.showupload = brandconfig === null ? true : false; // need a refactor
        this.editedbody = brandconfig;
        this.loading = false;
        this.ref.detectChanges();
    };
    Configuration.prototype.uploadfile = function (evt) {
        var _this = this;
        try {
            var files = evt.target.files;
            if (!files.length) {
                console.log('No file selected!');
                this.errorNotification.open();
                this.errorNotification.open();
                return;
            }
            var file = files[0];
            var reader = new FileReader();
            var self_1 = this;
            reader.onload = function (event) {
                var data = JSON.parse(event.target.result);
                _this.body = data;
                _this.brandconfig = data;
                _this.editedbody = data;
                _this.showupload = false;
                _this.ref.detectChanges();
            };
            reader.readAsText(file);
        }
        catch (err) {
            console.error(err);
        }
    };
    Configuration.prototype.Deleteconfig = function (e) {
        e.preventDefault();
        this.body = this.nullbrand;
        ;
        this.brandconfig = this.nullbrand;
        ;
        this.editedbody = this.nullbrand;
        ;
        this.showupload = true;
        this.ref.detectChanges();
    };
    Configuration.prototype.Copyorg = function (flag) {
        this.orgcopyflag = false;
        this.brugervendcopyflag = false;
        this.textcopyflag = false;
        switch (flag) {
            case "orgcopyflag":
                this.orgcopyflag = true;
                break;
            case "brugervendcopyflag":
                this.brugervendcopyflag = true;
                break;
            case "textcopyflag":
                this.textcopyflag = true;
                break;
            default:
                break;
        }
    };
    Configuration.prototype.copied = function (e) {
        console.log(e);
    };
    Configuration.prototype.Cancel = function (e) {
        e.preventDefault();
        this.skabelonlist.length = 0;
        this.skabeloncontent = null;
    };
    Configuration.prototype.saveConfiguraiton = function () {
        var _this = this;
        // console.log(this.editedbody);
        this.baserestService.savesConfiguration(this.slctdok, JSON.stringify(this.editedbody), this.userToken).then(function (savedcontent) {
            _this.successNotification.open();
            _this.successNotification.open();
            _this.loading = false;
        }, function (error) {
            console.log(error);
            _this.errorNotification.open();
            _this.errorNotification.open();
            _this.loading = false;
        });
    };
    Configuration.prototype.getData = function (e) {
        this.editedbody = e;
    };
    Configuration.prototype.DownloadJson = function (e) {
        var _this = this;
        e.preventDefault();
        var item = this.alldokumentList.filter(function (x) { return x.uuid == _this.slctdok; })[0];
        var blob = new Blob([JSON.stringify(this.body)], { type: 'application/json' });
        Object(file_saver__WEBPACK_IMPORTED_MODULE_7__["saveAs"])(blob, item.titel + '.json');
    };
    Configuration.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"] },
        { type: ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__["ClipboardService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], Configuration.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], Configuration.prototype, "errorNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorComponent"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorComponent"])
    ], Configuration.prototype, "editor", void 0);
    Configuration = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'configuration',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./configuration.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/configuration/configuration.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"], ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__["ClipboardService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], Configuration);
    return Configuration;
}());

// With Promises:


/***/ }),

/***/ "./src/components/createpage/createpage.ts":
/*!*************************************************!*\
  !*** ./src/components/createpage/createpage.ts ***!
  \*************************************************/
/*! exports provided: CreatePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatePage", function() { return CreatePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _models_localization_localization__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../models/localization/localization */ "./src/models/localization/localization.ts");







var CreatePage = /** @class */ (function () {
    function CreatePage(service, auth, activatedRoute, services) {
        this.service = service;
        this.auth = auth;
        this.activatedRoute = activatedRoute;
        this.services = services;
        this.loading = false;
        this.selectitem = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    CreatePage.prototype.ngOnInit = function () {
        this.loading = true;
        this.brugervendtnoegle = this.activatedRoute.snapshot.params.brugervendtnoegle;
        this.heading = this.services.getselectedMenu();
        this.listId = this.services.getlistId();
        this.uuid = this.services.getClickedRowuuiid();
        this.localization = new _models_localization_localization__WEBPACK_IMPORTED_MODULE_6__["Localization"]();
        this.localization = this.localization.localizationobj;
        this.getDetailedContent(this.uuid, this.listId);
    };
    CreatePage.prototype.getDetailedContent = function (uuid, listId) {
        var _this = this;
        this.service.getcreateDetailsdata(listId)
            .then(function (detailsData) {
            _this.detailsData = detailsData;
            _this.loading = false;
        }, function (error) {
            _this.errorMessage = error.status.besked;
            _this.loading = false;
        });
    };
    CreatePage.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__["AuthRestService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CreatePage.prototype, "selectitem", void 0);
    CreatePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'createpage',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./createpage.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/createpage/createpage.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__["AuthRestService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"]])
    ], CreatePage);
    return CreatePage;
}());



/***/ }),

/***/ "./src/components/customform/customform.ts":
/*!*************************************************!*\
  !*** ./src/components/customform/customform.ts ***!
  \*************************************************/
/*! exports provided: customForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customForm", function() { return customForm; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _models_localization_localization__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../models/localization/localization */ "./src/models/localization/localization.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var ckeditor4_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ckeditor4-angular */ "./node_modules/ckeditor4-angular/fesm2015/ckeditor4-angular.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../providers/utils/helperService */ "./src/providers/utils/helperService.ts");















var customForm = /** @class */ (function () {
    function customForm(service, dateTimeAdapter, modalService, helperService, services, activatedRoute, auth, formBuilder, ref, location) {
        var _this = this;
        this.service = service;
        this.dateTimeAdapter = dateTimeAdapter;
        this.modalService = modalService;
        this.helperService = helperService;
        this.services = services;
        this.activatedRoute = activatedRoute;
        this.auth = auth;
        this.formBuilder = formBuilder;
        this.ref = ref;
        this.location = location;
        this.selectedTab = 0;
        this.selectedContainer = 0;
        this.loading = false;
        this.tabstoDisplay = [];
        this.itemss = [];
        this.addresserrorMesg = false;
        this.enableaddresserrorMsg = false;
        this.highlightedIndex = 0;
        this.selectedStreet = '';
        this.AllFiles = [];
        this.readonly = false;
        this.opentoogle = false;
        this.config = {
            height: '300px',
            startupOutlineBlocks: true,
            removeButtons: 'About,Image,Subscript,Table,Superscript,Anchor,Spellchecker,Removeformat',
            removeDialogTabs: 'link:target;link:advanced;link:protocol',
            removeInfoTabs: 'link:protocol',
            fontSize: 21
        };
        this.objekt_uuid = null;
        this.activeIds = [];
        this.buttons = [];
        this.ngbModalOptions = {
            backdrop: 'static',
            keyboard: false
        };
        this.selectitem = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.auth.userToken.subscribe(function (userToken) {
            _this.userToken = userToken;
        });
        this.dateTimeAdapter.setLocale('da-DK');
    }
    customForm.prototype.ngOnInit = function () {
        this.loading = true;
        this.brugervendtnoegle = this.activatedRoute.snapshot.params.brugervendtnoegle;
        this.heading = this.services.getselectedMenu(); //this.activatedRoute.snapshot.data.title;   
        this.navigationElements = [this.heading]; // need to make new breadcrum in future
        this.listId = this.services.getlistId();
        this.uuid = this.services.getClickedRowuuiid();
        this.localization = new _models_localization_localization__WEBPACK_IMPORTED_MODULE_8__["Localization"]();
        this.environmentUrl = this.service.getEnvironment();
        this.localization = this.localization.localizationobj;
        this.setData();
    };
    customForm.prototype.getHeaderInfo = function () {
        var details = this.detailsData;
        this.actions = this.detailsData.result.actions;
        this.displayheadingName = details.status.displayname;
        this.services.setdisplayName(details.status.displayname);
        this.brugerName = details.status.brugernavn;
        this.readonly = details.status.readonly ? details.status.readonly : false;
        if (details.status.update_timestamp) {
            this.lastupdatedTime = new Date(details.status.update_timestamp);
            var dd = this.lastupdatedTime.getDate();
            var mm = this.lastupdatedTime.getMonth() + 1;
            var yyyy = this.lastupdatedTime.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            this.lastupdatedTime = dd + '-' + mm + '-' + yyyy;
        }
    };
    customForm.prototype.setData = function () {
        var details = this.detailsData;
        this.getHeaderInfo();
        this.lastupdatedTimeStamp = details.status.update_timestamp;
        this.detailsForm = this.formBuilder.group({
            items: this.formBuilder.array([])
        });
        this.addItem();
        this.buttons = this.actions && this.actions.buttons ? this.actions.buttons.reverse() : [];
    };
    customForm.prototype.addItem = function () {
        this.items = this.detailsForm.get('items');
        for (var i in this.detailsData.result.detaljer) {
            this.items.push(this.createDyanicForm(this.detailsData.result.detaljer[i]));
        }
        setTimeout(function () {
            $('.selectpicker').selectpicker();
        }, 100);
        this.loading = false;
        // console.log(this.detailsForm);
        this.detailsform = this.detailsForm;
    };
    customForm.prototype.createDyanicForm = function (item) {
        var tooltipinfo = this.detailsData.beskrivelser[item.ld_brugervendtnoegle];
        if (item.type == "input" && item.ld_brugervendtnoegle != "adresser" && item.type != "file" && item.type != "date") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                readonly: item.readonly,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
        if (item.type === "accordion") {
            var accordians = [];
            var k = null;
            var accheading = item.titel;
            var lang = item.ld_brugervendtnoegle.split('_').pop();
            for (var j in item) {
                if (item[j] && item[j].property && item[j].property.accordion) {
                    k = {
                        name: item[j].titel,
                        tabIndex: this.tabIndex,
                        value: item[j].selected_values[0],
                        type: item[j].type,
                        required: item[j].mandatory,
                        tooltip: tooltipinfo,
                        hide: item[j].property && item[j].property.hide ? item[j].property.hide : false,
                        accordian: item[j].property && item[j].property.accordian ? item[j].property.accordian : false,
                        readonly: item[j].readonly,
                        ld_brugervendtnoegle: item[j].ld_brugervendtnoegle,
                        relationindeks: item[j].relationindeks,
                        selected_list: [item[j].selected_values],
                        relationsvirkning: item[j].relationsvirkning
                    };
                    accordians.push(k);
                }
            }
            return this.formBuilder.group({
                accordian: true,
                title: accheading,
                lang: lang,
                allaccordians: [accordians]
            });
        }
        if (item.type == "textarea") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                readonly: item.readonly,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
        if (item.type == "html") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0].replace(/(?:\r\n|\r|\n)/g, '<br>'),
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                readonly: item.readonly,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
        if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
            this.selectedStreet = item.selected_values[0];
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
        if (item.type == "file") {
            this.selectedStreet = item.selected_values[0];
            this.documentuuid = item.dokument_uuid;
            var image = item.image ? item.image : null;
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                documentuuid: item.dokument_uuid,
                image: image,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
        if (item.type == "date") {
            var formatdate = null;
            var minimumdate = null;
            this.mindate = this.helperService.formatDate(null);
            if (item.selected_values && item.selected_values[0] && item.selected_values[0].length != 0) {
                formatdate = this.helperService.formatDate(item.selected_values[0]);
            }
            else {
                if (item.property && item.property.future === "true" && item.property.past === "false") {
                    formatdate = this.helperService.formatDate(null);
                    minimumdate = formatdate;
                }
            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: formatdate ? formatdate : null,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values[0]],
                formatdate: formatdate ? formatdate : null,
                mindate: minimumdate,
                relationsvirkning: item.relationsvirkning
            });
        }
        if (item.type == "dropdown" && item.relationsvirkning === false) {
            if (item.multiselect === "false") {
                var selected_name = void 0;
                var selected_names = [];
                for (var i in item.valuelist) {
                    selected_name = item.valuelist[i].selected === true ? item.valuelist[i].rel_uri : null;
                    if (selected_name) {
                        selected_names.push(selected_name);
                    }
                }
                return this.formBuilder.group({
                    name: item.titel,
                    tabIndex: this.tabIndex,
                    options: [item.valuelist],
                    multiselect: item.multiselect,
                    tooltip: tooltipinfo,
                    hide: item.property && item.property.hide ? item.property.hide : false,
                    accordian: item.property && item.property.accordian ? item.property.accordian : false,
                    type: item.type,
                    readonly: item.readonly,
                    selected_name: selected_names,
                    required: item.mandatory,
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    relationindeks: item.relationindeks,
                    valuelist: item.valuelist,
                    relationsvirkning: item.relationsvirkning
                });
            }
            if (item.multiselect === "true") {
                var selected_name = void 0;
                var selected_names = [];
                for (var i in item.valuelist) {
                    selected_name = item.valuelist[i].selected === true ? item.valuelist[i].rel_uri : null;
                    if (selected_name) {
                        selected_names.push(selected_name);
                    }
                }
                return this.formBuilder.group({
                    name: item.titel,
                    tabIndex: this.tabIndex,
                    options: [item.valuelist],
                    multiselect: item.multiselect,
                    tooltip: tooltipinfo,
                    hide: item.property && item.property.hide ? item.property.hide : false,
                    accordian: item.property && item.property.accordian ? item.property.accordian : false,
                    type: item.type,
                    readonly: item.readonly,
                    selected_name: [selected_names],
                    required: item.mandatory,
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    relationindeks: item.relationindeks,
                    valuelist: item.valuelist,
                    relationsvirkning: item.relationsvirkning
                });
            }
        }
        if (item.type == "dropdown" && item.relationsvirkning === true) {
            for (var i in item.valuelist) {
                var intervalitem = {
                    fra: item.valuelist[i].fra,
                    til: item.valuelist[i].til,
                    note: item.valuelist[i].note
                };
                item.valuelist[i].intervals = [intervalitem];
            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                options: [item.valuelist],
                multiselect: item.multiselect,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                valuelist: item.valuelist,
                selected_list: [item.selected_values],
                // selected_names: [selected_names],
                //selected_names: selected_names && selected_names.length ? [selected_names]:null,
                selected_values: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
    };
    customForm.prototype.downloadFile = function (event) {
        var _this = this;
        event.preventDefault();
        this.service.downloadFileDocument(this.listId, this.documentuuid).then(function (res) { _this.downloadDetails = res; _this.getFile(res); _this.successnotification.open(); _this.loading = false; }, function (error) { _this.errornotification.open(); _this.errorMessage = error.status.besked; console.log(error); _this.loading = false; });
    };
    customForm.prototype.getFile = function (result) {
        var temp_Url = result.result.temp_path;
        var envi = this.service.getEnvironment();
        var e = envi.indexOf('/api');
        var formurl = envi.substring(0, e);
        var tempindex = temp_Url.lastIndexOf('temp');
        var filedownload_url = temp_Url.substring(tempindex, temp_Url.length);
        var downloadURL = formurl.concat("/" + filedownload_url);
        window.open('' + downloadURL, '_blank');
    };
    customForm.prototype.Select = function (event, itemname) {
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0];
        for (var j in itemname.value.options) {
            if (itemname.value.selected_name === filtereditem.options[j].rel_uri) {
                filtereditem.options[j].selected = true;
                this.selected_reluuid = filtereditem.options[j].rel_uuid;
            }
            else {
                filtereditem.options[j].selected = false;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0] = filtereditem;
        if (itemname.value.ld_brugervendtnoegle === 'kampagneskabeloner' && this.selected_reluuid != null) {
            var modalref = this.modalService.open(this.warning, this.ngbModalOptions);
            this.services.setmodalReference(modalref);
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0] = filtereditem;
        $('.invalid-feedback.' + itemname.value.ld_brugervendtnoegle).addClass('hide');
    };
    customForm.prototype.Multiselect = function (event, itemname) {
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0];
        for (var j in itemname.value.options) {
            if (itemname.value.selected_name.indexOf(itemname.value.options[j].rel_uri) != -1) {
                filtereditem.options[j].selected = true;
            }
            else {
                filtereditem.options[j].selected = false;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0] = filtereditem;
        $('.invalid-feedback.' + itemname.value.ld_brugervendtnoegle).addClass('hide');
    };
    customForm.prototype.Multiselectrelation = function (event, item, optionitem) {
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === optionitem.rel_uuid) {
                filtereditem.options[j] = optionitem;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = filtereditem;
    };
    customForm.prototype.ErrorMsg = function () {
        if (this.itemss.length > 0) {
            this.addresserrorMesg = false;
            this.enableaddresserrorMsg = false;
        }
        if (this.selectedStreet && this.selectedStreet.length > 0) {
            this.addresserrorMesg = false;
            this.enableaddresserrorMsg = false;
        }
        else {
            this.addresserrorMesg = true;
            this.enableaddresserrorMsg = true;
        }
    };
    customForm.prototype.onItems = function (items) {
        this.itemss = items;
    };
    customForm.prototype.onItemHighlighted = function (index) {
        this.highlightedIndex = index;
        if (index == 0) {
            this.addresserrorMesg = true;
            this.enableaddresserrorMsg = true;
        }
    };
    customForm.prototype.onItemSelected = function (item) {
        this.itemss = [];
        this.highlightedIndex = 0;
        this.selectedStreet = item.text;
        this.addresserrorMesg = false;
        this.enableaddresserrorMsg = false;
    };
    customForm.prototype.done = function (res) {
        this.successnotification.open();
        this.successnotification.open(); // weird works after calling 2 times
        this.ref.detectChanges();
        this.loading = false;
        this.location.back();
    };
    customForm.prototype.selectFile = function (event, item) {
        item.value.value = event.target.files[0].name;
        this.fileNavn = event.target.files[0].name;
        this.File = event.target.files[0];
        var fileitem = {
            name: item.value.ld_brugervendtnoegle,
            value: this.File
        };
        this.AllFiles.push(fileitem);
        ;
        var selected_item = item.value.name;
        var item = this.detailsForm.controls.items.value.filter(function (x) { return x.name == selected_item; })[0];
        item.value = this.fileNavn;
        this.detailsForm.controls.items.value.filter(function (x) { return x.name == selected_item; })[0] = item;
    };
    customForm.prototype.formatData = function (item) {
        if (item) {
            if (item.type == "input" && item.ld_brugervendtnoegle != "adresser") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value]
                };
            }
            if (item.type == "editor") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value]
                };
            }
            if (item.accordian && item.allaccordians) {
                var allaccords_1 = [];
                item.allaccordians.forEach(function (element) {
                    allaccords_1.push({
                        ld_brugervendtnoegle: element.ld_brugervendtnoegle,
                        value: [element.value]
                    });
                });
                return allaccords_1;
            }
            if (item.type == "textarea") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value]
                };
            }
            if (item.type == "html") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value.replace(/(?:\r\n|\r|\n)/g, '<br>')]
                };
            }
            if (item.type == "date") {
                var formateddate = null;
                if (item.formatdate) {
                    formateddate = moment__WEBPACK_IMPORTED_MODULE_10__(item.formatdate).format('YYYY-MM-DD HH:mm:ss');
                    if (formateddate === 'Invalid date') {
                        formateddate = null;
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: formateddate ? [formateddate] : [null]
                };
            }
            if (item.type == "file") {
                var image = item.image ? item.image : null;
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                    image: image
                };
            }
            if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [this.selectedStreet],
                    relationindeks: item.relationindeks
                };
            }
            if (item.type == "dropdown" && item.multiselect == "false" && item.relationsvirkning === false) {
                var optionitem = item.options.filter(function (x) { return x.selected === true; })[0];
                if (optionitem) {
                    return {
                        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                        value: [optionitem.rel_uuid]
                    };
                }
                else {
                    return {
                        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                        value: [null]
                    };
                }
            }
            if (item.type == "dropdown" && item.multiselect == "true" && item.relationsvirkning === false) {
                var values = [];
                if (item.selected_name && item.selected_name.length != 0) {
                    for (var i in item.options) {
                        if (item.selected_name.indexOf(item.options[i].rel_uri) != -1) {
                            values.push(item.options[i].rel_uuid);
                        }
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: values
                };
            }
            if (item.type == "dropdown" && item.multiselect == "true" && item.relationsvirkning === true) {
                var values = [];
                if (item.options && item.options.length != 0) {
                    for (var i in item.options) {
                        if (item.options[i].selected === true) {
                            for (var j in item.options[i].intervals) {
                                var fra = null;
                                var til = null;
                                if (item.options[i].intervals[j].fra) {
                                    item.options[i].intervals[j].fra = moment__WEBPACK_IMPORTED_MODULE_10__(item.options[i].intervals[j].fra).format('YYYY-MM-DD HH:mm:ss');
                                }
                                if (item.options[i].intervals[j].til) {
                                    item.options[i].intervals[j].til = moment__WEBPACK_IMPORTED_MODULE_10__(item.options[i].intervals[j].til).format('YYYY-MM-DD HH:mm:ss');
                                    ;
                                }
                                var slct = {
                                    rel_uri: item.options[i].rel_uri,
                                    rel_uuid: item.options[i].rel_uuid,
                                    fra: fra,
                                    til: til,
                                    note: item.options[i].intervals[j].note
                                };
                                values.push(slct);
                            }
                        }
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: values
                };
            }
        }
    };
    customForm.prototype.dateChange = function (item, index) {
        var dateformat = null;
        console.log(item);
        if (index && index.selected) {
            dateformat = moment__WEBPACK_IMPORTED_MODULE_10__(index.selected._d).format('YYYY-MM-DD HH:mm:ss');
        }
        var slcitem = item.value.ld_brugervendtnoegle;
        item.value.value = dateformat;
        item.value.formatdate = dateformat;
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == slcitem; })[0] = item;
    };
    customForm.prototype.clearDate = function (item) {
        item.value.value = null;
        item.value.formatdate = null;
        var slcitem = item.value.ld_brugervendtnoegle;
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == slcitem; })[0] = item;
    };
    customForm.prototype.dateEditfra = function (item, intervalitem, optionitem, index, j) {
        var dateformat = null;
        console.log(item);
        console.log(intervalitem);
        if (intervalitem && intervalitem.fra) {
            dateformat = moment__WEBPACK_IMPORTED_MODULE_10__(intervalitem.fra._d).format('YYYY-MM-DD HH:mm:ss');
        }
        intervalitem.fra = dateformat;
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j_1 in item.value.options.intervals) {
            if (item.value.options[j_1].rel_uri === optionitem.rel_uri) {
                filtereditem.options[j_1].intervalitem[index].fra = dateformat;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = item;
        $('.invalid-feedback.' + item.value.ld_brugervendtnoegle).addClass('hide');
    };
    customForm.prototype.dateEdittil = function (item, intervalitem, optionitem, index, j) {
        var dateformat = null;
        // console.log(item);
        // console.log(optionitem);
        if (intervalitem && intervalitem.til) {
            dateformat = moment__WEBPACK_IMPORTED_MODULE_10__(intervalitem.til._d).format('YYYY-MM-DD HH:mm:ss');
        }
        intervalitem.til = dateformat;
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j_2 in item.value.options.intervalitem) {
            if (item.value.options[j_2].rel_uri === optionitem.rel_uri) {
                filtereditem.options[j_2].intervalitem[index].til = dateformat;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = item;
        $('.invalid-feedback.' + item.value.ld_brugervendtnoegle).addClass('hide');
    };
    customForm.prototype.Deletedateitem = function (e, item, relationoptionitem, index) {
        e.preventDefault();
        console.log(relationoptionitem);
        console.log(item);
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === relationoptionitem.rel_uuid) {
                filtereditem.options[j].intervals.splice(index, 1);
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = filtereditem;
        console.log(this.detailsForm.controls.items.value);
    };
    customForm.prototype.tooglepakker = function (e) {
        e.preventDefault();
        this.opentoogle = this.opentoogle === false ? true : false;
    };
    customForm.prototype.Createdateitem = function (e, item, relationoptionitem) {
        e.preventDefault();
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === relationoptionitem.rel_uuid) {
                var intervalitem = {
                    fra: null,
                    til: null,
                    note: ''
                };
                filtereditem.options[j].intervals.push(intervalitem);
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = filtereditem;
    };
    customForm.prototype.onchngEditordata = function (editor) {
        var data = editor.editor.getData();
        this.editorInstance = editor;
    };
    customForm.prototype.openfilter = function (modal, event, item) {
        this.fletfilters = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == "flettefelter"; })[0];
        var modalref = this.modalService.open(modal, this.ngbModalOptions);
        this.services.setmodalReference(modalref);
    };
    customForm.prototype.setDatatoEditor = function (data) {
        this.editorInstance.editor.insertHtml(data, 'text');
    };
    customForm.prototype.replaceeditorContent = function (confirmreplace) {
        var _this = this;
        if (confirmreplace === true) {
            this.service.getskabeloncontent(this.selected_reluuid, 'en').then(function (skabeloncontent) {
                // this.editorInstance.editor.insertHtml(skabeloncontent, 'text');
                var filteredaccordian = _this.detailsForm.controls.items.value.filter(function (x) { return x.lang === 'en'; })[0];
                filteredaccordian.allaccordians.map(function (item) {
                    if (item.ld_brugervendtnoegle.indexOf('titel') != -1) {
                        item.selected_list[0] = skabeloncontent.title;
                        item.value = skabeloncontent.title;
                    }
                    if (item.ld_brugervendtnoegle.indexOf('tekst') != -1) {
                        item.selected_list[0] = skabeloncontent.body[0];
                        item.value = skabeloncontent.body[0];
                    }
                });
                _this.detailsForm.controls.items.value.filter(function (x) { return x.lang === 'en'; })[0] = filteredaccordian;
            }, function (error) {
                console.log(error);
                _this.loading = false;
            });
            this.service.getskabeloncontent(this.selected_reluuid, 'da').then(function (skabeloncontent) {
                var filteredaccordian = _this.detailsForm.controls.items.value.filter(function (x) { return x.lang === 'da'; })[0];
                filteredaccordian.allaccordians.map(function (item) {
                    if (item.ld_brugervendtnoegle.indexOf('titel') != -1) {
                        item.selected_list[0] = skabeloncontent.title;
                        item.value = skabeloncontent.title;
                    }
                    if (item.ld_brugervendtnoegle.indexOf('tekst') != -1) {
                        item.selected_list[0] = skabeloncontent.body[0];
                        item.value = skabeloncontent.body[0];
                    }
                });
                _this.detailsForm.controls.items.value.filter(function (x) { return x.lang === 'da'; })[0] = filteredaccordian;
            }, function (error) {
                console.log(error);
                _this.loading = false;
            });
        }
    };
    customForm.prototype.cancel = function () {
        this.modalService.dismissAll('cancelled');
    };
    customForm.prototype.saveData = function () {
        var _this = this;
        if (!this.detailForm.nativeElement.checkValidity()) {
            this.detailForm.nativeElement.classList.add("was-validated");
        }
        var formvalid = $(".invalid-feedback").is(":visible");
        if (!formvalid) {
            var formvalues = JSON.stringify(this.detailsForm.controls.items.value);
            var formdata = [];
            formdata.push(this.detailsForm.controls.items.value.map(this.formatData, this));
            var submitting_1 = [];
            formdata[0].map(function (e) {
                if (e && e.ld_brugervendtnoegle) {
                    submitting_1.push(e);
                }
                else {
                    e.map(function (el) {
                        if (el && el.ld_brugervendtnoegle) {
                            submitting_1.push(el);
                        }
                    });
                }
            });
            this.loading = true;
            this.service
                .updateDetailsdata(this.listId, '', this.detailsData.status.update_timestamp, JSON.stringify(submitting_1), this.AllFiles)
                .then(function (res) {
                _this.loading = false;
                _this.modalService.dismissAll('saved');
                _this.successnotification.open();
            }, function (error) {
                _this.loading = false;
                _this.errornotification.open();
                var error_messsage = error.error.status.besked;
                _this.errorMessage = error_messsage;
            }, function (complete) {
                _this.loading = false;
            });
        }
    };
    customForm.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: ng_pick_datetime__WEBPACK_IMPORTED_MODULE_11__["DateTimeAdapter"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModal"] },
        { type: _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_14__["HelperSerice"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_9__["AuthRestService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successnotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], customForm.prototype, "successnotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errornotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], customForm.prototype, "errornotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('detailForm', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], customForm.prototype, "detailForm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('editor', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ckeditor4_angular__WEBPACK_IMPORTED_MODULE_12__["CKEditorComponent"])
    ], customForm.prototype, "editorComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('warning', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], customForm.prototype, "warning", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"])('acc'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], customForm.prototype, "acc", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], customForm.prototype, "selectitem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], customForm.prototype, "detailsData", void 0);
    customForm = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'customform',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./customform.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/customform/customform.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], ng_pick_datetime__WEBPACK_IMPORTED_MODULE_11__["DateTimeAdapter"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModal"], _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_14__["HelperSerice"],
            _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_9__["AuthRestService"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"]])
    ], customForm);
    return customForm;
}());



/***/ }),

/***/ "./src/components/dashboard/dashboard.ts":
/*!***********************************************!*\
  !*** ./src/components/dashboard/dashboard.ts ***!
  \***********************************************/
/*! exports provided: Dashboard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dashboard", function() { return Dashboard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var Dashboard = /** @class */ (function () {
    function Dashboard() {
        google.charts.load("current", { packages: ["corechart"] });
        //google.charts.load('current', {packages:['gantt']});
    }
    Dashboard.prototype.ngAfterViewInit = function () {
        google.charts.setOnLoadCallback(this.drawChart);
        google.charts.setOnLoadCallback(this.drawChart2);
        //google.charts.setOnLoadCallback(this.drawChartGantt)
    };
    Dashboard.prototype.drawChart = function () {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Work', 11],
            ['Eat', 2],
            ['Commute', 2],
            ['Watch TV', 2],
            ['Sleep', 7]
        ]);
        var options = {
            title: 'My Daily Activities',
            is3D: true,
        };
        var chart1 = new google.visualization.PieChart(document.getElementById('donutchart'));
        // selectHandler() function.
        chart1.draw(data, options);
        //   google.visualization.events.addListener(chart1, 'select', function () {
        //     var selectedBar = chart1.getSelection();
        //     ////console.log(selectedBar);
        //     //data.getRowLabel(selectedBar[0].row);
        //     console.log(selectedBar);
        //    /// for Bar charts, normally row-parameters are used
        //    /// manipulate on 'data' which refers to data-table to get desired results
        // });
        //google.visualization.events.addListener(chart, 'select',selectHandler);
        // var data = google.visualization.arrayToDataTable([['Task', 'Hours per Day'], ['Work', 11], ['Eat', 2], ['Commute',  2], ['Watch TV', 2], ['Sleep', 7] ]);          
        // var options = { title: 'My Daily Activities', pieHole: 0.2 };          
        // var chart = new google.visualization.PieChart(document.getElementById('donutchart'));         
        // chart.draw(data, options);
        // function selectHandler(){
        //   var selection = chart.getSelection();
        //   console.log(selection);
        //   var person = prompt("Please enter your name", "Harry Potter");
        // if (person != null) {
        //    console.log("Hello " + person + "! How are you today?");
        // }
        //  }
    };
    Dashboard.prototype.drawChart2 = function () {
        var data = google.visualization.arrayToDataTable([
            ["Element", "Density", { role: "style" }],
            ["Monday", 8.94, "#b87333"],
            ["Tuesday", 10.49, "#06B73C"],
            ["Wednesday", 19.30, "#FD8600"],
            ["Thursday", 21.45, "color: #644591"],
            ["Friday", 8.94, "#61A8DF"],
            ["Saturday", 10.49, "#C22933"],
            ["Sunday", 19.30, "#FDCC46"]
        ]);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);
        var options = {
            title: "UGE 32 2018",
            width: 900,
            height: 400,
            is3D: true,
            bar: { groupWidth: "95%" },
            legend: { position: "none" },
            vAxis: {
                ticks: [{ v: 0, f: 'Activity1 Gå en tur..' }, { v: 10, f: 'Activity2 Tage tapppen...' }, { v: 20, f: 'Activity3 Ligge på rygge...' }]
            },
            pointSize: 30,
            series: {
                0: { pointShape: 'circle' },
                1: { pointShape: 'triangle' },
                2: { pointShape: 'square' },
                3: { pointShape: 'diamond' },
                4: { pointShape: 'star' },
                5: { pointShape: 'polygon' }
            }
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("barchart_values"));
        chart.draw(view, options);
    };
    Dashboard.prototype.drawChartGantt = function () {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Task ID');
        data.addColumn('string', 'Task Name');
        data.addColumn('string', 'Resource');
        data.addColumn('date', 'Start Date');
        data.addColumn('date', 'End Date');
        data.addColumn('number', 'Duration');
        data.addColumn('number', 'Percent Complete');
        data.addColumn('string', 'Dependencies');
        data.addRows([
            ['2014Spring', 'Spring 2014', 'spring',
                new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
            ['2014Summer', 'Summer 2014', 'summer',
                new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
            ['2014Autumn', 'Autumn 2014', 'autumn',
                new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
            ['2014Winter', 'Winter 2014', 'winter',
                new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
            ['2015Spring', 'Spring 2015', 'spring',
                new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
            ['2015Summer', 'Summer 2015', 'summer',
                new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
            ['2015Autumn', 'Autumn 2015', 'autumn',
                new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
            ['2015Winter', 'Winter 2015', 'winter',
                new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
            ['Football', 'Football Season', 'sports',
                new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
            ['Baseball', 'Baseball Season', 'sports',
                new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
            ['Basketball', 'Basketball Season', 'sports',
                new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
            ['Hockey', 'Hockey Season', 'sports',
                new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
        ]);
        var options = {
            height: 400,
            gantt: {
                trackHeight: 30
            }
        };
        var chart = new google.visualization.Gantt(document.getElementById("gantt_chart"));
        chart.draw(data, options);
    };
    Dashboard.prototype.changeActivity = function (event) {
        console.log(event);
        var emojistatus;
        var eventstatus = event.target.form;
        for (var v = 0; v <= 3; v++) {
            if (eventstatus[v].checked) {
                if (eventstatus[v].value == "Not Done") {
                    emojistatus = "red";
                    $("#emoji1").removeClass();
                    $("#emoji1").addClass("dashboard__chart-emoji dashboard__chart-emoji--red");
                    $("#emoji1 i").removeClass();
                    $("#emoji1 i").addClass("fa fa-frown-o");
                }
                if (eventstatus[v].value == "Partially") {
                    emojistatus = "yellow";
                    $("#emoji1").removeClass();
                    $("#emoji1").addClass("dashboard__chart-emoji dashboard__chart-emoji--yellow");
                    $("#emoji1 i").removeClass();
                    $("#emoji1 i").addClass("fa fa-meh-o");
                }
                if (eventstatus[v].value == "Done") {
                    $("#emoji1").removeClass();
                    $("#emoji1").addClass("dashboard__chart-emoji dashboard__chart-emoji--green");
                    $("#emoji1 i").removeClass();
                    $("#emoji1 i").addClass("fa fa-smile-o");
                }
            }
        }
    };
    Dashboard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'dashboard',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/dashboard/dashboard.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Dashboard);
    return Dashboard;
}());



/***/ }),

/***/ "./src/components/delete/delete.ts":
/*!*****************************************!*\
  !*** ./src/components/delete/delete.ts ***!
  \*****************************************/
/*! exports provided: DeleteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteComponent", function() { return DeleteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");



var DeleteComponent = /** @class */ (function () {
    function DeleteComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DeleteComponent.prototype.cancel = function () {
        this.dialogRef.close();
    };
    DeleteComponent.ctorParameters = function () { return [
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
    ]; };
    DeleteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "delete-viewer",
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./delete.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/delete/delete.html")).default,
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object])
    ], DeleteComponent);
    return DeleteComponent;
}());



/***/ }),

/***/ "./src/components/detailspage/detailspage.ts":
/*!***************************************************!*\
  !*** ./src/components/detailspage/detailspage.ts ***!
  \***************************************************/
/*! exports provided: DetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsPage", function() { return DetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _models_localization_localization__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../models/localization/localization */ "./src/models/localization/localization.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var ckeditor4_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ckeditor4-angular */ "./node_modules/ckeditor4-angular/fesm2015/ckeditor4-angular.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../providers/utils/helperService */ "./src/providers/utils/helperService.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _delete_delete__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../delete/delete */ "./src/components/delete/delete.ts");

















var DetailsPage = /** @class */ (function () {
    function DetailsPage(service, dialog, activeModal, modalService, helperService, services, activatedRoute, auth, formBuilder, location, dateTimeAdapter) {
        this.service = service;
        this.dialog = dialog;
        this.activeModal = activeModal;
        this.modalService = modalService;
        this.helperService = helperService;
        this.services = services;
        this.activatedRoute = activatedRoute;
        this.auth = auth;
        this.formBuilder = formBuilder;
        this.location = location;
        this.dateTimeAdapter = dateTimeAdapter;
        this.selectedTab = 0;
        this.selectedContainer = 0;
        this.loading = false;
        this.tabstoDisplay = [];
        this.itemss = [];
        this.addresserrorMesg = false;
        this.enableaddresserrorMsg = false;
        this.highlightedIndex = 0;
        this.selectedStreet = "";
        this.AllFiles = [];
        this.readonly = false;
        this.opentoogle = false;
        this.buttons = [];
        this.config = {
            height: "300px",
            startupOutlineBlocks: true,
            removeButtons: "About,Image,Subscript,Table,Superscript,Anchor,Spellchecker,Removeformat",
            removeDialogTabs: "link:target;link:advanced;link:protocol",
            removeInfoTabs: "link:protocol",
            fontSize: 21,
        };
        this.confirmreplace = false;
        this.ngbModalOptions = {
            backdrop: "static",
            keyboard: false,
        };
        this.selectitem = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.dateTimeAdapter.setLocale("da-DK");
    }
    DetailsPage.prototype.ngOnInit = function () {
        this.loading = true;
        this.brugervendtnoegle = this.activatedRoute.snapshot.params.brugervendtnoegle;
        this.heading = this.services.getselectedMenu(); //this.activatedRoute.snapshot.data.title;
        this.listId = this.services.getlistId();
        this.uuid = this.services.getClickedRowuuiid();
        this.localization = new _models_localization_localization__WEBPACK_IMPORTED_MODULE_8__["Localization"]();
        this.environmentUrl = this.service.getEnvironment();
        this.localization = this.localization.localizationobj;
        this.getDetailedContent(this.uuid, this.listId);
    };
    DetailsPage.prototype.getDetailedContent = function (uuid, listId) {
        var _this = this;
        this.service.getDetailsdata(uuid, listId).then(function (detailsData) {
            _this.detailsData = detailsData;
            _this.setData();
        }, function (error) { _this.errorNotification.open(); console.log("try again"); });
    };
    DetailsPage.prototype.getHeaderInfo = function () {
        var details = this.detailsData;
        this.actions = this.detailsData.result.actions;
        this.buttons = this.actions && this.actions.buttons ? this.actions.buttons.reverse() : [];
        this.displayheadingName = details.status.displayname;
        this.services.setdisplayName(details.status.displayname);
        this.brugerName = details.status.brugernavn;
        this.readonly = details.status.readonly ? details.status.readonly : false;
        this.lastupdatedTime = details.status.update_timestamp
            ? details.status.update_timestamp
            : "";
        this.constrcutBreadcrum();
    };
    DetailsPage.prototype.constrcutBreadcrum = function () {
        if (!this.activatedRoute.snapshot.data.issubmenu) {
            this.services.addNavigation(this.displayheadingName);
        }
        if (this.activatedRoute.snapshot.data.issubmenu) {
            this.services.setselectedMenu(this.brugervendtnoegle);
            this.services.addNavigation(this.displayheadingName);
        }
        if (this.displayheadingName) {
            this.navigationElements = [this.displayheadingName];
        }
    };
    DetailsPage.prototype.setData = function () {
        var details = this.detailsData;
        this.getHeaderInfo();
        this.lastupdatedTimeStamp = details.status.update_timestamp;
        this.detailsForm = this.formBuilder.group({
            items: this.formBuilder.array([]),
        });
        this.addItem();
    };
    DetailsPage.prototype.addItem = function () {
        this.items = this.detailsForm.get("items");
        for (var i in this.detailsData.result.detaljer) {
            this.items.push(this.createDyanicForm(this.detailsData.result.detaljer[i]));
        }
        setTimeout(function () {
            $(".selectpicker").selectpicker();
        }, 100);
        this.loading = false;
        this.detailsform = this.detailsForm;
    };
    DetailsPage.prototype.createDyanicForm = function (item) {
        var tooltipinfo = this.detailsData.beskrivelser[item.ld_brugervendtnoegle];
        if (item.type == "input" &&
            item.ld_brugervendtnoegle != "adresser" &&
            item.type != "file" &&
            item.type != "date") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                readonly: item.readonly,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian
                    ? item.property.accordian
                    : false,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning,
            });
        }
        if (item.type === "accordion") {
            var accordians = [];
            var k = null;
            var accheading = item.titel;
            var lang = item.ld_brugervendtnoegle.split("_").pop();
            for (var j in item) {
                if (item[j] && item[j].property && item[j].property.accordion) {
                    k = {
                        name: item[j].titel,
                        tabIndex: this.tabIndex,
                        value: item[j].selected_values[0],
                        type: item[j].type,
                        required: item[j].mandatory,
                        tooltip: tooltipinfo,
                        hide: item[j].property && item[j].property.hide
                            ? item[j].property.hide
                            : false,
                        accordian: item[j].property && item[j].property.accordian
                            ? item[j].property.accordian
                            : false,
                        readonly: item[j].readonly,
                        ld_brugervendtnoegle: item[j].ld_brugervendtnoegle,
                        relationindeks: item[j].relationindeks,
                        selected_list: [item[j].selected_values],
                        relationsvirkning: item[j].relationsvirkning,
                    };
                    accordians.push(k);
                }
            }
            return this.formBuilder.group({
                accordian: true,
                title: accheading,
                lang: lang,
                allaccordians: [accordians],
            });
        }
        if (item.type == "textarea") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian
                    ? item.property.accordian
                    : false,
                readonly: item.readonly,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning,
            });
        }
        if (item.type == "html") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0].replace(/(?:\r\n|\r|\n)/g, "<br>"),
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                readonly: item.readonly,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning,
            });
        }
        if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
            this.selectedStreet = item.selected_values[0];
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian
                    ? item.property.accordian
                    : false,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning,
            });
        }
        if (item.type == "file") {
            this.selectedStreet = item.selected_values[0];
            this.documentuuid = item.dokument_uuid;
            var image = item.image ? item.image : null;
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian
                    ? item.property.accordian
                    : false,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                documentuuid: item.dokument_uuid,
                image: image,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning,
            });
        }
        if (item.type == "date") {
            var formatdate = null;
            var minimumdate = null;
            this.mindate = this.helperService.formatDate(null);
            if (item.selected_values &&
                item.selected_values[0] &&
                item.selected_values[0].length != 0) {
                formatdate = this.helperService.formatDate(item.selected_values[0]);
            }
            else {
                if (item.property &&
                    item.property.future === "true" &&
                    item.property.past === "false") {
                    formatdate = this.helperService.formatDate(null);
                    minimumdate = formatdate;
                }
            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: formatdate ? formatdate : null,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian
                    ? item.property.accordian
                    : false,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values[0]],
                formatdate: formatdate ? formatdate : null,
                mindate: minimumdate,
                relationsvirkning: item.relationsvirkning,
            });
        }
        if (item.type == "dropdown" && item.relationsvirkning === false) {
            if (item.multiselect === "false") {
                var selected_name = void 0;
                var selected_names = [];
                for (var i in item.valuelist) {
                    selected_name =
                        item.valuelist[i].selected === true
                            ? item.valuelist[i].rel_uri
                            : null;
                    if (selected_name) {
                        selected_names.push(selected_name);
                    }
                }
                return this.formBuilder.group({
                    name: item.titel,
                    tabIndex: this.tabIndex,
                    options: [item.valuelist],
                    multiselect: item.multiselect,
                    tooltip: tooltipinfo,
                    hide: item.property && item.property.hide ? item.property.hide : false,
                    accordian: item.property && item.property.accordian
                        ? item.property.accordian
                        : false,
                    type: item.type,
                    readonly: item.readonly,
                    selected_name: selected_names,
                    required: item.mandatory,
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    relationindeks: item.relationindeks,
                    valuelist: item.valuelist,
                    relationsvirkning: item.relationsvirkning,
                });
            }
            if (item.multiselect === "true") {
                var selected_name = void 0;
                var selected_names = [];
                for (var i in item.valuelist) {
                    selected_name =
                        item.valuelist[i].selected === true
                            ? item.valuelist[i].rel_uri
                            : null;
                    if (selected_name) {
                        selected_names.push(selected_name);
                    }
                }
                return this.formBuilder.group({
                    name: item.titel,
                    tabIndex: this.tabIndex,
                    options: [item.valuelist],
                    multiselect: item.multiselect,
                    tooltip: tooltipinfo,
                    hide: item.property && item.property.hide ? item.property.hide : false,
                    accordian: item.property && item.property.accordian
                        ? item.property.accordian
                        : false,
                    type: item.type,
                    readonly: item.readonly,
                    selected_name: [selected_names],
                    required: item.mandatory,
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    relationindeks: item.relationindeks,
                    valuelist: item.valuelist,
                    relationsvirkning: item.relationsvirkning,
                });
            }
        }
        if (item.type == "dropdown" && item.relationsvirkning === true) {
            for (var i in item.valuelist) {
                var intervalitem = {
                    fra: item.valuelist[i].fra,
                    til: item.valuelist[i].til,
                    note: item.valuelist[i].note,
                };
                item.valuelist[i].intervals = [intervalitem];
            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                options: [item.valuelist],
                multiselect: item.multiselect,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian
                    ? item.property.accordian
                    : false,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                valuelist: item.valuelist,
                selected_list: [item.selected_values],
                // selected_names: [selected_names],
                //selected_names: selected_names && selected_names.length ? [selected_names]:null,
                selected_values: [item.selected_values],
                relationsvirkning: item.relationsvirkning,
            });
        }
    };
    DetailsPage.prototype.downloadFile = function (event) {
        var _this = this;
        event.preventDefault();
        // detailsData => { this.detailsData = detailsData; this.setData() },
        this.service.downloadFileDocument(this.listId, this.documentuuid).then(function (res) {
            _this.downloadDetails = res;
            _this.getFile(res);
            _this.successNotification.open();
            _this.loading = false;
        }, function (error) {
            _this.errorNotification.open();
            _this.errorMessage = error.status.besked;
            _this.loading = false;
        });
    };
    DetailsPage.prototype.getFile = function (result) {
        var temp_Url = result.result.temp_path;
        var envi = this.service.getEnvironment();
        var e = envi.indexOf("/api");
        var formurl = envi.substring(0, e);
        var tempindex = temp_Url.lastIndexOf("temp");
        var filedownload_url = temp_Url.substring(tempindex, temp_Url.length);
        var downloadURL = formurl.concat("/" + filedownload_url);
        window.open("" + downloadURL, "_blank");
        //let openWindow:any = this.windowrefService.nativeWindow();
        // openWindow.open(''+downloadURL, '_blank');
        //openWindow.location.replace(''+downloadURL);
    };
    DetailsPage.prototype.Select = function (event, itemname) {
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0];
        for (var j in itemname.value.options) {
            if (itemname.value.selected_name === filtereditem.options[j].rel_uri) {
                filtereditem.options[j].selected = true;
                this.selected_reluuid = filtereditem.options[j].rel_uuid;
            }
            else {
                filtereditem.options[j].selected = false;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0] = filtereditem;
        if (itemname.value.ld_brugervendtnoegle === "kampagneskabeloner" &&
            this.selected_reluuid != null) {
            var modalref = this.modalService.open(this.warning, this.ngbModalOptions);
            this.services.setmodalReference(modalref);
        }
        $(".invalid-feedback." + itemname.value.ld_brugervendtnoegle).addClass("hide");
    };
    DetailsPage.prototype.Multiselect = function (event, itemname) {
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0];
        for (var j in itemname.value.options) {
            if (itemname.value.selected_name.indexOf(itemname.value.options[j].rel_uri) != -1) {
                filtereditem.options[j].selected = true;
            }
            else {
                filtereditem.options[j].selected = false;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0] = filtereditem;
        $(".invalid-feedback." + itemname.value.ld_brugervendtnoegle).addClass("hide");
    };
    DetailsPage.prototype.Multiselectrelation = function (event, item, optionitem) {
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === optionitem.rel_uuid) {
                filtereditem.options[j] = optionitem;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = filtereditem;
    };
    DetailsPage.prototype.ErrorMsg = function () {
        if (this.itemss.length > 0) {
            this.addresserrorMesg = false;
            this.enableaddresserrorMsg = false;
        }
        if (this.selectedStreet && this.selectedStreet.length > 0) {
            this.addresserrorMesg = false;
            this.enableaddresserrorMsg = false;
        }
        else {
            this.addresserrorMesg = true;
            this.enableaddresserrorMsg = true;
        }
    };
    DetailsPage.prototype.onItems = function (items) {
        this.itemss = items;
    };
    DetailsPage.prototype.onItemHighlighted = function (index) {
        this.highlightedIndex = index;
        if (index == 0) {
            this.addresserrorMesg = true;
            this.enableaddresserrorMsg = true;
        }
    };
    DetailsPage.prototype.onItemSelected = function (item) {
        this.itemss = [];
        this.highlightedIndex = 0;
        //this.selectedStreet = item.fullStreet;
        this.selectedStreet = item.text;
        // this.ref.detectChanges();
        this.addresserrorMesg = false;
        this.enableaddresserrorMsg = false;
    };
    DetailsPage.prototype.selectFile = function (event, item) {
        item.value.value = event.target.files[0].name;
        this.fileNavn = event.target.files[0].name;
        this.File = event.target.files[0];
        var fileitem = {
            name: item.value.ld_brugervendtnoegle,
            value: this.File,
        };
        this.AllFiles.push(fileitem);
        // let selected_index = event.args.item.index;
        // let selected_value = event.args.item.value;
        var selected_item = item.value.name;
        var item = this.detailsForm.controls.items.value.filter(function (x) { return x.name == selected_item; })[0];
        item.value = this.fileNavn;
        this.detailsForm.controls.items.value.filter(function (x) { return x.name == selected_item; })[0] = item;
    };
    DetailsPage.prototype.formatData = function (item) {
        if (item) {
            if (item.type == "input" && item.ld_brugervendtnoegle != "adresser") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                };
            }
            if (item.type == "editor") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                };
            }
            if (item.accordian && item.allaccordians) {
                var allaccords_1 = [];
                item.allaccordians.forEach(function (element) {
                    allaccords_1.push({
                        ld_brugervendtnoegle: element.ld_brugervendtnoegle,
                        value: [element.value],
                    });
                });
                return allaccords_1;
            }
            if (item.type == "textarea") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                };
            }
            if (item.type == "html") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value.replace(/(?:\r\n|\r|\n)/g, "<br>")],
                };
            }
            if (item.type == "date") {
                var formateddate = null;
                if (item.formatdate) {
                    formateddate = moment__WEBPACK_IMPORTED_MODULE_11__(item.formatdate).format("YYYY-MM-DD HH:mm:ss");
                    if (formateddate === "Invalid date") {
                        formateddate = null;
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: formateddate ? [formateddate] : [null],
                };
            }
            if (item.type == "file") {
                var image = item.image ? item.image : null;
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                    image: image,
                };
            }
            if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [this.selectedStreet],
                    relationindeks: item.relationindeks,
                };
            }
            if (item.type == "dropdown" &&
                item.multiselect == "false" &&
                item.relationsvirkning === false) {
                var optionitem = item.options.filter(function (x) { return x.selected === true; })[0];
                if (optionitem) {
                    return {
                        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                        value: [optionitem.rel_uuid],
                    };
                }
                else {
                    return {
                        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                        value: [null],
                    };
                }
            }
            if (item.type == "dropdown" &&
                item.multiselect == "true" &&
                item.relationsvirkning === false) {
                var values = [];
                if (item.selected_name && item.selected_name.length != 0) {
                    for (var i in item.options) {
                        if (item.selected_name.indexOf(item.options[i].rel_uri) != -1) {
                            values.push(item.options[i].rel_uuid);
                        }
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: values,
                };
            }
            if (item.type == "dropdown" &&
                item.multiselect == "true" &&
                item.relationsvirkning === true) {
                var values = [];
                if (item.options && item.options.length != 0) {
                    for (var i in item.options) {
                        if (item.options[i].selected === true) {
                            for (var j in item.options[i].intervals) {
                                var fra = null;
                                var til = null;
                                if (item.options[i].intervals[j].fra) {
                                    item.options[i].intervals[j].fra = moment__WEBPACK_IMPORTED_MODULE_11__(item.options[i].intervals[j].fra).format("YYYY-MM-DD HH:mm:ss");
                                }
                                if (item.options[i].intervals[j].til) {
                                    item.options[i].intervals[j].til = moment__WEBPACK_IMPORTED_MODULE_11__(item.options[i].intervals[j].til).format("YYYY-MM-DD HH:mm:ss");
                                }
                                var slct = {
                                    rel_uri: item.options[i].rel_uri,
                                    rel_uuid: item.options[i].rel_uuid,
                                    fra: fra,
                                    til: til,
                                    note: item.options[i].intervals[j].note,
                                };
                                values.push(slct);
                            }
                        }
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: values,
                };
            }
        }
    };
    DetailsPage.prototype.dateChange = function (item, index) {
        var dateformat = null;
        if (index && index.selected) {
            dateformat = moment__WEBPACK_IMPORTED_MODULE_11__(index.selected._d).format("YYYY-MM-DD HH:mm:ss");
        }
        var slcitem = item.value.ld_brugervendtnoegle;
        item.value.value = dateformat;
        item.value.formatdate = dateformat;
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == slcitem; })[0] = item;
    };
    DetailsPage.prototype.clearDate = function (item) {
        item.value.value = null;
        item.value.formatdate = null;
        var slcitem = item.value.ld_brugervendtnoegle;
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == slcitem; })[0] = item;
    };
    DetailsPage.prototype.dateEditfra = function (item, intervalitem, optionitem, index, j) {
        var dateformat = null;
        if (intervalitem && intervalitem.fra) {
            dateformat = moment__WEBPACK_IMPORTED_MODULE_11__(intervalitem.til._d).format("YYYY-MM-DD HH:mm:ss");
        }
        intervalitem.fra = dateformat;
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j_1 in item.value.options.intervals) {
            if (item.value.options[j_1].rel_uri === optionitem.rel_uri) {
                filtereditem.options[j_1].intervalitem[index].fra = dateformat;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = item;
        $(".invalid-feedback." + item.value.ld_brugervendtnoegle).addClass("hide");
    };
    DetailsPage.prototype.dateEdittil = function (item, intervalitem, optionitem, index, j) {
        var dateformat = null;
        if (intervalitem && intervalitem.til) {
            dateformat = moment__WEBPACK_IMPORTED_MODULE_11__(intervalitem.til._d).format("YYYY-MM-DD HH:mm:ss");
        }
        intervalitem.til = dateformat;
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j_2 in item.value.options.intervalitem) {
            if (item.value.options[j_2].rel_uri === optionitem.rel_uri) {
                filtereditem.options[j_2].intervalitem[index].til = dateformat;
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = item;
        $(".invalid-feedback." + item.value.ld_brugervendtnoegle).addClass("hide");
    };
    DetailsPage.prototype.Deletedateitem = function (e, item, relationoptionitem, index) {
        e.preventDefault();
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === relationoptionitem.rel_uuid) {
                filtereditem.options[j].intervals.splice(index, 1);
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = filtereditem;
    };
    DetailsPage.prototype.tooglepakker = function (e) {
        e.preventDefault();
        this.opentoogle = this.opentoogle === false ? true : false;
    };
    DetailsPage.prototype.Createdateitem = function (e, item, relationoptionitem) {
        e.preventDefault();
        var filtereditem = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0];
        for (var j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === relationoptionitem.rel_uuid) {
                var intervalitem = {
                    fra: null,
                    til: null,
                    note: "",
                };
                filtereditem.options[j].intervals.push(intervalitem);
            }
        }
        this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = filtereditem;
    };
    DetailsPage.prototype.onchngEditordata = function (editor) {
        var data = editor.editor.getData();
        this.editorInstance = editor;
    };
    DetailsPage.prototype.openfilter = function (modal, event, item) {
        this.fletfilters = this.detailsForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == "flettefelter"; })[0];
        var modalref = this.modalService.open(modal, this.ngbModalOptions);
        this.services.setmodalReference(modalref);
    };
    DetailsPage.prototype.setDatatoEditor = function (data) {
        this.editorInstance.editor.insertHtml(data, "text");
    };
    DetailsPage.prototype.replaceeditorContent = function (confirmreplace) {
        var _this = this;
        if (confirmreplace === true) {
            this.service.getskabeloncontent(this.selected_reluuid, "en").then(function (skabeloncontent) {
                // this.editorInstance.editor.insertHtml(skabeloncontent, 'text');
                var filteredaccordian = _this.detailsForm.controls.items.value.filter(function (x) { return x.lang === "en"; })[0];
                filteredaccordian.allaccordians.map(function (item) {
                    if (item.ld_brugervendtnoegle.indexOf("titel") != -1) {
                        item.selected_list[0] = skabeloncontent.title;
                        item.value = skabeloncontent.title;
                    }
                    if (item.ld_brugervendtnoegle.indexOf("tekst") != -1) {
                        item.selected_list[0] = skabeloncontent.body[0];
                        item.value = skabeloncontent.body[0];
                    }
                });
                _this.detailsForm.controls.items.value.filter(function (x) { return x.lang === "en"; })[0] = filteredaccordian;
            }, function (error) {
                console.log(error);
                _this.errorNotification.open();
                _this.loading = false;
            });
            this.service.getskabeloncontent(this.selected_reluuid, "da").then(function (skabeloncontent) {
                var filteredaccordian = _this.detailsForm.controls.items.value.filter(function (x) { return x.lang === "da"; })[0];
                filteredaccordian.allaccordians.map(function (item) {
                    if (item.ld_brugervendtnoegle.indexOf("titel") != -1) {
                        item.selected_list[0] = skabeloncontent.title;
                        item.value = skabeloncontent.title;
                    }
                    if (item.ld_brugervendtnoegle.indexOf("tekst") != -1) {
                        item.selected_list[0] = skabeloncontent.body[0];
                        item.value = skabeloncontent.body[0];
                    }
                });
                _this.detailsForm.controls.items.value.filter(function (x) { return x.lang === "da"; })[0] = filteredaccordian;
            }, function (error) {
                _this.errorNotification.open();
                console.log(error);
                _this.loading = false;
            });
        }
    };
    DetailsPage.prototype.cancel = function () {
        this.activeModal.dismissAll('cancelled');
    };
    DetailsPage.prototype.saveData = function () {
        var _this = this;
        if (!this.detailForm.nativeElement.checkValidity()) {
            this.detailForm.nativeElement.classList.add("was-validated");
        }
        var formvalid = $(".invalid-feedback").is(":visible");
        if (!formvalid) {
            var formvalues = JSON.stringify(this.detailsForm.controls.items.value);
            var formdata = [];
            formdata.push(this.detailsForm.controls.items.value.map(this.formatData, this));
            var submitting_1 = [];
            formdata[0].map(function (e) {
                if (e && e.ld_brugervendtnoegle) {
                    submitting_1.push(e);
                }
                else {
                    e.map(function (el) {
                        if (el && el.ld_brugervendtnoegle) {
                            submitting_1.push(el);
                        }
                    });
                }
            });
            this.loading = true;
            this.service
                .updateDetailsdata(this.listId, this.uuid, this.detailsData.status.update_timestamp, JSON.stringify(submitting_1), this.AllFiles)
                .then(function (res) {
                _this.loading = false;
                _this.modalService.dismissAll('saved');
                _this.successNotification.open();
            }, function (error) {
                _this.loading = false;
                _this.errorNotification.open();
                var error_messsage = error.error.status.besked;
                _this.errorMessage = error_messsage;
            }, function (complete) {
                _this.loading = false;
            });
        }
    };
    DetailsPage.prototype.Delete = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_delete_delete__WEBPACK_IMPORTED_MODULE_16__["DeleteComponent"], {
            panelClass: "delete_popup",
            data: { uuid: this.uuid, displayheadingName: this.displayheadingName },
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result && result.uuid) {
                _this.service
                    .deleteDetailsdata(_this.listId, _this.uuid, _this.lastupdatedTimeStamp)
                    .then(function (res) {
                    _this.modalService.dismissAll('saved');
                    _this.successNotification.open();
                    _this.loading = false;
                }, function (error) {
                    _this.errorNotification.open();
                    _this.errorMessage = error.status.besked;
                    _this.loading = false;
                }, function (complete) {
                    _this.loading = false;
                });
            }
        });
    };
    DetailsPage.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__["MatDialog"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModal"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModal"] },
        { type: _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_14__["HelperSerice"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_9__["AuthRestService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"] },
        { type: ng_pick_datetime__WEBPACK_IMPORTED_MODULE_10__["DateTimeAdapter"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("successNotification", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], DetailsPage.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("errorNotification", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], DetailsPage.prototype, "errorNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("detailForm", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DetailsPage.prototype, "detailForm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DetailsPage.prototype, "selectitem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"])("editor"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ckeditor4_angular__WEBPACK_IMPORTED_MODULE_12__["CKEditorComponent"])
    ], DetailsPage.prototype, "editorComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("warning", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], DetailsPage.prototype, "warning", void 0);
    DetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "detailspage",
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./detailspage.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/detailspage/detailspage.html")).default,
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__["MatDialog"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModal"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModal"],
            _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_14__["HelperSerice"],
            _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_9__["AuthRestService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"],
            ng_pick_datetime__WEBPACK_IMPORTED_MODULE_10__["DateTimeAdapter"]])
    ], DetailsPage);
    return DetailsPage;
}());



/***/ }),

/***/ "./src/components/filter/filter.ts":
/*!*****************************************!*\
  !*** ./src/components/filter/filter.ts ***!
  \*****************************************/
/*! exports provided: filterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterComponent", function() { return filterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var ang_jsoneditor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ang-jsoneditor */ "./node_modules/ang-jsoneditor/fesm5/ang-jsoneditor.js");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");








var filterComponent = /** @class */ (function () {
    function filterComponent(auth, baserestService, services, activeModal) {
        var _this = this;
        this.auth = auth;
        this.baserestService = baserestService;
        this.services = services;
        this.activeModal = activeModal;
        this.createnewmodel = false;
        this.overwritemodel = false;
        this.loading = false;
        this.filternavnmodelerror = false;
        this.allredaktoerList = [{
                value: 'Intet valgt',
                key: 0
            }];
        this.filternavnenable = true;
        this.filterdata = null;
        this.filtereditorOptions = new ang_jsoneditor__WEBPACK_IMPORTED_MODULE_4__["JsonEditorOptions"]();
        this.filtereditorOptions.mode = "view";
        this.services.filtersdata.subscribe(function (filterdata) {
            _this.filterdata = filterdata;
        });
    }
    filterComponent.prototype.ngOnInit = function () {
        this.loading = true;
    };
    filterComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.baserestService.getEditors().then(function (allredaktoerList) {
            _this.allredaktoerList = allredaktoerList;
            _this.loading = false;
            $('.selectpicker').selectpicker('refresh');
        }, function (error) {
            console.log(error);
            _this.loading = false;
        });
    };
    filterComponent.prototype.close = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    filterComponent.prototype.createnew = function (e) {
        e.preventDefault();
        e.target.checked = true;
        this.createnewmodel = true;
        this.overwritemodel = false;
        this.filternavnenable = false;
        this.filteruuid = 0;
        this.choosefilter.nativeElement.setAttribute('disabled', true);
        $('#choosefilter').val('').selectpicker('refresh');
    };
    filterComponent.prototype.overwrite = function (e) {
        e.preventDefault();
        e.target.checked = true;
        this.createnewmodel = false;
        this.overwritemodel = true;
        this.filternavnmodelerror = false;
        this.choosefilter.nativeElement.setAttribute('disabled', false);
        this.filternavnenable = true;
        $(".selectpicker").prop("disabled", false);
        $('.selectpicker').selectpicker('refresh');
    };
    filterComponent.prototype.getFilterData = function (e, item) {
        //FUTURE TO-DO
        //this.loading = true;
        // this.baserestService.readfilterdata(this.filteruuid, this.autorisation).then(
        //     (filtersdata: any) => {
        //         console.log(filtersdata)
        //         // this.filtersdata = filtersdata;
        //         // this.body = filtersdata;
        //         $('.selectpicker').selectpicker('refresh');
        //         this.loading = false;
        //     },
        //     (error) => { console.log(error); this.loading = false; }
        // )
    };
    filterComponent.prototype.saveFilter = function (e) {
        if (this.createnewmodel === true && this.filternavnmodel) {
            this.save();
        }
        if (this.overwritemodel === true && (this.filteruuid && this.filteruuid != 0)) {
            this.save();
        }
    };
    filterComponent.prototype.save = function () {
        var _this = this;
        if (this.allredaktoerslctd) {
            this.loading = true;
            this.skema_uuid = this.services.getlistId();
            var requestdata = {};
            if (this.createnewmodel === true) {
                var filterobjekt = {};
                if (this.filtersdata && this.filtersdata.filters) {
                    requestdata = {
                        titel: this.filternavnmodel,
                        knapnavn: this.services.getselectedmenuTitle(),
                        skema_uuid: this.skema_uuid,
                        redaktoer: this.allredaktoerslctd
                    };
                    filterobjekt = this.filtersdata;
                    requestdata.filter = JSON.stringify(filterobjekt);
                }
                else {
                    filterobjekt = {
                        filters: this.filterdata,
                    };
                    var filtered = filterobjekt;
                    requestdata = {
                        titel: this.filternavnmodel,
                        knapnavn: this.services.getselectedmenuTitle(),
                        skema_uuid: this.skema_uuid,
                        redaktoer: this.allredaktoerslctd
                    };
                    requestdata.filter = JSON.stringify(filtered);
                }
            }
            this.baserestService.savefilterdata(JSON.stringify(requestdata)).then(function (success) {
                _this.getFilters();
            }, function (error) {
                _this.error(error);
            });
        }
    };
    filterComponent.prototype.getFilters = function () {
        var _this = this;
        this.baserestService.getmenufilters(this.skema_uuid).then(function (filters) {
            var newupdatedfilterList = [{
                    titel: 'Intet valgt',
                    uuid: 0
                }];
            newupdatedfilterList = newupdatedfilterList.concat(filters.data);
            _this.services.setnewallfilterList(newupdatedfilterList);
            _this.Success();
        }, function (error) { _this.loading = false; console.log(error); });
    };
    filterComponent.prototype.Success = function () {
        this.successNotification.refresh();
        this.successNotification.open();
        this.activeModal.dismissAll();
        this.loading = false;
    };
    filterComponent.prototype.error = function (error) {
        console.log(error);
        this.errorNotification.refresh();
        this.errorNotification.open();
        this.loading = false;
    };
    filterComponent.prototype.cancel = function () {
        this.activeModal.dismissAll();
    };
    filterComponent.ctorParameters = function () { return [
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__["AuthRestService"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_6__["Services"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ang_jsoneditor__WEBPACK_IMPORTED_MODULE_4__["JsonEditorComponent"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ang_jsoneditor__WEBPACK_IMPORTED_MODULE_4__["JsonEditorComponent"])
    ], filterComponent.prototype, "editor", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('choosefilter', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], filterComponent.prototype, "choosefilter", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('availablefor', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], filterComponent.prototype, "availablefor", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('filternavn', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], filterComponent.prototype, "filternavn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_7__["jqxNotificationComponent"])
    ], filterComponent.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_7__["jqxNotificationComponent"])
    ], filterComponent.prototype, "errorNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], filterComponent.prototype, "allfilterList", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], filterComponent.prototype, "filtersdata", void 0);
    filterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'filter-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./filter.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/filter/filter.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__["AuthRestService"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"], _providers_services_services__WEBPACK_IMPORTED_MODULE_6__["Services"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"]])
    ], filterComponent);
    return filterComponent;
}());



/***/ }),

/***/ "./src/components/fletfilters/fletfilters.ts":
/*!***************************************************!*\
  !*** ./src/components/fletfilters/fletfilters.ts ***!
  \***************************************************/
/*! exports provided: fletfiltersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fletfiltersComponent", function() { return fletfiltersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");




var fletfiltersComponent = /** @class */ (function () {
    // private loading=false;
    // @ViewChild('successNotification', { static: true }) successNotification: jqxNotificationComponent;
    // @ViewChild('errorNotification', { static: true }) errorNotification: jqxNotificationComponent;
    function fletfiltersComponent(activeModal, services) {
        this.activeModal = activeModal;
        this.services = services;
        this.selectedfletfilters = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    fletfiltersComponent.prototype.ngOnInit = function () {
        setTimeout(function () {
            $('.selectpicker').selectpicker();
        }, 100);
    };
    fletfiltersComponent.prototype.cancel = function (e) {
        this.modalReference = this.services.getmodalReference();
        if (this.modalReference) {
            this.modalReference.close();
        }
        else {
            this.activeModal.dismissAll();
        }
    };
    fletfiltersComponent.prototype.apply = function (e) {
        console.log(e);
    };
    fletfiltersComponent.prototype.setFilter = function (e, item) {
        this.selectedfletfilters.emit(this.rel_uuid);
        this.modalReference = this.services.getmodalReference();
        if (this.modalReference) {
            this.modalReference.close();
        }
        else {
            this.activeModal.dismissAll();
        }
    };
    fletfiltersComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], fletfiltersComponent.prototype, "fletfilters", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], fletfiltersComponent.prototype, "selectedfletfilters", void 0);
    fletfiltersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'fletfilters-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./fletfilters.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/fletfilters/fletfilters.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"], _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"]])
    ], fletfiltersComponent);
    return fletfiltersComponent;
}());



/***/ }),

/***/ "./src/components/footer/footer-page.ts":
/*!**********************************************!*\
  !*** ./src/components/footer/footer-page.ts ***!
  \**********************************************/
/*! exports provided: FooterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterPage", function() { return FooterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterPage = /** @class */ (function () {
    function FooterPage() {
    }
    FooterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'footer-page',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./footer-page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/footer/footer-page.html")).default
        })
    ], FooterPage);
    return FooterPage;
}());



/***/ }),

/***/ "./src/components/ganttchart/ganttchart.ts":
/*!*************************************************!*\
  !*** ./src/components/ganttchart/ganttchart.ts ***!
  \*************************************************/
/*! exports provided: GanttChart */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GanttChart", function() { return GanttChart; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _amcharts_amcharts3_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @amcharts/amcharts3-angular */ "./node_modules/@amcharts/amcharts3-angular/es2015/index.js");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");






// declare var jquery: any;
// declare var $: any;
// declare var google: any;
var GanttChart = /** @class */ (function () {
    function GanttChart(AmCharts, activatedRoute, services, datepipe) {
        this.AmCharts = AmCharts;
        this.activatedRoute = activatedRoute;
        this.services = services;
        this.datepipe = datepipe;
        this.chartstartDate = null;
    }
    GanttChart.prototype.ngOnInit = function () {
        this.parentheading = this.services.getselectedMenu();
        if (this.parentheading != "Personer") {
            this.parentheading = null;
        }
        this.parentheading = this.services.getselectedMenu();
        this.displayname = this.services.getdisplayName();
        this.heading = this.activatedRoute.snapshot.data.title;
        if (!this.activatedRoute.snapshot.data.issubmenu) {
            console.log("submenu false");
            this.services.setselectedMenu(this.heading);
        }
        //this.services.setselectedMenu(this.heading);
        this.selectedMenu = this.activatedRoute.snapshot.routeConfig.path;
        this.uuid = this.activatedRoute.snapshot.data.listId;
        this.services.setlistId(this.uuid);
        this.clickedrow = this.services.getClickedRowuuiid();
        console.log(this.activatedRoute.snapshot);
        //this.uuid = 'e4c4c23f-0c51-4ee4-8bc3-f70c27e5b786';//this.router.routerState.snapshot.root.queryParams['uuid'];
        console.log(this.uuid);
        this.getValueFromObservable();
    };
    GanttChart.prototype.breadCrumb = function () {
        if (!this.activatedRoute.snapshot.data.issubmenu) {
            console.log("submenu false");
            this.services.clearNavigation();
            this.services.setselectedMenu(this.heading);
            if (this.services.getContext() != null) {
                this.services.removeNavigation();
            }
            this.services.addNavigation(this.heading);
        }
        if (this.activatedRoute.snapshot.data.issubmenu && this.services.getContext() == null) {
            console.log("submenu true");
            this.services.setselectedMenu(this.heading);
            if (this.services.getContext() == null && this.services.getNavigation().length > 1) {
                this.services.removeNavigation();
            }
            this.services.addNavigation(this.heading);
        }
        if (this.activatedRoute.snapshot.data.issubmenu && this.services.getContext() != null) {
            if (this.services.getNavigation().length == 3) {
                this.services.removeNavigation();
            }
            this.services.addNavigation(this.heading);
        }
        this.navigationElements = this.services.getNavigation();
    };
    GanttChart.prototype.getValueFromObservable = function () {
        this.breadCrumb();
        // this.services.getGriddata(this.uuid, this.gridData, '', this.clickedrow).then(
        //   res => { this.gridrowsandColumns = res; this.setData() },
        //   error => { this.dataReceived() },
        //   complete => this.dataReceived()
        // );
    };
    GanttChart.prototype.setData = function () {
        console.log(this.gridrowsandColumns.result.objekt_liste);
        this.constructChart();
        var chartdata = this.gridrowsandColumns.result.objekt_liste;
        var formatteddata = [];
        var chartItem;
        var temparry = [];
        var segments;
        var chartdate;
        for (var i in chartdata) {
            console.log(chartdata[i].ansvarlig);
            if (chartdata[i].ansvarlig) {
                var startdate = this.datepipe.transform(chartdata[i].fra, 'yyyy-MM-dd');
                var enddate = this.datepipe.transform(chartdata[i].til, 'yyyy-MM-dd');
                // let duration = startdate - enddate;
                var task = chartdata[i].emne + '-' + chartdata[i].udfoerer;
                //let task = chartdata[i].brugervendtnoegle; 
                var startdatee = new Date(startdate);
                var enddatee = new Date(enddate);
                var duration = enddatee.getTime() - startdatee.getTime();
                this.chart;
                //getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.
                duration = Math.round(Math.abs(duration / (1000 * 60 * 60 * 24)));
                var segmentColor = "";
                //let startdateee = this.AmCharts.formatDate(new Date(2014, 1, 3), "YYYY MMM, YYYY");
                if (chartdata[i].aktivitetstype && chartdata[i].aktivitetstype == "Mødeaktivitet") {
                    segmentColor = "#46615e";
                }
                if (chartdata[i].aktivitetstype && chartdata[i].aktivitetstype == "Udredningsaktivitet") {
                    segmentColor = "#727d6f";
                }
                if (chartdata[i].aktivitetstype && chartdata[i].aktivitetstype == "Indsatsaktivitet") {
                    segmentColor = "#8dc49f";
                }
                if (chartdata[i].aktivitetstype && chartdata[i].aktivitetstype == "Observationsaktivitet") {
                    segmentColor = "#FFE4C4";
                }
                // let startdateCal = startdate.substring(startdate.lastIndexOf('-') + 1, startdate.length);
                var startdateCal = startdate.substring(startdate.indexOf('-') + 1, startdate.lastIndexOf('-'));
                console.log(startdateCal);
                chartItem = {
                    "category": chartdata[i].ansvarlig,
                    "segments": []
                };
                if (this.chartstartDate == null) {
                    this.chartstartDate = startdatee;
                }
                if (this.chartstartDate) {
                    if (this.chartstartDate < startdatee) {
                    }
                    if (this.chartstartDate > startdatee) {
                        this.chartstartDate = startdatee;
                    }
                }
                segments = {
                    "start": startdatee,
                    "duration": duration,
                    "color": segmentColor,
                    "task": task
                };
                // if(chartdata[i].brugervendtnoegle=="Udredning af Rasmus Geertsen"){
                //   segments = {
                //     "start": 900,
                //     "duration": 4,
                //     "color": segmentColor,
                //     "task": task
                //   }
                // }else{
                //   segments = {
                //     "start": startdateCal,
                //     "duration": duration,
                //     "color": segmentColor,
                //     "task": task
                //   }
                // }
                // if (formatteddata.filter(x => x.category == chartItem.category)[0]){
                //   console.log(formatteddata);
                //  return chartItem.segements;
                // }
                chartItem.segments.push(segments);
                if (formatteddata.length == 0) {
                    formatteddata.push(chartItem);
                }
                if (formatteddata.length >= 1) {
                    if (!formatteddata.filter(function (x) { return x.category == chartItem.category; })[0]) {
                        formatteddata.push(chartItem);
                    }
                    //    if (formatteddata.filter(x => x.category == chartItem.category)[0]) {
                    formatteddata.filter(function (x) {
                        if (x.category == chartItem.category) {
                            if (chartItem.segments[0]) {
                                if (x.segments[0].color != chartItem.segments[0].color) {
                                    x.segments.push(chartItem.segments[0]);
                                }
                            }
                        }
                    });
                    console.log(formatteddata);
                    console.log(chartItem);
                    console.log(chartItem.segements);
                    console.log(formatteddata);
                }
            }
        }
        this.allcategories = formatteddata;
        this.constructChart();
    };
    GanttChart.prototype.constructChart = function () {
        console.log(this.chartstartDate);
        var chartiniitalDate = this.chartstartDate;
        if (this.chartstartDate) {
            var month = this.chartstartDate.getMonth() + 1;
            var date = this.chartstartDate.getDate();
            var year = this.chartstartDate.getFullYear();
            if (month < 10) {
                month = '0' + month;
            }
            if (date < 10) {
                date = '0' + date;
            }
            this.chartstartDate = year + '-' + month + '-' + date;
            this.chartstartDate.toString();
        }
        for (var i in this.allcategories) {
            for (var s in this.allcategories[i].segments) {
                var sdate = new Date(this.allcategories[i].segments[s].start);
                var edate = new Date(chartiniitalDate);
                var duration = sdate.getTime() - edate.getTime();
                duration = Math.round(Math.abs(duration / (1000 * 3600 * 24)));
                console.log(duration);
                this.allcategories[i].segments[s].start = duration;
            }
        }
        this.chart = this.AmCharts.makeChart("chartdiv", {
            "type": "gantt",
            "theme": "light",
            "marginRight": 70,
            "period": "DD",
            "dataDateFormat": "YYYY-MM-DD",
            "balloonDateFormat": "JJ:NN",
            "columnWidth": 0.5,
            "valueAxis": {
                "type": "date"
            },
            "brightnessStep": 10,
            "graph": {
                "fillAlphas": 1,
                "balloonText": "<b>[[task]]</b>: [[open]] [[value]]"
            },
            "rotate": true,
            "categoryField": "category",
            "segmentsField": "segments",
            "colorField": "color",
            "startDate": this.chartstartDate ? this.chartstartDate : null,
            "startField": "start",
            "endField": "end",
            "durationField": "duration",
            "dataProvider": this.allcategories,
            "valueScrollbar": {
                "autoGridCount": true
            },
            "chartCursor": {
                "cursorColor": "#55bb76",
                "valueBalloonsEnabled": false,
                "cursorAlpha": 0,
                "valueLineAlpha": 0.5,
                "valueLineBalloonEnabled": true,
                "valueLineEnabled": true,
                "zoomable": false,
                "valueZoomable": true
            },
            "export": {
                "enabled": true
            }
        });
    };
    GanttChart.prototype.dataReceived = function () {
        console.log(this.gridrowsandColumns);
    };
    GanttChart.prototype.ngOnDestroy = function () {
        if (this.chart) {
            this.AmCharts.destroyChart(this.chart);
        }
    };
    GanttChart.ctorParameters = function () { return [
        { type: _amcharts_amcharts3_angular__WEBPACK_IMPORTED_MODULE_2__["AmChartsService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('chartdiv', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GanttChart.prototype, "chartdiv", void 0);
    GanttChart = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ganttchart',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./ganttchart.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/ganttchart/ganttchart.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_amcharts_amcharts3_angular__WEBPACK_IMPORTED_MODULE_2__["AmChartsService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]])
    ], GanttChart);
    return GanttChart;
}());



/***/ }),

/***/ "./src/components/header/header-page.ts":
/*!**********************************************!*\
  !*** ./src/components/header/header-page.ts ***!
  \**********************************************/
/*! exports provided: HeaderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderPage", function() { return HeaderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/user.model */ "./src/models/user.model.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../providers/storageservice/storageservice */ "./src/providers/storageservice/storageservice.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var HeaderPage = /** @class */ (function () {
    function HeaderPage(authService, document, router, storageService) {
        var _this = this;
        this.authService = authService;
        this.document = document;
        this.router = router;
        this.storageService = storageService;
        this.authService.userobj.subscribe(function (userobj) {
            if (userobj && userobj.id) {
                _this.user = new _models_user_model__WEBPACK_IMPORTED_MODULE_3__["UserData"](userobj);
            }
            else {
                _this.user = null;
            }
        });
        this.version = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].version;
        this.envi = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].envi;
    }
    HeaderPage.prototype.ngOnInit = function () {
        this.user && this.user.uuid ? this.router.navigateByUrl('') : this.router.navigateByUrl('login');
    };
    HeaderPage.prototype.home = function (e) {
        e.preventDefault();
        this.user && this.user.uuid ? this.router.navigateByUrl('') : this.router.navigateByUrl('login');
    };
    HeaderPage.prototype.logout = function () {
        // this.router.navigateByUrl('/logout.php');
        // ADD logout service from Openid
        this.storageService.clear();
        this.authService.setuserToken(null);
        this.authService.setuserobj(null);
        this.authService.setmenulist(null);
        this.authService.setuserrelations(null);
        window.location.reload();
        // this.router.navigateByUrl('/');
    };
    HeaderPage.ctorParameters = function () { return [
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_2__["AuthRestService"] },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["DOCUMENT"],] }] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
        { type: _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_5__["StorageService"] }
    ]; };
    HeaderPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'header-page',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header-page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/header/header-page.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_4__["DOCUMENT"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_2__["AuthRestService"], Object, _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"], _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_5__["StorageService"]])
    ], HeaderPage);
    return HeaderPage;
}());



/***/ }),

/***/ "./src/components/itsystems/itsystems.ts":
/*!***********************************************!*\
  !*** ./src/components/itsystems/itsystems.ts ***!
  \***********************************************/
/*! exports provided: Itsystems */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Itsystems", function() { return Itsystems; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var _models_grid_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models/grid.data */ "./src/models/grid.data.ts");
/* harmony import */ var _models_localization_localization__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../models/localization/localization */ "./src/models/localization/localization.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxgrid__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid.ts");
/* harmony import */ var _detailspage_detailspage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../detailspage/detailspage */ "./src/components/detailspage/detailspage.ts");
/* harmony import */ var _search_search__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../search/search */ "./src/components/search/search.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");













var xlsx = __webpack_require__(/*! json-as-xlsx */ "./node_modules/json-as-xlsx/index.js");
var dateFormat = __webpack_require__(/*! dateformat */ "./node_modules/dateformat/lib/dateformat.js");
var Itsystems = /** @class */ (function () {
    function Itsystems(service, services, router, location, activatedRoute, modalService, ref) {
        var _this = this;
        this.service = service;
        this.services = services;
        this.router = router;
        this.location = location;
        this.activatedRoute = activatedRoute;
        this.modalService = modalService;
        this.ref = ref;
        this.columns = null;
        this.loading = false;
        this.uploadStatus = false;
        this.fileStatus = false;
        this.uploadContainer = false;
        this.fetchingfilter = false;
        this.enablenotification = false;
        this.click_action = null;
        this.allfilterList = [
            {
                titel: "Intet valgt",
                uuid: 0,
            },
        ];
        this.ngbModalOptions = {
            backdrop: 'static',
            keyboard: true,
        };
        this.services.slctmenuitem.subscribe(function (bredcrum) {
            _this.bredcrum = bredcrum;
        });
        this.services.isLoading.subscribe(function (isLoading) {
            _this.loading = isLoading;
        });
        this.services.newactionsList.subscribe(function (newactionsList) {
            _this.actions = newactionsList;
            _this.enablenotification = true;
            _this.handlingdata = _this.actions.actions;
        });
    }
    Itsystems.prototype.ngOnInit = function () {
        this.loading = true;
        this.localization = new _models_localization_localization__WEBPACK_IMPORTED_MODULE_5__["Localization"]();
        this.localization = this.localization.localizationobj;
        this.parentheading = this.services.getselectedMenu();
        this.heading = this.activatedRoute.snapshot.data.title;
        this.selectedMenu = this.activatedRoute.snapshot.routeConfig.path;
        this.uuid = this.activatedRoute.snapshot.data.listId;
        this.click_action = this.activatedRoute.snapshot.data.click_action ? this.activatedRoute.snapshot.data.click_action : null;
        this.services.setlistId(this.uuid);
        this.clickedrow = this.services.getClickedRowuuiid();
        this.environmentUrl = this.service.getEnvironment();
        this.services.setselectedMenu(this.activatedRoute.snapshot.routeConfig.path);
        this.getValueFromObservable();
    };
    Itsystems.prototype.onSelect = function (event) {
        var args = event.args;
        var fileName = args.file;
        var fileSize = args.size;
        var ext = fileName.substr(fileName.lastIndexOf(".") + 1);
        if (ext == "csv" && fileSize < 1000000) {
            this.fileStatus = false;
        }
        else {
            this.fileStatus = true;
        }
    };
    Itsystems.prototype.onfileClick = function (event) {
        event.preventDefault();
        var initializevalue = this.inputFile;
        initializevalue.nativeElement.value = null;
        this.inputFile = initializevalue;
    };
    Itsystems.prototype.getValueFromObservable = function () {
        var _this = this;
        var contextid;
        contextid = this.services.getContext()
            ? this.services.getContext()
            : this.clickedrow;
        this.loading = false;
        if (this.click_action != "getObjektfilter") {
            this.service.getGriddata(this.uuid).then(function (res) {
                _this.gridrowsandColumns = res;
                _this.setdata();
            }, function (error) {
                _this.errorNotification.refresh();
                _this.errorNotification.open();
                _this.errorMessage = error.status.besked;
                console.log(error);
                _this.loading = false;
            });
        }
    };
    //dead code
    Itsystems.prototype.breadCrum = function () {
        this.navigationElements = [this.bredcrum];
    };
    Itsystems.prototype.RefreshData = function (data) {
        this.gridrowsandColumns = data;
        this.setdata();
    };
    Itsystems.prototype.setdata = function () {
        // this.actions = this.actions && this.actions.length>0 ?null: this.actions;
        this.breadCrum();
        this.rows = new _models_grid_data__WEBPACK_IMPORTED_MODULE_4__["RowsList"]({});
        this.columnss = new _models_grid_data__WEBPACK_IMPORTED_MODULE_4__["Columns"]({});
        this.datafileds = new _models_grid_data__WEBPACK_IMPORTED_MODULE_4__["DataFields"]({});
        var grid = this.gridrowsandColumns;
        if (this.gridrowsandColumns && this.gridrowsandColumns.result.actions) {
            this.services.setnewactionsList(this.gridrowsandColumns.result.actions);
        }
        this.rows = new _models_grid_data__WEBPACK_IMPORTED_MODULE_4__["RowsList"](grid.result.objekt_liste);
        this.datafileds = new _models_grid_data__WEBPACK_IMPORTED_MODULE_4__["DataFields"](grid.result.datafields);
        this.columnss = new _models_grid_data__WEBPACK_IMPORTED_MODULE_4__["Columns"](grid.result.columns);
        this.objektType = new _models_grid_data__WEBPACK_IMPORTED_MODULE_4__["ObjectType"](grid.result.hovedobjekt);
        this.generateGrid(this.rows, this.columnss, this.datafileds);
        this.getFilters();
    };
    Itsystems.prototype.getFilters = function () {
        var _this = this;
        this.service.getmenufilters(this.uuid).then(function (filters) {
            _this.allfilterList.length = 1;
            _this.allfilterList = _this.allfilterList.concat(filters.data);
            _this.filterList = _this.allfilterList;
            $(".selectpicker").selectpicker("refresh");
            setTimeout(function () {
                if (_this.itsystemGrid) {
                    $(".jqx-action-button:has(.jqx-icon-calendar)").addClass("itsystems__dateicon");
                    _this.applyfilters(_this.gridfilterdata);
                }
            }, 50);
        }, function (error) {
            console.log(error);
        });
    };
    Itsystems.prototype.generateGrid = function (row, columndata, datafields) {
        this.loading = false;
        var datafield_item;
        var formatted_datafields = [];
        for (var d in datafields) {
            var item = datafields[d];
            item.replace(/"/g, "");
            eval("datafield_item=" + item);
            if (datafield_item.type == "date") {
                datafield_item.format = "dd-MM-yyyy HH:mm:ss";
            }
            formatted_datafields.push(datafield_item);
        }
        var formatted_columnsfields = [];
        var columnfield_item;
        this.source = {
            localdata: row,
            datafields: formatted_datafields,
            datatype: "array",
            nullable: true,
            sortcolumn: "levering",
            sortdirection: "desc",
        };
        var dataAdapter = new jqx.dataAdapter(this.source);
        for (var c in columndata) {
            var item = columndata[c];
            item.replace(/"/g, "");
            eval("columnfield_item=" + item);
            formatted_columnsfields.push(columnfield_item);
        }
        this.columns = formatted_columnsfields;
        this.dataAdapter = dataAdapter;
        this.gridfilterdata = this.services.getgridState();
    };
    Itsystems.prototype.Filter = function (event) {
        event.preventDefault();
        if (this.itsystemGrid && this.itsystemGrid.savestate() && this.click_action != "getObjektfilter") {
            this.services.setgridState(this.itsystemGrid.savestate());
            this.services.setfiltersData(this.itsystemGrid.savestate());
        }
    };
    Itsystems.prototype.Sort = function (event) {
        event.preventDefault();
        if (this.itsystemGrid && this.itsystemGrid.savestate() && this.click_action != "getObjektfilter") {
            this.services.setgridState(this.itsystemGrid.savestate());
            this.services.setfiltersData(this.itsystemGrid.savestate());
        }
    };
    Itsystems.prototype.Rowclick = function (event) {
        var _this = this;
        event.preventDefault();
        var submenu = this.services.gethasSubmenu();
        if (event.args.row.bounddata.uuid) {
            var id = event.args.row.bounddata.uuid;
            this.services.setClickedRowuuiid(id);
            if (submenu) {
                this.services.setContext(id);
            }
            this.modalReference = this.modalService.open(_detailspage_detailspage__WEBPACK_IMPORTED_MODULE_10__["DetailsPage"], this.ngbModalOptions);
            this.modalReference.issubmenu = this.activatedRoute.snapshot.data.issubmenu;
            this.modalReference.result.then(function (data) {
                // on close
            }, function (reason) {
                // on dismiss
                if (reason && reason != 'cancelled') {
                    if (_this.click_action === 'getObjektfilter') {
                        _this.searchcomponent.Search();
                    }
                    else {
                        _this.ngOnInit();
                    }
                }
            });
        }
    };
    Itsystems.prototype.filtereddata = function (e) {
        event.preventDefault();
    };
    Itsystems.prototype.upload = function (event) {
        event.preventDefault();
        this.uploadContainer = true;
    };
    Itsystems.prototype.onColumnreordered = function (event) {
        event.preventDefault();
        this.services.setgridState(this.itsystemGrid.savestate());
        this.services.setfiltersData(this.itsystemGrid.savestate());
    };
    Itsystems.prototype.applyfilters = function (applyfilters) {
        if (this.itsystemGrid) {
            this.itsystemGrid.loadstate(applyfilters);
            this.itsystemGrid.applyfilters();
        }
    };
    Itsystems.prototype.filteredGrid = function (data) {
        this.gridrowsandColumns = data;
        this.setdata();
        this.services.setnewactionsList(this.gridrowsandColumns.result.actions);
    };
    Itsystems.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_12__["ActivatedRoute"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModal"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("successNotification", { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_7__["jqxNotificationComponent"])
    ], Itsystems.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("errorNotification", { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_7__["jqxNotificationComponent"])
    ], Itsystems.prototype, "errorNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("gridReference", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxgrid__WEBPACK_IMPORTED_MODULE_9__["jqxGridComponent"])
    ], Itsystems.prototype, "itsystemGrid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("uploadfileForm", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], Itsystems.prototype, "uploadfileForm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("inputFile", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], Itsystems.prototype, "inputFile", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("modal", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], Itsystems.prototype, "Modal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("searchcomponent", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _search_search__WEBPACK_IMPORTED_MODULE_11__["searchComponent"])
    ], Itsystems.prototype, "searchcomponent", void 0);
    Itsystems = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "itsystems",
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./itsystems.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/itsystems/itsystems.html")).default,
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"],
            _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"],
            _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"],
            _angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"],
            _angular_router__WEBPACK_IMPORTED_MODULE_12__["ActivatedRoute"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModal"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], Itsystems);
    return Itsystems;
}());



/***/ }),

/***/ "./src/components/jsonviewer/jsonviewer.ts":
/*!*************************************************!*\
  !*** ./src/components/jsonviewer/jsonviewer.ts ***!
  \*************************************************/
/*! exports provided: jsonviewerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "jsonviewerComponent", function() { return jsonviewerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var ang_jsoneditor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ang-jsoneditor */ "./node_modules/ang-jsoneditor/fesm5/ang-jsoneditor.js");






var jsonviewerComponent = /** @class */ (function () {
    function jsonviewerComponent(fb, modal, ref, baserestService) {
        this.fb = fb;
        this.modal = modal;
        this.ref = ref;
        this.baserestService = baserestService;
        this.body = {
            "data": "not available"
        };
        this.filtereditorOptions = new ang_jsoneditor__WEBPACK_IMPORTED_MODULE_5__["JsonEditorOptions"]();
        this.filtereditorOptions.mode = "view";
        console.log(this);
    }
    jsonviewerComponent.prototype.ngOnInit = function () {
        this.body = this.filtersdata ? this.filtersdata : this.body;
    };
    jsonviewerComponent.prototype.cancel = function (e) {
        this.modal.dismissAll();
    };
    jsonviewerComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__["BaseRestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], jsonviewerComponent.prototype, "filtersdata", void 0);
    jsonviewerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'jsonviewer-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./jsonviewer.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/jsonviewer/jsonviewer.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__["BaseRestService"]])
    ], jsonviewerComponent);
    return jsonviewerComponent;
}());



/***/ }),

/***/ "./src/components/login/login.ts":
/*!***************************************!*\
  !*** ./src/components/login/login.ts ***!
  \***************************************/
/*! exports provided: Login */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Login", function() { return Login; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/storageservice/storageservice */ "./src/providers/storageservice/storageservice.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");






var Login = /** @class */ (function () {
    function Login(router, ref, baserestService, authService, storageService) {
        var _this = this;
        this.router = router;
        this.ref = ref;
        this.baserestService = baserestService;
        this.authService = authService;
        this.storageService = storageService;
        this.error = false;
        this.loading = false;
        this.showpwd = false;
        this.userinfo = null;
        this.errormessage = null;
        this.userToken = '';
        this.fetching = true;
        this.fetching = true;
        this.storageService.get('carexadmin').then(function (userinfo) {
            if (userinfo) {
                _this.userinfo = userinfo;
                _this.fetching = true;
                _this.validateAuthorisation();
            }
            else {
                _this.fetching = false;
                _this.router.navigateByUrl('login');
            }
        });
        this.authService.userToken.subscribe(function (userToken) {
            _this.userToken = userToken;
        });
    }
    Login.prototype.ngOnInit = function () {
        if (this.userToken && this.userToken.length != 0) {
            this.router.navigateByUrl('');
        }
    };
    Login.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.baserestService.login(this.username, this.passwordkey).then(function (userinfo) {
            _this.userinfo = userinfo;
            _this.validateAuthorisation();
        }, function (error) { _this.loading = false; _this.error = true; });
    };
    Login.prototype.resetpassword = function () {
        this.router.navigate(['username'], { state: { username: this.username } });
    };
    Login.prototype.validateAuthorisation = function () {
        var _this = this;
        this.baserestService.getAllMenuList(this.userinfo.id).then(function (menulist) {
            _this.userToken = menulist.userToken;
            _this.authService.setuserToken(menulist.userToken);
            _this.authService.setmenulist(menulist);
            if (menulist.relationer) {
                _this.authService.setuserrelations(menulist.relationer);
            }
            _this.storageService.set('carexadmin', _this.userinfo);
            _this.authService.setuserobj(_this.userinfo);
            _this.router.navigateByUrl('');
            _this.loading = false;
            _this.fetching = false;
        }, function (error) {
            _this.loading = false;
            _this.fetching = false;
            _this.errormessage = error.error.besked;
        });
    };
    Login.prototype.showpassword = function () {
        this.showpwd = this.showpwd === true ? false : true;
        if (this.showpwd === true) {
            this.password.nativeElement.type = "text";
            this.ref.detectChanges();
        }
        else {
            this.password.nativeElement.type = "password";
            this.ref.detectChanges();
        }
    };
    Login.prototype.reset = function () {
        this.errormessage = null;
        this.error = false;
    };
    Login.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__["AuthRestService"] },
        { type: _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_4__["StorageService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('password', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], Login.prototype, "password", void 0);
    Login = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'login',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/login/login.html")).default,
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_5__["AuthRestService"], _providers_storageservice_storageservice__WEBPACK_IMPORTED_MODULE_4__["StorageService"]])
    ], Login);
    return Login;
}());



/***/ }),

/***/ "./src/components/menu-left/menu-left.ts":
/*!***********************************************!*\
  !*** ./src/components/menu-left/menu-left.ts ***!
  \***********************************************/
/*! exports provided: MenuLeft */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuLeft", function() { return MenuLeft; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_itsystems_itsystems__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/itsystems/itsystems */ "./src/components/itsystems/itsystems.ts");
/* harmony import */ var _components_ganttchart_ganttchart__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/ganttchart/ganttchart */ "./src/components/ganttchart/ganttchart.ts");
/* harmony import */ var _components_detailspage_detailspage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/detailspage/detailspage */ "./src/components/detailspage/detailspage.ts");
/* harmony import */ var _components_createpage_createpage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/createpage/createpage */ "./src/components/createpage/createpage.ts");
/* harmony import */ var _components_skema_skema__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../components/skema/skema */ "./src/components/skema/skema.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var _configuration_configuration__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../configuration/configuration */ "./src/components/configuration/configuration.ts");
/* harmony import */ var _analytics_analytics__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../analytics/analytics */ "./src/components/analytics/analytics.ts");
/* harmony import */ var _uploads_uploads__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../uploads/uploads */ "./src/components/uploads/uploads.ts");
/* harmony import */ var _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../providers/utils/helperService */ "./src/providers/utils/helperService.ts");

















var MenuLeft = /** @class */ (function () {
    function MenuLeft(baserestService, auth, helperService, router, location, services) {
        var _this = this;
        this.baserestService = baserestService;
        this.auth = auth;
        this.helperService = helperService;
        this.router = router;
        this.location = location;
        this.services = services;
        this.submenulist = [];
        this.client = "tryg";
        this.userobj = null;
        this.buttonsMennu = [];
        this.auth.userobj.subscribe(function (userobj) {
            if (userobj && userobj.id) {
                _this.userobj = userobj;
            }
        });
        this.auth.menulist.subscribe(function (menulist) {
            if (menulist && menulist.result) {
                _this.configureRouting(menulist.result);
            }
        });
    }
    MenuLeft.prototype.ngOnInit = function () {
    };
    MenuLeft.prototype.configureRouting = function (resultmenu) {
        var makemenu = [];
        for (var key in resultmenu) {
            if (resultmenu.hasOwnProperty(key)) {
                var val = resultmenu[key];
                makemenu.push(val);
            }
        }
        this.menulist = this.helperService.sortmenuList(makemenu);
        ;
        this.menulist.map(this.getRoutingItem, this);
    };
    MenuLeft.prototype.getRoutingItem = function (item) {
        if (item.titel == "Forløb") {
            this.router.config.push({ path: item.brugervendtnoegle, component: _components_ganttchart_ganttchart__WEBPACK_IMPORTED_MODULE_7__["GanttChart"], data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': true } });
        }
        if (item.click_action != 'list_objekt_data') {
            if (item.click_action === 'configuration') {
                this.router.config.push({ path: item.click_action, component: _configuration_configuration__WEBPACK_IMPORTED_MODULE_13__["Configuration"], data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            if (item.click_action === 'getuseranalytics') {
                this.router.config.push({ path: item.click_action, component: _analytics_analytics__WEBPACK_IMPORTED_MODULE_14__["analyticsComponent"], data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            if (item.click_action === 'uploads') {
                this.router.config.push({ path: item.click_action, component: _uploads_uploads__WEBPACK_IMPORTED_MODULE_15__["uploadsComponent"], data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            if (item.click_action === 'skemaeditor') {
                this.router.config.push({ path: item.click_action, component: _components_skema_skema__WEBPACK_IMPORTED_MODULE_10__["Skema"], data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            if (item.click_action === 'skemaeditor') {
                this.router.config.push({ path: item.click_action, component: _components_skema_skema__WEBPACK_IMPORTED_MODULE_10__["Skema"], data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
        }
        if (item.click_action === 'list_objekt_data') {
            this.router.config.push({ path: item.brugervendtnoegle, component: _components_itsystems_itsystems__WEBPACK_IMPORTED_MODULE_6__["Itsystems"], data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
        }
        if (item.click_action === 'getObjektfilter') {
            this.router.config.push({ path: item.brugervendtnoegle, component: _components_itsystems_itsystems__WEBPACK_IMPORTED_MODULE_6__["Itsystems"], data: { 'listId': item.skema_uuid, 'click_action': item.click_action, 'title': item.titel, 'issubmenu': false } });
        }
        this.router.config.push({ path: item.brugervendtnoegle + '/CreatePage', component: _components_createpage_createpage__WEBPACK_IMPORTED_MODULE_9__["CreatePage"] });
        this.router.config.push({ path: item.brugervendtnoegle + '/:uuid', component: _components_detailspage_detailspage__WEBPACK_IMPORTED_MODULE_8__["DetailsPage"], data: { 'issubmenu': false } });
    };
    MenuLeft.prototype.getsubmenuroutingItem = function (item) {
        var usersJson = Array.of(item.submenu);
        return usersJson;
    };
    MenuLeft.prototype.active = function (event, item, uuid, name) {
        event.preventDefault();
        this.services.setContext(null);
        this.services.setgridState(null);
        this.services.setfiltersData(null);
        this.services.setselectedmenuTitle(item.funktionsnavn);
        this.services.setslctmenuitem(item.funktionsnavn);
        if (item.submenu) {
            this.services.sethasSubmenu(true);
        }
        if (!item.submenu) {
            this.services.sethasSubmenu(false);
        }
        if (name != "personliste") {
            this.router.navigate(['' + name]);
        }
        if (item.click_action === 'configuration') {
            this.router.navigate(['configuration']);
        }
        if (item.click_action === 'getuseranalytics') {
            this.router.navigate(['getuseranalytics']);
        }
        if (item.click_action === 'translation') {
            this.router.navigate(['translation']);
        }
        if (item.click_action === 'templates') {
            this.router.navigate(['templates']);
        }
        if (item.click_action === 'skemaeditor') {
            this.router.navigate(['skemaeditor']);
        }
        if (item.click_action === 'uploads') {
            this.router.navigate(['uploads']);
        }
    };
    MenuLeft.prototype.activeapp = function (event, item) {
        this.router.navigate(['' + item]);
    };
    MenuLeft.prototype.activesubmenu = function (event, item, uuid, name) {
        event.stopPropagation();
    };
    MenuLeft.prototype.activeConfiguration = function (name) {
        this.router.navigate(['ConfigureApp']);
    };
    MenuLeft.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_11__["AuthRestService"] },
        { type: _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_16__["HelperSerice"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_12__["Services"] }
    ]; };
    MenuLeft = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'menu-left',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./menu-left.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/menu-left/menu-left.html")).default,
            providers: [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__["BaseRestService"], _angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_4__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_11__["AuthRestService"], _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_16__["HelperSerice"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _providers_services_services__WEBPACK_IMPORTED_MODULE_12__["Services"]])
    ], MenuLeft);
    return MenuLeft;
}());



/***/ }),

/***/ "./src/components/menu-right/menu-right.ts":
/*!*************************************************!*\
  !*** ./src/components/menu-right/menu-right.ts ***!
  \*************************************************/
/*! exports provided: MenuRight */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuRight", function() { return MenuRight; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");





var MenuRight = /** @class */ (function () {
    function MenuRight(router, auth) {
        var _this = this;
        this.router = router;
        this.auth = auth;
        this.items = ["Profile", "Settings", "Import", "Export", "Calender"];
        this.state = 'inactive';
        this.auth.userrelations.subscribe(function (userrelations) {
            {
                _this.userrelations = userrelations;
            }
        });
    }
    MenuRight.prototype.showItems = function () {
        this.items = ["Profile", "Settings", "Import", "Export", "Calender"];
    };
    MenuRight.prototype.hideItems = function () {
        this.items = [];
    };
    MenuRight.prototype.toogleMenu = function (event) {
        event.preventDefault();
        console.log("in left menu");
        this.state = this.state === 'active' ? 'inactive' : 'active';
    };
    MenuRight.prototype.toogleMenuOpen = function (event) {
        event.preventDefault();
        console.log("in left menu");
        this.state = this.state === 'active' ? 'inactive' : 'active';
    };
    MenuRight.prototype.getAnalytics = function () {
        this.router.navigate(['analytics']);
    };
    MenuRight.prototype.serviceAnalytics = function () {
        this.router.navigate(['serviceanalytics']);
    };
    MenuRight.prototype.sendpushNotification = function (e) {
        e.preventDefault();
        this.router.navigate(['sendpushNotification']);
    };
    MenuRight.prototype.translation = function (e) {
        e.preventDefault();
        this.router.navigate(['translation']);
    };
    MenuRight.prototype.templates = function (e) {
        e.preventDefault();
        this.router.navigate(['templates']);
    };
    MenuRight.prototype.configuration = function (e) {
        e.preventDefault();
        this.router.navigate(['configuration']);
    };
    MenuRight.prototype.uploads = function (e) {
        e.preventDefault();
        this.router.navigate(['uploads']);
    };
    MenuRight.prototype.sendmessages = function (e) {
        e.preventDefault();
        this.router.navigate(['sendmessages']);
    };
    MenuRight.prototype.skemaeditor = function (e) {
        e.preventDefault();
        this.router.navigate(['skemaeditor']);
    };
    MenuRight.prototype.newgrid = function (e) {
        e.preventDefault();
        this.router.navigate(['newgrid']);
    };
    MenuRight.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__["AuthRestService"] }
    ]; };
    MenuRight = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'menu-right',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./menu-right.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/menu-right/menu-right.html")).default,
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('menuState', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["state"])('inactive', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({
                    // transform: "translate(100%,0)"
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["state"])('active', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({
                    //   transform: "translate(0,0)"
                    })),
                ])
            ]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__["AuthRestService"]])
    ], MenuRight);
    return MenuRight;
}());



/***/ }),

/***/ "./src/components/message-form/message-form.ts":
/*!*****************************************************!*\
  !*** ./src/components/message-form/message-form.ts ***!
  \*****************************************************/
/*! exports provided: MessageForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageForm", function() { return MessageForm; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_message__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/message */ "./src/models/message.ts");



var MessageForm = /** @class */ (function () {
    function MessageForm() {
    }
    /*   @Input('messages')
      private messages : Message[]; */
    MessageForm.prototype.ngOnInit = function () {
        console.log(this.message);
    };
    MessageForm.prototype.sendMessage = function () {
        this.message.timestamp = new Date();
        this.messages.push(this.message);
        /*     this.dialogFlowService.getResponse(this.message.content).subscribe(res => {
              this.messages.push(
                new Message(res.result.fulfillment.speech, 'assets/icons/user.png', res.timestamp)
              );
            });
        
            this.message = new Message('', 'assets/icons/user.png');
        } */
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('message'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_message__WEBPACK_IMPORTED_MODULE_2__["Message"])
    ], MessageForm.prototype, "message", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('messages'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], MessageForm.prototype, "messages", void 0);
    MessageForm = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'message-form',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./message-form.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/message-form/message-form.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MessageForm);
    return MessageForm;
}());



/***/ }),

/***/ "./src/components/message-item/message-item.ts":
/*!*****************************************************!*\
  !*** ./src/components/message-item/message-item.ts ***!
  \*****************************************************/
/*! exports provided: MessageItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageItem", function() { return MessageItem; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_message__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/message */ "./src/models/message.ts");



var MessageItem = /** @class */ (function () {
    function MessageItem() {
    }
    MessageItem.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('message'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_message__WEBPACK_IMPORTED_MODULE_2__["Message"])
    ], MessageItem.prototype, "message", void 0);
    MessageItem = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'message-item',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./message-item.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/message-item/message-item.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MessageItem);
    return MessageItem;
}());



/***/ }),

/***/ "./src/components/message-list/message-list.ts":
/*!*****************************************************!*\
  !*** ./src/components/message-list/message-list.ts ***!
  \*****************************************************/
/*! exports provided: MessageList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageList", function() { return MessageList; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MessageList = /** @class */ (function () {
    function MessageList() {
    }
    MessageList.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('message'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], MessageList.prototype, "messages", void 0);
    MessageList = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'message-list',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./message-list.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/message-list/message-list.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MessageList);
    return MessageList;
}());



/***/ }),

/***/ "./src/components/modal/modal.ts":
/*!***************************************!*\
  !*** ./src/components/modal/modal.ts ***!
  \***************************************/
/*! exports provided: modalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "modalComponent", function() { return modalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");



var modalComponent = /** @class */ (function () {
    function modalComponent(modalService) {
        this.modalService = modalService;
        this.list = [];
    }
    modalComponent.prototype.ngOnInit = function () {
        //this.ngbModalRef.result(this.div);
        // this.modalReference = this.modalService.open(this.modalComponent);
        // this.ngbModalRef.open(this.modalComponent);
        //     this.modalService.result.then((result) => {
        //         //this.closeResult = `Closed with: ${result}`;
        //     }, (reason) => {
        //         //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        //     });
        //   this.list.push('PROFIT!!!');
        this.modalService.open(this.div);
    };
    modalComponent.prototype.openModal = function (modalComponent) {
        console.log(modalComponent);
        this.modalService.open(modalComponent);
    };
    modalComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('modal', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], modalComponent.prototype, "div", void 0);
    modalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'modal',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./modal.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/modal/modal.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"]])
    ], modalComponent);
    return modalComponent;
}());



/***/ }),

/***/ "./src/components/newgrid/newgrid.ts":
/*!*******************************************!*\
  !*** ./src/components/newgrid/newgrid.ts ***!
  \*******************************************/
/*! exports provided: newGridComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newGridComponent", function() { return newGridComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/fesm2015/primeng-table.js");





var newGridComponent = /** @class */ (function () {
    function newGridComponent(baserestService, auth) {
        this.baserestService = baserestService;
        this.auth = auth;
        this.columns = [];
        this.names = [];
        this.columnsdata = [];
        this.representatives = [];
    }
    newGridComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.representatives = [
            { name: "Amy Elsner", image: "amyelsner.png" },
            { name: "Anna Fali", image: "annafali.png" },
            { name: "Asiya Javayant", image: "asiyajavayant.png" },
            { name: "Bernardo Dominic", image: "bernardodominic.png" },
            { name: "Elwin Sharvill", image: "elwinsharvill.png" },
            { name: "Ioni Bowcher", image: "ionibowcher.png" },
            { name: "Ivan Magalhaes", image: "ivanmagalhaes.png" },
            { name: "Onyama Limba", image: "onyamalimba.png" },
            { name: "Stephen Shaw", image: "stephenshaw.png" },
            { name: "XuXue Feng", image: "xuxuefeng.png" }
        ];
        this.baserestService.getGriddata('9eacc9e4-4359-48e8-9e17-8ce331b7ad59').then(function (res) {
            _this.griddata = res;
            console.log(res);
            _this.setData();
        }, function (error) {
            console.log(error);
        });
    };
    // "{text: 'Levering', datafield: 'levering', filtertype: 'range', nullable: 'true', width:'10%', cellsformat: 'dd-MM-yyyy'}"
    newGridComponent.prototype.setData = function () {
        this.griddata.result.columns.map(function (item) {
            var columnitem = eval('item=' + item);
            this.columns.push({ text: item.text, datafield: item.datafield });
        }, this);
        for (var i in this.griddata.result.objekt_liste) {
            this.columnsdata.push(this.griddata.result.objekt_liste[i]);
            this.names.push({
                name: this.griddata.result.objekt_liste[i].navn
            });
        }
        console.log(this.columns);
        console.log(this.columnsdata);
        console.log(this.names);
    };
    newGridComponent.prototype.onDateSelect = function (value) {
        this.table.filter(this.formatDate(value), 'date', 'equals');
    };
    newGridComponent.prototype.formatDate = function (date) {
        var month = date.getMonth() + 1;
        var day = date.getDate();
        if (month < 10) {
            month = '0' + month;
        }
        if (day < 10) {
            day = '0' + day;
        }
        return date.getFullYear() + '-' + month + '-' + day;
    };
    newGridComponent.prototype.onRepresentativeChange = function (event) {
        this.table.filter(event.value, "representative", "in");
    };
    newGridComponent.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dt', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_table__WEBPACK_IMPORTED_MODULE_4__["Table"])
    ], newGridComponent.prototype, "table", void 0);
    newGridComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'newgrid',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./newgrid.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/newgrid/newgrid.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"]])
    ], newGridComponent);
    return newGridComponent;
}());



/***/ }),

/***/ "./src/components/notifications/notifications.ts":
/*!*******************************************************!*\
  !*** ./src/components/notifications/notifications.ts ***!
  \*******************************************************/
/*! exports provided: Notifications */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Notifications", function() { return Notifications; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var Notifications = /** @class */ (function () {
    function Notifications() {
        console.log(this);
    }
    Notifications.prototype.ngOnInit = function () {
        console.log(this.message);
    };
    Notifications.prototype.ngAfterViewInit = function () {
        var _this = this;
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        console.log(this);
        setTimeout(function () {
            _this.success = false;
        }, 3000);
    };
    Notifications.prototype.ngOnChanges = function (changes) {
        var _this = this;
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        console.log(changes);
        console.log(this);
        setTimeout(function () {
            _this.success = false;
        }, 3000);
    };
    Notifications.prototype.verify = function () {
        console.log(this);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], Notifications.prototype, "message", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], Notifications.prototype, "warning", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], Notifications.prototype, "error", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], Notifications.prototype, "success", void 0);
    Notifications = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'notifications-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./notifications.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/notifications/notifications.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Notifications);
    return Notifications;
}());



/***/ }),

/***/ "./src/components/notifyviewer/notifyviewer.ts":
/*!*****************************************************!*\
  !*** ./src/components/notifyviewer/notifyviewer.ts ***!
  \*****************************************************/
/*! exports provided: notifyviewerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notifyviewerComponent", function() { return notifyviewerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);







var notifyviewerComponent = /** @class */ (function () {
    function notifyviewerComponent(activeModal, baserestService, dateTimeAdapter) {
        this.activeModal = activeModal;
        this.baserestService = baserestService;
        this.dateTimeAdapter = dateTimeAdapter;
        this.selecteddatenow = '';
        this.dateTimeAdapter.setLocale('da-DK');
        this.createstdate();
    }
    notifyviewerComponent.prototype.ngOnInit = function () {
    };
    notifyviewerComponent.prototype.cancel = function (e) {
        this.activeModal.dismissAll();
    };
    notifyviewerComponent.prototype.clearDate = function (item) {
        console.log(item);
    };
    notifyviewerComponent.prototype.DateChanged = function (selecteddate) {
        if (selecteddate && selecteddate._d) {
            this.selectedtime = moment__WEBPACK_IMPORTED_MODULE_6__(selecteddate._d).format('YYYY-MM-DD HH:mm:ss');
        }
    };
    notifyviewerComponent.prototype.saveFilter = function (e) {
        var _this = this;
        console.log(e);
        var selectedUuids = [];
        this.displayedrows.map(function (item) {
            if (item.uuid) {
                selectedUuids.push(item.uuid);
            }
        });
        this.baserestService.notificationBatch(this.selectedItem.key, JSON.stringify(selectedUuids), this.selectedtime).then(function (success) {
            _this.successNotification.refresh();
            _this.successNotification.open();
            _this.activeModal.dismissAll();
        }, function (error) {
            _this.errorNotification.refresh();
            _this.errorNotification.open();
            _this.activeModal.dismissAll();
        });
    };
    notifyviewerComponent.prototype.createstdate = function () {
        this.startat = new Date().toLocaleString('dk-DA', { timeZone: 'Europe/Copenhagen' });
        var dd;
        var mm;
        var yyyy;
        if (this.startat.indexOf('/') != -1) {
            dd = this.startat.split("/")[0];
            mm = this.startat.split("/")[1];
            yyyy = this.startat.split("/")[2].split(",")[0];
        }
        else {
            dd = this.startat.split(".")[0];
            mm = this.startat.split(".")[1];
            yyyy = this.startat.split(".")[2].split(" ")[0];
        }
        var time = this.startat.split(' ')[1];
        var hh = time.split(':')[0];
        var mi = time.split(':')[1];
        var secs = time.split(':')[2];
        this.mindate = new Date(yyyy, mm - 1, dd, hh, mi);
    };
    notifyviewerComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] },
        { type: ng_pick_datetime__WEBPACK_IMPORTED_MODULE_5__["DateTimeAdapter"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], notifyviewerComponent.prototype, "displayedrows", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], notifyviewerComponent.prototype, "selectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], notifyviewerComponent.prototype, "selectedmenuItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], notifyviewerComponent.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], notifyviewerComponent.prototype, "errorNotification", void 0);
    notifyviewerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'notifyviewer-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./notifyviewer.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/notifyviewer/notifyviewer.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"], ng_pick_datetime__WEBPACK_IMPORTED_MODULE_5__["DateTimeAdapter"]])
    ], notifyviewerComponent);
    return notifyviewerComponent;
}());



/***/ }),

/***/ "./src/components/operations/operations.ts":
/*!*************************************************!*\
  !*** ./src/components/operations/operations.ts ***!
  \*************************************************/
/*! exports provided: OperationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OperationsComponent", function() { return OperationsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var _createpage_createpage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../createpage/createpage */ "./src/components/createpage/createpage.ts");







var xlsx = __webpack_require__(/*! json-as-xlsx */ "./node_modules/json-as-xlsx/index.js");
var OperationsComponent = /** @class */ (function () {
    function OperationsComponent(services, modalService) {
        var _this = this;
        this.services = services;
        this.modalService = modalService;
        this.newlistdata = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.filtereddata = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.alloperations = [];
        this.buttons = [];
        this.Handlinger = [];
        this.Udsend = [];
        this.notificationoperation = {
            "actions": {
                "list": {
                    "buttons": [
                        {
                            "name": "Gem filter"
                        },
                        {
                            "name": "Opret"
                        },
                        {
                            "name": "Import"
                        },
                        {
                            "name": "Export"
                        }
                    ]
                },
                "detail": {
                    "buttons": [
                        {
                            "name": "Annuller"
                        }
                    ]
                }
            }
        };
        this.defaultoperation = {
            "actions": {
                "list": {
                    "buttons": [
                        {
                            "name": "Gem filter"
                        },
                        {
                            "name": "Opret"
                        },
                        {
                            "name": "Import"
                        },
                        {
                            "name": "Export"
                        }
                    ]
                },
                "detail": {
                    "buttons": [
                        {
                            "name": "Gem"
                        },
                        {
                            "name": "Annuller"
                        },
                        {
                            "name": "Slet"
                        }
                    ]
                }
            }
        }; //remove in future
        this.operationbuttons = [];
        this.ngbModalOptions = {
            backdrop: 'static',
            keyboard: false
        };
        this.services.newactionsList.subscribe(function (newactionslist) {
            _this.actions = newactionslist;
            _this.Handlinger = _this.actions && _this.actions.actions && _this.actions.actions.Handlinger ? _this.actions.actions.Handlinger : [];
            _this.buttons = _this.actions && _this.actions.buttons ? _this.actions.buttons : [];
            _this.Udsend = _this.actions && _this.actions.actions && _this.actions.actions.Udsend ? _this.actions.actions.Udsend : [];
        });
        this.services.newallfilterList.subscribe(function (newallfilterList) {
            _this.actions = newallfilterList;
            if (_this.sharedthis) {
                _this.services.setisLoading(true);
                _this.sharedthis.allfilterList = newallfilterList;
                _this.sharedthis.filterList = newallfilterList;
            }
            setTimeout(function () {
                $('.selectpicker').selectpicker('refresh');
                _this.services.setisLoading(false);
            }, 20);
        });
    }
    OperationsComponent.prototype.ngOnInit = function () {
        var alloprts = [];
        this.Handlinger = this.actions && this.actions.actions && this.actions.actions.Handlinger ? this.actions.actions.Handlinger : [];
        this.buttons = this.actions && this.actions.buttons ? this.actions.buttons : [];
        this.Udsend = this.actions && this.actions.actions && this.actions.actions.Udsend ? this.actions.actions.Udsend : [];
    };
    OperationsComponent.prototype.ngOnDestroy = function () {
        this.buttons.length = 0;
        this.Handlinger.length = 0;
        this.Udsend.length = 0;
    };
    OperationsComponent.prototype.ngAfterViewInit = function () {
    };
    OperationsComponent.prototype.download = function (event) {
        event.preventDefault();
        this.services.setisLoading(true);
        var checkrows = this.sharedthis.itsystemGrid.getdisplayrows();
        var allrows = [];
        for (var i in checkrows) {
            allrows.push(checkrows[i]);
        }
        if (event.target.id == "xlsx") {
            var excelcolumns = [];
            this.sharedthis.columns.map(function (item) {
                excelcolumns.push({ label: item.text, value: item.datafield });
            });
            var settings = {
                sheetName: this.sharedthis.selectedMenu,
                fileName: this.sharedthis.selectedMenu
            };
            if (checkrows.length === 0) {
                allrows.push(['']);
            }
            xlsx(excelcolumns, allrows, settings);
        }
        else {
            this.sharedthis.itsystemGrid.exportdata(event.target.id, '' + this.sharedthis.selectedMenu, true, allrows, false, _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].saveUrl, 'utf-8');
        }
        this.sharedthis.successNotification.refresh();
        this.sharedthis.successNotification.open();
        this.services.setisLoading(false);
    };
    OperationsComponent.prototype.CreatePage = function (event) {
        event.preventDefault();
        this.sharedthis.modalReference = this.sharedthis.modalService.open(_createpage_createpage__WEBPACK_IMPORTED_MODULE_6__["CreatePage"], this.ngbModalOptions);
        // this.sharedthis.router.navigate(['' + this.sharedthis.selectedMenu + '/CreatePage']);
    };
    OperationsComponent.prototype.open = function (modal, event) {
        this.sharedthis.ref.detectChanges();
        event.preventDefault();
        if (this.sharedthis.click_action != "getObjektfilter") {
            this.sharedthis.filtersdata = this.services.getgridState();
            this.filtersdata = this.services.getgridState();
        }
        this.sharedthis.modalReference = this.sharedthis.modalService.open(modal, this.ngbModalOptions);
        this.sharedthis.modalReference.result.then(function (result) {
        }, function (reason) {
            //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    };
    OperationsComponent.prototype.openNotification = function (modal, event, item) {
        this.selectedItem = item;
        this.displayedrows = this.sharedthis.itsystemGrid ? this.sharedthis.itsystemGrid.getdisplayrows() : null;
        this.open(modal, event);
    };
    OperationsComponent.prototype.onfileSubmit = function (content, event) {
        var _this = this;
        var filetype;
        if (this.sharedthis.fileName && this.sharedthis.fileName.type) {
            filetype = this.sharedthis.fileName.type;
        }
        else {
            if (this.sharedthis.fileName && this.sharedthis.fileName[0] && this.sharedthis.fileName[0].type) {
                filetype = this.sharedthis.fileName[0].type;
            }
            else {
                filetype = this.sharedthis.fileName.name.split('.').pop();
            }
        }
        var uplaodedfile = this.sharedthis.fileName[0] ? this.sharedthis.fileName[0] : this.sharedthis.fileName;
        var filename = this.sharedthis.fileName.name;
        if (filetype == "text/csv" || filetype == "csv" || filetype == "application/pdf"
            || filetype == "application/json" || filetype == "json" || filetype == "xlsx" || filetype == "application/vnd.ms-excel"
            || filetype == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            this.services.setisLoading(true);
            this.sharedthis.service.uploadcsvFile(uplaodedfile, filename, this.sharedthis.uuid).then(function (res) {
                if (res && res.response && res.response.success) {
                    _this.sharedthis.serviceErrorMessage = res.response.success;
                    _this.sharedthis.successNotification.refresh();
                    _this.sharedthis.successNotification.open();
                    _this.sharedthis.uploadStatus = false;
                    _this.sharedthis.fileName = null;
                    _this.sharedthis.uploadContainer = false;
                    _this.services.setisLoading(false);
                    _this.sharedthis.modalReference.close();
                }
                else {
                    $('.errormessage').text(res.status.besked);
                    _this.sharedthis.serviceErrorMessage = res && res.status && res.status.besked ? res.status.besked : "Error";
                    _this.sharedthis.errorNotification.refresh();
                    _this.sharedthis.errorNotification.open();
                    _this.sharedthis.uploadStatus = false;
                    _this.sharedthis.fileName = null;
                    _this.sharedthis.uploadContainer = false;
                    _this.services.setisLoading(false);
                    _this.sharedthis.modalReference.close();
                }
            }, function (error) {
                _this.sharedthis.errorNotification.refresh();
                _this.sharedthis.errorNotification.open();
                _this.sharedthis.uploadStatus = false;
                _this.sharedthis.fileName = null;
                _this.sharedthis.uploadContainer = false;
                _this.services.setisLoading(false);
            });
        }
        else {
            this.sharedthis.fileStatus = true;
            this.sharedthis.uploadContainer = false;
            this.sharedthis.fileName = null;
            this.sharedthis.uploadContainer = false;
            this.services.setisLoading(false);
            setTimeout(function () {
                _this.sharedthis.fileStatus = false;
            }, 4000);
        }
    };
    OperationsComponent.prototype.fileuploadCancel = function (event) {
        event.preventDefault();
        this.sharedthis.uploadContainer = false;
        var uploadFile = this.sharedthis.uploadfileForm;
        this.sharedthis.fileName = null;
        this.sharedthis.uploadContainer = false;
    };
    OperationsComponent.prototype.Cancel = function (event) {
        event.preventDefault();
        this.sharedthis.location.back();
    };
    OperationsComponent.prototype.Delete = function (event) {
        var _this = this;
        event.preventDefault();
        if (confirm("Are you sure you want to delete this entry of ID::" + this.sharedthis.uuid) == true) {
            this.sharedthis.service.deleteDetailsdata(this.sharedthis.listId, this.sharedthis.uuid, this.sharedthis.lastupdatedTimeStamp).then(function (res) {
                _this.sharedthis.location.back();
                _this.successnotification.open();
                _this.sharedthis.loading = false;
            }, function (error) { _this.errornotification.open(); _this.sharedthis.errorMessage = error.status.besked; ; _this.sharedthis.loading = false; }, function (complete) { _this.sharedthis.loading = false; });
        }
        else {
            console.log("You pressed Cancel!");
        }
    };
    OperationsComponent.prototype.saveData = function (event) {
        var _this = this;
        event.preventDefault();
        if (this.sharedthis.acc && this.sharedthis.acc._results) {
            this.sharedthis.acc && this.sharedthis.acc._results.map(function (item) {
                this.sharedthis.activeIds.push(item.panels._results[0].id);
            }, this);
        }
        if (!this.sharedthis.detailForm.nativeElement.checkValidity()) {
            this.sharedthis.detailForm.nativeElement.classList.add('was-validated');
        }
        var formvalid = $('.invalid-feedback').is(':visible');
        if (!formvalid) {
            var formvalues = JSON.stringify(this.sharedthis.detailsForm.controls.items.value);
            var formdata = [];
            formdata.push(this.sharedthis.detailsForm.controls.items.value.map(this.sharedthis.formatData, this));
            var submitting_1 = [];
            formdata[0].map(function (e) {
                if (e && e.ld_brugervendtnoegle) {
                    submitting_1.push(e);
                }
                else {
                    e.map(function (el) {
                        if (el && el.ld_brugervendtnoegle) {
                            submitting_1.push(el);
                        }
                    });
                }
            });
            this.sharedthis.loading = true;
            this.sharedthis.service.updateDetailsdata(this.sharedthis.listId, this.objekt_uuid, this.sharedthis.detailsData.status.update_timestamp, JSON.stringify(submitting_1), this.sharedthis.AllFiles).then(function (res) {
                _this.sharedthis.loading = false;
                _this.sharedthis.location.back();
                _this.successnotification.open();
            }, function (error) {
                _this.sharedthis.loading = false;
                _this.errornotification.open();
                var error_messsage = error.error.status.besked;
                _this.sharedthis.errorMessage = error_messsage;
            }, function (complete) { _this.sharedthis.loading = false; });
        }
    };
    OperationsComponent.prototype.showfilterData = function (modal, event) {
        this.sharedthis.ref.detectChanges();
        event.preventDefault();
        this.sharedthis.ref.detectChanges();
        this.sharedthis.modalReference = this.sharedthis.modalService.open(modal, this.ngbModalOptions);
        this.sharedthis.modalReference.result.then(function (result) {
        }, function (reason) {
            //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    };
    OperationsComponent.prototype.getFilterData = function (e, item) {
        var _this = this;
        this.sharedthis.fetchingfilter = true;
        if (this.sharedthis.filteruuid != 0) {
            if (this.sharedthis.click_action === 'getObjektfilter') {
                this.sharedthis.service.readsearchData(this.sharedthis.uuid, this.filteruuid).then(function (res) {
                    // this.filtersdata = filtersdata;
                    // this.filtereddata.emit(filtersdata);
                    // $('.selectpicker').selectpicker('refresh');
                    // this.sharedthis.fetchingfilter = false;
                    _this.sharedthis.fetchingfilter = false;
                    _this.sharedthis.gridrowsandColumns = res;
                    _this.newlistdata.emit(res);
                }, function (error) {
                    console.log(error);
                    _this.sharedthis.fetchingfilter = false;
                });
            }
            else {
                this.sharedthis.service.readfilterdata(this.filteruuid).then(function (filtersdata) {
                    _this.filtersdata = filtersdata;
                    // this.sharedthis.applyfilters(filtersdata);
                    _this.filtereddata.emit(filtersdata);
                    $('.selectpicker').selectpicker('refresh');
                    _this.sharedthis.fetchingfilter = false;
                }, function (error) { console.log(error); });
            }
        }
        else {
            this.sharedthis.itsystemGrid.clearfilters();
            this.filtersdata = {
                "nofilters": "Data does not contains filters"
            };
            this.sharedthis.fetchingfilter = false;
        }
    };
    OperationsComponent.prototype.onfileChange = function (event) {
        event.preventDefault();
        this.sharedthis.uploadContainer = true;
        this.sharedthis.fileName = event.target.files[0];
    };
    OperationsComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    OperationsComponent.prototype.openSendEmail = function (modal, event, item) {
        event.preventDefault();
        this.action = item;
        this.sharedthis.filtersdata = this.services.getgridState();
        this.filtersdata = this.services.getgridState();
        this.sharedthis.displayedrows = this.sharedthis.itsystemGrid ? this.sharedthis.itsystemGrid.getdisplayrows() : null;
        this.sharedthis.modalReference = this.sharedthis.modalService.open(modal, event, this.ngbModalOptions);
    };
    OperationsComponent.prototype.getValueFromObservable = function () {
        var _this = this;
        var contextid;
        this.sharedthis.loading = true;
        contextid = this.sharedthis.services.getContext() ? this.sharedthis.services.getContext() : this.sharedthis.clickedrow;
        this.sharedthis.service.getGriddata(this.sharedthis.uuid).then(function (res) {
            _this.sharedthis.gridrowsandColumns = res;
            _this.newlistdata.emit(res);
        }, function (error) {
            _this.sharedthis.errorNotification.refresh();
            _this.sharedthis.errorNotification.open();
            _this.sharedthis.errorMessage = error.status.besked;
            console.log(error);
            _this.sharedthis.loading = false;
        });
    };
    OperationsComponent.ctorParameters = function () { return [
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_2__["Services"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OperationsComponent.prototype, "sharedthis", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OperationsComponent.prototype, "list", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OperationsComponent.prototype, "details", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OperationsComponent.prototype, "filterList", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OperationsComponent.prototype, "objekt_uuid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OperationsComponent.prototype, "actions", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OperationsComponent.prototype, "newlistdata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OperationsComponent.prototype, "filtereddata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successnotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], OperationsComponent.prototype, "successnotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errornotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], OperationsComponent.prototype, "errornotification", void 0);
    OperationsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'operations-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./operations.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/operations/operations.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_services_services__WEBPACK_IMPORTED_MODULE_2__["Services"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]])
    ], OperationsComponent);
    return OperationsComponent;
}());



/***/ }),

/***/ "./src/components/otp/otp.ts":
/*!***********************************!*\
  !*** ./src/components/otp/otp.ts ***!
  \***********************************/
/*! exports provided: OtpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpComponent", function() { return OtpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var OtpComponent = /** @class */ (function () {
    function OtpComponent(fb, authService, baserestService, router) {
        this.fb = fb;
        this.authService = authService;
        this.baserestService = baserestService;
        this.router = router;
        this.error = false;
        this.loading = false;
        this.loginForm = this.fb.group({
            otp1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            otp2: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            otp3: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            otp4: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.username = this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.username ? this.router.getCurrentNavigation().extras.state.username : '';
    }
    OtpComponent.prototype.ngOnInit = function () {
        console.log("on init");
    };
    OtpComponent.prototype.gotochangePasword = function () {
        var _this = this;
        this.loading = true;
        this.otpval1 = this.loginForm.value.otp1;
        this.otpval2 = this.loginForm.value.otp2;
        this.otpval3 = this.loginForm.value.otp3;
        this.otpval4 = this.loginForm.value.otp4;
        var otp = '' + this.otpval1 + this.otpval2 + this.otpval3 + this.otpval4;
        if (this.loginForm.valid) {
            this.baserestService.verifyOTP(this.username, otp).then(function (userid) {
                _this.error = false;
                _this.userid = userid;
                _this.setuserData();
            }, function (error) {
                _this.error = true;
                _this.otp1_input.nativeElement.focus();
                _this.otpval1 = null;
                _this.loginForm.value.otp1 = null;
                _this.otpval2 = null;
                _this.loginForm.value.otp2 = null;
                _this.otpval3 = null;
                _this.loginForm.value.otp3 = null;
                _this.otpval4 = null;
                _this.loginForm.value.otp4 = null;
                _this.loading = false;
                _this.errormessage = error.error;
            }), this;
        }
    };
    OtpComponent.prototype.next1 = function (event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            //this.removeFocus(event.currentTarget.id);
            this.otpval1 = null;
            this.loginForm.value.otp1 = null;
        }
        else {
            this.otpval1 = event.key;
            this.otp2_input.nativeElement.focus();
        }
    };
    OtpComponent.prototype.next2 = function (event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval1 = null;
            this.loginForm.value.otp1 = null;
            this.otp1_input.nativeElement.focus();
        }
        else {
            this.otpval2 = event.key;
            this.otp3_input.nativeElement.focus();
        }
    };
    OtpComponent.prototype.next3 = function (event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval2 = null;
            this.loginForm.value.otp2 = null;
            this.otp2_input.nativeElement.focus();
        }
        else {
            this.otpval3 = event.key;
            this.otp4_input.nativeElement.focus();
        }
    };
    OtpComponent.prototype.next4 = function (event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval3 = null;
            this.loginForm.value.otp3 = null;
            this.otp3_input.nativeElement.focus();
        }
        else {
            this.otpval4 = event.key;
            if (this.otpval1 && this.otpval2 && this.otpval3 && this.otpval4) {
                this.gotochangePasword();
            }
        }
    };
    OtpComponent.prototype.setuserData = function () {
        this.loading = false;
        this.router.navigate(['changepassword'], { state: { userid: this.userid } });
    };
    OtpComponent.prototype.showKeyboard = function () {
        this.otp1_input.nativeElement.focus();
        this.otpval1 = null;
        this.loginForm.value.otp1 = null;
        this.otpval2 = null;
        this.loginForm.value.otp2 = null;
        this.otpval3 = null;
        this.loginForm.value.otp3 = null;
        this.otpval4 = null;
        this.loginForm.value.otp4 = null;
    };
    OtpComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__["AuthRestService"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("otp1", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], OtpComponent.prototype, "otp1_input", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("otp2", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], OtpComponent.prototype, "otp2_input", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("otp3", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], OtpComponent.prototype, "otp3_input", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("otp4", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], OtpComponent.prototype, "otp4_input", void 0);
    OtpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'otp-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./otp.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/otp/otp.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_4__["AuthRestService"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], OtpComponent);
    return OtpComponent;
}());



/***/ }),

/***/ "./src/components/search/search.ts":
/*!*****************************************!*\
  !*** ./src/components/search/search.ts ***!
  \*****************************************/
/*! exports provided: searchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchComponent", function() { return searchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);







var searchComponent = /** @class */ (function () {
    function searchComponent(baserestService, ref, services, formBuilder) {
        var _this = this;
        this.baserestService = baserestService;
        this.ref = ref;
        this.services = services;
        this.formBuilder = formBuilder;
        this.loading = false;
        this.searchData = null;
        this.searchresultData = null;
        this.searchform = null;
        this.uuid = null;
        this.filteruuid = null;
        this.gridrowsandColumns = null;
        this.activeIds = "search_panel";
        this.allfilters = [];
        this.allfilterList = [
            {
                titel: "Intet valgt",
                uuid: 0,
            },
        ];
        this.filterList = [];
        this.filteredGriddata = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.updateOperations = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.buttons =
            this.actions && this.actions.buttons ? this.actions.buttons : [];
        this.services.newallfilterList.subscribe(function (newallfilterList) {
            _this.filterList = newallfilterList;
            setTimeout(function () {
                $(".selectpicker").selectpicker("refresh");
            }, 100);
        });
    }
    searchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loading = true;
        this.uuid = this.services.getlistId();
        this.baserestService.getsearch(this.uuid).then(function (result) {
            _this.searchresultData = result.result;
            _this.services.setnewactionsList(result.result.actions);
            _this.setData();
        }, function (error) {
            console.log(error);
        });
        this.getFilters();
    };
    searchComponent.prototype.setData = function () {
        if (this.searchresultData && this.searchresultData.soegemuligheder) {
            this.searchData = this.searchresultData.soegemuligheder;
        }
        this.searchForm = this.formBuilder.group({
            items: this.formBuilder.array([]),
        });
        this.addItem();
    };
    searchComponent.prototype.addItem = function () {
        this.items = this.searchForm.get("items");
        for (var i in this.searchData) {
            this.items.push(this.createDyanicForm(this.searchData[i]));
        }
        this.searchform = this.searchForm;
        this.loading = false;
        setTimeout(function () {
            $(".selectpicker").selectpicker();
        }, 100);
    };
    searchComponent.prototype.resetSearch = function () {
        this.setData();
    };
    searchComponent.prototype.panelExpanded = function (event) {
        event.preventDefault();
        this.activeIds = this.activeIds == event.panelId ? "" : event.panelId;
        setTimeout(function () {
            $(".selectpicker").selectpicker();
        }, 500);
    };
    searchComponent.prototype.clearDate = function (item) {
        item.value.value = null;
        item.value.formatdate = null;
        var slcitem = item.value.ld_brugervendtnoegle;
        this.searchForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == slcitem; })[0] = item;
    };
    searchComponent.prototype.createDyanicForm = function (item) {
        var tooltipinfo = item.title;
        if (item.type == "input" &&
            item.ld_brugervendtnoegle != "adresser" &&
            item.type != "file" &&
            item.type != "date") {
            return this.formBuilder.group({
                name: item.titel,
                value: item.value,
                type: item.type,
                property: item.property,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                filtercondition: item.filtercondition,
                filtercondition1: item.filtercondition1,
                filtervalue0: item.filtervalue0,
                filtervalue1: item.filtervalue1,
                filtertype: item.filtertype,
            });
        }
        if (item.type == "datepicker") {
            return this.formBuilder.group({
                name: item.titel,
                value: item.value,
                type: item.type,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                requestParameter: item.requestParameter,
                property: item.property,
                filtercondition0: item.filtercondition0,
                filtercondition1: item.filtercondition1,
                filtervalue: item.filtervalue,
                filtertype: item.filtertype,
                frastartdate: item.value,
                fraenddate: item.value,
                issame: false,
            });
        }
        if (item.type == "button") {
            return this.formBuilder.group({
                name: item.titel,
                value: item.value,
                type: item.type,
                property: item.property,
                requestParameter: item.requestParameter,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
            });
        }
        if (item.type == "dropdown" && item.multiselect === true) {
            var selected_name = void 0;
            var selected_names = [];
            return this.formBuilder.group({
                name: item.titel,
                value: [item.valuelist],
                options: [item.valuelist],
                multiselect: item.multiselect,
                type: item.type,
                selected_name: [selected_names],
                property: item.property,
                requestParameter: item.requestParameter,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                valuelist: item.valuelist,
                filtercondition: item.filtercondition,
                filtervalue: item.filtervalue,
                filtertype: item.filtertype,
            });
        }
    };
    searchComponent.prototype.dateChange = function (item, index) {
        var dateformat = null;
        var frastartdate = null;
        var fraenddate = null;
        if (item.value.formatdate[0] && item.value.formatdate[0]._d) {
            frastartdate = moment__WEBPACK_IMPORTED_MODULE_6__(item.value.formatdate[0]._d).format("DD-MM-YYYY HH:mm");
        }
        if (item.value.formatdate[1] && item.value.formatdate[1]._d) {
            fraenddate = moment__WEBPACK_IMPORTED_MODULE_6__(item.value.formatdate[1]._d).format("DD-MM-YYYY HH:mm");
        }
        if (moment__WEBPACK_IMPORTED_MODULE_6__(frastartdate, "DD-MM-YYYY").isSame(moment__WEBPACK_IMPORTED_MODULE_6__(fraenddate, "DD-MM-YYYY"), "day")) {
            frastartdate =
                moment__WEBPACK_IMPORTED_MODULE_6__(item.value.formatdate[0]._d).format("DD-MM-YYYY") + " 00:00";
            fraenddate =
                moment__WEBPACK_IMPORTED_MODULE_6__(item.value.formatdate[1]._d).format("DD-MM-YYYY") + " 23:59";
            item.value.formatdate[0] = moment__WEBPACK_IMPORTED_MODULE_6__(frastartdate, "DD-MM-YYYY HH:mm");
            item.value.formatdate[1] = moment__WEBPACK_IMPORTED_MODULE_6__(fraenddate, "DD-MM-YYYY HH:mm");
            item.value.issame = true;
        }
        item.value.frastartdate = frastartdate;
        item.value.fraenddate = fraenddate;
        this.searchForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle; })[0] = item;
        this.ref.detectChanges();
    };
    searchComponent.prototype.Multiselect = function (event, itemname) {
        var filtereditem = this.searchForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0];
        for (var j in itemname.value.options) {
            if (itemname.value.selected_name.indexOf(itemname.value.options[j].rel_uri) != -1) {
                filtereditem.options[j].selected = true;
            }
            else {
                filtereditem.options[j].selected = false;
            }
        }
        this.searchForm.controls.items.value.filter(function (x) { return x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle; })[0] = filtereditem;
    };
    searchComponent.prototype.Search = function () {
        this.allfilters.length = 0;
        for (var i in this.searchForm.controls.items.value) {
            if (this.searchForm.controls.items.value[i].type == "input" &&
                this.searchForm.controls.items.value[i].property != "hidden" &&
                this.searchForm.controls.items.value[i].value) {
                this.allfilters.push(this.searchForm.controls.items.value[i]);
            }
            if (this.searchForm.controls.items.value[i].type == "dropdown" &&
                this.searchForm.controls.items.value[i].selected_name.length != 0) {
                this.allfilters.push(this.searchForm.controls.items.value[i]);
            }
            if (this.searchForm.controls.items.value[i].type == "datepicker" &&
                this.searchForm.controls.items.value[i].formatdate) {
                this.allfilters.push(this.searchForm.controls.items.value[i]);
            }
        }
        if (this.allfilters && this.allfilters.length > 0) {
            this.loading = true;
            this.getSearchData();
        }
        else if (this.allfilters && this.allfilters.length == 0) {
            this.loading = true;
            this.getGriddata();
        }
    };
    searchComponent.prototype.getSearchData = function () {
        var _this = this;
        var formdata = {};
        var filteritem = {};
        // let filterscount=null;
        var operatypetye = 0;
        var filtercount = null;
        var totalfiltercount = 0;
        var fltercountdropdown = null;
        var fltercountdate = null;
        var filterscountdate = null;
        var selectedels = [];
        for (var i in this.allfilters) {
            if (this.allfilters[i].type == "input" &&
                this.allfilters[i].property != "hidden" &&
                this.allfilters[i].value) {
                var itemname = this.allfilters[i].ld_brugervendtnoegle + "operator";
                if (filtercount === null) {
                    filtercount = 0;
                }
                else {
                    filtercount++;
                }
                //filtercount = filtercount?filtercount:0;
                filteritem[itemname] = "and";
                var filteroperatorindex = "filteroperator" + filtercount;
                var filtervalue = "filtervalue" + filtercount;
                var filtercondition = "filtercondition" + filtercount;
                var filteroperator = "filteroperator" + filtercount;
                var filterdatafield = "filterdatafield" + filtercount;
                var filtertype = "filtertype" + filtercount;
                totalfiltercount += 1;
                filteritem["filterscount"] = totalfiltercount;
                filteritem[filtervalue] = this.allfilters[i].value;
                filteritem[filtercondition] = this.allfilters[i].filtercondition;
                filteritem[filteroperatorindex] = operatypetye;
                filteritem[filterdatafield] = this.allfilters[i].ld_brugervendtnoegle;
                filteritem[filtertype] = this.allfilters[i].filtertype;
                // selectedels.push(filteritem)
            }
        }
        for (var i in this.allfilters) {
            if (this.allfilters[i].type == "dropdown" &&
                this.allfilters[i].selected_name.length != 0) {
                var itemname = this.allfilters[i].ld_brugervendtnoegle + "operator";
                var selectedelscount = 0;
                if (!selectedels.length) {
                    selectedelscount = 0;
                }
                else {
                    selectedelscount = selectedels.length;
                }
                filteritem[itemname] = "and";
                if (fltercountdropdown === null) {
                    fltercountdropdown = 0;
                }
                else {
                    fltercountdropdown++;
                }
                for (var j in this.allfilters[i].selected_name) {
                    selectedelscount = selectedels.length + 1;
                    totalfiltercount += 1;
                    if (filtercount === null) {
                        filtercount = 0;
                    }
                    else {
                        filtercount += 1;
                    }
                    filteritem["filterscount"] = totalfiltercount;
                    if (selectedels.length == 0) {
                        selectedelscount = 0;
                    }
                    else {
                        selectedelscount = selectedels.length;
                    }
                    var filteroperatorindex = "filteroperator" + filtercount; //+(+i+1);//
                    var filtervalue = "filtervalue" + filtercount; // +(+i+1);
                    var filtercondition = "filtercondition" + filtercount; //+j;// +selectedelscount;
                    var filteroperator = "filteroperator" + filtercount; //+j;//+selectedelscount;
                    var filterdatafield = "filterdatafield" + filtercount; //+j;//+selectedelscount;
                    var filtertype = "filtertype" + filtercount; //+j;// +selectedelscount;
                    filteritem[filtervalue] = this.allfilters[i].selected_name[j];
                    selectedels.push(this.allfilters[i].selected_name[j]);
                    filteritem[filtercondition] = this.allfilters[i].filtercondition;
                    filteritem[filteroperatorindex] = this.allfilters[i].filtervalue;
                    filteritem[filterdatafield] = this.allfilters[i].ld_brugervendtnoegle;
                    filteritem[filtertype] = this.allfilters[i].filtertype;
                }
            }
        }
        for (var i in this.allfilters) {
            if (this.allfilters[i].type == "datepicker" &&
                this.allfilters[i].formatdate) {
                var itemindex = "filtertype" + i;
                var filteroperatorindex = "filteroperator" + i;
                if (filterscountdate === null) {
                    filterscountdate = 0;
                }
                else {
                    filterscountdate++;
                }
                // let filteroperator = 'filteroperator' + i;
                var filterdatafield = void 0;
                if (this.allfilters[i].formatdate[0] ||
                    this.allfilters[i].formatdate[1]) {
                    var itemname = this.allfilters[i].ld_brugervendtnoegle + "operator";
                    filteritem[itemname] = "and";
                }
                if (this.allfilters[i].formatdate[0]) {
                    if (filtercount === null) {
                        filtercount = 0;
                    }
                    else {
                        filtercount += 1;
                    }
                    if (fltercountdate === null) {
                        fltercountdate = 0;
                    }
                    else {
                        fltercountdate++;
                    }
                    totalfiltercount += 1;
                    var filteroperatorindex_1 = "filteroperator" + filtercount;
                    var filtervalue = "filtervalue" + filtercount;
                    var filtercondition = "filtercondition" + filtercount;
                    var filteroperator = "filteroperator" + filtercount;
                    var filterdatafield_1 = "filterdatafield" + filtercount;
                    var filtertype = "filtertype" + filtercount;
                    filteritem[filtervalue] = this.allfilters[i].frastartdate; // moment(this.searchForm.controls.items.value[i].formatdate[0]).format('DD-MM-yyyy HH:mm:ss');   //+ ' 00:00:00';
                    filteritem[filtercondition] = this.allfilters[i].filtercondition0;
                    filteritem[filteroperatorindex_1] = this.allfilters[i].filtervalue;
                    filteritem[filterdatafield_1] = this.allfilters[i].ld_brugervendtnoegle;
                    filteritem[filtertype] = this.allfilters[i].filtertype;
                    filteritem["filterscount"] = totalfiltercount;
                }
                if (this.allfilters[i].formatdate[1]) {
                    if (filtercount === null) {
                        filtercount = 0;
                    }
                    else {
                        filtercount += 1;
                    }
                    totalfiltercount += 1;
                    var filteroperatorindex_2 = "filteroperator" + filtercount;
                    var filtervalue = "filtervalue" + filtercount;
                    var filtercondition = "filtercondition" + filtercount;
                    var filteroperator = "filteroperator" + filtercount;
                    var filterdatafield_2 = "filterdatafield" + filtercount;
                    var filtertype = "filtertype" + filtercount;
                    filteritem[filtervalue] = this.allfilters[i].fraenddate; // moment(this.searchForm.controls.items.value[i].formatdate[0]).format('DD-MM-yyyy HH:mm:ss');   //+ ' 00:00:00';
                    filteritem[filtercondition] = this.allfilters[i].filtercondition1;
                    filteritem[filteroperatorindex_2] = this.allfilters[i].filtervalue;
                    filteritem[filterdatafield_2] = this.allfilters[i].ld_brugervendtnoegle;
                    filteritem[filtertype] = this.allfilters[i].filtertype;
                    filteritem["filterscount"] = totalfiltercount;
                }
            }
        }
        this.services.setfiltersData(filteritem);
        var action = this.searchForm.controls.items.value.filter(function (item) { return item.requestParameter == "action"; })[0].value;
        this.baserestService.searchgridData(this.uuid, action, filteritem).then(function (res) {
            _this.successNotification.refresh();
            _this.successNotification.open();
            _this.loading = false;
            _this.setdata(res);
        }, function (error) {
            console.log(error);
            _this.errorNotification.open();
            _this.errorNotification.refresh();
            _this.loading = false;
        });
    };
    searchComponent.prototype.Fetchfilter = function () {
        var _this = this;
        this.sharedthis.fetchingfilter = true;
        if (this.sharedthis.filteruuid != 0) {
            this.sharedthis.service.getFilterData(this.filteruuid, this.uuid).then(function (res) {
                _this.successNotification.refresh();
                _this.successNotification.open();
                _this.loading = false;
                _this.setdata(res);
                $(".selectpicker").selectpicker("refresh");
                _this.sharedthis.fetchingfilter = false;
            }, function (error) {
                _this.errorNotification.open();
                console.log(error);
            });
        }
        else {
            this.sharedthis.fetchingfilter = false;
        }
    };
    searchComponent.prototype.getFilters = function () {
        var _this = this;
        this.baserestService.getmenufilters(this.uuid).then(function (filters) {
            _this.allfilterList = _this.allfilterList.concat(filters.data);
            _this.filterList = _this.allfilterList;
            _this.services.setnewallfilterList(_this.filterList);
            $(".selectpicker").selectpicker("refresh");
            _this.ref.detectChanges();
        }, function (error) {
            _this.errorNotification.open();
            console.log(error);
        });
    };
    searchComponent.prototype.setdata = function (result) {
        this.gridrowsandColumns = result;
        this.filteredGriddata.emit(this.gridrowsandColumns);
        this.activeIds = "";
    };
    searchComponent.prototype.formatData = function (item) {
        if (item && item.gui_filter) {
            if (item.type == "input") {
                return {
                    filtertype0: "stringfilter",
                    value: [item.value],
                };
            }
            if (item.type == "editor") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                };
            }
            if (item.type == "textarea") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                };
            }
            if (item.type == "html") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value.replace(/(?:\r\n|\r|\n)/g, "<br>")],
                };
            }
            if (item.type == "date") {
                var formateddate = null;
                if (item.formatdate) {
                    formateddate = moment__WEBPACK_IMPORTED_MODULE_6__(item.formatdate).format("YYYY-MM-DD HH:mm:ss");
                    if (formateddate === "Invalid date") {
                        formateddate = null;
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: formateddate ? [formateddate] : [null],
                };
            }
            if (item.type == "dropdown" && item.multiselect == true) {
                var values = [];
                if (item.selected_name && item.selected_name.length != 0) {
                    for (var i in item.options) {
                        if (item.selected_name.indexOf(item.options[i].rel_uri) != -1) {
                            values.push(item.options[i].rel_uuid);
                        }
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: values,
                };
            }
        }
    };
    searchComponent.prototype.getGriddata = function () {
        var _this = this;
        this.baserestService.getGriddata(this.uuid).then(function (res) {
            _this.successNotification.refresh();
            _this.successNotification.open();
            _this.loading = false;
            _this.setdata(res);
        }, function (error) {
            console.log(error);
            _this.errorNotification.open();
            _this.errorNotification.refresh();
            _this.loading = false;
        });
    };
    searchComponent.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], searchComponent.prototype, "sharedthis", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], searchComponent.prototype, "actions", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], searchComponent.prototype, "filteredGriddata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], searchComponent.prototype, "updateOperations", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("successNotification", { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], searchComponent.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("errorNotification", { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], searchComponent.prototype, "errorNotification", void 0);
    searchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "search-viewer",
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./search.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/search/search.html")).default,
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]])
    ], searchComponent);
    return searchComponent;
}());



/***/ }),

/***/ "./src/components/sendemail/sendemail.ts":
/*!***********************************************!*\
  !*** ./src/components/sendemail/sendemail.ts ***!
  \***********************************************/
/*! exports provided: sendemailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendemailComponent", function() { return sendemailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");





var sendemailComponent = /** @class */ (function () {
    function sendemailComponent(activeModal, baserestService) {
        this.activeModal = activeModal;
        this.baserestService = baserestService;
        this.loading = false;
    }
    sendemailComponent.prototype.ngOnInit = function () {
    };
    sendemailComponent.prototype.cancel = function (e) {
        this.activeModal.dismissAll();
    };
    sendemailComponent.prototype.sendEmail = function (e) {
        var _this = this;
        this.loading = true;
        var action = this.action.key ? this.action.key : this.action[0].key;
        this.baserestService.notificationsDispatch(action).then(function (success) {
            _this.loading = false;
            _this.successNotification.refresh();
            _this.successNotification.open();
            _this.activeModal.dismissAll();
        }, function (error) {
            _this.loading = false;
            _this.errorNotification.refresh();
            _this.errorNotification.open();
            _this.activeModal.dismissAll();
        });
    };
    sendemailComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], sendemailComponent.prototype, "action", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], sendemailComponent.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], sendemailComponent.prototype, "errorNotification", void 0);
    sendemailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'sendemail-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sendemail.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/sendemail/sendemail.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"]])
    ], sendemailComponent);
    return sendemailComponent;
}());



/***/ }),

/***/ "./src/components/sendmessages/sendmessages.ts":
/*!*****************************************************!*\
  !*** ./src/components/sendmessages/sendmessages.ts ***!
  \*****************************************************/
/*! exports provided: sendmessagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendmessagesComponent", function() { return sendmessagesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var ng_pick_datetime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng-pick-datetime */ "./node_modules/ng-pick-datetime/picker.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);






var sendmessagesComponent = /** @class */ (function () {
    function sendmessagesComponent(baserestService, auth, dateTimeAdapter, _zone, _elm, ref) {
        this.baserestService = baserestService;
        this.auth = auth;
        this.dateTimeAdapter = dateTimeAdapter;
        this._zone = _zone;
        this.ref = ref;
        this.allreceivers = [
            {
                'uuid': '1212',
                'titel': 'xyz'
            },
            {
                'uuid': '1212',
                'titel': 'abc'
            },
            {
                'uuid': '1212',
                'titel': 'def'
            }
        ];
        this.editorData = '';
        this.configoptions = "{ toolbar: [ 'heading', '|', 'bold', 'italic', 'link', '|', 'bulletedList', 'numberedList', 'blockQuote', 'codeBlock', '|', 'undo', 'redo']}";
        this.dateTimeAdapter.setLocale('da-DK');
        this.createstdate();
        this.config = {
            height: '500px',
            startupOutlineBlocks: true
        };
    }
    sendmessagesComponent.prototype.ngOnInit = function () {
    };
    sendmessagesComponent.prototype.ngAfterViewInit = function () {
        setTimeout(function () {
            $('select').selectpicker();
        }, 10);
    };
    sendmessagesComponent.prototype.DateChanged = function (selecteddate) {
        if (selecteddate && selecteddate._d) {
            this.selecteddate = moment__WEBPACK_IMPORTED_MODULE_5__(selecteddate._d).format('YYYY-MM-DD HH:mm:ss');
        }
    };
    sendmessagesComponent.prototype.selectedDate = function (e) {
        console.log(e);
    };
    sendmessagesComponent.prototype.onChange = function (e, i) {
        console.log(i);
    };
    sendmessagesComponent.prototype.onchngEditordata = function (editor) {
        // const data = editor.editor.getData();
        // c
        console.log(editor);
    };
    sendmessagesComponent.prototype.openfilter = function (editor) {
        console.log("open");
        editor.instances.IDofEditor.insertText('some text here');
        // this.editorComponent.editing.view.change( writer => {
        //     const viewEditableRoot = editor.editing.view.document.getRoot();
        //     writer.setAttribute( 'myAttribute', 'value', viewEditableRoot );
        // } );
    };
    sendmessagesComponent.prototype.insertdata = function () {
        // this.editorComponent.instances.IDofEditor.insertText('some text here');  
        // CKEditor4.instances.IDofEditor.insertText('some text here');
        //   const selection = this.editorComponent.model.document.selection;
        //   const range = selection.getFirstRange();
        //   this.editorComponent.model.change(writer => {
        //     writer.insert('appendData', range.start);
        //   });
        //   const el = document.getElementById('editor');
        //   insertTextAtCursor(el, 'foobar');
    };
    sendmessagesComponent.prototype.createstdate = function () {
        this.startat = new Date().toLocaleString('dk-DA', { timeZone: 'Europe/Copenhagen' });
        var dd;
        var mm;
        var yyyy;
        if (this.startat.indexOf('/') != -1) {
            dd = this.startat.split("/")[0];
            mm = this.startat.split("/")[1];
            yyyy = this.startat.split("/")[2].split(",")[0];
        }
        else {
            dd = this.startat.split(".")[0];
            mm = this.startat.split(".")[1];
            yyyy = this.startat.split(".")[2].split(" ")[0];
        }
        var time = this.startat.split(' ')[1];
        var hh = time.split(':')[0];
        var mi = time.split(':')[1];
        var secs = time.split(':')[2];
        this.mindate = new Date(yyyy, mm - 1, dd, hh, mi);
    };
    sendmessagesComponent.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"] },
        { type: ng_pick_datetime__WEBPACK_IMPORTED_MODULE_4__["DateTimeAdapter"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('editor', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], sendmessagesComponent.prototype, "editorComponent", void 0);
    sendmessagesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'sendmessages',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sendmessages.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/sendmessages/sendmessages.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"], ng_pick_datetime__WEBPACK_IMPORTED_MODULE_4__["DateTimeAdapter"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], sendmessagesComponent);
    return sendmessagesComponent;
}());



/***/ }),

/***/ "./src/components/skema/skema.ts":
/*!***************************************!*\
  !*** ./src/components/skema/skema.ts ***!
  \***************************************/
/*! exports provided: Skema */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Skema", function() { return Skema; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-clipboard */ "./node_modules/ngx-clipboard/fesm5/ngx-clipboard.js");
/* harmony import */ var ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ang-jsoneditor */ "./node_modules/ang-jsoneditor/fesm5/ang-jsoneditor.js");
/* harmony import */ var _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../providers/utils/helperService */ "./src/providers/utils/helperService.ts");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");










var Skema = /** @class */ (function () {
    function Skema(baserestService, auth, ref, modalService, clipboardService, helperService) {
        var _this = this;
        this.baserestService = baserestService;
        this.auth = auth;
        this.ref = ref;
        this.modalService = modalService;
        this.clipboardService = clipboardService;
        this.helperService = helperService;
        this.slctorg = null;
        this.skema = null;
        this.teaser = '';
        this.beskrivelse = '';
        this.flettefelter = "[]";
        this.slctlang = 'da';
        this.loading = false;
        this.orgcopyflag = false;
        this.brugervendcopyflag = false;
        this.textcopyflag = false;
        this.issubmitted = false;
        this.showupload = false;
        this.slctdSkema = null;
        this.ngbModalOptions = {
            backdrop: 'static',
            keyboard: false
        };
        this.editorOptions = new ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorOptions"]();
        this.auth.userrelations.subscribe(function (userrelations) { _this.userrelations = userrelations; });
    }
    Skema.prototype.ngOnInit = function () {
        console.log("in Skema");
        console.log(this.userrelations);
        var sortlist = this.helperService.sortList(this.userrelations);
        this.sorteduserrelations = sortlist;
        this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
    };
    Skema.prototype.ngAfterViewInit = function () {
        $('select').selectpicker();
    };
    Skema.prototype.onChange = function (e, value) {
        var _this = this;
        this.loading = true;
        console.log(e.target.value);
        this.skemacontent = null;
        var item = this.sorteduserrelations.filter(function (x) { return x.organisation_uuid == _this.slctorg; })[0];
        this.slcOrgname = item.org;
        this.getskemaslist();
    };
    Skema.prototype.getskemaslist = function () {
        var _this = this;
        this.skemalist = null;
        this.skema = null;
        this.ref.detectChanges();
        this.baserestService.getskemaslist(this.slctorg).then(function (skemalist) { _this.renderSkema(skemalist.data); }, function (error) { console.log(error); _this.loading = false; });
    };
    Skema.prototype.renderSkema = function (skemalist) {
        var _this = this;
        if (skemalist && skemalist.length > 0) {
            var sortedList = this.sortbyAlpha(skemalist);
            this.skemalist = sortedList;
            setTimeout(function () {
                $('select').selectpicker();
            });
        }
        this.slctdSkema = this.skemalist.filter(function (x) { return x.uuid == _this.skema; })[0];
        console.log(this.slctdSkema);
        this.loading = false;
    };
    Skema.prototype.sortbyAlpha = function (skemalist) {
        var sorted = skemalist.sort(function (a, b) {
            var nameA = a.brugervendtnoegle.toLowerCase(), nameB = b.brugervendtnoegle.toLowerCase();
            if (nameA < nameB)
                return -1;
            if (nameA > nameB)
                return 1;
            return 0;
        });
        return sorted;
    };
    Skema.prototype.onchangeSkema = function () {
        var _this = this;
        this.loading = true;
        this.slctdSkema = this.skemalist.filter(function (x) { return x.uuid == _this.skema; })[0];
        this.slctbrugervendt = this.slctdSkema.brugervendtnoegle;
        this.baserestService.getskemacontent(this.skema).then(function (skemacontent) { _this.skemacontent = skemacontent; _this.setData(); }, function (error) { console.log(error); _this.loading = false; });
    };
    Skema.prototype.setData = function () {
        this.loading = false;
        this.body = this.skemacontent;
        this.changedjsondata = this.skemacontent.body;
        setTimeout(function () {
            $('select').selectpicker();
        });
    };
    Skema.prototype.Copyorg = function (flag) {
        this.orgcopyflag = false;
        this.brugervendcopyflag = false;
        this.textcopyflag = false;
        switch (flag) {
            case "orgcopyflag":
                this.orgcopyflag = true;
                break;
            case "brugervendcopyflag":
                this.brugervendcopyflag = true;
                break;
            case "textcopyflag":
                this.textcopyflag = true;
                break;
            default:
                break;
        }
        //   this.clipboardService.copyFromContent(this.slcOrgname);
    };
    Skema.prototype.copied = function (e) {
        console.log(e);
    };
    Skema.prototype.Cancel = function (e) {
        e.preventDefault();
        this.skemalist = null;
        this.skemacontent = null;
        this.slctdSkema = null;
    };
    Skema.prototype.saveSkema = function (modal, event) {
        var _this = this;
        this.issubmitted = true;
        var bodydata;
        bodydata = JSON.stringify(this.changedjsondata);
        if (bodydata && this.showupload === false) {
            this.loading = true;
            this.baserestService.saveskemaoncontent(bodydata).then(function (savedcontent) {
                _this.successNotification.open();
                _this.successNotification.open();
                _this.loading = false;
                _this.getskemaslist();
            }, function (error) {
                console.log(error);
                _this.errorNotification.open();
                _this.errorNotification.open();
                _this.loading = false;
            });
        }
        else if ((!bodydata || bodydata == "null") && this.showupload === true) {
            this.modalReference = this.modalService.open(modal, event);
            this.loading = false;
        }
    };
    Skema.prototype.createSkema = function (modal, event) {
        this.modalReference = this.modalService.open(modal, event);
    };
    Skema.prototype.DownloadJson = function (e) {
        e.preventDefault();
        var blob = new Blob([JSON.stringify(this.body)], { type: 'application/json' });
        Object(file_saver__WEBPACK_IMPORTED_MODULE_8__["saveAs"])(blob, this.slctbrugervendt + '_skema' + '.json');
    };
    Skema.prototype.Deleteconfig = function (e) {
        e.preventDefault();
        this.body = null;
        ;
        this.changedjsondata = null;
        ;
        this.skemacontent = null;
        ;
        this.showupload = true;
        this.ref.detectChanges();
    };
    Skema.prototype.uploadfile = function (evt) {
        var _this = this;
        try {
            var files = evt.target.files;
            if (!files.length) {
                console.log('No file selected!');
                this.errorNotification.open();
                this.errorNotification.open();
                return;
            }
            var file = files[0];
            var reader = new FileReader();
            var self_1 = this;
            reader.onload = function (event) {
                var data = JSON.parse(event.target.result);
                _this.body = data;
                _this.changedjsondata = data;
                _this.skemacontent = data;
                _this.showupload = false;
                _this.ref.detectChanges();
            };
            reader.readAsText(file);
        }
        catch (err) {
            console.error(err);
        }
    };
    Skema.prototype.getData = function (e) {
        this.changedjsondata = e;
    };
    Skema.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbModal"] },
        { type: ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__["ClipboardService"] },
        { type: _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_7__["HelperSerice"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], Skema.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], Skema.prototype, "errorNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorComponent"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorComponent"])
    ], Skema.prototype, "editor", void 0);
    Skema = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'skema',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./skema.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/skema/skema.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbModal"],
            ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__["ClipboardService"], _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_7__["HelperSerice"]])
    ], Skema);
    return Skema;
}());



/***/ }),

/***/ "./src/components/skemacreate/skemacreate.ts":
/*!***************************************************!*\
  !*** ./src/components/skemacreate/skemacreate.ts ***!
  \***************************************************/
/*! exports provided: skemacreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "skemacreateComponent", function() { return skemacreateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var ang_jsoneditor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ang-jsoneditor */ "./node_modules/ang-jsoneditor/fesm5/ang-jsoneditor.js");






var skemacreateComponent = /** @class */ (function () {
    function skemacreateComponent(activeModal, ref, baserestService) {
        this.activeModal = activeModal;
        this.ref = ref;
        this.baserestService = baserestService;
        this.loading = false;
        this.showupload = true;
        this.editorOptions = new ang_jsoneditor__WEBPACK_IMPORTED_MODULE_5__["JsonEditorOptions"]();
    }
    skemacreateComponent.prototype.ngOnInit = function () {
        this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
    };
    skemacreateComponent.prototype.cancel = function (e) {
        this.activeModal.dismissAll();
    };
    skemacreateComponent.prototype.uploadNew = function (e) {
        e.preventDefault();
        this.bodydata = JSON.stringify(this.changedjsondata);
        this.changedjsondata = this.bodydata;
        this.ref.detectChanges();
    };
    skemacreateComponent.prototype.getDataa = function (e) {
        this.changedjsondata = e;
    };
    skemacreateComponent.prototype.uploadfilee = function (evt) {
        var _this = this;
        try {
            var files = evt.target.files;
            if (!files.length) {
                console.log('No file selected!');
                this.errorNotification.open();
                this.errorNotification.open();
                return;
            }
            var file = files[0];
            var reader = new FileReader();
            var self_1 = this;
            reader.onload = function (event) {
                var data = JSON.parse(event.target.result);
                _this.changedjsondata = data;
                _this.bodydata = data;
                _this.showupload = false;
                _this.ref.detectChanges();
            };
            reader.readAsText(file);
        }
        catch (err) {
            console.error(err);
        }
    };
    skemacreateComponent.prototype.skemadelete = function (e) {
        console.log(e);
        this.loading = true;
        var bodydata;
        bodydata = JSON.stringify(this.changedjsondata);
        console.log(bodydata);
    };
    skemacreateComponent.prototype.skemacreate = function () {
        var _this = this;
        this.loading = true;
        this.baserestService.saveskemaoncontent(JSON.stringify(this.changedjsondata)).then(function (savedcontent) {
            _this.successNotification.open();
            _this.successNotification.open();
            _this.loading = false;
            _this.activeModal.dismissAll();
        }, function (error) {
            console.log(error);
            _this.errorNotification.open();
            _this.errorNotification.open();
            _this.loading = false;
            _this.activeModal.dismissAll();
        });
    };
    skemacreateComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], skemacreateComponent.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], skemacreateComponent.prototype, "errorNotification", void 0);
    skemacreateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'skemacreate-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./skemacreate.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/skemacreate/skemacreate.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"]])
    ], skemacreateComponent);
    return skemacreateComponent;
}());



/***/ }),

/***/ "./src/components/skemadelete/skemadelete.ts":
/*!***************************************************!*\
  !*** ./src/components/skemadelete/skemadelete.ts ***!
  \***************************************************/
/*! exports provided: skemadeleteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "skemadeleteComponent", function() { return skemadeleteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");





var skemadeleteComponent = /** @class */ (function () {
    function skemadeleteComponent(activeModal, baserestService) {
        this.activeModal = activeModal;
        this.baserestService = baserestService;
        this.loading = false;
    }
    skemadeleteComponent.prototype.ngOnInit = function () {
    };
    skemadeleteComponent.prototype.cancel = function (e) {
        this.activeModal.dismissAll();
    };
    skemadeleteComponent.prototype.skemadelete = function (e) {
        var _this = this;
        console.log(e);
        this.loading = true;
        this.baserestService.deleteSkema(this.slctdSkema.uuid).then(function (success) {
            _this.loading = false;
            _this.successNotification.refresh();
            _this.successNotification.open();
            _this.activeModal.dismissAll();
        }, function (error) {
            _this.loading = false;
            _this.errorNotification.refresh();
            _this.errorNotification.open();
            _this.activeModal.dismissAll();
        });
    };
    skemadeleteComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], skemadeleteComponent.prototype, "slctdSkema", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], skemadeleteComponent.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], skemadeleteComponent.prototype, "errorNotification", void 0);
    skemadeleteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'skemadelete-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./skemadelete.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/skemadelete/skemadelete.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"]])
    ], skemadeleteComponent);
    return skemadeleteComponent;
}());



/***/ }),

/***/ "./src/components/templates/templates.ts":
/*!***********************************************!*\
  !*** ./src/components/templates/templates.ts ***!
  \***********************************************/
/*! exports provided: Templates */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Templates", function() { return Templates; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-clipboard */ "./node_modules/ngx-clipboard/fesm5/ngx-clipboard.js");
/* harmony import */ var ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ang-jsoneditor */ "./node_modules/ang-jsoneditor/fesm5/ang-jsoneditor.js");
/* harmony import */ var _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../providers/utils/helperService */ "./src/providers/utils/helperService.ts");








var Templates = /** @class */ (function () {
    function Templates(baserestService, auth, clipboardService, helperService) {
        var _this = this;
        this.baserestService = baserestService;
        this.auth = auth;
        this.clipboardService = clipboardService;
        this.helperService = helperService;
        this.slctorg = "vaælge";
        this.skebelon = "vaælge";
        this.teaser = '';
        this.beskrivelse = '';
        this.ikon = '';
        this.flettefelter = "[]";
        this.slctlang = 'da';
        this.loading = false;
        this.orgcopyflag = false;
        this.brugervendcopyflag = false;
        this.textcopyflag = false;
        this.issubmitted = false;
        this.editorOptions = new ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorOptions"]();
        this.auth.userrelations.subscribe(function (userrelations) {
            _this.userrelations = userrelations;
        });
    }
    Templates.prototype.ngOnInit = function () {
        console.log("in Translation");
        console.log(this.userrelations);
        var sortlist = this.helperService.sortList(this.userrelations);
        this.sorteduserrelations = sortlist;
        this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
    };
    Templates.prototype.ngAfterViewInit = function () {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        $('select').selectpicker();
    };
    Templates.prototype.onChange = function (e, value) {
        var _this = this;
        this.loading = true;
        console.log(e.target.value);
        this.skemaList = null;
        this.skabeloncontent = null;
        var item = this.sorteduserrelations.filter(function (x) { return x.organisation_uuid == _this.slctorg; })[0];
        this.slcOrgname = item.org;
        this.baserestService.getskabelonlist(this.slctorg).then(function (skabelonlist) { _this.skabelonlist = skabelonlist.data; _this.renderSkema(); }, function (error) { console.log(error); _this.loading = false; });
    };
    Templates.prototype.renderSkema = function () {
        if (this.skabelonlist && this.skabelonlist.length > 0) {
            var sortedList = this.sortbyAlpha(this.skabelonlist);
            this.skemaList = sortedList;
            setTimeout(function () {
                $('select').selectpicker();
            });
        }
        this.loading = false;
    };
    Templates.prototype.sortbyAlpha = function (skabelonlist) {
        var sorted = skabelonlist.sort(function (a, b) {
            var nameA = a.brugervendtnoegle.toLowerCase(), nameB = b.brugervendtnoegle.toLowerCase();
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
        return sorted;
    };
    Templates.prototype.onChangelang = function (e) {
        var _this = this;
        this.loading = true;
        this.slctlang = e.target.value;
        this.baserestService.getskabeloncontent(this.skebelon, this.slctlang).then(function (skabeloncontent) { _this.skabeloncontent = skabeloncontent; _this.setData(); }, function (error) { console.log(error); _this.loading = false; });
    };
    Templates.prototype.onchangeSkabelon = function () {
        var _this = this;
        this.loading = true;
        var item = this.skabelonlist.filter(function (x) { return x.uuid == _this.skebelon; })[0];
        this.slctbrugervendt = item.brugervendtnoegle;
        this.baserestService.getskabeloncontent(this.skebelon, this.slctlang).then(function (skabeloncontent) { _this.skabeloncontent = skabeloncontent; _this.setData(); }, function (error) { console.log(error); _this.loading = false; });
    };
    Templates.prototype.setData = function () {
        this.loading = false;
        this.title = this.skabeloncontent.title;
        this.body = this.skabeloncontent.body;
        this.changedjsondata = this.skabeloncontent.body;
        this.teaser = this.skabeloncontent.teaser;
        this.beskrivelse = this.skabeloncontent.beskrivelse;
        this.ikon = this.skabeloncontent.ikon;
        this.jsoneditor = this.skabeloncontent.jsoneditor;
        this.flettefelter = this.skabeloncontent.flettefelter ? this.skabeloncontent.flettefelter : "[]";
        setTimeout(function () {
            $('select').selectpicker();
        });
        //this.slctlang = this.skabeloncontent.sprog ?this.skabeloncontent.sprog:'da' ;
    };
    Templates.prototype.Copyorg = function (flag) {
        this.orgcopyflag = false;
        this.brugervendcopyflag = false;
        this.textcopyflag = false;
        switch (flag) {
            case "orgcopyflag":
                this.orgcopyflag = true;
                break;
            case "brugervendcopyflag":
                this.brugervendcopyflag = true;
                break;
            case "textcopyflag":
                this.textcopyflag = true;
                break;
            default:
                break;
        }
        //   this.clipboardService.copyFromContent(this.slcOrgname);
    };
    Templates.prototype.copied = function (e) {
        console.log(e);
    };
    Templates.prototype.Cancel = function (e) {
        e.preventDefault();
        this.skabelonlist.length = 0;
        this.skabeloncontent = null;
    };
    Templates.prototype.saveSkabelon = function () {
        var _this = this;
        this.loading = true;
        this.issubmitted = true;
        var bodydata;
        if (this.jsoneditor === true) {
            bodydata = JSON.stringify(this.changedjsondata);
        }
        else {
            bodydata = this.body;
        }
        if (this.title && this.title.length != 0) {
            this.baserestService.saveskabeloncontent(this.skebelon, this.title, this.teaser, this.beskrivelse, this.ikon, this.flettefelter, this.slctlang, bodydata).then(function (savedcontent) {
                _this.successNotification.open();
                _this.successNotification.open();
                _this.loading = false;
            }, function (error) {
                console.log(error);
                _this.errorNotification.open();
                _this.errorNotification.open();
                _this.loading = false;
            });
        }
        else {
            this.loading = false;
        }
    };
    Templates.prototype.getData = function (e) {
        this.changedjsondata = e;
    };
    Templates.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"] },
        { type: ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__["ClipboardService"] },
        { type: _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_7__["HelperSerice"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], Templates.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_4__["jqxNotificationComponent"])
    ], Templates.prototype, "errorNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorComponent"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ang_jsoneditor__WEBPACK_IMPORTED_MODULE_6__["JsonEditorComponent"])
    ], Templates.prototype, "editor", void 0);
    Templates = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'templates',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./templates.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/templates/templates.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"], ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__["ClipboardService"], _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_7__["HelperSerice"]])
    ], Templates);
    return Templates;
}());



/***/ }),

/***/ "./src/components/translation/translation.ts":
/*!***************************************************!*\
  !*** ./src/components/translation/translation.ts ***!
  \***************************************************/
/*! exports provided: Translation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Translation", function() { return Translation; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");






var xlsx = __webpack_require__(/*! json-as-xlsx */ "./node_modules/json-as-xlsx/index.js");
var Translation = /** @class */ (function () {
    function Translation(baserestService, modalService, changeDetector, auth) {
        var _this = this;
        this.baserestService = baserestService;
        this.modalService = modalService;
        this.changeDetector = changeDetector;
        this.auth = auth;
        this.Objekttype = "skema";
        this.slctorg = "";
        this.loading = false;
        this.uploadStatus = false;
        this.fileStatus = false;
        this.uploadContainer = false;
        this.auth.userrelations.subscribe(function (userrelations) {
            {
                _this.userrelations = userrelations;
            }
        });
    }
    Translation.prototype.ngOnInit = function () {
        console.log("in Translation");
        console.log(this.userrelations);
    };
    Translation.prototype.ngAfterViewInit = function () {
        $('select').selectpicker();
    };
    Translation.prototype.open = function (modal, event) {
        this.changeDetector.detectChanges();
        event.preventDefault();
        this.changeDetector.detectChanges();
        this.modalReference = this.modalService.open(modal);
        this.modalReference.result.then(function (result) {
            //this.closeResult = `Closed with: ${result}`;
        }, function (reason) {
            //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    };
    Translation.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    Translation.prototype.onChange = function (e, value) {
        console.log(value);
        console.log(e.target.value);
        setTimeout(function () {
            $('select').selectpicker();
        }, 10);
    };
    Translation.prototype.export = function (modal, event) {
        var _this = this;
        this.changeDetector.detectChanges();
        event.preventDefault();
        console.log(this);
        this.loading = true;
        this.baserestService.translateExport(this.Objekttype, this.slctorg).then(function (res) { console.log(res); _this.loading = false; _this.downloaData(res); }, function (error) { console.log(error); _this.loading = false; });
    };
    Translation.prototype.downloaData = function (res) {
        if (res && res.data && res.data.length != 0) {
            var orgname;
            this.userrelations.map(function (org) {
                if (this.slctorg == org.organisation_uuid) {
                    orgname = org.org;
                    return;
                }
            }, this);
            var data = res.data; //? res.data : [{ Nodata: 'No data found..' }];
            var fileName = orgname + '_' + this.Objekttype;
            var exportType = 'xls';
            //  exportFromJSON({ data, fileName, exportType });
            var columns = [];
            res.overskrifter.map(function (item) {
                columns.push({ label: item, value: item });
            });
            var settings = {
                sheetName: this.Objekttype,
                fileName: orgname
            };
            if (res.data.length === 0) {
                res.data.push(['']);
            }
            xlsx(columns, res.data, settings);
            this.successNotification.open();
        }
        else {
            this.infoNotification.open();
        }
    };
    Translation.prototype.onfileChange = function (event) {
        event.preventDefault();
        this.uploadContainer = true;
        this.fileName = event.target.files[0];
    };
    Translation.prototype.onfileSubmit = function (content, event) {
        var _this = this;
        console.log(content);
        this.loading = true;
        var filetype;
        if (this.fileName && this.fileName.type) {
            filetype = this.fileName.type;
        }
        else {
            if (this.fileName && this.fileName[0] && this.fileName[0].type) {
                filetype = this.fileName[0].type;
            }
            else {
                filetype = this.fileName.name.split('.').pop();
            }
        }
        var uplaodedfile = this.fileName[0] ? this.fileName[0] : this.fileName;
        var filename = this.fileName.name;
        if (filetype == "text/csv" || filetype == "csv" || filetype == "application/pdf"
            || filetype == "application/json" || filetype == "json" || filetype == "xlsx" || filetype == "application/vnd.ms-excel"
            || filetype == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            console.log("in submit");
            this.loading = true;
            this.baserestService.translateImport(uplaodedfile, filename, this.Objekttype, this.slctorg).then(function (res) {
                console.log(res);
                //if (res.status.kode != 9) {
                if (res && res.response && res.response.success) {
                    _this.serviceErrorMessage = res.response.success;
                    _this.successNotification.open();
                    _this.uploadStatus = false;
                    _this.fileName = null;
                    _this.uploadContainer = false;
                    _this.loading = false;
                    _this.modalReference.close();
                }
                else {
                    //$('.errormessage').text(res.status.besked);
                    _this.serviceErrorMessage = res && res.status && res.status.besked ? res.status.besked : "Error";
                    _this.errorNotification.open();
                    _this.uploadStatus = false;
                    _this.fileName = null;
                    _this.uploadContainer = false;
                    _this.loading = false;
                    _this.modalReference.close();
                }
            }, function (error) {
                _this.errorNotification.open();
                _this.uploadStatus = false;
                _this.fileName = null;
                _this.uploadContainer = false;
                _this.loading = false;
            });
        }
        else {
            this.fileStatus = true;
            this.uploadContainer = false;
            console.log("in cancel ");
            //uploadFile.nativeElement[0].files[0] = null
            this.fileName = null;
            this.uploadContainer = false;
            this.loading = false;
            setTimeout(function () {
                _this.fileStatus = false;
            }, 4000);
        }
    };
    Translation.prototype.fileuploadCancel = function (event) {
        event.preventDefault();
        this.uploadContainer = false;
        console.log("in cancel ");
        this.fileName = null;
        this.uploadContainer = false;
    };
    Translation.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], Translation.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], Translation.prototype, "errorNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('infoNotification', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_5__["jqxNotificationComponent"])
    ], Translation.prototype, "infoNotification", void 0);
    Translation = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'translation',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./translation.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/translation/translation.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"]])
    ], Translation);
    return Translation;
}());



/***/ }),

/***/ "./src/components/uploads/uploads.ts":
/*!*******************************************!*\
  !*** ./src/components/uploads/uploads.ts ***!
  \*******************************************/
/*! exports provided: uploadsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadsComponent", function() { return uploadsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");
/* harmony import */ var _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/auth.rest.service */ "./src/providers/auth.rest.service.ts");
/* harmony import */ var _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/utils/helperService */ "./src/providers/utils/helperService.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");







var uploadsComponent = /** @class */ (function () {
    function uploadsComponent(baserestService, modalService, auth, helperService) {
        var _this = this;
        this.baserestService = baserestService;
        this.modalService = modalService;
        this.auth = auth;
        this.helperService = helperService;
        this.slctpath = null;
        this.picconfig = {
            multiple: true,
            formatsAllowed: ".jpg,.png,.pdf",
            maxSize: "25",
            uploadAPI: {
                url: "https//uat-admin.carex.dk/pictures/",
                headers: {
                    "Content-Type": "text/plain;charset=UTF-8",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT",
                    "Accept": "application/json",
                    "Access-Control-Allow-Headers": "X-Requested-With"
                }
            },
            theme: "",
            hideProgressBar: true,
            hideResetBtn: true,
            hideSelectBtn: false,
            replaceTexts: {
                selectFileBtn: 'Upload files',
                resetBtn: 'Reset files',
                uploadBtn: 'Upload pictures',
                dragNDropBox: 'Drag N Drop',
                attachPinBtn: 'Attach Files...',
                afterUploadMsg_success: 'Successfully Uploaded !',
                afterUploadMsg_error: 'Upload Failed !'
            }
        };
        this.loading = false;
        this.fileToUpload = null;
        this.overwrite = false;
        this.error = false;
        this.success = false;
        this.failedfiles = [];
        this.alloverwrite = false;
        this.ngbModalOptions = {
            backdrop: 'static',
            keyboard: false
        };
        this.message = '';
        this.paths = [
            {
                uuid: 'gjensidige',
                path: 'Public'
            },
            {
                uuid: 'path_uploads',
                path: 'path_uploads'
            },
            {
                uuid: 'path_pictures',
                path: 'path_pictures'
            },
        ];
        this.auth.userrelations.subscribe(function (userrelations) {
            _this.userrelations = userrelations;
        });
    }
    uploadsComponent.prototype.ngOnInit = function () {
        var sortlist = this.helperService.sortList(this.userrelations);
        this.sorteduserrelations = sortlist;
    };
    uploadsComponent.prototype.ngAfterViewInit = function () {
        $('select').selectpicker();
    };
    uploadsComponent.prototype.onChange = function (event, item) {
        if (this.fileUpload && this.fileUpload.selectedFiles) {
            this.fileUpload.resetFileUpload();
        }
        setTimeout(function () {
            $('select').selectpicker();
        }, 50);
    };
    uploadsComponent.prototype.onchangeOrg = function (event, item) {
        console.log('');
    };
    uploadsComponent.prototype.Changeoverwrite = function (e) {
        console.log('');
    };
    uploadsComponent.prototype.savefiles = function () {
        var _this = this;
        this.loading = true;
        this.baserestService.uploadFiles(this.slctpath, this.fileUpload.selectedFiles, this.overwrite).then(function (success) {
            _this.loading = false;
            _this.handler(success);
        }, function (error) {
            _this.loading = false;
            _this.error = true;
            _this.handler(error);
            _this.message = error.error.besked;
            console.log(error);
        });
    };
    uploadsComponent.prototype.handler = function (staus) {
        var statusfile = [];
        if (this.modalReference && !staus.errors) {
            this.modalReference.close();
            this.fileUpload.resetFileUpload();
            this.slctpath = null;
            $('select').selectpicker();
        }
        if (staus && staus.errors) {
            statusfile = staus.errors;
            statusfile.map(function (item) { item.ischecked = false; });
            this.failedfiles = statusfile;
            this.modalReference = this.modalService.open(this.uploadhandler, this.ngbModalOptions);
            this.modalReference.result.then(function (result) {
            }, function (reason) {
                console.log(reason);
            });
        }
        else {
            this.success = true;
            this.successNotification.open();
            this.message = 'Success';
        }
    };
    uploadsComponent.prototype.Changeoverwritefile = function (e, item) {
        item.ischecked = e.target.checked;
    };
    uploadsComponent.prototype.Saveagain = function () {
        var _this = this;
        this.loading = true;
        var result = [];
        if (this.alloverwrite === true) {
            result = this.fileUpload.selectedFiles.filter(function (o) { return _this.failedfiles.some(function (_a) {
                var file = _a.file;
                return o.name === file;
            }); });
        }
        else {
            result = this.fileUpload.selectedFiles.filter(function (o) { return _this.failedfiles.some(function (_a) {
                var file = _a.file, ischecked = _a.ischecked;
                return o.name === file && ischecked === true;
            }); });
        }
        this.baserestService.uploadFiles(this.slctpath, result, true).then(function (success) {
            _this.loading = false;
            _this.successNotification.open();
            _this.handler(success);
        }, function (error) {
            _this.loading = false;
            _this.error = true;
            _this.handler(error);
            _this.message = error.error.besked;
            console.log(error);
            _this.errorNotification.open();
            _this.errorNotification.open();
        });
        this.loading = false;
    };
    uploadsComponent.prototype.cancel = function () {
        this.modalReference.close();
        this.loading = false;
    };
    uploadsComponent.ctorParameters = function () { return [
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModal"] },
        { type: _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"] },
        { type: _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_4__["HelperSerice"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileUpload', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], uploadsComponent.prototype, "fileUpload", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('uploadhandler', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], uploadsComponent.prototype, "uploadhandler", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('successNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_6__["jqxNotificationComponent"])
    ], uploadsComponent.prototype, "successNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errorNotification', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__WEBPACK_IMPORTED_MODULE_6__["jqxNotificationComponent"])
    ], uploadsComponent.prototype, "errorNotification", void 0);
    uploadsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'uploads-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./uploads.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/uploads/uploads.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_base_rest_service__WEBPACK_IMPORTED_MODULE_2__["BaseRestService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModal"], _providers_auth_rest_service__WEBPACK_IMPORTED_MODULE_3__["AuthRestService"], _providers_utils_helperService__WEBPACK_IMPORTED_MODULE_4__["HelperSerice"]])
    ], uploadsComponent);
    return uploadsComponent;
}());



/***/ }),

/***/ "./src/components/username/username.ts":
/*!*********************************************!*\
  !*** ./src/components/username/username.ts ***!
  \*********************************************/
/*! exports provided: UsernameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsernameComponent", function() { return UsernameComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/base.rest.service */ "./src/providers/base.rest.service.ts");




var UsernameComponent = /** @class */ (function () {
    function UsernameComponent(router, baserestService) {
        this.router = router;
        this.baserestService = baserestService;
        this.error = false;
        this.loading = false;
        this.noauthorisation = false;
        this.username = this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.username ? this.router.getCurrentNavigation().extras.state.username : '';
    }
    UsernameComponent.prototype.ngOnInit = function () {
        //Called after the constructor, i
        //Add 'implements OnInit' to the class.
    };
    UsernameComponent.prototype.otp = function () {
        var _this = this;
        this.loading = true;
        this.baserestService.OTP(this.username).then(function (success) {
            _this.loading = false;
            _this.setuserData(success);
        }, function (error) {
            _this.error = true;
            _this.loading = false;
            _this.errormessage = error.error.text ? error.error.text : error.error.error;
        });
    };
    UsernameComponent.prototype.setuserData = function (success) {
        this.loading = false;
        if (success === true) {
            this.router.navigate(['otp'], { state: { username: this.username } });
            return;
        }
        if (success === false) {
            this.error = true;
            return;
        }
        else {
            this.noauthorisation = true;
            return;
        }
    };
    UsernameComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"] }
    ]; };
    UsernameComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'username-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./username.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/username/username.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers_base_rest_service__WEBPACK_IMPORTED_MODULE_3__["BaseRestService"]])
    ], UsernameComponent);
    return UsernameComponent;
}());



/***/ }),

/***/ "./src/components/warning/warning.ts":
/*!*******************************************!*\
  !*** ./src/components/warning/warning.ts ***!
  \*******************************************/
/*! exports provided: warningComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "warningComponent", function() { return warningComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _providers_services_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/services/services */ "./src/providers/services/services.ts");




var warningComponent = /** @class */ (function () {
    function warningComponent(activeModal, services) {
        this.activeModal = activeModal;
        this.services = services;
        this.confirmreplace = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    warningComponent.prototype.ngOnInit = function () {
    };
    warningComponent.prototype.cancel = function (e) {
        this.replace = false;
        this.confirmreplace.emit(this.replace);
        this.modalref = this.services.getmodalReference();
        this.modalref.close();
    };
    warningComponent.prototype.apply = function (e) {
        this.replace = true;
        this.confirmreplace.emit(this.replace);
        this.modalref = this.services.getmodalReference();
        this.modalref.close();
    };
    warningComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"] },
        { type: _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], warningComponent.prototype, "confirmreplace", void 0);
    warningComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'warning-viewer',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./warning.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/warning/warning.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"], _providers_services_services__WEBPACK_IMPORTED_MODULE_3__["Services"]])
    ], warningComponent);
    return warningComponent;
}());



/***/ }),

/***/ "./src/components/widgets/widgets.ts":
/*!*******************************************!*\
  !*** ./src/components/widgets/widgets.ts ***!
  \*******************************************/
/*! exports provided: Widgets */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Widgets", function() { return Widgets; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jqwidgets_scripts_jqwidgets_ts_angular_jqxcalendar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar */ "./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar.ts");



var Widgets = /** @class */ (function () {
    function Widgets() {
    }
    /*     ngOnInit(){
        this.date = new Date();
        console.log(this.messages);
        console.log(this.message); */
    Widgets.prototype.ngAfterViewInit = function () {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        var date1 = new Date();
        var date2 = new Date();
        var date3 = new Date();
        date1.setDate(5);
        date2.setDate(15);
        date3.setDate(16);
        // Add special dates by invoking the addSpecialDate method.
        this.widgetCalendar.addSpecialDate(date1, '', 'Appointment with Alborg Kommune');
        this.widgetCalendar.addSpecialDate(date2, '', 'Meeting with Diabeticc patient from Arhus');
        this.widgetCalendar.addSpecialDate(date3, '', 'Doctors Unit meeting');
    };
    Widgets.prototype.ngOnInit = function () {
        //  this.message = new Message("hi how are you ", "assets/icons/user.png", new Date);
        console.log(this.message);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('widgetCalendar', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", jqwidgets_scripts_jqwidgets_ts_angular_jqxcalendar__WEBPACK_IMPORTED_MODULE_2__["jqxCalendarComponent"])
    ], Widgets.prototype, "widgetCalendar", void 0);
    Widgets = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'widgets',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./widgets.html */ "./node_modules/raw-loader/dist/cjs.js!./src/components/widgets/widgets.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Widgets);
    return Widgets;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _package_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../package.json */ "./package.json");
var _package_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./../../package.json */ "./package.json", 1);


var environment = {
    production: false,
    baseURL: "https://qa-api.carex.dk/api/endpoints/api_services.php",
    loginservicesUrl: "https://qa-idp.carex.dk/endpoints/login_services.php",
    skabelonUrl: "https://qa-idp.carex.dk/html/skabelonapi.php",
    idpservicesUrl: "https://qa-idp.carex.dk/endpoints/idp_service.php",
    idpservicesUrl_v2: "https://qa-idp.carex.dk/v2/endpoints/idp_service.php",
    picturesUrl: "https//qa-admin.carex.dk/pictures/",
    saveUrl: "https://qa-admin.carex.dk/pictures/save-file.php",
    applikation_uuid: "48b00aae-5001-4b4f-9eea-b7bd61145686",
    context: "1c812baf-8189-4f32-ad50-658b29006f0d",
    version: _package_json__WEBPACK_IMPORTED_MODULE_1__["version"],
    envi: "qa"
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "./src/models/details.data.ts":
/*!************************************!*\
  !*** ./src/models/details.data.ts ***!
  \************************************/
/*! exports provided: DetailsData, Structure */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsData", function() { return DetailsData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Structure", function() { return Structure; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var DetailsData = /** @class */ (function () {
    function DetailsData(input) {
        this.bi_objekter = input;
    }
    return DetailsData;
}());

var Structure = /** @class */ (function () {
    function Structure(input) {
        var structure = input;
        return structure;
    }
    return Structure;
}());



/***/ }),

/***/ "./src/models/grid.data.ts":
/*!*********************************!*\
  !*** ./src/models/grid.data.ts ***!
  \*********************************/
/*! exports provided: GridData, Columns, RowsList, ObjectType, DataFields */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GridData", function() { return GridData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Columns", function() { return Columns; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RowsList", function() { return RowsList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ObjectType", function() { return ObjectType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataFields", function() { return DataFields; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var GridData = /** @class */ (function () {
    function GridData(griddata) {
        this.uuid = griddata.uuid;
        this.fra = griddata.fra;
        this.til = griddata.til;
        this.note = griddata.note;
        this.facetbeskrivelse = griddata.facetbeskrivelse;
        this.brugervendtnoegle = griddata.brugervendtnoegle;
        this.facetophavsret = griddata.facetophavsret;
        this.facetopbygning = griddata.facetopbygning;
        this.facetplan = griddata.facetplan;
        this.note = griddata.note;
        this.facetophavsret = griddata.facetophavsret;
        this.retskilde = griddata.retskilde;
        this.publiceret = griddata.publiceret;
        this.redaktoerer = griddata.create_timestamp;
        this.facetsupplement = griddata.facetsupplement;
        this.ansvarlig_uuid = griddata.ansvarlig_uuid;
        this.ejer_uuid = griddata.ejer_uuid;
        this.facettilhoerer_uuid = griddata.facettilhoerer_uuid;
        this.redaktoerer_uuid = griddata.redaktoerer_uuid;
        this.ejer = griddata.ejer;
    }
    return GridData;
}());

var Columns = /** @class */ (function () {
    function Columns(input) {
        var ColumnsList = input;
        return ColumnsList;
    }
    return Columns;
}());

var RowsList = /** @class */ (function () {
    function RowsList(input) {
        var RowsList = input;
        return RowsList;
    }
    return RowsList;
}());

var ObjectType = /** @class */ (function () {
    function ObjectType(hovedobjekt) {
        var ObjektType = hovedobjekt;
        return ObjektType;
    }
    return ObjectType;
}());

var DataFields = /** @class */ (function () {
    function DataFields(input) {
        var DataFieldsList = input;
        return DataFieldsList;
    }
    return DataFields;
}());



/***/ }),

/***/ "./src/models/localization/localization.ts":
/*!*************************************************!*\
  !*** ./src/models/localization/localization.ts ***!
  \*************************************************/
/*! exports provided: Localization */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Localization", function() { return Localization; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var Localization = /** @class */ (function () {
    function Localization() {
        this.localizationobj = {};
        this.localizationobj.pagergotopagestring = "Gå til:";
        this.localizationobj.pagershowrowsstring = "Antal pr. side:";
        this.localizationobj.pagerrangestring = " af ";
        this.localizationobj.pagerfirstbuttonstring = "første";
        this.localizationobj.pagerlastbuttonstring = "sidste";
        this.localizationobj.pagernextbuttonstring = "næste";
        this.localizationobj.pagerpreviousbuttonstring = "forrige";
        this.localizationobj.sortascendingstring = "Sorter stigende";
        this.localizationobj.sortdescendingstring = "Sorter faldende";
        this.localizationobj.sortremovestring = "Fjern sortering";
        this.localizationobj.filterchoosestring = "Alle";
        this.localizationobj.loadtext = "Indlæser..";
        this.localizationobj.filterselectallstring = "Alle";
        this.localizationobj.emptydatastring = "ingen oplysninger fundet";
        this.localizationobj.groupsheaderstring = "Træk en kolonneoverskrift hertil for at gruppere";
        this.localizationobj.filterselectstring = "Filtrer";
        this.localizationobj.filterstring = "Filtrer";
        this.localizationobj.todaystring = "<b>I dag<b>";
        this.localizationobj.clearstring = "<b>Ryd</b>";
        this.localizationobj.filterstringcomparisonoperators = ['contains', 'does not contain'];
        this.localizationobj.filternumericcomparisonoperators = ['less than', 'greater than'];
        this.localizationobj.filterdatecomparisonoperators = ['less than', 'greater than'];
        this.localizationobj.filterbooleancomparisonoperators = ['equal', 'not equal'];
        this.localizationobj.filterstringcomparisonoperators = ['tom', 'ikke tom', 'indeholder', 'indeholder (match tilfælde)', 'indeholder ikke ', ' indeholder ikke (match tilfælde) ', ' starter med ', ' starter med(match tilfælde)', 'slutter med ', ' slutter med (match tilfælde) ', ' lige ', ' lige (match tilfælde) ', ' null ', ' ikke null'];
        this.localizationobj.filternumericcomparisonoperators = ['lige', 'ikke lige', 'mindre end', 'mindre end eller lige', 'større end', 'større end eller lige', 'null', 'ikke null'];
        this.localizationobj.filterdatecomparisonoperators = ['lige', 'ikke lige', 'mindre end', 'mindre end eller lige', 'større end', 'større end eller lige', 'null', 'ikke null'];
        this.localizationobj.filterbooleancomparisonoperators = ['lige', 'ikke lige'];
        this.localizationobj.days = {
            names: ["Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag"],
            namesAbbr: ["Søn", "Man", "Tir", "Ons", "Tors", "Fre", "Lør"],
            namesShort: ["Sø", "Ma", "Ti", "On", "To", "Fr", "Lø"]
        };
        this.localizationobj.months = {
            names: ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December", ""],
            namesAbbr: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""]
        };
        this.localizationobj.browseButton = "gennemse";
        this.localizationobj.uploadButton = "Upload";
        this.localizationobj.cancelButton = "Afbestille";
        this.localizationobj.uploadFileTooltip = "Upload";
        this.localizationobj.cancelFileTooltip = "Afbestille";
        this.localizationobj.calendars = {
            standard: {
                "/": "-",
                firstDay: 1,
                days: {
                    names: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"],
                    namesAbbr: ["sø", "ma", "ti", "on", "to", "fr", "lø"],
                    namesShort: ["sø", "ma", "ti", "on", "to", "fr", "lø"]
                },
                months: {
                    names: ["januar", "februar", "marts", "april", "maj", "juni", "juli", "august", "september", "oktober", "november", "december", ""],
                    namesAbbr: ["jan", "feb", "mar", "apr", "maj", "jun", "jul", "aug", "sep", "okt", "nov", "dec", ""]
                },
                AM: null,
                PM: null,
                patterns: {
                    d: "dd-MM-yyyy",
                    D: "d. MMMM yyyy",
                    t: "HH:mm",
                    T: "HH:mm:ss",
                    f: "d. MMMM yyyy HH:mm",
                    F: "d. MMMM yyyy HH:mm:ss",
                    M: "d. MMMM",
                    Y: "MMMM yyyy"
                }
            }
        };
    }
    return Localization;
}());



/***/ }),

/***/ "./src/models/message.ts":
/*!*******************************!*\
  !*** ./src/models/message.ts ***!
  \*******************************/
/*! exports provided: Message */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Message", function() { return Message; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var Message = /** @class */ (function () {
    //constructor(content: string, avatar: string, timestamp?: Date){
    function Message(input) {
        this.content = input.content;
        this.timestamp = input.timestamp;
        this.avatar = input.avatar;
    }
    return Message;
}());



/***/ }),

/***/ "./src/models/messages.ts":
/*!********************************!*\
  !*** ./src/models/messages.ts ***!
  \********************************/
/*! exports provided: Messages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Messages", function() { return Messages; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var Messages = /** @class */ (function () {
    //constructor(content: string, avatar: string, timestamp?: Date){
    function Messages(content) {
        this.content = content;
    }
    return Messages;
}());



/***/ }),

/***/ "./src/models/user.model.ts":
/*!**********************************!*\
  !*** ./src/models/user.model.ts ***!
  \**********************************/
/*! exports provided: UserData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserData", function() { return UserData; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var UserData = /** @class */ (function () {
    function UserData(input) {
        this.uuid = input.id;
        this.emailid = input.email;
        this.gyldighed = input.gyldighed;
        this.username = input.username;
        this.brugernavn = input.brugernavn;
    }
    return UserData;
}());



/***/ }),

/***/ "./src/providers/auth.rest.service.ts":
/*!********************************************!*\
  !*** ./src/providers/auth.rest.service.ts ***!
  \********************************************/
/*! exports provided: AuthRestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRestService", function() { return AuthRestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/user.model */ "./src/models/user.model.ts");




var AuthRestService = /** @class */ (function () {
    function AuthRestService() {
        this.userrelations = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.menulist = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.userToken = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.userobj = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
    }
    AuthRestService.prototype.setUserData = function (data) {
        this.userdata = new _models_user_model__WEBPACK_IMPORTED_MODULE_3__["UserData"](data.result[0]);
    };
    AuthRestService.prototype.setuserrelations = function (userrelations) {
        this.userrelations.next(userrelations);
    };
    AuthRestService.prototype.getClient = function () {
        return this.client;
    };
    AuthRestService.prototype.setClient = function (client) {
        this.client = client;
    };
    AuthRestService.prototype.setuserToken = function (value) {
        this.userToken.next(value);
    };
    AuthRestService.prototype.setuserobj = function (value) {
        this.userobj.next(value);
    };
    AuthRestService.prototype.setmenulist = function (value) {
        this.menulist.next(value);
    };
    AuthRestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AuthRestService);
    return AuthRestService;
}());



/***/ }),

/***/ "./src/providers/base.rest.service.ts":
/*!********************************************!*\
  !*** ./src/providers/base.rest.service.ts ***!
  \********************************************/
/*! exports provided: BaseRestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseRestService", function() { return BaseRestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/user.model */ "./src/models/user.model.ts");
/* harmony import */ var _models_details_data__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/details.data */ "./src/models/details.data.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _auth_rest_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./auth.rest.service */ "./src/providers/auth.rest.service.ts");










var BaseRestService = /** @class */ (function () {
    // URL to web api
    function BaseRestService(http, auth, authService) {
        var _this = this;
        this.http = http;
        this.auth = auth;
        this.authService = authService;
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
            'Accept': 'application/json',
            "Access-Control-Allow-Headers": "X-Requested-With"
        });
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
        this.authService.userobj.subscribe(function (userobj) {
            if (userobj && userobj.id) {
                _this.userData = new _models_user_model__WEBPACK_IMPORTED_MODULE_6__["UserData"](userobj);
            }
        });
        this.baseUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].baseURL;
        this.idpservicesUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].idpservicesUrl_v2;
        this.auth.userToken.subscribe(function (userToken) {
            _this.userToken = userToken;
        });
    }
    BaseRestService.prototype.getEnvironment = function () {
        return this.baseUrl;
    };
    BaseRestService.prototype.getAllMenuList = function (uid) {
        this.formdata = new FormData();
        var userid = uid ? uid : this.userData.uuid;
        this.formdata.append('action', 'list_menuitems');
        this.formdata.append('bruger_uuid', userid);
        this.formdata.append('applikation_uuid', _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].applikation_uuid);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getGriddata = function (uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', uuid);
        this.formdata.append('pageSize', 10000);
        this.formdata.append('page', 1);
        this.formdata.append('context', _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].context);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getDetailsdata = function (uuid, listId) {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_objekt_detaljer');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('objekt_uuid', uuid);
        this.formdata.append('skema_uuid', listId);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getcreateDetailsdata = function (listId) {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_objekt_detaljer');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', listId);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.downloadcsvFile = function (listId, userToken, requestedfiletype) {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_eksporter_objektdata');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', listId);
        this.formdata.append('requestedfiletype', requestedfiletype);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.uploadcsvFile = function (file, filename, listId) {
        var _this = this;
        this.formdata = new FormData();
        var o = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"](function (obs) { return _this.subscriber = obs; });
        this.formdata.append('action', 'CALL_importer_objektdata');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', listId);
        this.formdata.append('fil', file);
        this.formdata.append('filnavn', filename);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.cancelRequest = function () {
        this.subscriber.error();
    };
    BaseRestService.prototype.updateDetailsdata = function (detailsuuid, listedefinition_uuid, update_timestamp, updateddata, allfiles) {
        this.formdata = new FormData();
        var objectuuid = listedefinition_uuid ? listedefinition_uuid : '';
        this.formdata.append('action', 'CALL_update_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('objekt_uuid', objectuuid);
        this.formdata.append('update_objekt', updateddata);
        this.formdata.append('update_timestamp', update_timestamp);
        if (allfiles) {
            for (var i = 0; i < allfiles.length; i++) {
                var name_1 = allfiles[i].name;
                var value = allfiles[i].value;
                this.formdata.append(name_1, value);
            }
            this;
        }
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    };
    BaseRestService.prototype.createDetailsdata = function (detailsuuid, update_timestamp, updateddata, allfiles) {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_update_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('update_objekt', updateddata);
        this.formdata.append('update_timestamp', update_timestamp);
        if (allfiles) {
            for (var i = 0; i < allfiles.length; i++) {
                var name_2 = allfiles[i].name;
                var value = allfiles[i].value;
                this.formdata.append(name_2, value);
            }
            this;
        }
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    };
    BaseRestService.prototype.getbrandConfig = function (brand_uuid, auth) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getBrand');
        this.formdata.append('version', '1.2.92');
        this.formdata.append('userToken', auth);
        this.formdata.append('itsystem_uuid', _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].applikation_uuid);
        this.formdata.append('organisation_uuid', _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].applikation_uuid);
        this.formdata.append('brand_uuid', brand_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.downloadFileDocument = function (detailsuuid, listedefinition_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_get_file');
        this.formdata.append('userToken', this.userData.uuid);
        this.formdata.append('dokument_uuid', listedefinition_uuid);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    };
    BaseRestService.prototype.exportFileDocument = function (detailsuuid, filenavn, path, auth) {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_get_exportfile');
        this.formdata.append('userToken', auth);
        this.formdata.append('filnavn', filenavn);
        this.formdata.append('filpath', path);
        this.formdata.append('itsystem_uuid', _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].applikation_uuid);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    };
    BaseRestService.prototype.deleteDetailsdata = function (detailsuuid, listedefinition_uuid, update_timestamp) {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_delete_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('objekt_uuid', listedefinition_uuid);
        this.formdata.append('update_timestamp', update_timestamp);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    };
    BaseRestService.prototype.getDokumentList = function () {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokument_list');
        this.formdata.append('dokument_type', 'brand');
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    };
    BaseRestService.prototype.getBrands = function (dokument_uuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokumentfil_read');
        this.formdata.append('objekt_uuid', dokument_uuid);
        this.formdata.append('userToken', authid);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    };
    BaseRestService.prototype.savesConfiguration = function (dokument_uuid, body, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokumentfil_ajour');
        this.formdata.append('objekt_uuid', dokument_uuid);
        this.formdata.append('userToken', authid);
        this.formdata.append('json', body);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    };
    BaseRestService.prototype.setupdatedDetailsdata = function (data) {
        this.structure = new _models_details_data__WEBPACK_IMPORTED_MODULE_7__["Structure"](data.result.struktur);
        this.detailsdata = new _models_details_data__WEBPACK_IMPORTED_MODULE_7__["DetailsData"](data.result.bi_objekter);
    };
    BaseRestService.prototype.SetDetailsData = function (data) {
        this.detailsdata = new _models_details_data__WEBPACK_IMPORTED_MODULE_7__["DetailsData"](data.result);
    };
    BaseRestService.prototype.getuseranalytics = function (itsystem_uuid, fromDate, toDate) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getuseranalytics');
        this.formdata.append('applikation_uuid', JSON.stringify(itsystem_uuid));
        this.formdata.append('fromDate', fromDate);
        this.formdata.append('toDate', toDate);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.translateImport = function (file, filename, objekttype, organisation) {
        this.formdata = new FormData();
        this.formdata.append('action', 'importTranslations');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('organisation', organisation);
        this.formdata.append('objekttype', objekttype);
        this.formdata.append('fil', file);
        this.formdata.append('filnavn', filename);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.translateExport = function (objekttype, organisation) {
        this.formdata = new FormData();
        this.formdata.append('action', 'exportTranslations');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('organisation', organisation);
        this.formdata.append('objekttype', objekttype);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getskabelonlist = function (organisationsid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getskabelon');
        this.formdata.append('organisationsid', organisationsid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('bruger_uuid', this.userData.uuid);
        this.formdata.append('applikation_uuid', _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].applikation_uuid);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getskemaslist = function (organisationsid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'skema_list');
        this.formdata.append('skema_tilhoerer_uuid', organisationsid);
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getskemacontent = function (skema_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'skema_read');
        this.formdata.append('skema_uuid', skema_uuid);
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.saveskemaoncontent = function (skema_json) {
        this.formdata = new FormData();
        this.formdata.append('action', 'skema_ajour');
        this.formdata.append('skema_json', skema_json);
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.deleteSkema = function (skema_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'skema_delete');
        this.formdata.append('skema_uuid', skema_uuid);
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getskabeloncontent = function (skabelonid, lang) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getskabeloncontent');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skabelonid', skabelonid);
        this.formdata.append('sprog', lang);
        return this.http.post(this.idpservicesUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.saveskabeloncontent = function (skabelonid, title, teaser, beskrivelse, ikon, flettefelter, slctlang, body) {
        this.formdata = new FormData();
        this.formdata.append('action', 'saveskabeloncontent');
        this.formdata.append('skabelonid', skabelonid);
        this.formdata.append('titel', title);
        this.formdata.append('teaser', teaser);
        this.formdata.append('beskrivelse', beskrivelse);
        this.formdata.append('ikon', ikon);
        this.formdata.append('sprog', slctlang);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('flettefelter', flettefelter);
        this.formdata.append('tekst', body);
        return this.http.post(this.idpservicesUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getmenufilters = function (objekt_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokument_list');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('dokument_type', 'filtergui');
        this.formdata.append('indhold', objekt_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.readfilterdata = function (dokument_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokumentfil_read');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('objekt_uuid', dokument_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.savefilterdata = function (filterRequest) {
        this.formdata = new FormData();
        this.formdata.append('action', 'filterAjour');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('filterRequest', filterRequest);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.getsearch = function (skema_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getObjektfilter');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', skema_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.getEditors = function () {
        this.formdata = new FormData();
        this.formdata.append('action', 'getRedaktoer');
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.notificationBatch = function (action, selectedUuids, selecteddate) {
        this.formdata = new FormData();
        this.formdata.append('action', 'notificationBatch');
        this.formdata.append('batchOperation', action);
        this.formdata.append('selectedUuids', selectedUuids);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('dateTime', selecteddate);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.notificationsDispatch = function (action) {
        this.formdata = new FormData();
        this.formdata.append('action', action);
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.uploadFiles = function (path, files, overwrite) {
        this.formdata = new FormData();
        for (var i in files) {
            this.formdata.append('files[]', files[i]);
        }
        this.formdata.append('action', 'uploadFileAppcontent');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('path', path);
        this.formdata.append('overwrite', overwrite);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.login = function (username, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'login');
        this.formdata.append('id', username);
        this.formdata.append('password', password);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.OTP = function (username) {
        this.formdata = new FormData();
        this.formdata.append('action', 'OTP');
        this.formdata.append('username', username);
        this.formdata.append('applikation_uuid', _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].applikation_uuid);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.verifyOTP = function (username, otp) {
        this.formdata = new FormData();
        this.formdata.append('action', 'verifyOTP');
        this.formdata.append('username', username);
        this.formdata.append('otp', otp);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.searchgridData = function (skema_uuid, action, formdata) {
        this.formdata = new FormData();
        this.formdata.append('action', action);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', skema_uuid);
        if ((Object.keys(formdata)).length > 0) {
            this.formdata.append('filter_json', JSON.stringify(formdata));
        }
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.searchgridDatausinggemtFilter = function (filter_uuid, uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', uuid);
        this.formdata.append('filter_uuid', filter_uuid);
        this.formdata.append('pageSize', 10000);
        this.formdata.append('page', 1);
        this.formdata.append('context', _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].context);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.getFilterData = function (filter_uuid, uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', uuid);
        this.formdata.append('filter_uuid', filter_uuid);
        return this.http.post(this.baseUrl, this.formdata).map(function (res) { return res; }).toPromise();
    };
    BaseRestService.prototype.changePassword = function (id, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'changePassword');
        this.formdata.append('id', id);
        this.formdata.append('password', password);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.prototype.readsearchData = function (skema_uuid, filter_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('skema_uuid', skema_uuid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('filter_uuid', filter_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    };
    BaseRestService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _auth_rest_service__WEBPACK_IMPORTED_MODULE_9__["AuthRestService"] },
        { type: _auth_rest_service__WEBPACK_IMPORTED_MODULE_9__["AuthRestService"] }
    ]; };
    BaseRestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _auth_rest_service__WEBPACK_IMPORTED_MODULE_9__["AuthRestService"], _auth_rest_service__WEBPACK_IMPORTED_MODULE_9__["AuthRestService"]])
    ], BaseRestService);
    return BaseRestService;
}());



/***/ }),

/***/ "./src/providers/services/dataflow.ts":
/*!********************************************!*\
  !*** ./src/providers/services/dataflow.ts ***!
  \********************************************/
/*! exports provided: DialogflowService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogflowService", function() { return DialogflowService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _models_message__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../models/message */ "./src/models/message.ts");






//future component Chat-Bot
var DialogflowService = /** @class */ (function () {
    function DialogflowService(http) {
        this.http = http;
        this.baseURL = "https://api.dialogflow.com/v1/query?v=20170712";
        this.token = 'ef33f3994bdb463dadc7641ca5f07e63';
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: this.headers });
    }
    DialogflowService.prototype.sendChatMessage = function (chatquestion) {
        var _this = this;
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({
            "Authorization": "Bearer ef33f3994bdb463dadc7641ca5f07e63",
            "Content-Type": "application/json",
            "sessionId": "1d0c2c63-6872-4084-848c-0dfde1c9b087",
            "lang": "en"
        });
        var settings = {
            "lang": "en",
            "query": chatquestion,
            "sessionId": "1d0c2c63-6872-4084-848c-0dfde1c9b087",
            "timezone": "Europe/Paris",
            "headers": {
                "Authorization": "Bearer ef33f3994bdb463dadc7641ca5f07e63",
                "Content-Type": "application/json"
            }
        };
        this.formdata = new FormData();
        this.headers.append("query", chatquestion);
        this.formdata.append('action', 'CALL_list_objekttyper');
        return this.http.post(this.baseURL, this.headers, settings).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) { return _this.chatReply(data); }, function () { return console.log('error'); }));
    };
    DialogflowService.prototype.chatReply = function (message) {
        console.log(message);
        this.message = new _models_message__WEBPACK_IMPORTED_MODULE_5__["Message"](message);
    };
    DialogflowService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    DialogflowService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], DialogflowService);
    return DialogflowService;
}());



/***/ }),

/***/ "./src/providers/services/router.service.ts":
/*!**************************************************!*\
  !*** ./src/providers/services/router.service.ts ***!
  \**************************************************/
/*! exports provided: RouterExtService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouterExtService", function() { return RouterExtService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



/** A router wrapper, adding extra functions. */
var RouterExtService = /** @class */ (function () {
    function RouterExtService(router) {
        var _this = this;
        this.router = router;
        this.previousUrl = undefined;
        this.currentUrl = undefined;
        this.currentUrl = this.router.url;
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                _this.previousUrl = _this.currentUrl;
                _this.currentUrl = event.url;
            }
            ;
        });
    }
    RouterExtService.prototype.getPreviousUrl = function () {
        return this.previousUrl;
    };
    RouterExtService.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    RouterExtService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], RouterExtService);
    return RouterExtService;
}());



/***/ }),

/***/ "./src/providers/services/services.ts":
/*!********************************************!*\
  !*** ./src/providers/services/services.ts ***!
  \********************************************/
/*! exports provided: Services */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Services", function() { return Services; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var Services = /** @class */ (function () {
    function Services() {
        this.navigationElements = [];
        this.slctmenuitem = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.isLoading = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.newallfilterList = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.newactionsList = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.filtersdata = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
    }
    Services.prototype.setlistId = function (uuid) {
        this.listId = uuid;
    };
    Services.prototype.getlistId = function () {
        return this.listId;
    };
    Services.prototype.setClickedRowuuiid = function (rowid) {
        this.rowId = rowid;
    };
    Services.prototype.getClickedRowuuiid = function () {
        return this.rowId;
    };
    Services.prototype.setselectedMenu = function (selectedMenu) {
        return this.selectedMenu = selectedMenu;
    };
    Services.prototype.getselectedMenu = function () {
        return this.selectedMenu;
    };
    Services.prototype.setdisplayName = function (displayName) {
        return this.displayName = displayName;
    };
    Services.prototype.getdisplayName = function () {
        return this.displayName;
    };
    Services.prototype.setheading = function (heading) {
        return this.heading = heading;
    };
    Services.prototype.getheading = function () {
        return this.heading;
    };
    Services.prototype.setslctmenuitem = function (value) {
        this.slctmenuitem.next(value);
    };
    Services.prototype.addNavigation = function (item) {
        this.navigationElements.push(item);
    };
    Services.prototype.removeNavigation = function () {
        this.navigationElements.pop();
    };
    Services.prototype.resettNavigation = function (item) {
        this.navigationElements = item;
    };
    Services.prototype.getNavigation = function () {
        return this.navigationElements;
    };
    Services.prototype.clearNavigation = function () {
        this.navigationElements.length = 0;
    };
    Services.prototype.sethasSubmenu = function (boolean) {
        this.hasSubmenu = boolean;
    };
    Services.prototype.gethasSubmenu = function () {
        return this.hasSubmenu;
    };
    Services.prototype.setContext = function (id) {
        this.context = id;
    };
    Services.prototype.getContext = function () {
        return this.context;
    };
    Services.prototype.getgridState = function () {
        return this.gridstate;
    };
    Services.prototype.setgridState = function (gridstate) {
        this.gridstate = gridstate;
    };
    Services.prototype.setselectedmenuTitle = function (title) {
        this.slctmenuTitle = title;
    };
    Services.prototype.getselectedmenuTitle = function () {
        return this.slctmenuTitle;
    };
    Services.prototype.setisLoading = function (value) {
        this.isLoading.next(value);
    };
    Services.prototype.setnewactionsList = function (value) {
        this.newactionsList.next(value);
    };
    Services.prototype.setnewallfilterList = function (value) {
        this.newallfilterList.next(value);
    };
    Services.prototype.setfiltersData = function (value) {
        this.filtersdata.next(value);
    };
    Services.prototype.setmodalReference = function (modal) {
        this.modal = modal;
    };
    Services.prototype.getmodalReference = function () {
        return this.modal;
    };
    Services = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Services);
    return Services;
}());



/***/ }),

/***/ "./src/providers/storageservice/storageservice.ts":
/*!********************************************************!*\
  !*** ./src/providers/storageservice/storageservice.ts ***!
  \********************************************************/
/*! exports provided: StorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageService", function() { return StorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var StorageService = /** @class */ (function () {
    function StorageService() {
    }
    StorageService.prototype.set = function (name, value) {
        try {
            localStorage.setItem(name, JSON.stringify(value));
        }
        catch (e) {
            console.error('Error saving to localStorage', e);
        }
    };
    StorageService.prototype.get = function (name) {
        return Promise.resolve().then(function () {
            return JSON.parse(localStorage.getItem(name));
        });
    };
    StorageService.prototype.clear = function () {
        try {
            localStorage.clear();
        }
        catch (e) {
            console.error('Error getting data from localStorage', e);
            return null;
        }
    };
    StorageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], StorageService);
    return StorageService;
}());



/***/ }),

/***/ "./src/providers/utils/helperService.ts":
/*!**********************************************!*\
  !*** ./src/providers/utils/helperService.ts ***!
  \**********************************************/
/*! exports provided: HelperSerice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelperSerice", function() { return HelperSerice; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);



var HelperSerice = /** @class */ (function () {
    function HelperSerice() {
    }
    HelperSerice.prototype.formatDate = function (datetoformat) {
        moment__WEBPACK_IMPORTED_MODULE_2__["locale"]('da');
        var startat, starthour, day, dd, mm, yyyy, hh, mi;
        if (datetoformat) {
            startat = moment__WEBPACK_IMPORTED_MODULE_2__(datetoformat).format('lll');
            starthour = moment__WEBPACK_IMPORTED_MODULE_2__(datetoformat).format('LT');
            day = moment__WEBPACK_IMPORTED_MODULE_2__(datetoformat).format('L');
        }
        else {
            startat = moment__WEBPACK_IMPORTED_MODULE_2__().format('lll');
            starthour = moment__WEBPACK_IMPORTED_MODULE_2__().format('LT');
            day = moment__WEBPACK_IMPORTED_MODULE_2__().format('L');
        }
        yyyy = day.split('.')[2];
        mm = day.split('.')[1];
        dd = day.split('.')[0];
        hh = starthour.split(':')[0];
        mi = starthour.split(':')[1];
        return new Date(yyyy, mm - 1, dd, hh, mi);
    };
    HelperSerice.prototype.analticsformatDate = function (datetoformat) {
        moment__WEBPACK_IMPORTED_MODULE_2__["locale"]('da');
        var startat, starthour, day, dd, mm, yyyy, hh, mi;
        if (datetoformat) {
            startat = moment__WEBPACK_IMPORTED_MODULE_2__(datetoformat).format('lll');
            starthour = moment__WEBPACK_IMPORTED_MODULE_2__(datetoformat).format('LT');
            day = moment__WEBPACK_IMPORTED_MODULE_2__(datetoformat).format('L');
        }
        else {
            startat = moment__WEBPACK_IMPORTED_MODULE_2__().format('lll');
            starthour = moment__WEBPACK_IMPORTED_MODULE_2__().format('LT');
            day = moment__WEBPACK_IMPORTED_MODULE_2__().format('L');
        }
        yyyy = day.split('.')[2];
        mm = day.split('.')[1];
        dd = day.split('.')[0];
        hh = starthour.split(':')[0];
        mi = starthour.split(':')[1];
        var startdate = moment__WEBPACK_IMPORTED_MODULE_2__(new Date(yyyy, mm - 1, dd, 0, 0)).subtract(1, 'months').format('yyyy-MM-DD HH:mm');
        return [new Date(startdate), new Date(yyyy, mm - 1, dd, 0, 0)];
    };
    HelperSerice.prototype.sortList = function (userrelations) {
        var sorted = userrelations.sort(function (a, b) {
            if (a.org && b.org) {
                var nameA = a.org.toLowerCase(), nameB = b.org.toLowerCase();
                if (nameA < nameB) { //sort string ascending
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                else {
                    return 0;
                }
            }
        });
        return sorted;
    };
    HelperSerice.prototype.sortmenuList = function (userrelations) {
        var sorted = userrelations.sort(function (a, b) {
            if (a.placering && b.placering) {
                var nameA = +a.placering, nameB = +b.placering;
                if (nameA < nameB) //sort string ascending
                    return -1;
                if (nameA > nameB)
                    return 1;
                return 0;
            }
        });
        return sorted;
    };
    HelperSerice = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HelperSerice);
    return HelperSerice;
}());



/***/ }),

/***/ "./src/providers/validators/password-validator.ts":
/*!********************************************************!*\
  !*** ./src/providers/validators/password-validator.ts ***!
  \********************************************************/
/*! exports provided: PasswordValidation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordValidation", function() { return PasswordValidation; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var PasswordValidation = /** @class */ (function () {
    function PasswordValidation() {
    }
    PasswordValidation.MatchPassword = function (AC) {
        var password = AC.get('password').value; // to get value in input tag
        var confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
        var hasNumber = new RegExp(/\d/);
        var hasUpper = new RegExp(/[A-Z]/);
        var hasLower = new RegExp(/[a-z]/);
        // console.log('Num, Upp, Low', hasNumber, hasUpper, hasLower);
        var valid = hasNumber && hasUpper && hasLower;
        if ((password != confirmPassword) === true) {
            AC.get('confirmPassword').setErrors({ MatchPassword: true });
        }
        if ((password === confirmPassword) === true) {
            AC.get('confirmPassword').setErrors({ MatchPassword: false });
        }
        if (hasNumber.test(password) === false) {
            AC.get('password').setErrors({ Numbers: true });
        }
        if (hasLower.test(password) === false) {
            AC.get('password').setErrors({ Lower: true });
        }
        if (hasUpper.test(password) === false) {
            AC.get('password').setErrors({ Upper: true });
        }
        if (password && password.length < 8) {
            AC.get('password').setErrors({ long: true });
        }
        if (hasNumber.test(password) && hasUpper.test(password) && hasLower.test(password) && (password.length >= 8) && (password == confirmPassword)) {
            return false;
        }
    };
    PasswordValidation.cprValidator = function (AC) {
        var cprnummner = AC.get('cprnummner').value; // to get value in input tag
        //  let validformat = '/[0-9]{6}\-[0-9]{4}$/';
        var cprformat = new RegExp(/^[0-9]{10}$/); //new RegExp("/[0-9]{6}\-[0-9]{4}$/");
        if (cprformat.test(cprnummner) == false) {
            // if (cprnummner==''||cprnummner==null||cprnummner==undefined) {
            AC.get('cprnummner').setErrors({ cprValidator: true });
        }
        else {
            return false;
        }
    };
    PasswordValidation.validateEmail = function (email) {
        var emailvalidRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailvalidRegx.test(String(email).toLowerCase());
    };
    return PasswordValidation;
}());



/***/ }),

/***/ "./src/providers/window.ref.ts":
/*!*************************************!*\
  !*** ./src/providers/window.ref.ts ***!
  \*************************************/
/*! exports provided: WindowRefService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WindowRefService", function() { return WindowRefService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WindowRefService = /** @class */ (function () {
    function WindowRefService() {
    }
    WindowRefService.prototype.nativeWindow = function () {
        return window;
    };
    WindowRefService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], WindowRefService);
    return WindowRefService;
}());



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/kiatec/Documents/Kiatec/carex_admin/carex_admin/src/main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** stream (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map