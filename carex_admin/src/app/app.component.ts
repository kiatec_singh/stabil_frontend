import { Component, HostListener, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { AuthRestService } from '../providers/auth.rest.service';
import { StorageService } from '../providers/storageservice/storageservice';
import { BaseRestService } from '../providers/base.rest.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Carex';
  private hostname;
  private userinfo: any = null;
  private userToken = null;
  constructor(private location: Location, private router: Router, private storageService: StorageService, private baserestService: BaseRestService, private authService: AuthRestService) {

  }

  ngOnInit(): void {
    let loc: any = this.location
    this.hostname = loc._platformStrategy._platformLocation.location.hostname;
  }
  @HostListener('window:unload', ['$event'])
  unloadHandler(event) {
    let loc: any = this.location
    if(loc._platformLocation.hostname!='localhost'){
          this.storageService.clear();
    }
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    let loc: any = this.location
    if(loc._platformLocation.hostname!='localhost'){
          this.storageService.clear();
    }
  }
  validateAuthorisation() {
    this.baserestService.getAllMenuList(this.userinfo.id).then(
      (menulist) => {
        this.userToken = menulist.userToken;
        this.authService.setuserToken(menulist.userToken);
        this.authService.setmenulist(menulist);
        if (menulist.relationer) {
          this.authService.setuserrelations(menulist.relationer);
        }
        this.authService.setuserobj(this.userinfo);
        this.router.navigateByUrl('');
        //this.loading=false;
      },
      (error) => {
        // this.loading=false;
        // this.errormessage= error.error.besked;    
      });
  }
}