//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgDatepickerModule } from 'ng2-datepicker';
import { NgbModule,NgbAccordion } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxRangeSelectorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxrangeselector';
import { jqxTabsComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxCalendarComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar';
import {  jqxTimePickerComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtimepicker';
import {  jqxPanelComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxpanel';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationStrategy, HashLocationStrategy, APP_BASE_HREF, DatePipe } from '@angular/common';
import { DawaAutocompleteModule } from 'ngx-dawa-autocomplete';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { GoogleChartsModule } from 'angular-google-charts';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { ClipboardModule } from 'ngx-clipboard';
import { MomentDateTimeAdapter, OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS } from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';
import {MatDialogModule} from '@angular/material/dialog';

//GRID
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import {CalendarModule} from 'primeng/calendar';
import {SliderModule} from 'primeng/slider';
import {MultiSelectModule} from 'primeng/multiselect';
import {ContextMenuModule} from 'primeng/contextmenu';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import {ProgressBarModule} from 'primeng/progressbar';
import {InputTextModule} from 'primeng/inputtext';

import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE,OwlDateTimeIntl } from 'ng-pick-datetime';
import { CKEditorModule } from 'ckeditor4-angular';
// here is the default text string - just adjust the strings to reflect your preferred language
export class DefaultIntl extends OwlDateTimeIntl {
  /** A label for the cancel button */
  cancelBtnLabel= 'Annuller';
  /** A label for the set button */
  setBtnLabel= 'Sæt';

}
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm',//'YYYY-MM-DD HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm',//'YYYY-MM-DD HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY HH:mm',//'YYYY-MM-DD HH:mm:ss',
  timePickerInput: 'DD-MM-YYYY',
  monthYearLabel: 'DD-MM-YYYY',
  dateA11yLabel: 'DD-MM-YYYY',
  monthYearA11yLabel: 'DD-MM-YYYY',
}



//pages
import { HeaderPage } from '../components/header/header-page';
import { FooterPage } from '../components/footer/footer-page';
import { MenuRight } from '../components/menu-right/menu-right';
import { MenuLeft } from '../components/menu-left/menu-left';
import { Itsystems } from '../components/itsystems/itsystems';
import { DetailsPage } from '../components/detailspage/detailspage';
import { CreatePage } from '../components/createpage/createpage';
import { Login } from '../components/login/login';
import { Widgets } from '../components/widgets/widgets';
import { MessageList } from '../components/message-list/message-list';
import { MessageItem } from '../components/message-item/message-item';
import { MessageForm } from '../components/message-form/message-form';
import { ChatContainer } from '../components/chat-container/chat-container';
import { Dashboard } from '../components/dashboard/dashboard';
import { Notifications } from '../components/notifications/notifications';
import { modalComponent } from '../components/modal/modal';
import { GanttChart } from '../components/ganttchart/ganttchart';
import { AppRoutingModule } from './app.routing.module';
import { BreadcrumComponent } from '../components/breadcrum/breadcrum';
import { Translation } from '../components/translation/translation'
import { Templates } from '../components/templates/templates';
import { Configuration } from '../components/configuration/configuration';
import { analyticsComponent } from '../components/analytics/analytics';
import { uploadsComponent } from '../components/uploads/uploads';
import { customForm } from '../components/customform/customform';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { filterComponent } from '../components/filter/filter';
import { jsonviewerComponent } from '../components/jsonviewer/jsonviewer';
import { notifyviewerComponent } from '../components/notifyviewer/notifyviewer';
import{OperationsComponent} from '../components/operations/operations';
import{sendemailComponent} from '../components/sendemail/sendemail';
import{sendmessagesComponent} from '../components/sendmessages/sendmessages';
import{fletfiltersComponent} from '../components/fletfilters/fletfilters';
import{newGridComponent} from '../components/newgrid/newgrid';
import { warningComponent } from '../components/warning/warning';
import { UsernameComponent } from '../components/username/username';
import { OtpComponent } from '../components/otp/otp';
import { changepasswordComponent } from '../components/changepassword/changepassword';
import { Skema } from '../components/skema/skema';
import { skemadeleteComponent } from '../components/skemadelete/skemadelete';
import { skemacreateComponent } from '../components/skemacreate/skemacreate';
import { searchComponent } from '../components/search/search';
import { DeleteComponent } from '../components/delete/delete';



//components
import { AppComponent } from './app.component';

//models




//providers
import { Services } from '../providers/services/services';
import { BaseRestService } from '../providers/base.rest.service';
import { AuthRestService } from '../providers/auth.rest.service';
import { DialogflowService } from '../providers/services/dataflow';
import { RouterExtService } from '../providers/services/router.service';
import { WindowRefService } from '../providers/window.ref';
import { StorageService } from '../providers/storageservice/storageservice';
import {HelperSerice} from '../providers/utils/helperService';
import { PasswordValidation } from '../providers/validators/password-validator';

//directives
import { RouterModule } from '@angular/router';



const componentsDeclarations = [
  HeaderPage,
  FooterPage,
  MenuLeft,
  MenuRight,
  GanttChart,
  Itsystems,
  DetailsPage,
  CreatePage,
  Login,
  Widgets,
  MessageList,
  MessageItem,
  MessageForm,
  ChatContainer,
  Dashboard,
  Notifications,
  modalComponent,
  BreadcrumComponent,
  customForm,
  Translation,
  Templates,
  Configuration,
  analyticsComponent,
  uploadsComponent,
  filterComponent,
  jsonviewerComponent,
  notifyviewerComponent,
  OperationsComponent,
  sendemailComponent,
  sendmessagesComponent,
  fletfiltersComponent,
  newGridComponent,
  OtpComponent,
  changepasswordComponent,
  UsernameComponent,
  Skema,
  skemadeleteComponent,
  skemacreateComponent,
  searchComponent,
  DeleteComponent,
  warningComponent
]

const providersDeclarations = [
  Services,
  BaseRestService,
  AuthRestService,
  DialogflowService,
  RouterExtService,
  WindowRefService,
  StorageService,
  DatePipe,
  HelperSerice,
  PasswordValidation

]
const pipeDeclarations = []

const directivesDeclarations = [

]


@NgModule({
  declarations: [
    AppComponent, jqxGridComponent,
    jqxRangeSelectorComponent,
    jqxTabsComponent,
    jqxFileUploadComponent,
    jqxDropDownListComponent,
    jqxCalendarComponent,
    jqxPanelComponent,
    jqxTimePickerComponent,
    jqxDateTimeInputComponent,
    jqxNotificationComponent,
    // OwlDateTimeModule, 
    // OwlMomentDateTimeModule,
    //globalization,
    //globalize,
    // Models
    // Components
    componentsDeclarations,
    // Directives
    directivesDeclarations,
    // Pipes
    pipeDeclarations

  ],
  entryComponents: [
    Itsystems,
    DetailsPage,
    customForm,
    CreatePage,
    modalComponent,
    GanttChart,
    BreadcrumComponent,
    customForm,
    Translation,
    Templates,
    analyticsComponent,
    uploadsComponent,
    fletfiltersComponent,
    filterComponent,
    skemacreateComponent,
    skemadeleteComponent,
    DeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    NgDatepickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AmChartsModule,
    NgbModule,
    ContextMenuModule,
    DialogModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
    ProgressBarModule,
    InputTextModule,
    CalendarModule,
    MultiSelectModule,
    ClipboardModule,
    SliderModule,
    AngularFileUploaderModule,
    NgJsonEditorModule,
    MatDialogModule,
    CKEditorModule,
    TableModule,
    ToastModule,
    DropdownModule,
    RouterModule.forRoot([]),
    GoogleChartsModule.forRoot(),
    DawaAutocompleteModule.forRoot()
  ],
  providers: [
    providersDeclarations,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'da'},
    {provide: OwlDateTimeIntl, useClass: DefaultIntl},
    { provide: OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
