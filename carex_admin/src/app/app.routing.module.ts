import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Itsystems } from '../components/itsystems/itsystems';
import { DetailsPage } from '../components/detailspage/detailspage';
import { Translation } from '../components/translation/translation';
import { Templates } from '../components/templates/templates';
import { analyticsComponent } from '../components/analytics/analytics';
import { uploadsComponent } from '../components/uploads/uploads';
import { Configuration } from '../components/configuration/configuration';
import { sendmessagesComponent } from '../components/sendmessages/sendmessages';
import { newGridComponent } from '../components/newgrid/newgrid';
import { Login } from '../components/login/login';
import { UsernameComponent } from '../components/username/username';
import { OtpComponent } from '../components/otp/otp';
import { changepasswordComponent } from '../components/changepassword/changepassword';
import { Skema } from '../components/skema/skema';

const routes: Routes = [
    // {path:'Organisationer/:listId', component:Itsystems},  future reference
    { path: 'Katalog TRYG', component: Itsystems },
    { path: 'tilstandliste/DetailsPage', component: DetailsPage },
    { path: 'translation', component: Translation },
    { path: 'configuration', component: Configuration },
    { path: 'getuseranalytics', component: analyticsComponent },
    { path: 'templates', component: Templates },
    { path: 'uploads', component: uploadsComponent },
    { path: 'sendmessages', component: sendmessagesComponent },
    { path: 'newgrid', component:  newGridComponent },
    { path: 'login', component: Login },
    { path: 'username', component: UsernameComponent },
    { path: 'otp', component:  OtpComponent},
    { path: 'changepassword', component: changepasswordComponent},
    { path: 'skemaeditor', component: Skema}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {

    ngOnInit() {
    }
};