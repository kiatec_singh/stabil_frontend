import { Component, Output, Input, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { HelperSerice } from '../../providers/utils/helperService';
import * as moment from "moment";

declare var require: any;
declare var $: any;
var xlsx = require('json-as-xlsx');

@Component({
    selector: 'analytics-viewer',
    templateUrl: 'analytics.html'
})
export class analyticsComponent {

    private alldomainsdata: any;
    private eventData: any;
    private myColumnNames: any;
    private options: any;
    private pieoptions: any;
    private platformsoptions: any;
    private platformspieoptions: any;
    private versionscolumnoptions: any;
    private activeuserscolumnoptions: any;
    private userrelationscolumnoptions: any;
    private isLoading = false;
    private serviceErrorMessage = '';


    private columnchart = "ColumnChart";
    private piechart = "PieChart";
    private donutchart = "donutchart";
    private barchart = "BarChart";
    private chartdata = [];
    private Dashboard = "Dashboard";
    private pagesevent;
    private allevents;
    private platforms;
    private userrelations;
    private platformsData: any;
    private activeusers = [];
    private totalusers = [];
    private activeusersData: any;
    private versions = [];
    private versionsData: any;
    private totalusersData: any;
    private userrelationsData: any;
    private filteredata: any;
    private slctapplication: any = '';
    private enddate = null;
    private startdate = null;
    private isdownloading = false;
    private filteredapplications: Array<any>;
    private selecteddates:any;
    private allapplications = [
        { applikation_uuid: 'ae9055e5-210e-4a0b-9d39-b334c12cf744', organisation_uuid: "7fb031ae-0cb9-4a04-8407-1156d3d52108", appname: "Tryg", selected: false },
        { applikation_uuid: 'dc252eac-7759-4de1-a371-25da7a76761b', organisation_uuid: "bef7e721-29cb-4eec-b0a9-af70fdc7b05d", appname: "Molholm", selected: false },
        //{ applikation_uuid: 'ae9055e5-210e-4a0b-9d39-b334c12cf744', organisation_uuid: "4f4645fc-7c9a-4de7-9592-145edafdfc4c", appname: "Willis", selected: false },
        { applikation_uuid: '86d383ea-644c-49df-9d8c-0fdde0811a35', organisation_uuid: "8e7fcffe-444b-4468-ab59-9f63b00d15c5", appname: "Sundhed via AP Pension", selected: false },
        { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "59aa2f87-d09e-4fd7-9837-d3342082b35a", appname: "Carex Workplace", selected: false },
        { applikation_uuid: 'e1bdaed0-9f7a-4288-ac95-a6b00c41857a', organisation_uuid: "38507c00-97bf-43f1-9c76-1d2f1159de31", appname: "Sundhedsunivers", selected: false }
        // { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "aa67c9ab-04a1-43fd-9656-e3f3d742ec9c", appname: "Aalborg", selected: false }
        // { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "aa67c9ab-04a1-43fd-9656-e3f3d742ec9c", appname: "Aalborg", selected: false },

    ];
    @Input() domainname: any;
    @Output() onChange = new EventEmitter()
    @ViewChild('errorNotification', { static: true }) errorNotification: jqxNotificationComponent;


    private analyticsForm: any;

    constructor(private fb: FormBuilder, private helperService: HelperSerice, private auth: AuthRestService, private ref: ChangeDetectorRef, private baserestService: BaseRestService) {
        this.analyticsForm = this.fb.group({
            application: ['', Validators.required],
            startdate: [''],
            enddate: ['']
        });
        this.auth.userrelations.subscribe(
            (userrelations) => {
                this.userrelations = userrelations;
            }
        );
    }

    ngOnInit() {
        console.log(this.userrelations);
        if (this.userrelations) {
            let temp = [];
            temp.push(
                this.userrelations.map((item) => this.allapplications.filter((filteritem) => (filteritem.organisation_uuid === item.organisation_uuid))[0]).filter(notUndefined => notUndefined !== undefined)
            );
            this.filteredapplications = temp[0];
        }
     //  this.selecteddates = this.helperService.analticsformatDate(null);//[new Date(2018, 1, 12, 10, 30), new Date(2018, 3, 21, 20, 30)];
       console.log(this.selecteddates);
       
    }
    ngAfterViewInit(): void {
        $('select').selectpicker();
    }
    createChart() {
        this.allevents = [];
        this.platforms = [];
        this.versions = [];
        this.versionsData = [];
        this.activeusers = [];
        this.totalusers = [];
        // this.userrelations = [];
        if (this.filteredata && this.filteredata.length > 0) {
            for (let i in this.filteredata) {
                if (this.filteredata[i].request) {
                    this.allevents.push(this.filteredata[i].request.eventname);
                    this.platforms.push(this.filteredata[i].request.platform);
                    this.versions.push(this.filteredata[i].request.version);
                    this.activeusers.push(this.filteredata[i].request.bruger_uuid);
                    // this.userrelations.push(this.filteredata[i].request.applikation_uuid);
                    if (this.totalusers.indexOf(this.filteredata[i].request.userToken) === -1) {
                        this.totalusers.push(this.filteredata[i].request.userToken);
                    }
                }
            }
            var eventscount = {};
            var platformscount = {};
            var versionscount = {};
            var activeuserscount = {};
            // var userrelationsscount = {};
            this.allevents.forEach(function (i) { eventscount[i] = (eventscount[i] || 0) + 1; });
            this.platforms.forEach(function (i) { platformscount[i] = (platformscount[i] || 0) + 1; });
            this.versions.forEach(function (i) { versionscount[i] = (versionscount[i] || 0) + 1; });
            this.activeusers.forEach(function (i) { activeuserscount[i] = (activeuserscount[i] || 0) + 1; });
            // this.userrelations.forEach(function (i) { userrelationsscount[i] = (userrelationsscount[i] || 0) + 1; });

            this.makeeventchart(eventscount);
            this.makeplatformschart(platformscount);
            this.makeversionschart(versionscount);
            this.makeactiveusersschart(activeuserscount);
            // this.makeuserbyrelationschart(userrelationsscount);
            this.totalusersData = this.totalusers;
        }
        else {
            this.platformsData = null;
            this.activeusersData = null;
            this.activeusersData = null;
            this.versionsData = null;
            this.userrelationsData = null;
            this.eventData = null;
        }
        this.isLoading = false;
    }
    makeeventchart(count) {
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        for (let i in count) {
            j = [i, count[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        // this.options = {
        //     is3D: true,
        //     width: 600,
        //     height: 600,
        //     bar: { groupWidth: "100%" },
        //     legend: { position: "none" },
        //     colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        // };
        this.pieoptions = {
            is3D: true,
            width: 400,
            height: 400,
            chartArea: {
                left: 50,
                right: 0,
                bottom: 60,
                top: 60,
                width: "100%",
                height: "100%"
            },
            //  colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        };
        this.eventData = allformatedevent;
        //  this.isLoading=false;
    }
    makeplatformschart(platformscount) {
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        for (let i in platformscount) {
            j = [i, platformscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.platformspieoptions = {
            pieHole: 0.5,
            width: 400,
            height: 400,
            chartArea: {
                left: 50,
                right: 0,
                bottom: 60,
                top: 60,
                width: "100%",
                height: "100%"
            },
        };
        this.platformsData = allformatedevent;
    }
    makeversionschart(versionscount) {
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        for (let i in versionscount) {
            j = [i, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.versionscolumnoptions = {
            is3D: true,
            width: 600,
            height: 400,
            bar: { groupWidth: "100%" },
            legend: { position: "none" },
            colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        };
        this.versionsData = allformatedevent;
    }
    makeactiveusersschart(versionscount) {
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        for (let i in versionscount) {
            j = [i, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.activeuserscolumnoptions = {
            is3D: true,
            width: 600,
            height: 400,
            bar: { groupWidth: "100%" },
            legend: { position: "none" },
            colors: allcolors, //['#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99', '#66AA00', '#3B3EAC', '#994499', '#FF9900', '#22AA99'],
        };
        this.activeusersData = allformatedevent;
    }
    makeuserbyrelationschart(versionscount) {
        let data = [];
        let allformatedevent = [];
        let allcolors = [];
        let j: any;
        let appname = 'undefined';
        let appcolor = '#0d3348';
        for (let i in versionscount) {
            if (i === "7fb031ae-0cb9-4a04-8407-1156d3d52108") {
                appname = 'Tryg';
                appcolor = '#dc0000';
            }
            if (i === "bef7e721-29cb-4eec-b0a9-af70fdc7b05d") {
                appname = 'Mølholm Sundhedsunivers';
                appcolor = '#595959';
            }
            if (i === "8e7fcffe-444b-4468-ab59-9f63b00d15c5") {
                appname = 'Sundhed via AP';
                appcolor = '#ff4d1d';
            }
            if (i === "59aa2f87-d09e-4fd7-9837-d3342082b35a") {
                appname = 'Connect';
                appcolor = '#ff4d1d';
            }
            if (i === "aa67c9ab-04a1-43fd-9656-e3f3d742ec9c") {
                appname = 'Aalborg';
                appcolor = '#ff4d1d';
            }
            if (i === "4f4645fc-7c9a-4de7-9592-145edafdfc4c") {
                appname = 'Willis';
                appcolor = '#ff4d1d';
            }

            if (i === "undefined") {
                appname = 'undefined';
                appcolor = '#0d3348';
            }

            j = [appname, versionscount[i]];
            allformatedevent.push(j);
            allcolors.push(this.getRandomColor());
        }
        this.userrelationscolumnoptions = {
            chartType: 'BarChart',
            type: 'BarChart',
            is3D: true,
            explorer: { axis: 'horizontal', keepInBounds: true },
            dataTable: allformatedevent,
            options: {
                is3D: true,
                width: 600,
                height: 400,
                bar: { groupWidth: "100%" },
                legend: { position: "none" },
            }
        };
        this.userrelationsData = allformatedevent;
    }
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    handleChange(name) {
        console.log(name);
    }
    getanalyticsData() {
        console.log("oin submit");
    }
    StartDate(i, e) {
   console.log(i);
   console.log(e);
    }
    clearstartDate(item) {
        console.log(item);
        this.selecteddates = null;
    }

    Multiselect(e, item) {
       // this.fetchdata();
       console.log(e);
       console.log(item);
       console.log(this.slctapplication);
    }
    fetchdata() {
        this.isLoading = true;
        var formatedstd = null;
        var formatedend = null;
        this.filteredata=[];
        var totalusersData = 0;
        if (this.slctapplication && this.slctapplication.length > 0) {
            let organisation_uuid = []
            let applikation_uuid = [];
            for (let i in this.slctapplication) {
                organisation_uuid.push(this.slctapplication[i].organisation_uuid);
                applikation_uuid.push(this.allapplications.filter((item) => item.organisation_uuid === this.slctapplication[i].organisation_uuid)[0].applikation_uuid);
            }
            if (this.selecteddates && this.selecteddates[0]) {
               let dateformat =  this.selecteddates[0]._d?this.selecteddates[0]._d: this.selecteddates[0];
                formatedstd = moment(dateformat).format("YYYY-MM-DD HH:mm") +':00';
            }
            if (this.selecteddates && this.selecteddates[1]) {
                let dateformat =  this.selecteddates[1]._d?this.selecteddates[1]._d: this.selecteddates[1];
                formatedend = moment(dateformat).format("YYYY-MM-DD HH:mm") +':59';
            }
            if (applikation_uuid && applikation_uuid.length > 0) {
                this.baserestService.getuseranalytics(applikation_uuid,
                    formatedstd, formatedend).then(
                        (alldomainsdata) => {
                            this.alldomainsdata = alldomainsdata;
                            this.filteredata = alldomainsdata;
                            this.createChart();
                        }
                    ),
                    error => {
                        this.errorNotification.refresh(); this.errorNotification.open(); this.serviceErrorMessage = error.status.besked ? error.status.besked : "Error";
                        this.isLoading = false;
                    }
            }

        }
        else {
            this.filteredata = null;
            this.platformsData = null;
            this.activeusersData = null;
            this.activeusersData = null;
            this.versionsData = null;
            this.userrelationsData = null;
            this.totalusersData = null;
            this.eventData = null;
            this.isLoading = false;
        }
    }
    download() {
        this.isdownloading = true;
        var excelcolumns = [];
        var exceldata = [];
        this.filteredata.map(function (item) {
            if (item && item.request) {
                let requestdata = item.request;
                requestdata.log_timestamp = item.log_timestamp ? item.log_timestamp : '';
                requestdata.action = item.service_action ? item.service_action : '';
                requestdata.cvr = requestdata.cvr ? requestdata.cvr : '';
                requestdata.log_id = item.log_id ? item.log_id : '';
                exceldata.push(requestdata);
            }
        });
        excelcolumns.push({ label: 'log_id', value: 'log_id' });
        excelcolumns.push({ label: 'action', value: 'action' });
        excelcolumns.push({ label: 'platform', value: 'platform' });
        excelcolumns.push({ label: 'userToken', value: 'userToken' });
        excelcolumns.push({ label: 'bruger_uuid', value: 'bruger_uuid' });
        excelcolumns.push({ label: 'eventname', value: 'eventname' });
        excelcolumns.push({ label: 'cvr', value: 'cvr' });
        excelcolumns.push({ label: 'itsystem_uuid', value: 'itsystem_uuid' });
        excelcolumns.push({ label: 'applikation_uuid', value: 'applikation_uuid' });
        excelcolumns.push({ label: 'version', value: 'version' });
        excelcolumns.push({ label: 'relation_organizationid', value: 'relation_organizationid' });
        excelcolumns.push({ label: 'relation_organizationname', value: 'relation_organizationname' });
        excelcolumns.push({ label: 'log_timestamp', value: 'log_timestamp' });
        var settings = {
            sheetName: "Analytics",
            fileName: "Analytics",
        }

        this.isdownloading = false;
        xlsx(excelcolumns, exceldata, settings);
    }
}
