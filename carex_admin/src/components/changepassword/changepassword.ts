import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { StorageService } from '../../providers/storageservice/storageservice';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { PasswordValidation } from '../../providers/validators/password-validator';
import { Router } from '@angular/router';

@Component({
    selector: 'changepassword-viewer',
    templateUrl: 'changepassword.html'
})
export class changepasswordComponent {

    rootPage: any;
    private loginForm: FormGroup;
    private userinfo;
    private password;
    private confirmPassword;
    private userid;
    private cp_content;
    private userchecklistdata;
    private loading= false;
    private serviceerror = false;
    private serviceerrormessage;
    public checklistdata: any;
    private selectedlanguage;
    private userToken;
    private showrepwd=false;
    private showpwd=false;
    @ViewChild('passwordele', { static: false }) passwordele:any;
    @ViewChild('repasswordele', { static: false }) repasswordele:any;


    constructor(private fb: FormBuilder, private ref: ChangeDetectorRef, 
        private authService: AuthRestService, private baserestService: BaseRestService, private storageService: StorageService,private router:Router) {
            this.userid = this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.userid ? this.router.getCurrentNavigation().extras.state.userid : '';
        this.loginForm = this.fb.group({
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        }, {
                validator: PasswordValidation.MatchPassword
            });
            this.authService.userToken.subscribe(
                (userToken: any) => {
                        this.userToken = userToken;
                }
            );
    }

    ngOnInit() {
    
    }

    gotologin() {
        this.loading = true;
        this.serviceerror = false;
        this.password = this.loginForm.value.password;
        this.confirmPassword = this.loginForm.value.confirmPassword;
        this.baserestService.changePassword(this.userid.id, this.password).then(
            (userinfo) => { 
                this.userinfo = userinfo; 
                this.validateAuthorisation(); 
            },
            error => {
                this.serviceerror = true;
                this.serviceerrormessage = error && error.error ? error.error : error.statusText;
                console.log(error.error);
            }
        )
    }
validateAuthorisation(){
    this.baserestService.getAllMenuList(this.userinfo.id).then(
        (menulist) => {
            this.userToken = menulist.userToken;
            this.authService.setuserToken(menulist.userToken);
            this.authService.setmenulist(menulist);
            if (menulist.relationer) {
                this.authService.setuserrelations(menulist.relationer);
            }
            this.storageService.set('carexadmin',this.userinfo);
            this.authService.setuserobj(this.userinfo);
            this.router.navigateByUrl('');
            this.loading=false;

        },
        (error) => { 
            this.loading=false;
            this.serviceerror =true;
            this.serviceerrormessage= error.error.besked;    
        });
    }

    ngOnDestroy(): void {
        this.ref.detach();
       
    }
    showrepassword(){
        
        this.showrepwd = this.showrepwd===true? false:true;
        if(this.showrepwd===true){
             this.repasswordele.nativeElement.type = "text";
             this.ref.detectChanges();
        } 
        else{
            this.repasswordele.nativeElement.type = "password";
            this.ref.detectChanges();
       } 
    }
    showpassword(){
        this.showpwd = this.showpwd===true? false:true;
        if(this.showpwd===true){
             this.passwordele.nativeElement.type = "text";
             this.ref.detectChanges();
        } 
        else{
            this.passwordele.nativeElement.type = "password";
            this.ref.detectChanges();
       } 
    }
    resetForm() {
        this.serviceerror = false;
        this.loginForm.reset();
    }
    
    inputvalueChange() {
        this.serviceerror = false;
        this.ref.reattach();
         this.ref.detectChanges();
    }
}