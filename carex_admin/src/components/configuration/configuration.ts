import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { ClipboardService } from 'ngx-clipboard';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';
import { saveAs } from 'file-saver';

declare var $: any;


@Component({
    selector: 'configuration',
    templateUrl: 'configuration.html'
})

export class Configuration {
    private userrelations: any;
    private skabelonlist: any;
    private skabeloncontent: any;
    private modalReference: any;
    private Objekttype: any;
    private slctdok: any = "vaælge";
    private brand: any = "vaælge";
    private title = '';
    private teaser = '';
    private beskrivelse = '';
    private flettefelter = "[]";
    private body: any;
    private slctlang = 'da';
    private loading = false;
    private userToken;
    private skemaList;
    private sorteduserrelations;
    private slcOrgname: any;
    private slctbrugervendt: any;
    private orgcopyflag: boolean = false;
    private brugervendcopyflag: boolean = false;
    private textcopyflag: boolean = false;
    private jsoneditor;
    private changedjsondata;
    private allbrandsList = [];
    private allbrands = [];
    private alldokumentList = [];
    private brandconfig;
    private editedbody;
    private showupload = false;
    private fileuploaded: any = '';
    private nullbrand={
        "configuration": "not available"
      };
    private defaultbrandconf = {
        "environment": {},
        "configuration": {}
    }

    //
    private allapplications = [
        { applikation_uuid: 'dc252eac-7759-4de1-a371-25da7a76761b', organisation_uuid: "bef7e721-29cb-4eec-b0a9-af70fdc7b05d", appname: "Molholm", selected: false },
        { applikation_uuid: '86d383ea-644c-49df-9d8c-0fdde0811a35', organisation_uuid: "8e7fcffe-444b-4468-ab59-9f63b00d15c5", appname: "Ap", selected: false },
        { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "59aa2f87-d09e-4fd7-9837-d3342082b35a", appname: "Connect", selected: false },
        { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "aa67c9ab-04a1-43fd-9656-e3f3d742ec9c", appname: "Aalborg", selected: false },
        { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "2a60966b-fd54-47dd-8478-92a120a54276", appname: "Zeppeline", selected: false },
        { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "80227aae-d26c-45a7-b9b4-e28210dca374", appname: "BKMedical", selected: false },
        { applikation_uuid: '6966bf6e-d334-41a5-b5ee-99e7d015118f', organisation_uuid: "4cf2e4ad-3daf-4139-93d3-d3891beefaa2", appname: "AON", selected: false }
    ];

    @ViewChild('successNotification', { static: false }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: false }) errorNotification: jqxNotificationComponent;
    public editorOptions: JsonEditorOptions;
    @ViewChild(JsonEditorComponent, { static: true }) editor: JsonEditorComponent;

    constructor(private baserestService: BaseRestService, private auth: AuthRestService, private clipboardService: ClipboardService, private ref:ChangeDetectorRef) {
        this.editorOptions = new JsonEditorOptions();
        this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
        this.auth.userToken.subscribe(
            (userToken) => {
                this.userToken = userToken;
            }
        );
        this.auth.userrelations.subscribe(
            (userrelations) => {
                this.userrelations = userrelations;
            }
        );

    }

    ngOnInit() {
        this.baserestService.getDokumentList().then(
            (alldokumentList: any) => {
                this.alldokumentList = alldokumentList.data;
                this.setDok();
            },
            (error) => { console.log(error); this.loading = false; }
        )
    }
    setDok() {
        setTimeout(() => {
            $('select').selectpicker();
        }, 100);

    }
    onChange(e, item) {
        this.loading = true;
        this.showupload = false;
        this.allbrands.length = 0;
        this.baserestService.getBrands(this.slctdok, this.userToken).then(
            (brandconfig: any) => { this.setbrandConfig(brandconfig); },
            (error) => { console.log(error); this.loading = false; }
        )
    }
    setbrandConfig(brandconfig) {
        this.jsoneditor = true;
        // this.showupload = brandconfig === null ? true : false;
        if(brandconfig===null){
            brandconfig=this.nullbrand;
        }
        this.brandconfig = brandconfig; //? brandconfig : this.defaultbrandconf;
        this.body = brandconfig; //? brandconfig : this.defaultbrandconf;
        this.showupload = brandconfig === null ? true : false; // need a refactor
        this.editedbody = brandconfig;
        this.loading = false;
        this.ref.detectChanges();
    }
    uploadfile(evt) {
        try {
            let files = evt.target.files;
            if (!files.length) {
                console.log('No file selected!');
                this.errorNotification.open();
                this.errorNotification.open();
                return;
            }
            let file = files[0];
            let reader = new FileReader();
            const self = this;
            reader.onload = (event: any) => {
                let data: any = JSON.parse(event.target.result);
                this.body = data;
                this.brandconfig = data;
                this.editedbody = data;
                this.showupload = false;
                this.ref.detectChanges();
            };
            reader.readAsText(file);
        } catch (err) {
            console.error(err);
        }
    }

    Deleteconfig(e) {
        e.preventDefault();
        this.body = this.nullbrand;;
        this.brandconfig = this.nullbrand;;
        this.editedbody = this.nullbrand;;
        this.showupload = true;
        this.ref.detectChanges();

    }

    Copyorg(flag) {
        this.orgcopyflag = false; this.brugervendcopyflag = false; this.textcopyflag = false;
        switch (flag) {
            case "orgcopyflag":
                this.orgcopyflag = true;
                break;
            case "brugervendcopyflag":
                this.brugervendcopyflag = true;
                break;
            case "textcopyflag":
                this.textcopyflag = true;
                break;
            default:
                break;
        }
    }
    copied(e) {
        console.log(e);
    }
    Cancel(e) {
        e.preventDefault();
        this.skabelonlist.length = 0;
        this.skabeloncontent = null;
    }
    saveConfiguraiton() {
        // console.log(this.editedbody);
        this.baserestService.savesConfiguration(this.slctdok, JSON.stringify(this.editedbody), this.userToken).then(
            (savedcontent) => {
                this.successNotification.open();
                this.successNotification.open(); this.loading = false;
            },
            (error) => {
                console.log(error);
                this.errorNotification.open();
                this.errorNotification.open(); this.loading = false;
            })
    }
    getData(e) {
        this.editedbody = e;
    }
    DownloadJson(e) {
        e.preventDefault();
        let item = this.alldokumentList.filter(x => x.uuid == this.slctdok)[0];
        const blob = new Blob([JSON.stringify(this.body)], { type: 'application/json' });
        saveAs(blob, item.titel + '.json');
    }
}
    // With Promises:
