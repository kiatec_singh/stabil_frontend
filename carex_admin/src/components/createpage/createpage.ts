import { Component, Output, EventEmitter } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { Services } from '../../providers/services/services';
import { ActivatedRoute } from '@angular/router';
import { AuthRestService } from '../../providers/auth.rest.service';
import { Localization } from '../../models/localization/localization';

@Component({
    selector: 'createpage',
    templateUrl: 'createpage.html'
})
export class CreatePage {
    public uuid;
    public detailsData: any;
    public loading = false;
    private listId: any;
    private localization: Localization;
    private brugervendtnoegle: any;
    private heading;
    private errorMessage;

    @Output() selectitem = new EventEmitter();
    public userToken: any;
    constructor(private service: BaseRestService, private auth: AuthRestService,
        private activatedRoute: ActivatedRoute, private services: Services) {
    }

    ngOnInit() {
        this.loading = true;
        this.brugervendtnoegle = this.activatedRoute.snapshot.params.brugervendtnoegle;
        this.heading = this.services.getselectedMenu();
        this.listId = this.services.getlistId();
        this.uuid = this.services.getClickedRowuuiid();
        this.localization = new Localization();
        this.localization = this.localization.localizationobj;
        this.getDetailedContent(this.uuid, this.listId);
    }
    getDetailedContent(uuid, listId) {
        this.service.getcreateDetailsdata(listId)
            .then(
                detailsData => {
                this.detailsData = detailsData;
                this.loading = false;
                },
                error => {
                    this.errorMessage = error.status.besked; this.loading = false;
                },
            );
    }
}