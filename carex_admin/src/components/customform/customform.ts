import { Component, ViewChild, Output, EventEmitter, Input, ChangeDetectorRef, ElementRef, ViewChildren } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { Services } from '../../providers/services/services';
import { ActivatedRoute } from '@angular/router';
import { Structure } from '../../models/details.data';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Localization } from '../../models/localization/localization';
import { DawaAutocompleteItem } from 'ngx-dawa-autocomplete';
import { AuthRestService } from '../../providers/auth.rest.service';
import * as moment from 'moment';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { CKEditorComponent } from 'ckeditor4-angular';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { HelperSerice } from '../../providers/utils/helperService';

declare var jquery: any;
declare var $: any;
declare var globalize: any;
declare var globalization: any;

declare var window: any;

@Component({
    selector: 'customform',
    templateUrl: 'customform.html'
})
export class customForm {
    public uuid;
    //public detailsData: DetailsData[];
    public structue: Structure;
    public bi_objekter: any;
    public tabsarray;
    public selectedTab = 0;
    public selectedContainer = 0;
    public detailsForm: FormGroup;
    public items: any;
    private formControl: FormControl;
    private formArray: FormArray;
    private currentTab;
    private currentFormElement;
    private currentItem;
    public loading = false;
    public tabIndex;
    public dropdowns: any;
    public dropdown: any;
    public tabstoDisplay: any = [];
    private listId: any;
    private objecttype: any;
    private localization: Localization;
    private brugervendtnoegle: any;
    private selectedMenu;
    private heading;
    public itemss: DawaAutocompleteItem[] = [];
    public addresserrorMesg = false;
    public enableaddresserrorMsg = false;
    public highlightedIndex: number = 0;
    public selectedStreet: string = '';
    private displayheadingName;
    private lastupdatedTime;
    private brugerName;
    private lastupdatedTimeStamp;
    private errorMessage;
    private File;
    private downloadDetails;
    private fileNavn;
    private documentuuid;
    private environmentUrl;
    private AllFiles = [];
    private navigationElements;
    private readonly = false;
    private userToken: any;
    private detailsform: any;
    private opentoogle = false;
    private config= {
        height: '300px',
        startupOutlineBlocks: true,
        removeButtons: 'About,Image,Subscript,Table,Superscript,Anchor,Spellchecker,Removeformat',
        removeDialogTabs: 'link:target;link:advanced;link:protocol',
        removeInfoTabs: 'link:protocol',
        fontSize: 21
    };
    private editorInstance: any;
    private fletfilters;
    private selected_reluuid;
    private mindate;
    private actions;
    public objekt_uuid = null;
    private activeIds:string[]=[];
    private buttons=[];


    private ngbModalOptions: NgbModalOptions = {
        backdrop: 'static',
        keyboard: false
    };

    @ViewChild('successnotification', { static: false }) successnotification: jqxNotificationComponent;
    @ViewChild('errornotification', { static: false }) errornotification: jqxNotificationComponent;

    @ViewChild('detailForm', { static: false }) detailForm;
    @ViewChild('editor', { static: false }) editorComponent: CKEditorComponent;
    @ViewChild('warning', { static: false }) warning: ElementRef;
    @ViewChildren('acc') acc: ElementRef;

    @Output() selectitem = new EventEmitter();
    @Input() detailsData: any;

    constructor(private service: BaseRestService, private dateTimeAdapter: DateTimeAdapter<any>, private modalService: NgbModal, private helperService: HelperSerice,
        private services: Services, private activatedRoute: ActivatedRoute, private auth: AuthRestService, private formBuilder: FormBuilder, private ref: ChangeDetectorRef, private location: Location) {
        this.auth.userToken.subscribe(
            (userToken: any) => {
                this.userToken = userToken;
            }
        );
        this.dateTimeAdapter.setLocale('da-DK');
    }

    ngOnInit() {
        this.loading = true;
        this.brugervendtnoegle = this.activatedRoute.snapshot.params.brugervendtnoegle;
        this.heading = this.services.getselectedMenu();//this.activatedRoute.snapshot.data.title;   
        this.navigationElements = [this.heading]; // need to make new breadcrum in future
        this.listId = this.services.getlistId();
        this.uuid = this.services.getClickedRowuuiid();
        this.localization = new Localization();
        this.environmentUrl = this.service.getEnvironment();
        this.localization = this.localization.localizationobj;
        this.setData()
    }


    getHeaderInfo() {
        let details: any = this.detailsData;
        this.actions = this.detailsData.result.actions;
        this.displayheadingName = details.status.displayname;
        this.services.setdisplayName(details.status.displayname);
        this.brugerName = details.status.brugernavn;
        this.readonly = details.status.readonly ? details.status.readonly : false;
        if (details.status.update_timestamp) {
            this.lastupdatedTime = new Date(details.status.update_timestamp);
            var dd: any = this.lastupdatedTime.getDate();
            var mm: any = this.lastupdatedTime.getMonth() + 1;
            var yyyy = this.lastupdatedTime.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            this.lastupdatedTime = dd + '-' + mm + '-' + yyyy;
        }
    }
    setData() {
        let details: any = this.detailsData;
        this.getHeaderInfo();
        this.lastupdatedTimeStamp = details.status.update_timestamp;
        this.detailsForm = this.formBuilder.group({
            items: this.formBuilder.array([])
        });
        this.addItem();
        this.buttons = this.actions && this.actions.buttons ? this.actions.buttons.reverse(): [];
    }
    addItem(): void {
        this.items = this.detailsForm.get('items') as FormArray;
        for (let i in this.detailsData.result.detaljer) {
            this.items.push(this.createDyanicForm(this.detailsData.result.detaljer[i]));
        }
        setTimeout(() => {
            $('.selectpicker').selectpicker();
        }, 100);
        this.loading = false;
        // console.log(this.detailsForm);
        this.detailsform = this.detailsForm
    }
    createDyanicForm(item): FormGroup {
        var tooltipinfo = this.detailsData.beskrivelser[item.ld_brugervendtnoegle];

        if (item.type == "input" && item.ld_brugervendtnoegle != "adresser" && item.type != "file" && item.type != "date") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                readonly: item.readonly,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning

            });
        }
        if (item.type === "accordion") {
            var accordians = [];
            var k: any = null;
            var accheading = item.titel;
            var lang = item.ld_brugervendtnoegle.split('_').pop();
            for (let j in item) {
                if (item[j] && item[j].property && item[j].property.accordion) {
                    k = {
                        name: item[j].titel,
                        tabIndex: this.tabIndex,
                        value: item[j].selected_values[0], // replace to type html
                        type: item[j].type,
                        required: item[j].mandatory,
                        tooltip: tooltipinfo,
                        hide: item[j].property && item[j].property.hide ? item[j].property.hide : false,
                        accordian: item[j].property && item[j].property.accordian ? item[j].property.accordian : false,
                        readonly: item[j].readonly,
                        ld_brugervendtnoegle: item[j].ld_brugervendtnoegle,
                        relationindeks: item[j].relationindeks,
                        selected_list: [item[j].selected_values],
                        relationsvirkning: item[j].relationsvirkning
                    };
                    accordians.push(k);
                }
            }
            return this.formBuilder.group({
                accordian: true,
                title: accheading,
                lang: lang,
                allaccordians: [accordians]
            });
        }
        if (item.type == "textarea") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0], // replace to type html
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                readonly: item.readonly,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning

            });
        }
        if (item.type == "html") {
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0].replace(/(?:\r\n|\r|\n)/g, '<br>'), // replace to type html
                type: item.type,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                readonly: item.readonly,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning

            });
        }
        if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
            this.selectedStreet = item.selected_values[0];
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
        if (item.type == "file") {
            this.selectedStreet = item.selected_values[0];
            this.documentuuid = item.dokument_uuid;
            let image = item.image ? item.image : null;
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: item.selected_values[0],
                type: item.type,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                documentuuid: item.dokument_uuid,
                image: image,
                selected_list: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
        if (item.type == "date") {
            var formatdate = null;
            var minimumdate = null;
            this.mindate = this.helperService.formatDate(null);
            if (item.selected_values && item.selected_values[0] && item.selected_values[0].length != 0) {
                formatdate = this.helperService.formatDate(item.selected_values[0]);
            }
            else {
                if (item.property && item.property.future === "true" && item.property.past === "false") {
                    formatdate = this.helperService.formatDate(null);
                    minimumdate = formatdate;
                }
            }
            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                value: formatdate ? formatdate : null,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                selected_list: [item.selected_values[0]],
                formatdate: formatdate ? formatdate : null,
                mindate: minimumdate,
                relationsvirkning: item.relationsvirkning
            });
        }

        if (item.type == "dropdown" && item.relationsvirkning === false) {
            if (item.multiselect === "false") {
                let selected_name;
                let selected_names = [];
                for (let i in item.valuelist) {
                    selected_name = item.valuelist[i].selected === true ? item.valuelist[i].rel_uri : null;
                    if (selected_name) {
                        selected_names.push(selected_name);
                    }
                }
                return this.formBuilder.group({
                    name: item.titel,
                    tabIndex: this.tabIndex,
                    options: [item.valuelist],
                    multiselect: item.multiselect,
                    tooltip: tooltipinfo,
                    hide: item.property && item.property.hide ? item.property.hide : false,
                    accordian: item.property && item.property.accordian ? item.property.accordian : false,
                    type: item.type,
                    readonly: item.readonly,
                    selected_name: selected_names,
                    required: item.mandatory,
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    relationindeks: item.relationindeks,
                    valuelist: item.valuelist,
                    relationsvirkning: item.relationsvirkning
                });
            }
            if (item.multiselect === "true") {
                let selected_name;
                let selected_names = [];
                for (let i in item.valuelist) {
                    selected_name = item.valuelist[i].selected === true ? item.valuelist[i].rel_uri : null;
                    if (selected_name) {
                        selected_names.push(selected_name);
                    }
                }
                return this.formBuilder.group({
                    name: item.titel,
                    tabIndex: this.tabIndex,
                    options: [item.valuelist],
                    multiselect: item.multiselect,
                    tooltip: tooltipinfo,
                    hide: item.property && item.property.hide ? item.property.hide : false,
                    accordian: item.property && item.property.accordian ? item.property.accordian : false,
                    type: item.type,
                    readonly: item.readonly,
                    selected_name: [selected_names],
                    required: item.mandatory,
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    relationindeks: item.relationindeks,
                    valuelist: item.valuelist,
                    relationsvirkning: item.relationsvirkning
                });
            }
        }

        if (item.type == "dropdown" && item.relationsvirkning === true) {
            for (let i in item.valuelist) {
                let intervalitem = {
                    fra: item.valuelist[i].fra,
                    til: item.valuelist[i].til,
                    note: item.valuelist[i].note
                }
                item.valuelist[i].intervals = [intervalitem];
            }

            return this.formBuilder.group({
                name: item.titel,
                tabIndex: this.tabIndex,
                options: [item.valuelist],
                multiselect: item.multiselect,
                tooltip: tooltipinfo,
                hide: item.property && item.property.hide ? item.property.hide : false,
                accordian: item.property && item.property.accordian ? item.property.accordian : false,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                relationindeks: item.relationindeks,
                valuelist: item.valuelist,
                selected_list: [item.selected_values],
                // selected_names: [selected_names],
                //selected_names: selected_names && selected_names.length ? [selected_names]:null,
                selected_values: [item.selected_values],
                relationsvirkning: item.relationsvirkning
            });
        }
    }

    downloadFile(event) {
        event.preventDefault();
        this.service.downloadFileDocument(this.listId, this.documentuuid).then(
            res => { this.downloadDetails = res; this.getFile(res); this.successnotification.open(); this.loading = false; },
            error => { this.errornotification.open(); this.errorMessage = error.status.besked; console.log(error); this.loading = false; }
        );
    }
    getFile(result) {
        let temp_Url = result.result.temp_path;
        let envi = this.service.getEnvironment();
        let e = envi.indexOf('/api');
        let formurl = envi.substring(0, e);
        let tempindex = temp_Url.lastIndexOf('temp');
        let filedownload_url = temp_Url.substring(tempindex, temp_Url.length);
        let downloadURL = formurl.concat("/" + filedownload_url);
        window.open('' + downloadURL, '_blank');
    }

    Select(event, itemname): void {
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle)[0];
        for (let j in itemname.value.options) {
            if (itemname.value.selected_name === filtereditem.options[j].rel_uri) {
                filtereditem.options[j].selected = true;
                this.selected_reluuid = filtereditem.options[j].rel_uuid;
            }
            else {
                filtereditem.options[j].selected = false;
            }
        }
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle)[0] = filtereditem;
        if (itemname.value.ld_brugervendtnoegle === 'kampagneskabeloner' && this.selected_reluuid != null) {
            let modalref = this.modalService.open(this.warning, this.ngbModalOptions);
            this.services.setmodalReference(modalref);
        }
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle)[0] = filtereditem;
        $('.invalid-feedback.' + itemname.value.ld_brugervendtnoegle).addClass('hide');
    }
    Multiselect(event, itemname): void {
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle)[0];
        for (let j in itemname.value.options) {
            if (itemname.value.selected_name.indexOf(itemname.value.options[j].rel_uri) != -1) {
                filtereditem.options[j].selected = true;
            }
            else {
                filtereditem.options[j].selected = false;
            }
        }
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle)[0] = filtereditem;
        $('.invalid-feedback.' + itemname.value.ld_brugervendtnoegle).addClass('hide');
    }
    Multiselectrelation(event, item, optionitem): void {
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0];
        for (let j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === optionitem.rel_uuid) {
                filtereditem.options[j] = optionitem
            }
        }
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0] = filtereditem;
    }
    ErrorMsg() {
        if (this.itemss.length > 0) {
            this.addresserrorMesg = false;
            this.enableaddresserrorMsg = false;
        }
        if (this.selectedStreet && this.selectedStreet.length > 0) {
            this.addresserrorMesg = false;
            this.enableaddresserrorMsg = false;
        }
        else {
            this.addresserrorMesg = true;
            this.enableaddresserrorMsg = true;
        }
    }
    public onItems(items) {
        this.itemss = items;
    }

    public onItemHighlighted(index) {
        this.highlightedIndex = index;
        if (index == 0) {
            this.addresserrorMesg = true;
            this.enableaddresserrorMsg = true;
        }
    }

    public onItemSelected(item) {
        this.itemss = [];
        this.highlightedIndex = 0;
        this.selectedStreet = item.text;
        this.addresserrorMesg = false;
        this.enableaddresserrorMsg = false;
    }

    done(res) {
        this.successnotification.open();
        this.successnotification.open(); // weird works after calling 2 times
        this.ref.detectChanges();
        this.loading = false;
        this.location.back();

    }
    selectFile(event, item) {
        item.value.value = event.target.files[0].name;
        this.fileNavn = event.target.files[0].name;
        this.File = event.target.files[0];
        let fileitem = {
            name: item.value.ld_brugervendtnoegle,
            value: this.File
        }
        this.AllFiles.push(fileitem);;
        let selected_item = item.value.name;
        var item = this.detailsForm.controls.items.value.filter(x => x.name == selected_item)[0];
        item.value = this.fileNavn;
        this.detailsForm.controls.items.value.filter(x => x.name == selected_item)[0] = item;
    }

    formatData(item) {
        if (item) {
            if (item.type == "input" && item.ld_brugervendtnoegle != "adresser") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value]
                }
            }
            if (item.type == "editor") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value]
                }
            }
            if (item.accordian && item.allaccordians) {
                let allaccords = [];
                item.allaccordians.forEach(element => {
                    allaccords.push({
                        ld_brugervendtnoegle: element.ld_brugervendtnoegle,
                        value: [element.value]
                    })
                });
                return allaccords;

            }
            if (item.type == "textarea") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value]
                }
            }
            if (item.type == "html") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value.replace(/(?:\r\n|\r|\n)/g, '<br>')]
                }
            }
            if (item.type == "date") {
                let formateddate: any = null;
                if (item.formatdate) {
                    formateddate = moment(item.formatdate).format('YYYY-MM-DD HH:mm:ss');
                    if (formateddate === 'Invalid date') {
                        formateddate = null;
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: formateddate ? [formateddate] : [null]
                }
            }
            if (item.type == "file") {
                let image = item.image ? item.image : null;
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [item.value],
                    image: image
                }
            }

            if (item.type == "input" && item.ld_brugervendtnoegle == "adresser") {
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: [this.selectedStreet],
                    relationindeks: item.relationindeks
                }
            }

            if (item.type == "dropdown" && item.multiselect == "false" && item.relationsvirkning === false) {
                var optionitem = item.options.filter(x => x.selected === true)[0];
                if (optionitem) {
                    return {
                        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                        value: [optionitem.rel_uuid]
                    }
                }
                else {
                    return {
                        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                        value: [null]
                    }
                }
            }

            if (item.type == "dropdown" && item.multiselect == "true" && item.relationsvirkning === false) {
                var values = [];
                if (item.selected_name && item.selected_name.length != 0) {
                    for (let i in item.options) {
                        if (item.selected_name.indexOf(item.options[i].rel_uri) != -1) {
                            values.push(item.options[i].rel_uuid);
                        }
                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: values
                }
            }
            if (item.type == "dropdown" && item.multiselect == "true" && item.relationsvirkning === true) {
                var values = [];
                if (item.options && item.options.length != 0) {
                    for (let i in item.options) {
                        if (item.options[i].selected === true) {
                            for (let j in item.options[i].intervals) {
                                let fra = null;
                                let til = null;
                                if (item.options[i].intervals[j].fra) {
                                    item.options[i].intervals[j].fra = moment(item.options[i].intervals[j].fra).format('YYYY-MM-DD HH:mm:ss');
                                }
                                if (item.options[i].intervals[j].til) {
                                    item.options[i].intervals[j].til = moment(item.options[i].intervals[j].til).format('YYYY-MM-DD HH:mm:ss');;
                                }
                                let slct = {
                                    rel_uri: item.options[i].rel_uri,
                                    rel_uuid: item.options[i].rel_uuid,
                                    fra: fra,
                                    til: til,
                                    note: item.options[i].intervals[j].note
                                }
                                values.push(slct);
                            }
                        }

                    }
                }
                return {
                    ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                    value: values
                }
            }

        }
    }
    dateChange(item, index) {
        let dateformat = null;
        console.log(item);
        if (index && index.selected) {
            dateformat = moment(index.selected._d).format('YYYY-MM-DD HH:mm:ss');
        }
        let slcitem = item.value.ld_brugervendtnoegle;
        item.value.value = dateformat;
        item.value.formatdate = dateformat;
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slcitem)[0] = item;
    }
    clearDate(item) {
        item.value.value = null;
        item.value.formatdate = null;
        let slcitem = item.value.ld_brugervendtnoegle;
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == slcitem)[0] = item;
    }
    dateEditfra(item, intervalitem, optionitem, index, j) {
        let dateformat = null;
        console.log(item);
        console.log(intervalitem);
        if (intervalitem && intervalitem.fra) {
            dateformat = moment(intervalitem.fra._d).format('YYYY-MM-DD HH:mm:ss');

        }
        intervalitem.fra = dateformat;
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0];
        for (let j in item.value.options.intervals) {
            if (item.value.options[j].rel_uri === optionitem.rel_uri) {
                filtereditem.options[j].intervalitem[index].fra = dateformat;
            }
        }
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0] = item;
        $('.invalid-feedback.' + item.value.ld_brugervendtnoegle).addClass('hide');


    }
    dateEdittil(item, intervalitem, optionitem, index, j) {
        let dateformat = null;
        // console.log(item);
        // console.log(optionitem);
        if (intervalitem && intervalitem.til) {
            dateformat = moment(intervalitem.til._d).format('YYYY-MM-DD HH:mm:ss');
        }
        intervalitem.til = dateformat;
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0];
        for (let j in item.value.options.intervalitem) {
            if (item.value.options[j].rel_uri === optionitem.rel_uri) {
                filtereditem.options[j].intervalitem[index].til = dateformat;
            }
        }
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0] = item;
        $('.invalid-feedback.' + item.value.ld_brugervendtnoegle).addClass('hide');
    }
    Deletedateitem(e, item, relationoptionitem, index) {
        e.preventDefault();
        console.log(relationoptionitem);
        console.log(item);
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0];
        for (let j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === relationoptionitem.rel_uuid) {
                filtereditem.options[j].intervals.splice(index, 1);
            }
        }
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0] = filtereditem;
        console.log(this.detailsForm.controls.items.value);
    }
    tooglepakker(e) {
        e.preventDefault();
        this.opentoogle = this.opentoogle === false ? true : false;
    }
    Createdateitem(e, item, relationoptionitem) {
        e.preventDefault();
        let filtereditem = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0];
        for (let j in filtereditem.options) {
            if (filtereditem.options[j].rel_uuid === relationoptionitem.rel_uuid) {
                let intervalitem = {
                    fra: null,
                    til: null,
                    note: ''
                }
                filtereditem.options[j].intervals.push(intervalitem)
            }
        }
        this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle)[0] = filtereditem;
    }

    onchngEditordata(editor) {
        let data = editor.editor.getData();
        this.editorInstance = editor;
    }

    openfilter(modal, event, item) {
        this.fletfilters = this.detailsForm.controls.items.value.filter(x => x.ld_brugervendtnoegle == "flettefelter")[0];
        let modalref =this.modalService.open(modal, this.ngbModalOptions);
        this.services.setmodalReference(modalref);
    }

    setDatatoEditor(data) {
        this.editorInstance.editor.insertHtml(data, 'text');
    }
    replaceeditorContent(confirmreplace) { //en,titel, dk etc needs to be replaces in feature
        if (confirmreplace === true) {
            this.service.getskabeloncontent(this.selected_reluuid, 'en').then(
                (skabeloncontent: any) => {
                    // this.editorInstance.editor.insertHtml(skabeloncontent, 'text');
                    let filteredaccordian = this.detailsForm.controls.items.value.filter(x => x.lang === 'en')[0];
                    filteredaccordian.allaccordians.map(function (item) {
                        if (item.ld_brugervendtnoegle.indexOf('titel') != -1) {
                            item.selected_list[0] = skabeloncontent.title;
                            item.value = skabeloncontent.title;
                        }
                        if (item.ld_brugervendtnoegle.indexOf('tekst') != -1) {
                            item.selected_list[0] = skabeloncontent.body[0];
                            item.value = skabeloncontent.body[0];
                        }

                    });
                    this.detailsForm.controls.items.value.filter(x => x.lang === 'en')[0] = filteredaccordian;

                },
                (error) => {
                    console.log(error); this.loading = false;
                }
            )
            this.service.getskabeloncontent(this.selected_reluuid, 'da').then(
                (skabeloncontent: any) => {
                    let filteredaccordian: any = this.detailsForm.controls.items.value.filter(x => x.lang === 'da')[0];
                    filteredaccordian.allaccordians.map(function (item) {
                        if (item.ld_brugervendtnoegle.indexOf('titel') != -1) {
                            item.selected_list[0] = skabeloncontent.title;
                            item.value = skabeloncontent.title;
                        }
                        if (item.ld_brugervendtnoegle.indexOf('tekst') != -1) {
                            item.selected_list[0] = skabeloncontent.body[0];
                            item.value = skabeloncontent.body[0];
                        }
                    });
                    this.detailsForm.controls.items.value.filter(x => x.lang === 'da')[0] = filteredaccordian;
                },
                (error) => {
                    console.log(error); this.loading = false;
                }
            )
        }
    }
    cancel(){
        this.modalService.dismissAll('cancelled');
    }
    saveData() {
        if (!this.detailForm.nativeElement.checkValidity()) {
            this.detailForm.nativeElement.classList.add("was-validated");
        }
        let formvalid = $(".invalid-feedback").is(":visible");
        if (!formvalid) {
            var formvalues = JSON.stringify(this.detailsForm.controls.items.value);
            let formdata: any = [];
            formdata.push(
                this.detailsForm.controls.items.value.map(this.formatData, this)
            );
            let submitting = [];
            formdata[0].map(function (e) {
                if (e && e.ld_brugervendtnoegle) {
                    submitting.push(e);
                } else {
                    e.map(function (el) {
                        if (el && el.ld_brugervendtnoegle) {
                            submitting.push(el);
                        }
                    });
                }
            });
            this.loading = true;
            this.service
                .updateDetailsdata(
                    this.listId,
                    '',
                    this.detailsData.status.update_timestamp,
                    JSON.stringify(submitting),
                    this.AllFiles
                )
                .then(
                    (res) => {
                        this.loading = false;
                        this.modalService.dismissAll('saved');
                        this.successnotification.open();
                    },
                    (error) => {
                        this.loading = false;
                        this.errornotification.open();
                        let error_messsage = error.error.status.besked;
                        this.errorMessage = error_messsage;
                    },
                    (complete) => {
                        this.loading = false;
                    }
                );
        }
    }
}