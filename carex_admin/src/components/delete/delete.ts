import { Component, ViewChild, Input, ElementRef, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: "delete-viewer",
  templateUrl: "delete.html",
})
export class DeleteComponent {
  constructor(
    public dialogRef: MatDialogRef<DeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}
    
  cancel() {
    this.dialogRef.close();
  }
}
