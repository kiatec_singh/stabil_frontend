import { Component, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseRestService } from '../../providers/base.rest.service';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';
import { AuthRestService } from '../../providers/auth.rest.service';
import { Services } from '../../providers/services/services';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';

declare var $: any;

@Component({
    selector: 'filter-viewer',
    templateUrl: 'filter.html'
})
export class filterComponent {

    public filtereditorOptions: JsonEditorOptions;
    private filteruuid;
    private createnewmodel = false;
    private overwritemodel = false;
    private filternavnmodel;
    private loading = false;
    private skema_uuid;
    private filternavnmodelerror = false;
    private allredaktoerList: any = [{
        value: 'Intet valgt',
        key: 0
    }];
    private allredaktoerslctd: any;
    private filternavnenable = true;
    @ViewChild(JsonEditorComponent, { static: true }) editor: JsonEditorComponent;
    @ViewChild('choosefilter', { static: false }) choosefilter: ElementRef;
    @ViewChild('availablefor', { static: false }) availablefor: ElementRef;
    @ViewChild('filternavn', { static: false }) filternavn: ElementRef;

    @ViewChild('successNotification', { static: true }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: true }) errorNotification: jqxNotificationComponent;

    @Input() allfilterList: any;
    @Input() filtersdata: any;
    private filterdata:any= null;


    constructor(private auth: AuthRestService, private baserestService: BaseRestService, private services: Services,
        public activeModal: NgbModal) {
        this.filtereditorOptions = new JsonEditorOptions();
        this.filtereditorOptions.mode = "view";
        this.services.filtersdata.subscribe(
            (filterdata)=>{
                this.filterdata = filterdata;
            });
    }
    ngOnInit() {
        this.loading = true;
    }
    ngAfterViewInit(): void {
        this.baserestService.getEditors().then(
            (allredaktoerList) => {
                this.allredaktoerList = allredaktoerList;
                this.loading = false;
                $('.selectpicker').selectpicker('refresh');
            },
            (error) => {
                console.log(error);
                this.loading = false;
            });
    }

    close(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
    createnew(e) {
        e.preventDefault();
        e.target.checked = true;
        this.createnewmodel = true;
        this.overwritemodel = false;
        this.filternavnenable = false;
        this.filteruuid = 0;
        this.choosefilter.nativeElement.setAttribute('disabled', true);
        $('#choosefilter').val('').selectpicker('refresh');
    }
    overwrite(e) {
        e.preventDefault();
        e.target.checked = true;
        this.createnewmodel = false;
        this.overwritemodel = true;
        this.filternavnmodelerror = false;
        this.choosefilter.nativeElement.setAttribute('disabled', false)
        this.filternavnenable = true;
        $(".selectpicker").prop("disabled", false);
        $('.selectpicker').selectpicker('refresh');
    }

    getFilterData(e, item) {
        //FUTURE TO-DO
        //this.loading = true;
        // this.baserestService.readfilterdata(this.filteruuid, this.autorisation).then(
        //     (filtersdata: any) => {
        //         console.log(filtersdata)
        //         // this.filtersdata = filtersdata;
        //         // this.body = filtersdata;
        //         $('.selectpicker').selectpicker('refresh');
        //         this.loading = false;
        //     },
        //     (error) => { console.log(error); this.loading = false; }
        // )

    }
    saveFilter(e) {
        if (this.createnewmodel === true && this.filternavnmodel) {
            this.save();
        }
        if (this.overwritemodel === true && (this.filteruuid && this.filteruuid != 0)) {
            this.save();
        }
    }
    save() {
        if (this.allredaktoerslctd) {
            this.loading = true;
            this.skema_uuid = this.services.getlistId();
            let requestdata: any = {};
            if (this.createnewmodel === true) {
                let filterobjekt:any={};
                if(this.filtersdata && this.filtersdata.filters){
                    requestdata = {
                        titel: this.filternavnmodel,
                        knapnavn: this.services.getselectedmenuTitle(),
                        skema_uuid: this.skema_uuid,
                        redaktoer: this.allredaktoerslctd
                    }
                    filterobjekt=this.filtersdata;
                    requestdata.filter = JSON.stringify(filterobjekt);
                } else{
                    filterobjekt={
                        filters:this.filterdata,   
                        }
                    let filtered = filterobjekt;
                    requestdata = {
                        titel: this.filternavnmodel,
                        knapnavn: this.services.getselectedmenuTitle(),
                        skema_uuid: this.skema_uuid,
                        redaktoer: this.allredaktoerslctd
                    }
                    requestdata.filter = JSON.stringify(filtered);
                }
            }
            this.baserestService.savefilterdata(JSON.stringify(requestdata)).then(
                (success: any) => {
                    this.getFilters();
                },
                (error) => {
                    this.error(error);
                }
            )
        }
    }
    getFilters() {
        this.baserestService.getmenufilters(this.skema_uuid).then(
            (filters: any) => {
                let newupdatedfilterList = [{
                    titel: 'Intet valgt',
                    uuid: 0
                }];
                newupdatedfilterList = newupdatedfilterList.concat(filters.data);
                this.services.setnewallfilterList(newupdatedfilterList);
                this.Success();

            },
            (error) => { this.loading = false; console.log(error) }
        )
    }
    Success() {
        this.successNotification.refresh();
        this.successNotification.open();
        this.activeModal.dismissAll();
        this.loading = false;
    }
    error(error) {
        console.log(error);
        this.errorNotification.refresh();
        this.errorNotification.open();
        this.loading = false;
    }
    cancel(){
        this.activeModal.dismissAll();
    }

}