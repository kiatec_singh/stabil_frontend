import { Component, Input, Output, EventEmitter, } from '@angular/core';
import { NgbModal,NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Services } from '../../providers/services/services';



declare var $: any;
@Component({
    selector: 'fletfilters-viewer',
    templateUrl: 'fletfilters.html'
})
export class fletfiltersComponent {

     @Input() fletfilters: any;
     @Output() selectedfletfilters= new EventEmitter<string>();
     private filteruuid;
     private rel_uuid;
     private modalReference: NgbModalRef;
    // private loading=false;
    // @ViewChild('successNotification', { static: true }) successNotification: jqxNotificationComponent;
    // @ViewChild('errorNotification', { static: true }) errorNotification: jqxNotificationComponent;
    constructor(public activeModal: NgbModal, private services:Services) {
    }

    ngOnInit() {
        setTimeout(() => {
            $('.selectpicker').selectpicker();
        }, 100);
    }

    cancel(e) {
        this.modalReference = this.services.getmodalReference();
        if(this.modalReference){
            this.modalReference.close();
        }else{
            this.activeModal.dismissAll();
        }
    }
    apply(e){
        console.log(e);
    }
    setFilter(e,item){
        this.selectedfletfilters.emit(this.rel_uuid);
        this.modalReference = this.services.getmodalReference();
        if(this.modalReference){
            this.modalReference.close();
        }else{
            this.activeModal.dismissAll();
        }
    }

}