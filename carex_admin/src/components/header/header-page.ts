import { Component, HostListener, Inject, OnDestroy } from '@angular/core';
import { AuthRestService } from '../../providers/auth.rest.service';
import { UserData } from '../../models/user.model';
import { DOCUMENT } from '@angular/common';
import { StorageService } from '../../providers/storageservice/storageservice';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

declare var window: any;

@Component({
    selector: 'header-page',
    templateUrl: 'header-page.html'
})



export class HeaderPage {
    private user: UserData;
    private version;
    private envi;
    constructor(private authService: AuthRestService, @Inject(DOCUMENT) private document: any, private router: Router, private storageService: StorageService) {
        this.authService.userobj.subscribe(
            (userobj) => {
                if (userobj && userobj.id) {
                    this.user = new UserData(userobj);
                } else {
                    this.user = null;
                }
            });
        this.version = environment.version;
        this.envi = environment.envi;
    }

    ngOnInit() {
        this.user && this.user.uuid ?   this.router.navigateByUrl(''):this.router.navigateByUrl('login');
    }

    home(e) {
        e.preventDefault();
        this.user && this.user.uuid ? this.router.navigateByUrl('') : this.router.navigateByUrl('login');
    }

    logout() {
        // this.router.navigateByUrl('/logout.php');
        // ADD logout service from Openid
        this.storageService.clear();
        this.authService.setuserToken(null);
        this.authService.setuserobj(null);
        this.authService.setmenulist(null);
        this.authService.setuserrelations(null);
        window.location.reload();
        // this.router.navigateByUrl('/');
    }
}