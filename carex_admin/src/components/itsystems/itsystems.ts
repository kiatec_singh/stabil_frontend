import { Component, ViewChild, ChangeDetectorRef, ElementRef } from "@angular/core";
import { BaseRestService } from "../../providers/base.rest.service";
import { Services } from "../../providers/services/services";
import { RowsList,GridData,Columns,DataFields,ObjectType,} from "../../models/grid.data";
import { Localization } from "../../models/localization/localization";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import { jqxNotificationComponent } from "jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification";
import { Location } from "@angular/common";
import { jqxGridComponent } from "jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid";
import { DetailsPage } from "../detailspage/detailspage";
import { searchComponent } from "../search/search";
import { modalComponent } from "../modal/modal";
import { Router, ActivatedRoute } from "@angular/router";
import { UserData } from "../../models/user.model";
import { Form } from "@angular/forms";

import * as moment from "moment";

declare var require: any;
var xlsx = require("json-as-xlsx");
var dateFormat = require("dateformat");
declare var $: any;

declare var window: any;

@Component({
  selector: "itsystems",
  templateUrl: "itsystems.html",
})
export class Itsystems {
  @ViewChild("successNotification", { static: true }) successNotification: jqxNotificationComponent;
  @ViewChild("errorNotification", { static: true }) errorNotification: jqxNotificationComponent;
  @ViewChild("gridReference", { static: false }) itsystemGrid: jqxGridComponent;
  @ViewChild("uploadfileForm", { static: false }) uploadfileForm: Form;
  @ViewChild("inputFile", { static: false }) inputFile;
  @ViewChild("modal", { static: false }) Modal;
  @ViewChild("searchcomponent",{static:false}) searchcomponent: searchComponent;
  // @ViewChild('modal') content: HTMLDivElement;

  private uuid: string;
  private gridData: GridData;
  private actions;
  private rows: RowsList;
  private columnss: Columns;
  private datafileds: DataFields;
  gridrowsandColumns: any;
  private localization: Localization;
  private objektType: ObjectType;
  private source: any;
  private columns = null;
  private dataAdapter;
  private jqxGridComponent: jqxGridComponent;
  private settings: any;
  private localizationObject: any;
  public loading = false;
  public UserInfo: UserData;
  public uploadStatus = false;
  public fileStatus = false;
  private selectedMenu;
  private uploadContainer = false;
  private fileName;
  private uploadedFile;
  public modalReference: any;
  private modalComponent: modalComponent;
  private heading;
  private documentuuid;
  private downloadDetails;
  private errorMessage;
  private filepath;
  private clickedrow;
  private parentheading;
  private displayname;
  private environmentUrl;
  private navigationElements;
  private serviceErrorMessage;
  private slctdfilter;
  private fetchingfilter = false;
  private handlingdata: any;
  private enablenotification = false;
  private displayedrows;
  private state: any;
  private bredcrum;
  private click_action = null;
  private allfilterList = [
    {
      titel: "Intet valgt",
      uuid: 0,
    },
  ];
  private ngbModalOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: true,
  };
  private filterList;
  private filteruuid;
  private gridfilterdata;

  constructor(
    private service: BaseRestService,
    private services: Services,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private ref: ChangeDetectorRef
  ) {
    this.services.slctmenuitem.subscribe((bredcrum) => {
      this.bredcrum = bredcrum;
    });
    this.services.isLoading.subscribe((isLoading) => {
      this.loading = isLoading;
    });
    this.services.newactionsList.subscribe((newactionsList) => {
      this.actions = newactionsList;
      this.enablenotification = true;
      this.handlingdata = this.actions.actions;
    });
  }

  ngOnInit() {
    this.loading = true;
    this.localization = new Localization();
    this.localization = this.localization.localizationobj;
    this.parentheading = this.services.getselectedMenu();
    this.heading = this.activatedRoute.snapshot.data.title;
    this.selectedMenu = this.activatedRoute.snapshot.routeConfig.path;
    this.uuid = this.activatedRoute.snapshot.data.listId;
    this.click_action = this.activatedRoute.snapshot.data.click_action ? this.activatedRoute.snapshot.data.click_action: null;
    this.services.setlistId(this.uuid);
    this.clickedrow = this.services.getClickedRowuuiid();
    this.environmentUrl = this.service.getEnvironment();
    this.services.setselectedMenu(
      this.activatedRoute.snapshot.routeConfig.path
    );
    this.getValueFromObservable();
  }

  onSelect(event: any): void {
    let args = event.args;
    let fileName = args.file;
    let fileSize = args.size;
    var ext = fileName.substr(fileName.lastIndexOf(".") + 1);
    if (ext == "csv" && fileSize < 1000000) {
      this.fileStatus = false;
    } else {
      this.fileStatus = true;
    }
  }

  onfileClick(event) {
    event.preventDefault();
    let initializevalue: any = this.inputFile;
    initializevalue.nativeElement.value = null;
    this.inputFile = initializevalue;
  }
  getValueFromObservable(): void {
    let contextid;
    contextid = this.services.getContext()
      ? this.services.getContext()
      : this.clickedrow;
    this.loading = false;
    if (this.click_action != "getObjektfilter") {
      this.service.getGriddata(this.uuid).then(
        (res) => {
          this.gridrowsandColumns = res;
          this.setdata();
        },
        (error) => {
          this.errorNotification.refresh();
          this.errorNotification.open();
          this.errorMessage = error.status.besked;
          console.log(error);
          this.loading = false;
        }
      );
    }
  }
  //dead code
  breadCrum() {
    this.navigationElements = [this.bredcrum];
  }
  RefreshData(data) {
    this.gridrowsandColumns = data;
    this.setdata();
  }

  setdata(): void {
    // this.actions = this.actions && this.actions.length>0 ?null: this.actions;
    this.breadCrum();
    this.rows = new RowsList({});
    this.columnss = new Columns({});
    this.datafileds = new DataFields({});
    let grid: any = this.gridrowsandColumns;
    if (this.gridrowsandColumns && this.gridrowsandColumns.result.actions) {
      this.services.setnewactionsList(this.gridrowsandColumns.result.actions);
    }
    this.rows = new RowsList(grid.result.objekt_liste);
    this.datafileds = new DataFields(grid.result.datafields);
    this.columnss = new Columns(grid.result.columns);
    this.objektType = new ObjectType(grid.result.hovedobjekt);
    this.generateGrid(this.rows, this.columnss, this.datafileds);
    this.getFilters();
  }
  getFilters() {
    this.service.getmenufilters(this.uuid).then(
      (filters: any) => {
        this.allfilterList.length =1;
        this.allfilterList = this.allfilterList.concat(filters.data);
        this.filterList = this.allfilterList;
        $(".selectpicker").selectpicker("refresh");
        setTimeout(() => {
          if(this.itsystemGrid) {
          $(".jqx-action-button:has(.jqx-icon-calendar)").addClass("itsystems__dateicon");
          this.applyfilters(this.gridfilterdata);
          }
        },50);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  generateGrid(row, columndata, datafields) {
    this.loading = false;
    var datafield_item;
    var formatted_datafields = [];
    for (let d in datafields) {
      let item = datafields[d];
      item.replace(/"/g, "");
      eval("datafield_item=" + item);
      if (datafield_item.type == "date") {
        datafield_item.format = "dd-MM-yyyy HH:mm:ss";
      }
      formatted_datafields.push(datafield_item);
    }
    var formatted_columnsfields = [];
    var columnfield_item;
    this.source = {
      localdata: row,
      datafields: formatted_datafields,
      datatype: "array",
      nullable: true,
      sortcolumn: "levering",
      sortdirection: "desc",
    };
    var dataAdapter = new jqx.dataAdapter(this.source);
    for (let c in columndata) {
      let item = columndata[c];
      item.replace(/"/g, "");
      eval("columnfield_item=" + item);
      formatted_columnsfields.push(columnfield_item);
    }
    this.columns = formatted_columnsfields;
    this.dataAdapter = dataAdapter;
    this.gridfilterdata = this.services.getgridState();
  }
  Filter(event: any): void {
    event.preventDefault();
    if (this.itsystemGrid && this.itsystemGrid.savestate() && this.click_action != "getObjektfilter") {
      this.services.setgridState(this.itsystemGrid.savestate());
      this.services.setfiltersData(this.itsystemGrid.savestate());
    }
  }
  Sort(event: any): void {
    event.preventDefault();
    if (this.itsystemGrid && this.itsystemGrid.savestate() && this.click_action != "getObjektfilter") {
      this.services.setgridState(this.itsystemGrid.savestate());
      this.services.setfiltersData(this.itsystemGrid.savestate());
    }
  }
  Rowclick(event: any): void {
    event.preventDefault();
    let submenu = this.services.gethasSubmenu();
    if (event.args.row.bounddata.uuid) {
      let id = event.args.row.bounddata.uuid;
      this.services.setClickedRowuuiid(id);
      if (submenu) {
        this.services.setContext(id);
      }
      this.modalReference = this.modalService.open(DetailsPage, this.ngbModalOptions);
      this.modalReference.issubmenu = this.activatedRoute.snapshot.data.issubmenu;
      this.modalReference.result.then((data) => {
        // on close
      }, (reason) => {
        // on dismiss
        if(reason && reason!='cancelled'){
         if(this.click_action==='getObjektfilter'){
          this.searchcomponent.Search();
        }
        else{
          this.ngOnInit();
        }
        }

    }); 
  }}
  filtereddata(e) {
    event.preventDefault();
  }
  upload(event: any) {
    event.preventDefault();
    this.uploadContainer = true;
  }
  onColumnreordered(event: any): void {
    event.preventDefault();
    this.services.setgridState(this.itsystemGrid.savestate());
    this.services.setfiltersData(this.itsystemGrid.savestate());
  }
  applyfilters(applyfilters) {
    if (this.itsystemGrid) {
      this.itsystemGrid.loadstate(applyfilters);
      this.itsystemGrid.applyfilters();
    }
  }
  filteredGrid(data) {
    this.gridrowsandColumns = data;
    this.setdata();
    this.services.setnewactionsList(this.gridrowsandColumns.result.actions);
  }
}
