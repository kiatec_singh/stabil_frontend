import { Component, Output, Input, EventEmitter, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseRestService } from '../../providers/base.rest.service';
import { Router } from '@angular/router';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';

@Component({
    selector: 'jsonviewer-viewer',
    templateUrl: 'jsonviewer.html'
})
export class jsonviewerComponent {

    public filtereditorOptions: JsonEditorOptions;
    @Input() filtersdata: any;
    

    private body = {
        "data": "not available"
    }
    constructor(private fb: FormBuilder, private modal: NgbModal, private ref: ChangeDetectorRef, private baserestService: BaseRestService) {
        this.filtereditorOptions = new JsonEditorOptions();
        this.filtereditorOptions.mode = "view";
    
        console.log(this);
    }

    ngOnInit() {
       
        this.body = this.filtersdata? this.filtersdata : this.body;
    }

    cancel(e) {
        this.modal.dismissAll();

    }


}