import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BaseRestService } from '../../providers/base.rest.service';
import { StorageService } from '../../providers/storageservice/storageservice';
import { AuthRestService } from '../../providers/auth.rest.service';


@Component({
    selector: 'login',
    templateUrl: 'login.html',

})

export class Login {

    private username;
    private passwordkey;
    private error=false;
    private loading =false;
    private showpwd=false;
    private userinfo=null;
    private errormessage=null;
    private userToken='';
    private fetching=true;
    @ViewChild('password', { static: false }) password:any;

    constructor(private router:Router,private ref:ChangeDetectorRef,
         private baserestService:BaseRestService,private authService:AuthRestService, private storageService:StorageService) {
        this.fetching = true;
        this.storageService.get('carexadmin').then(
            (userinfo)=>{
              if(userinfo){
                this.userinfo = userinfo;
                this.fetching = true;
                this.validateAuthorisation();
              }
              else{
                this.fetching =false;
                this.router.navigateByUrl('login');
              }
            }
          )
          this.authService.userToken.subscribe(
            (userToken: any) => {
                    this.userToken = userToken;
            }
        );
    }

    ngOnInit(): void {
      if(this.userToken && this.userToken.length!=0){
        this.router.navigateByUrl('');
      }
    }
    login(){
        this.loading = true;
        this.baserestService.login(this.username,this.passwordkey).then(
            userinfo=>{
                this.userinfo=userinfo;
                this.validateAuthorisation();
            },
            error=>{  this.loading = false;this.error=true}
        )
    }
    resetpassword(){
        this.router.navigate(['username'],{ state: { username: this.username  } });
    }

validateAuthorisation(){
    this.baserestService.getAllMenuList(this.userinfo.id).then(
        (menulist) => {
            this.userToken = menulist.userToken;
            this.authService.setuserToken(menulist.userToken);
            this.authService.setmenulist(menulist);
            if (menulist.relationer) {
                this.authService.setuserrelations(menulist.relationer);
            }
            this.storageService.set('carexadmin',this.userinfo);
            this.authService.setuserobj(this.userinfo);
            this.router.navigateByUrl('');
            this.loading=false;
            this.fetching =false;

        },
        (error) => { 
            this.loading=false;
            this.fetching =false;
            this.errormessage= error.error.besked;    
        });
    }

    showpassword(){
        this.showpwd = this.showpwd===true? false:true;
        if(this.showpwd===true){
             this.password.nativeElement.type = "text";
             this.ref.detectChanges();
        } 
        else{
            this.password.nativeElement.type = "password";
            this.ref.detectChanges();
       } 
    }
    reset(){
        this.errormessage= null;
        this.error=false;
    }
}