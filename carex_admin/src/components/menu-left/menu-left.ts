import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Http } from '@angular/http';
import { Menu, MenuListItems } from '../../models/menu.items';
import { BaseRestService } from '../../providers/base.rest.service';
import { UserData } from '../../models/user.model';
import { Router } from '@angular/router';
import { Itsystems } from '../../components/itsystems/itsystems';
import { GanttChart } from '../../components/ganttchart/ganttchart';
import { DetailsPage } from '../../components/detailspage/detailspage';
import { CreatePage } from '../../components/createpage/createpage';
import { Skema } from '../../components/skema/skema';
import { AuthRestService } from '../../providers/auth.rest.service';
import { Services } from '../../providers/services/services';
import { Configuration } from '../configuration/configuration';
import { analyticsComponent } from '../analytics/analytics';
import { uploadsComponent } from '../uploads/uploads';
import { HelperSerice } from '../../providers/utils/helperService';

@Component({
    selector: 'menu-left',
    templateUrl: 'menu-left.html',
    providers: [BaseRestService, Http]
})

export class MenuLeft implements OnInit {

    private menuitems: Menu;
    private userdata: UserData;
    private menulist: MenuListItems[];
    private usermenulist: MenuListItems[];
    private menuItems;
    public submenulist: any[] = [];
    public client = "tryg";
    private mymenuItems;
    private clientMenu;
    private userobj: any = null;
    private buttonsMennu = [];

    constructor(private baserestService: BaseRestService, private auth: AuthRestService,private helperService:HelperSerice, private router: Router, private location: Location, private services: Services) {
        this.auth.userobj.subscribe(
            (userobj) => {
                if (userobj && userobj.id) {
                    this.userobj = userobj;
                }
            })
        this.auth.menulist.subscribe(
            (menulist) => {
                if (menulist && menulist.result) {
                    this.configureRouting(menulist.result);
                }
            })
    }
    ngOnInit() {

    }

    configureRouting(resultmenu) {
        let makemenu: any = [];
        for (var key in resultmenu) {
            if (resultmenu.hasOwnProperty(key)) {
                var val = resultmenu[key];
                makemenu.push(val);
            }
        }
        this.menulist = this.helperService.sortmenuList(makemenu);;
        this.menulist.map(this.getRoutingItem, this);
    }

    getRoutingItem(item) {
        if (item.titel == "Forløb") {
            this.router.config.push({ path: item.brugervendtnoegle, component: GanttChart, data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': true } });
        }
        if (item.click_action != 'list_objekt_data') {
            if (item.click_action === 'configuration') {
                this.router.config.push({ path: item.click_action, component: Configuration, data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            if (item.click_action === 'getuseranalytics') {
                this.router.config.push({ path: item.click_action, component: analyticsComponent, data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            if (item.click_action === 'uploads') {
                this.router.config.push({ path: item.click_action, component: uploadsComponent, data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            if (item.click_action === 'skemaeditor') {
                this.router.config.push({ path: item.click_action, component: Skema, data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            if (item.click_action === 'skemaeditor') {
                this.router.config.push({ path: item.click_action, component: Skema, data: { 'listId': item.skema_uuid, 'title': item.titel, 'issubmenu': false } });
            }
            
        } if (item.click_action === 'list_objekt_data') {
            this.router.config.push({ path: item.brugervendtnoegle, component: Itsystems, data: { 'listId': item.skema_uuid,  'title': item.titel, 'issubmenu': false } });
        }
        if (item.click_action === 'getObjektfilter') {
            this.router.config.push({ path: item.brugervendtnoegle, component: Itsystems, data: { 'listId': item.skema_uuid, 'click_action':item.click_action , 'title': item.titel, 'issubmenu': false } });
        }
        
        this.router.config.push({ path: item.brugervendtnoegle + '/CreatePage', component: CreatePage });
        this.router.config.push({ path: item.brugervendtnoegle + '/:uuid', component: DetailsPage, data: { 'issubmenu': false } });

    }

    getsubmenuroutingItem(item) {
        var usersJson: any[] = Array.of(item.submenu);
        return usersJson;
    }
    active(event: any, item, uuid, name) {
        event.preventDefault();

        this.services.setContext(null);
        this.services.setgridState(null);
        this.services.setfiltersData(null);
        this.services.setselectedmenuTitle(item.funktionsnavn);
        this.services.setslctmenuitem(item.funktionsnavn);
        if (item.submenu) {
            this.services.sethasSubmenu(true);
        }
        if (!item.submenu) {
            this.services.sethasSubmenu(false);
        }
        if (name != "personliste") {
            this.router.navigate(['' + name]);
        }
        if (item.click_action === 'configuration') {
            this.router.navigate(['configuration']);
        }
        if (item.click_action === 'getuseranalytics') {
            this.router.navigate(['getuseranalytics']);
        }
        if (item.click_action === 'translation') {
            this.router.navigate(['translation']);
        }
        if (item.click_action === 'templates') {
            this.router.navigate(['templates']);
        }
        if (item.click_action === 'skemaeditor') {
            this.router.navigate(['skemaeditor']);
        }
        if (item.click_action === 'uploads') {
            this.router.navigate(['uploads']);
        }

    }
    activeapp(event: any, item) {
        this.router.navigate(['' + item]);
    }
    activesubmenu(event: any, item, uuid, name) {
        event.stopPropagation();
    }
    activeConfiguration(name) {
        this.router.navigate(['ConfigureApp']);
    }
}