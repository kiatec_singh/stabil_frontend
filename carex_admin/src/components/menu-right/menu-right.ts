import { Component, ViewChild } from '@angular/core';
import { trigger, state, style, animate, transition, query, stagger } from '@angular/animations';
import { Router } from '@angular/router';
import { AuthRestService } from '../../providers/auth.rest.service';


@Component({
    selector: 'menu-right',
    templateUrl: 'menu-right.html',
    animations: [
        trigger('menuState', [
            state('inactive', style({
                // transform: "translate(100%,0)"
            })),
            state('active', style({
                //   transform: "translate(0,0)"
            })),
            // transition('inactive => active', animate('300ms ease-in')),
            // transition('active => inactive', animate('300ms ease-out'))
        ])
    ]
})

export class MenuRight {
    private items = ["Profile", "Settings", "Import", "Export", "Calender"];
    public state = 'inactive';
    private authorisation;
    private userrelations;
    constructor(private router: Router, private auth: AuthRestService) { 
        this.auth.userrelations.subscribe(
            (userrelations) => {
                {
                    this.userrelations = userrelations;
                }
            }
        );
    }


    showItems() {
        this.items = ["Profile", "Settings", "Import", "Export", "Calender"];
    }

    hideItems() {
        this.items = [];
    }

    toogleMenu(event) {
        event.preventDefault();
        console.log("in left menu");
        this.state = this.state === 'active' ? 'inactive' : 'active';
    }
    toogleMenuOpen(event) {
        event.preventDefault();
        console.log("in left menu");
        this.state = this.state === 'active' ? 'inactive' : 'active';
    }
    getAnalytics() {
        this.router.navigate(['analytics']);
    }
    serviceAnalytics(){
        this.router.navigate(['serviceanalytics']);
    }
    sendpushNotification(e) {
        e.preventDefault();
        this.router.navigate(['sendpushNotification']);
    }
    translation(e) {
        e.preventDefault();
        this.router.navigate(['translation']);
    }
    templates(e) {
        e.preventDefault();
        this.router.navigate(['templates']);
    }
    configuration(e) {
        e.preventDefault();
        this.router.navigate(['configuration']);
    }
    uploads(e) {
        e.preventDefault();
        this.router.navigate(['uploads']);
    }
    sendmessages(e){
        e.preventDefault();
        this.router.navigate(['sendmessages']);
    }
    skemaeditor(e){
        e.preventDefault();
        this.router.navigate(['skemaeditor']); 
    }
    newgrid(e){
        e.preventDefault();
        this.router.navigate(['newgrid']);
    }
}