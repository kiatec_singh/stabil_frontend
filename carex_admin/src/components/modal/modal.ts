


import { Component, OnInit,ElementRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'modal',
    templateUrl: 'modal.html'
  })
  
  export class modalComponent {
    title: string;
    closeBtnName: string;
    list: any[] = [];
    @ViewChild('modal',{static:false}) div;
  
  
    constructor( private modalService: NgbModal) {
    }
  
    ngOnInit() {
        //this.ngbModalRef.result(this.div);
       // this.modalReference = this.modalService.open(this.modalComponent);
        // this.ngbModalRef.open(this.modalComponent);
    //     this.modalService.result.then((result) => {
    //         //this.closeResult = `Closed with: ${result}`;
    //     }, (reason) => {
    //         //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //     });
    //   this.list.push('PROFIT!!!');
    this.modalService.open(this.div);
    }
    openModal(modalComponent){
        console.log(modalComponent);
        this.modalService.open(modalComponent);
    }
  }
  