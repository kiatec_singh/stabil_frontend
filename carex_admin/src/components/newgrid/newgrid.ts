import { Component, ChangeDetectorRef, ViewChild, ElementRef, NgZone } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { Table } from 'primeng/table';

declare var $: any;
declare var document: any;

@Component({
    selector: 'newgrid',
    templateUrl: 'newgrid.html'
})

export class newGridComponent {

private griddata;
private columns=[];
private names=[];
private columnsdata=[];
private representatives=[];
@ViewChild('dt',{static:false}) table: Table;

    constructor(private baserestService:BaseRestService,private auth:AuthRestService) {
    }


    ngOnInit(): void {
        this.representatives = [
            { name: "Amy Elsner", image: "amyelsner.png" },
            { name: "Anna Fali", image: "annafali.png" },
            { name: "Asiya Javayant", image: "asiyajavayant.png" },
            { name: "Bernardo Dominic", image: "bernardodominic.png" },
            { name: "Elwin Sharvill", image: "elwinsharvill.png" },
            { name: "Ioni Bowcher", image: "ionibowcher.png" },
            { name: "Ivan Magalhaes", image: "ivanmagalhaes.png" },
            { name: "Onyama Limba", image: "onyamalimba.png" },
            { name: "Stephen Shaw", image: "stephenshaw.png" },
            { name: "XuXue Feng", image: "xuxuefeng.png" }
          ];

        this.baserestService.getGriddata('9eacc9e4-4359-48e8-9e17-8ce331b7ad59').then(
            res => { this.griddata = res;
            console.log(res); 
            this.setData()
        },
            error => {
               console.log(error)
            }
        );
       
    }

    // "{text: 'Levering', datafield: 'levering', filtertype: 'range', nullable: 'true', width:'10%', cellsformat: 'dd-MM-yyyy'}"


    setData(){
        
        this.griddata.result.columns.map(
        function (item) {
           let columnitem =  eval('item='+item)
            this.columns.push(
                 { text: item.text, datafield: item.datafield }
                );
        },this);


        for(let i in  this.griddata.result.objekt_liste){
            this.columnsdata.push(this.griddata.result.objekt_liste[i]);
            this.names.push({
                name:this.griddata.result.objekt_liste[i].navn}
                )
        }


console.log(this.columns);
console.log(this.columnsdata);
console.log(this.names);

    }
    onDateSelect(value) {
        this.table.filter(this.formatDate(value), 'date', 'equals')
    }
    formatDate(date) {
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (month < 10) {
            month = '0' + month;
        }

        if (day < 10) {
            day = '0' + day;
        }

        return date.getFullYear() + '-' + month + '-' + day;
    }
    onRepresentativeChange(event) {
        this.table.filter(event.value, "representative", "in");
      }
}