import {Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core'

@Component({
    selector: 'notifications-viewer',
    templateUrl: 'notifications.html'
})

export class Notifications implements OnChanges{

   @Input() message: any;
   @Input() warning: any;
   @Input() error: any;
    constructor() {
       console.log(this);

    }
    ngOnInit() {
console.log(this.message);
      }
     ngAfterViewInit(): void {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        console.log(this);
        setTimeout(() => {
           this.success=false;
        }, 3000);
     }

     ngOnChanges(changes: any): void {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        console.log(changes);
        console.log(this);
        setTimeout(() => {
           this.success=false;
        }, 3000);
        
     }
     @Input() success:any;
     verify(){
        console.log(this);
     }

}