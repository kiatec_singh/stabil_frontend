import { Component, Output, Input, EventEmitter, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseRestService } from '../../providers/base.rest.service';
import { Router } from '@angular/router';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { DateTimeAdapter } from 'ng-pick-datetime';
import * as moment from 'moment';
import * as  momentime from 'moment-timezone';
import { concat } from 'rxjs';
declare var $: any;
@Component({
    selector: 'notifyviewer-viewer',
    templateUrl: 'notifyviewer.html'
})
export class notifyviewerComponent {

    @Input() displayedrows: any;
    private body: any;
    private selecteddate;
    private selecteddatenow = '';
    private handlingaction;
    private mindate;
    private selectedtime;
    private startat;
    @Input() selectedItem: any;
    @Input() selectedmenuItem: any;
    @ViewChild('successNotification', { static: true }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: true }) errorNotification: jqxNotificationComponent;


    constructor(public activeModal: NgbModal, private baserestService: BaseRestService,private dateTimeAdapter: DateTimeAdapter<any>) {
       this.dateTimeAdapter.setLocale('da-DK');
       this.createstdate();
    }

    ngOnInit() {
    }

    cancel(e) {
        this.activeModal.dismissAll();
    }
    clearDate(item) {
        console.log(item);
    }
    DateChanged(selecteddate){
        if(selecteddate && selecteddate._d){
            this.selectedtime = moment(selecteddate._d).format('YYYY-MM-DD HH:mm:ss');
       }
    }
    saveFilter(e) {
        console.log(e);
        let selectedUuids: any = [];
        this.displayedrows.map(function (item) {
            if (item.uuid) {
                selectedUuids.push(item.uuid);
            }
        });
        this.baserestService.notificationBatch(this.selectedItem.key, JSON.stringify(selectedUuids), this.selectedtime).then(
            (success) => {
                this.successNotification.refresh();
                this.successNotification.open();
                this.activeModal.dismissAll();
            },
            (error) => {
                this.errorNotification.refresh();
                this.errorNotification.open();
                this.activeModal.dismissAll();
            }
        )
    }
    createstdate(){
        this.startat = new Date().toLocaleString('dk-DA', { timeZone: 'Europe/Copenhagen' });
        let dd;
        let mm;
        let yyyy;
       if(this.startat.indexOf('/')!=-1){
         dd = this.startat.split("/")[0];
        mm =  this.startat.split("/")[1];
        yyyy =  this.startat.split("/")[2].split(",")[0];  
       }
       else{
        dd = this.startat.split(".")[0];
        mm =  this.startat.split(".")[1];
        yyyy =  this.startat.split(".")[2].split(" ")[0];  
       }

        let time = this.startat.split(' ')[1];
        let hh =  time.split(':')[0];
        let mi =   time.split(':')[1];
        let secs =  time.split(':')[2];
       this.mindate =  new Date(yyyy, mm-1, dd,hh,mi);
    }
}