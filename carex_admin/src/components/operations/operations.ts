import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Services } from '../../providers/services/services';
import { environment } from '../../environments/environment';
import { ModalDismissReasons, NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { CreatePage } from '../createpage/createpage';


declare var require: any;
var xlsx = require('json-as-xlsx');
declare var $: any;

@Component({
  selector: 'operations-viewer',
  templateUrl: 'operations.html'
})
export class OperationsComponent {

  @Input() sharedthis: any;
  @Input() list: any;
  @Input() details: any;
  @Input() filterList: any;
  @Input() objekt_uuid: any;
  @Input() actions: any;
  @Output() newlistdata = new EventEmitter<string>();
  @Output() filtereddata = new EventEmitter<string>();
  @ViewChild('successnotification', { static: false }) successnotification: jqxNotificationComponent;
  @ViewChild('errornotification', { static: false }) errornotification: jqxNotificationComponent;
  private filtersdata;
  private operations;
  private action;
  private filteruuid: any;
  private alloperations = [];
  private buttons = [];
  private Handlinger = [];
  private Udsend = [];
  private notificationoperation = {
    "actions": {
      "list": {
        "buttons": [
          {
            "name": "Gem filter"
          },
          {
            "name": "Opret"
          },
          {
            "name": "Import"
          },
          {
            "name": "Export"
          }
        ]
      },
      "detail": {
        "buttons": [
          {
            "name": "Annuller"
          }
        ]
      }
    }
  };
  private defaultoperation = {
    "actions": {
      "list": {
        "buttons": [
          {
            "name": "Gem filter"
          },
          {
            "name": "Opret"
          },
          {
            "name": "Import"
          },
          {
            "name": "Export"
          }
        ]
      },
      "detail": {
        "buttons": [
          {
            "name": "Gem"
          },
          {
            "name": "Annuller"
          },
          {
            "name": "Slet"
          }
        ]
      }
    }
  };//remove in future
  private operationbuttons = [];
  private selectedItem;
  private ngbModalOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  private displayedrows;
  constructor(private services: Services,    private modalService: NgbModal) {
    this.services.newactionsList.subscribe(
      (newactionslist) => {
        this.actions = newactionslist;
        this.Handlinger = this.actions && this.actions.actions && this.actions.actions.Handlinger ? this.actions.actions.Handlinger : [];
        this.buttons = this.actions && this.actions.buttons ? this.actions.buttons : [];
        this.Udsend = this.actions && this.actions.actions && this.actions.actions.Udsend ? this.actions.actions.Udsend : [];
      });
    this.services.newallfilterList.subscribe(
      (newallfilterList) => {
        this.actions = newallfilterList;
        if (this.sharedthis) {
          this.services.setisLoading(true);
          this.sharedthis.allfilterList = newallfilterList;
          this.sharedthis.filterList = newallfilterList;
        }
        setTimeout(() => {
          $('.selectpicker').selectpicker('refresh');
          this.services.setisLoading(false);
        }, 20);
      });
  }
  ngOnInit() {
    let alloprts = []
    this.Handlinger = this.actions && this.actions.actions && this.actions.actions.Handlinger ? this.actions.actions.Handlinger : [];
    this.buttons = this.actions && this.actions.buttons ? this.actions.buttons : [];
    this.Udsend = this.actions && this.actions.actions && this.actions.actions.Udsend ? this.actions.actions.Udsend : [];
  }
  ngOnDestroy(): void {
    this.buttons.length = 0;
    this.Handlinger.length = 0;
    this.Udsend.length = 0;
  }
  ngAfterViewInit(): void {
  }
  download(event: any): void {
    event.preventDefault();
    this.services.setisLoading(true);
    let checkrows: any = this.sharedthis.itsystemGrid.getdisplayrows();
    let allrows = [];
    for (let i in checkrows) {
      allrows.push(checkrows[i]);
    }
    if (event.target.id == "xlsx") {
      var excelcolumns = [];
      this.sharedthis.columns.map(function (item) {
        excelcolumns.push({ label: item.text, value: item.datafield });
      });
      var settings = {
        sheetName: this.sharedthis.selectedMenu,
        fileName: this.sharedthis.selectedMenu
      }
      if (checkrows.length === 0) {
        allrows.push(['']);
      }
      xlsx(excelcolumns, allrows, settings);
    }
    else {
      this.sharedthis.itsystemGrid.exportdata(event.target.id, '' + this.sharedthis.selectedMenu, true, allrows, false, environment.saveUrl, 'utf-8');
    }
    this.sharedthis.successNotification.refresh();
    this.sharedthis.successNotification.open();
    this.services.setisLoading(false);
  }
  CreatePage(event: any): void {
    event.preventDefault();
    this.sharedthis.modalReference = this.sharedthis.modalService.open(CreatePage, this.ngbModalOptions);
    // this.sharedthis.router.navigate(['' + this.sharedthis.selectedMenu + '/CreatePage']);
  }
  open(modal, event) {
    this.sharedthis.ref.detectChanges();
    event.preventDefault();
    if(this.sharedthis.click_action != "getObjektfilter"){
      this.sharedthis.filtersdata = this.services.getgridState();
      this.filtersdata = this.services.getgridState();
    }
    this.sharedthis.modalReference = this.sharedthis.modalService.open(modal, this.ngbModalOptions);
    this.sharedthis.modalReference.result.then((result) => {
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  openNotification(modal, event, item) {
    this.selectedItem = item;
    this.displayedrows = this.sharedthis.itsystemGrid?this.sharedthis.itsystemGrid.getdisplayrows():null;
    this.open(modal, event);
  }
  onfileSubmit(content, event: any) {
    let filetype;
    if (this.sharedthis.fileName && this.sharedthis.fileName.type) {
      filetype = this.sharedthis.fileName.type;
    }
    else {
      if (this.sharedthis.fileName && this.sharedthis.fileName[0] && this.sharedthis.fileName[0].type) {
        filetype = this.sharedthis.fileName[0].type;
      }
      else {
        filetype = this.sharedthis.fileName.name.split('.').pop();
      }
    }

    let uplaodedfile = this.sharedthis.fileName[0] ? this.sharedthis.fileName[0] : this.sharedthis.fileName;
    let filename = this.sharedthis.fileName.name;
    if (filetype == "text/csv" || filetype == "csv" || filetype == "application/pdf"
      || filetype == "application/json" || filetype == "json" || filetype == "xlsx" || filetype == "application/vnd.ms-excel"
      || filetype == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
      this.services.setisLoading(true);
      this.sharedthis.service.uploadcsvFile(uplaodedfile, filename, this.sharedthis.uuid).then(
        res => {
          if (res && res.response && res.response.success) {
            this.sharedthis.serviceErrorMessage = res.response.success;
            this.sharedthis.successNotification.refresh();
            this.sharedthis.successNotification.open();
            this.sharedthis.uploadStatus = false;
            this.sharedthis.fileName = null;
            this.sharedthis.uploadContainer = false;
            this.services.setisLoading(false);
            this.sharedthis.modalReference.close();
          }
          else {
            $('.errormessage').text(res.status.besked);
            this.sharedthis.serviceErrorMessage = res && res.status && res.status.besked ? res.status.besked : "Error";
            this.sharedthis.errorNotification.refresh();
            this.sharedthis.errorNotification.open();
            this.sharedthis.uploadStatus = false;
            this.sharedthis.fileName = null;
            this.sharedthis.uploadContainer = false;
            this.services.setisLoading(false);
            this.sharedthis.modalReference.close();
          }

        },
        error => {
          this.sharedthis.errorNotification.refresh();
          this.sharedthis.errorNotification.open();
          this.sharedthis.uploadStatus = false;
          this.sharedthis.fileName = null;
          this.sharedthis.uploadContainer = false;
          this.services.setisLoading(false);
        }
      );
    }
    else {
      this.sharedthis.fileStatus = true;
      this.sharedthis.uploadContainer = false;
      this.sharedthis.fileName = null;
      this.sharedthis.uploadContainer = false;
      this.services.setisLoading(false);
      setTimeout(() => {
        this.sharedthis.fileStatus = false;
      }, 4000);
    }

  }
  fileuploadCancel(event) {
    event.preventDefault();
    this.sharedthis.uploadContainer = false;
    let uploadFile: any = this.sharedthis.uploadfileForm;
    this.sharedthis.fileName = null;
    this.sharedthis.uploadContainer = false;

  }
  Cancel(event): void {
    event.preventDefault();
    this.sharedthis.location.back();
  }
  Delete(event) {
    event.preventDefault();
    if (confirm("Are you sure you want to delete this entry of ID::" + this.sharedthis.uuid) == true) {
      this.sharedthis.service.deleteDetailsdata(this.sharedthis.listId, this.sharedthis.uuid, this.sharedthis.lastupdatedTimeStamp).then(
        res => {
          this.sharedthis.location.back();
          this.successnotification.open(); this.sharedthis.loading = false;
        },
        error => { this.errornotification.open(); this.sharedthis.errorMessage = error.status.besked;; this.sharedthis.loading = false; },
        complete => { this.sharedthis.loading = false; }
      );
    } else {
      console.log("You pressed Cancel!");
    }

  }
  saveData(event) {
    event.preventDefault();
    if (this.sharedthis.acc && this.sharedthis.acc._results) {
      this.sharedthis.acc && this.sharedthis.acc._results.map(function (item) {
        this.sharedthis.activeIds.push(item.panels._results[0].id);
      }, this)
    }
    if (!this.sharedthis.detailForm.nativeElement.checkValidity()) {
      this.sharedthis.detailForm.nativeElement.classList.add('was-validated');
    }
    let formvalid = $('.invalid-feedback').is(':visible');
    if (!formvalid) {
      var formvalues = JSON.stringify(this.sharedthis.detailsForm.controls.items.value);
      let formdata: any = [];
      formdata.push(this.sharedthis.detailsForm.controls.items.value.map(this.sharedthis.formatData, this));
      let submitting = [];
      formdata[0].map(function (e) {
        if (e && e.ld_brugervendtnoegle) {
          submitting.push(e)
        } else {
          e.map(function (el) {
            if (el && el.ld_brugervendtnoegle) {
              submitting.push(el)
            }
          });
        }
      });
      this.sharedthis.loading = true;
      this.sharedthis.service.updateDetailsdata(this.sharedthis.listId, this.objekt_uuid, this.sharedthis.detailsData.status.update_timestamp, JSON.stringify(submitting), this.sharedthis.AllFiles).then(
        res => {
          this.sharedthis.loading = false;
          this.sharedthis.location.back();
          this.successnotification.open();
        },
        error => {
          this.sharedthis.loading = false;
          this.errornotification.open();
          let error_messsage = error.error.status.besked;
          this.sharedthis.errorMessage = error_messsage;
        },
        complete => { this.sharedthis.loading = false; }
      );
    }

  }
  showfilterData(modal, event) {
    this.sharedthis.ref.detectChanges();
    event.preventDefault();
    this.sharedthis.ref.detectChanges();
    this.sharedthis.modalReference = this.sharedthis.modalService.open(modal, this.ngbModalOptions);
    this.sharedthis.modalReference.result.then((result) => {
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  getFilterData(e, item) {
    this.sharedthis.fetchingfilter = true;
    if (this.sharedthis.filteruuid != 0) {
      if(this.sharedthis.click_action==='getObjektfilter'){
        this.sharedthis.service.readsearchData(this.sharedthis.uuid,this.filteruuid).then(
          (res: any) => {
            // this.filtersdata = filtersdata;
            // this.filtereddata.emit(filtersdata);
            // $('.selectpicker').selectpicker('refresh');
            // this.sharedthis.fetchingfilter = false;
            this.sharedthis.fetchingfilter = false;
            this.sharedthis.gridrowsandColumns = res;
            this.newlistdata.emit(res);
          },
          (error) => { 
            console.log(error);
            this.sharedthis.fetchingfilter = false;
           }
        ) 
      }
      else{
          this.sharedthis.service.readfilterdata(this.filteruuid).then(
        (filtersdata: any) => {
          this.filtersdata = filtersdata;
          // this.sharedthis.applyfilters(filtersdata);
          this.filtereddata.emit(filtersdata);
          $('.selectpicker').selectpicker('refresh');
          this.sharedthis.fetchingfilter = false;
        },
        (error) => { console.log(error) }
      )    
      }

    }

    else {
      this.sharedthis.itsystemGrid.clearfilters();
      this.filtersdata = {
        "nofilters": "Data does not contains filters"
      };
      this.sharedthis.fetchingfilter = false;
    }

  }
  onfileChange(event) {
    event.preventDefault();
    this.sharedthis.uploadContainer = true;
    this.sharedthis.fileName = event.target.files[0];

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  openSendEmail(modal, event, item) {
    event.preventDefault();
    this.action = item;
    this.sharedthis.filtersdata = this.services.getgridState();
    this.filtersdata = this.services.getgridState();
    this.sharedthis.displayedrows = this.sharedthis.itsystemGrid?this.sharedthis.itsystemGrid.getdisplayrows():null;
    this.sharedthis.modalReference = this.sharedthis.modalService.open(modal, event, this.ngbModalOptions);
  }
  getValueFromObservable(): void {
    let contextid;
    this.sharedthis.loading = true;
    contextid = this.sharedthis.services.getContext() ? this.sharedthis.services.getContext() : this.sharedthis.clickedrow;
    this.sharedthis.service.getGriddata(this.sharedthis.uuid).then(
      res => {
        this.sharedthis.gridrowsandColumns = res;
        this.newlistdata.emit(res);
      },
      error => {
        this.sharedthis.errorNotification.refresh(); this.sharedthis.errorNotification.open(); this.sharedthis.errorMessage = error.status.besked; console.log(error); this.sharedthis.loading = false;
      }
    );
  }

}


