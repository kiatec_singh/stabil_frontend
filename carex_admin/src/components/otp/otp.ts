import { Component, ViewChild, Input, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { Router } from '@angular/router';


@Component({
    selector: 'otp-viewer',
    templateUrl: 'otp.html'
})
export class OtpComponent {


    private loginForm: FormGroup;
    private userid;
    private otpval1;
    private otpval2;
    private otpval3;
    private otpval4;
    @ViewChild("otp1", { static: false }) otp1_input:ElementRef;
    @ViewChild("otp2", { static: false }) otp2_input:ElementRef;
    @ViewChild("otp3", { static: false }) otp3_input:ElementRef;
    @ViewChild("otp4", { static: false }) otp4_input:ElementRef;
    private username;
    private error = false;
    private loading = false;
    private otpcontent;
    private selectedlanguage;
    private errormessage;


    constructor(private fb: FormBuilder, private authService: AuthRestService, private baserestService: BaseRestService, private router: Router) {
        this.loginForm = this.fb.group({
            otp1: ['', Validators.required],
            otp2: ['', Validators.required],
            otp3: ['', Validators.required],
            otp4: ['', Validators.required]
        });
        this.username = this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.username ? this.router.getCurrentNavigation().extras.state.username : '';
    }

    ngOnInit() {
        console.log("on init");
    }

    gotochangePasword() {
        this.loading = true;
        this.otpval1 = this.loginForm.value.otp1;
        this.otpval2 = this.loginForm.value.otp2;
        this.otpval3 = this.loginForm.value.otp3;
        this.otpval4 = this.loginForm.value.otp4;
        let otp = '' + this.otpval1 + this.otpval2 + this.otpval3 + this.otpval4;
        if (this.loginForm.valid) {
            this.baserestService.verifyOTP(this.username, otp).then(
                (userid) => {
                    this.error = false;
                    this.userid = userid;
                    this.setuserData();
                },
                error => {
                    this.error = true;
                    this.otp1_input.nativeElement.focus();
                    this.otpval1 = null; this.loginForm.value.otp1 = null;
                    this.otpval2 = null; this.loginForm.value.otp2 = null;
                    this.otpval3 = null; this.loginForm.value.otp3 = null;
                    this.otpval4 = null; this.loginForm.value.otp4 = null;
                    this.loading = false;
                    this.errormessage = error.error;
                }
            ), this
        }

    }
    next1(event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            //this.removeFocus(event.currentTarget.id);
            this.otpval1 = null; this.loginForm.value.otp1 = null;
        }
        else {
            this.otpval1 = event.key;
            this.otp2_input.nativeElement.focus();
        }

    }
    next2(event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval1 = null; this.loginForm.value.otp1 = null;
            this.otp1_input.nativeElement.focus();
        }
        else {
                this.otpval2 = event.key;
            this.otp3_input.nativeElement.focus();
        }

    }
    next3(event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval2 = null; this.loginForm.value.otp2 = null;
            this.otp2_input.nativeElement.focus();
        }
        else {
                this.otpval3 = event.key;
            this.otp4_input.nativeElement.focus();
        }

    }
    next4(event) {
        this.error = false;
        if (event.keycode == 46 || event.key == "Backspace") {
            this.otpval3 = null; this.loginForm.value.otp3 = null;
            this.otp3_input.nativeElement.focus();
        }
        else {
                this.otpval4 = event.key;
            if (this.otpval1 && this.otpval2 && this.otpval3 && this.otpval4) {
                this.gotochangePasword();
            }

        }

    }
    setuserData() {
        this.loading = false;
        this.router.navigate(['changepassword'],{ state: { userid: this.userid  } });
    }

    showKeyboard() {
        this.otp1_input.nativeElement.focus();
        this.otpval1 = null; this.loginForm.value.otp1 = null;
        this.otpval2 = null; this.loginForm.value.otp2 = null;
        this.otpval3 = null; this.loginForm.value.otp3 = null;
        this.otpval4 = null; this.loginForm.value.otp4 = null;

    }
}