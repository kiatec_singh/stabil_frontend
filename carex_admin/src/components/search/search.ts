import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from "@angular/core";
import { BaseRestService } from "../../providers/base.rest.service";
import { JsonEditorComponent, JsonEditorOptions } from "ang-jsoneditor";
import { Services } from "../../providers/services/services";
import { jqxNotificationComponent } from "jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification";
import { FormArray, FormBuilder, FormGroup } from "@angular/forms";
import * as moment from "moment";

declare var $: any;

@Component({
  selector: "search-viewer",
  templateUrl: "search.html",
})
export class searchComponent {
  public filtereditorOptions: JsonEditorOptions;
  public loading = false;
  public searchData: any = null;
  public searchresultData: any = null;
  public searchForm;
  public searchform: any = null;
  public items;
  public uuid = null;
  public buttons: any;
  public filteruuid = null;
  public gridrowsandColumns = null;
  public activeIds = "search_panel";
  private allfilters = [];
  private allfilterList = [
    {
      titel: "Intet valgt",
      uuid: 0,
    },
  ];

  @Input() sharedthis: any;
  private filterList = [];
  @Input() actions: any;
  @Output() filteredGriddata = new EventEmitter<string>();
  @Output() updateOperations = new EventEmitter<string>();

  @ViewChild("successNotification", { static: true })
  successNotification: jqxNotificationComponent;
  @ViewChild("errorNotification", { static: true })
  errorNotification: jqxNotificationComponent;

  constructor(
    private baserestService: BaseRestService,
    private ref: ChangeDetectorRef,
    private services: Services,
    private formBuilder: FormBuilder
  ) {
    this.buttons =
      this.actions && this.actions.buttons ? this.actions.buttons : [];

    this.services.newallfilterList.subscribe((newallfilterList) => {
      this.filterList = newallfilterList;
      setTimeout(() => {
        $(".selectpicker").selectpicker("refresh");
      }, 100);
    });
  }

  ngOnInit() {
    this.loading = true;
    this.uuid = this.services.getlistId();
    this.baserestService.getsearch(this.uuid).then(
      (result: any) => {
        this.searchresultData = result.result;
        this.services.setnewactionsList(result.result.actions);
        this.setData();
      },
      (error) => {
        console.log(error);
      }
    );
    this.getFilters();
  }
  setData() {
    if (this.searchresultData && this.searchresultData.soegemuligheder) {
      this.searchData = this.searchresultData.soegemuligheder;
    }
    this.searchForm = this.formBuilder.group({
      items: this.formBuilder.array([]),
    });
    this.addItem();
  }
  addItem(): void {
    this.items = this.searchForm.get("items") as FormArray;
    for (let i in this.searchData) {
      this.items.push(this.createDyanicForm(this.searchData[i]));
    }
    this.searchform = this.searchForm;
    this.loading = false;
    setTimeout(() => {
      $(".selectpicker").selectpicker();
    }, 100);
  }
  resetSearch() {
    this.setData();
  }
  panelExpanded(event) {
    event.preventDefault();
    this.activeIds = this.activeIds == event.panelId ? "" : event.panelId;
    setTimeout(() => {
      $(".selectpicker").selectpicker();
    }, 500);
  }
  clearDate(item) {
    item.value.value = null;
    item.value.formatdate = null;
    let slcitem = item.value.ld_brugervendtnoegle;
    this.searchForm.controls.items.value.filter(
      (x) => x.ld_brugervendtnoegle == slcitem
    )[0] = item;
  }
  createDyanicForm(item): FormGroup {
    var tooltipinfo = item.title;
    if (
      item.type == "input" &&
      item.ld_brugervendtnoegle != "adresser" &&
      item.type != "file" &&
      item.type != "date"
    ) {
      return this.formBuilder.group({
        name: item.titel,
        value: item.value,
        type: item.type,
        property: item.property,
        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
        filtercondition: item.filtercondition,
        filtercondition1: item.filtercondition1,
        filtervalue0: item.filtervalue0,
        filtervalue1: item.filtervalue1,
        filtertype: item.filtertype,
      });
    }
    if (item.type == "datepicker") {
      return this.formBuilder.group({
        name: item.titel,
        value: item.value,
        type: item.type,
        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
        requestParameter: item.requestParameter,
        property: item.property,
        filtercondition0: item.filtercondition0,
        filtercondition1: item.filtercondition1,
        filtervalue: item.filtervalue,
        filtertype: item.filtertype,
        frastartdate: item.value,
        fraenddate: item.value,
        issame: false,
      });
    }
    if (item.type == "button") {
      return this.formBuilder.group({
        name: item.titel,
        value: item.value,
        type: item.type,
        property: item.property,
        requestParameter: item.requestParameter,
        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
      });
    }

    if (item.type == "dropdown" && item.multiselect === true) {
      let selected_name;
      let selected_names = [];
      return this.formBuilder.group({
        name: item.titel,
        value: [item.valuelist],
        options: [item.valuelist],
        multiselect: item.multiselect,
        type: item.type,
        selected_name: [selected_names],
        property: item.property,
        requestParameter: item.requestParameter,
        ld_brugervendtnoegle: item.ld_brugervendtnoegle,
        valuelist: item.valuelist,
        filtercondition: item.filtercondition,
        filtervalue: item.filtervalue,
        filtertype: item.filtertype,
      });
    }
  }

  dateChange(item, index) {
    let dateformat = null;
    let frastartdate = null;
    let fraenddate = null;
    if (item.value.formatdate[0] && item.value.formatdate[0]._d) {
      frastartdate = moment(item.value.formatdate[0]._d).format(
        "DD-MM-YYYY HH:mm"
      );
    }
    if (item.value.formatdate[1] && item.value.formatdate[1]._d) {
      fraenddate = moment(item.value.formatdate[1]._d).format(
        "DD-MM-YYYY HH:mm"
      );
    }
    if (
      moment(frastartdate, "DD-MM-YYYY").isSame(
        moment(fraenddate, "DD-MM-YYYY"),
        "day"
      )
    ) {
      frastartdate =
        moment(item.value.formatdate[0]._d).format("DD-MM-YYYY") + " 00:00";
      fraenddate =
        moment(item.value.formatdate[1]._d).format("DD-MM-YYYY") + " 23:59";
      item.value.formatdate[0] = moment(frastartdate, "DD-MM-YYYY HH:mm");
      item.value.formatdate[1] = moment(fraenddate, "DD-MM-YYYY HH:mm");
      item.value.issame = true;
    }
    item.value.frastartdate = frastartdate;
    item.value.fraenddate = fraenddate;
    this.searchForm.controls.items.value.filter(
      (x) => x.ld_brugervendtnoegle == item.value.ld_brugervendtnoegle
    )[0] = item;
    this.ref.detectChanges();
  }
  Multiselect(event, itemname): void {
    let filtereditem = this.searchForm.controls.items.value.filter(
      (x) => x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle
    )[0];
    for (let j in itemname.value.options) {
      if (
        itemname.value.selected_name.indexOf(
          itemname.value.options[j].rel_uri
        ) != -1
      ) {
        filtereditem.options[j].selected = true;
      } else {
        filtereditem.options[j].selected = false;
      }
    }
    this.searchForm.controls.items.value.filter(
      (x) => x.ld_brugervendtnoegle == itemname.value.ld_brugervendtnoegle
    )[0] = filtereditem;
  }
  Search() {
    this.allfilters.length = 0;
    for (let i in this.searchForm.controls.items.value) {
      if (
        this.searchForm.controls.items.value[i].type == "input" &&
        this.searchForm.controls.items.value[i].property != "hidden" &&
        this.searchForm.controls.items.value[i].value
      ) {
        this.allfilters.push(this.searchForm.controls.items.value[i]);
      }
      if (
        this.searchForm.controls.items.value[i].type == "dropdown" &&
        this.searchForm.controls.items.value[i].selected_name.length != 0
      ) {
        this.allfilters.push(this.searchForm.controls.items.value[i]);
      }
      if (
        this.searchForm.controls.items.value[i].type == "datepicker" &&
        this.searchForm.controls.items.value[i].formatdate
      ) {
        this.allfilters.push(this.searchForm.controls.items.value[i]);
      }
    }
    if (this.allfilters && this.allfilters.length > 0) {
      this.loading = true;
      this.getSearchData();
    } else if (this.allfilters && this.allfilters.length == 0) {
      this.loading = true;
      this.getGriddata();
    }
  }
  getSearchData() {
    let formdata: any = {};
    let filteritem: any = {};
    // let filterscount=null;
    let operatypetye = 0;
    let filtercount: any = null;
    let totalfiltercount = 0;
    let fltercountdropdown: any = null;
    let fltercountdate: any = null;
    let filterscountdate = null;
    let selectedels = [];
    for (let i in this.allfilters) {
      if (
        this.allfilters[i].type == "input" &&
        this.allfilters[i].property != "hidden" &&
        this.allfilters[i].value
      ) {
        let itemname = this.allfilters[i].ld_brugervendtnoegle + "operator";
        if (filtercount === null) {
          filtercount = 0;
        } else {
          filtercount++;
        }
        //filtercount = filtercount?filtercount:0;
        filteritem[itemname] = "and";
        let filteroperatorindex = "filteroperator" + filtercount;
        let filtervalue = "filtervalue" + filtercount;
        let filtercondition = "filtercondition" + filtercount;
        let filteroperator = "filteroperator" + filtercount;
        let filterdatafield = "filterdatafield" + filtercount;
        let filtertype = "filtertype" + filtercount;
        totalfiltercount += 1;
        filteritem["filterscount"] = totalfiltercount;
        filteritem[filtervalue] = this.allfilters[i].value;
        filteritem[filtercondition] = this.allfilters[i].filtercondition;
        filteritem[filteroperatorindex] = operatypetye;
        filteritem[filterdatafield] = this.allfilters[i].ld_brugervendtnoegle;
        filteritem[filtertype] = this.allfilters[i].filtertype;
        // selectedels.push(filteritem)
      }
    }
    for (let i in this.allfilters) {
      if (
        this.allfilters[i].type == "dropdown" &&
        this.allfilters[i].selected_name.length != 0
      ) {
        let itemname = this.allfilters[i].ld_brugervendtnoegle + "operator";
        let selectedelscount = 0;
        if (!selectedels.length) {
          selectedelscount = 0;
        } else {
          selectedelscount = selectedels.length;
        }
        filteritem[itemname] = "and";

        if (fltercountdropdown === null) {
          fltercountdropdown = 0;
        } else {
          fltercountdropdown++;
        }
        for (let j in this.allfilters[i].selected_name) {
          selectedelscount = selectedels.length + 1;
          totalfiltercount += 1;
          if (filtercount === null) {
            filtercount = 0;
          } else {
            filtercount += 1;
          }
          filteritem["filterscount"] = totalfiltercount;
          if (selectedels.length == 0) {
            selectedelscount = 0;
          } else {
            selectedelscount = selectedels.length;
          }
          let filteroperatorindex = "filteroperator" + filtercount; //+(+i+1);//
          let filtervalue = "filtervalue" + filtercount; // +(+i+1);
          let filtercondition = "filtercondition" + filtercount; //+j;// +selectedelscount;
          let filteroperator = "filteroperator" + filtercount; //+j;//+selectedelscount;
          let filterdatafield = "filterdatafield" + filtercount; //+j;//+selectedelscount;
          let filtertype = "filtertype" + filtercount; //+j;// +selectedelscount;
          filteritem[filtervalue] = this.allfilters[i].selected_name[j];
          selectedels.push(this.allfilters[i].selected_name[j]);
          filteritem[filtercondition] = this.allfilters[i].filtercondition;
          filteritem[filteroperatorindex] = this.allfilters[i].filtervalue;
          filteritem[filterdatafield] = this.allfilters[i].ld_brugervendtnoegle;
          filteritem[filtertype] = this.allfilters[i].filtertype;
        }
      }
    }
    for (let i in this.allfilters) {
      if (
        this.allfilters[i].type == "datepicker" &&
        this.allfilters[i].formatdate
      ) {
        let itemindex = "filtertype" + i;
        let filteroperatorindex = "filteroperator" + i;
        if (filterscountdate === null) {
          filterscountdate = 0;
        } else {
          filterscountdate++;
        }
        // let filteroperator = 'filteroperator' + i;
        let filterdatafield;
        if (
          this.allfilters[i].formatdate[0] ||
          this.allfilters[i].formatdate[1]
        ) {
          let itemname = this.allfilters[i].ld_brugervendtnoegle + "operator";
          filteritem[itemname] = "and";
        }
        if (this.allfilters[i].formatdate[0]) {
          if (filtercount === null) {
            filtercount = 0;
          } else {
            filtercount += 1;
          }
          if (fltercountdate === null) {
            fltercountdate = 0;
          } else {
            fltercountdate++;
          }
          totalfiltercount += 1;
          let filteroperatorindex = "filteroperator" + filtercount;
          let filtervalue = "filtervalue" + filtercount;
          let filtercondition = "filtercondition" + filtercount;
          let filteroperator = "filteroperator" + filtercount;
          let filterdatafield = "filterdatafield" + filtercount;
          let filtertype = "filtertype" + filtercount;
          filteritem[filtervalue] = this.allfilters[i].frastartdate; // moment(this.searchForm.controls.items.value[i].formatdate[0]).format('DD-MM-yyyy HH:mm:ss');   //+ ' 00:00:00';
          filteritem[filtercondition] = this.allfilters[i].filtercondition0;
          filteritem[filteroperatorindex] = this.allfilters[i].filtervalue;
          filteritem[filterdatafield] = this.allfilters[i].ld_brugervendtnoegle;
          filteritem[filtertype] = this.allfilters[i].filtertype;
          filteritem["filterscount"] = totalfiltercount;
        }
        if (this.allfilters[i].formatdate[1]) {
          if (filtercount === null) {
            filtercount = 0;
          } else {
            filtercount += 1;
          }
          totalfiltercount += 1;
          let filteroperatorindex = "filteroperator" + filtercount;
          let filtervalue = "filtervalue" + filtercount;
          let filtercondition = "filtercondition" + filtercount;
          let filteroperator = "filteroperator" + filtercount;
          let filterdatafield = "filterdatafield" + filtercount;
          let filtertype = "filtertype" + filtercount;
          filteritem[filtervalue] = this.allfilters[i].fraenddate; // moment(this.searchForm.controls.items.value[i].formatdate[0]).format('DD-MM-yyyy HH:mm:ss');   //+ ' 00:00:00';
          filteritem[filtercondition] = this.allfilters[i].filtercondition1;
          filteritem[filteroperatorindex] = this.allfilters[i].filtervalue;
          filteritem[filterdatafield] = this.allfilters[i].ld_brugervendtnoegle;
          filteritem[filtertype] = this.allfilters[i].filtertype;
          filteritem["filterscount"] = totalfiltercount;
        }
      }
    }
    this.services.setfiltersData(filteritem);
    let action = this.searchForm.controls.items.value.filter(
      (item) => item.requestParameter == "action"
    )[0].value;
    this.baserestService.searchgridData(this.uuid, action, filteritem).then(
      (res) => {
        this.successNotification.refresh();
        this.successNotification.open();
        this.loading = false;
        this.setdata(res);
      },
      (error) => {
        console.log(error);
        this.errorNotification.open();
        this.errorNotification.refresh();
        this.loading = false;
      }
    );
  }
  Fetchfilter() {
    this.sharedthis.fetchingfilter = true;
    if (this.sharedthis.filteruuid != 0) {
      this.sharedthis.service.getFilterData(this.filteruuid, this.uuid).then(
        (res: any) => {
          this.successNotification.refresh();
          this.successNotification.open();
          this.loading = false;
          this.setdata(res);
          $(".selectpicker").selectpicker("refresh");
          this.sharedthis.fetchingfilter = false;
        },
        (error) => {
          this.errorNotification.open();
          console.log(error);
        }
      );
    } else {
      this.sharedthis.fetchingfilter = false;
    }
  }
  getFilters() {
    this.baserestService.getmenufilters(this.uuid).then(
      (filters: any) => {
        this.allfilterList = this.allfilterList.concat(filters.data);
        this.filterList = this.allfilterList;
        this.services.setnewallfilterList(this.filterList);
        $(".selectpicker").selectpicker("refresh");
        this.ref.detectChanges();
      },
      (error) => {
        this.errorNotification.open();
        console.log(error);
      }
    );
  }
  setdata(result) {
    this.gridrowsandColumns = result;
    this.filteredGriddata.emit(this.gridrowsandColumns);
    this.activeIds = "";
  }
  formatData(item) {
    if (item && item.gui_filter) {
      if (item.type == "input") {
        return {
          filtertype0: "stringfilter",
          value: [item.value],
        };
      }
      if (item.type == "editor") {
        return {
          ld_brugervendtnoegle: item.ld_brugervendtnoegle,
          value: [item.value],
        };
      }
      if (item.type == "textarea") {
        return {
          ld_brugervendtnoegle: item.ld_brugervendtnoegle,
          value: [item.value],
        };
      }
      if (item.type == "html") {
        return {
          ld_brugervendtnoegle: item.ld_brugervendtnoegle,
          value: [item.value.replace(/(?:\r\n|\r|\n)/g, "<br>")],
        };
      }
      if (item.type == "date") {
        let formateddate: any = null;
        if (item.formatdate) {
          formateddate = moment(item.formatdate).format("YYYY-MM-DD HH:mm:ss");
          if (formateddate === "Invalid date") {
            formateddate = null;
          }
        }
        return {
          ld_brugervendtnoegle: item.ld_brugervendtnoegle,
          value: formateddate ? [formateddate] : [null],
        };
      }
      if (item.type == "dropdown" && item.multiselect == true) {
        var values = [];
        if (item.selected_name && item.selected_name.length != 0) {
          for (let i in item.options) {
            if (item.selected_name.indexOf(item.options[i].rel_uri) != -1) {
              values.push(item.options[i].rel_uuid);
            }
          }
        }
        return {
          ld_brugervendtnoegle: item.ld_brugervendtnoegle,
          value: values,
        };
      }
    }
  }
  getGriddata() {
    this.baserestService.getGriddata(this.uuid).then(
        (res) => {
            this.successNotification.refresh();
            this.successNotification.open();
            this.loading = false;
            this.setdata(res);
          },
          (error) => {
            console.log(error);
            this.errorNotification.open();
            this.errorNotification.refresh();
            this.loading = false;
          }
        );
  }
}
