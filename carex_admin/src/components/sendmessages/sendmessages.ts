import { Component, ChangeDetectorRef, ViewChild, ElementRef, NgZone } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { DateTimeAdapter } from 'ng-pick-datetime';
import * as moment from 'moment';
import { CKEditorComponent,CKEditor4 } from 'ckeditor4-angular';
import insertTextAtCursor from 'insert-text-at-cursor';


declare var $: any;
declare var document: any;

@Component({
    selector: 'sendmessages',
    templateUrl: 'sendmessages.html'
})

export class sendmessagesComponent {

    private allreceivers = [
        {
            'uuid': '1212',
            'titel': 'xyz'
        },
        {
            'uuid': '1212',
            'titel': 'abc'
        },
        {
            'uuid': '1212',
            'titel': 'def'
        }];

    private selecteddate: any;
    private slctreceiver;
    private skabelonname;
    private status;
    private slctskabelon;
    private editorData = '';
    public mindate: any;
    public startat;
    public config;

    private accordianitem;
    public ckeditor;


   // @ViewChild('editor', { static: false }) editorComponent:CKEditorComponent;



    @ViewChild('editor', { static: false }) editorComponent:any;




    private configoptions = "{ toolbar: [ 'heading', '|', 'bold', 'italic', 'link', '|', 'bulletedList', 'numberedList', 'blockQuote', 'codeBlock', '|', 'undo', 'redo']}";

    constructor(private baserestService: BaseRestService, private auth: AuthRestService, private dateTimeAdapter: DateTimeAdapter<any>,
        private _zone: NgZone, _elm: ElementRef, private ref: ChangeDetectorRef) {
        this.dateTimeAdapter.setLocale('da-DK');
        this.createstdate();
        this.config = {
            height : '500px',
            startupOutlineBlocks: true};
    }

    ngOnInit(): void {
       
    }
    ngAfterViewInit(): void {
        setTimeout(() => {
            $('select').selectpicker();
         
        }, 10);

    }
    DateChanged(selecteddate) {
        if (selecteddate && selecteddate._d) {
            this.selecteddate = moment(selecteddate._d).format('YYYY-MM-DD HH:mm:ss');
        }
    }
    selectedDate(e) {
        console.log(e);
    }
    onChange(e, i) {
        console.log(i);
    }
    onchngEditordata(editor) {
        // const data = editor.editor.getData();
        // c
        console.log(editor);
    }
    openfilter(editor){
        console.log("open");
        editor.instances.IDofEditor.insertText('some text here');
        // this.editorComponent.editing.view.change( writer => {
        //     const viewEditableRoot = editor.editing.view.document.getRoot();
        
        //     writer.setAttribute( 'myAttribute', 'value', viewEditableRoot );
        // } );

    }
    insertdata(){
        // this.editorComponent.instances.IDofEditor.insertText('some text here');  
        // CKEditor4.instances.IDofEditor.insertText('some text here');

        //   const selection = this.editorComponent.model.document.selection;
        //   const range = selection.getFirstRange();
        //   this.editorComponent.model.change(writer => {
        //     writer.insert('appendData', range.start);
        //   });
        //   const el = document.getElementById('editor');
        //   insertTextAtCursor(el, 'foobar');

        
    }
    createstdate() {
        this.startat = new Date().toLocaleString('dk-DA', { timeZone: 'Europe/Copenhagen' });
        let dd;
        let mm;
        let yyyy;
        if (this.startat.indexOf('/') != -1) {
            dd = this.startat.split("/")[0];
            mm = this.startat.split("/")[1];
            yyyy = this.startat.split("/")[2].split(",")[0];
        }
        else {
            dd = this.startat.split(".")[0];
            mm = this.startat.split(".")[1];
            yyyy = this.startat.split(".")[2].split(" ")[0];
        }

        let time = this.startat.split(' ')[1];
        let hh = time.split(':')[0];
        let mi = time.split(':')[1];
        let secs = time.split(':')[2];
        this.mindate = new Date(yyyy, mm - 1, dd, hh, mi);
    }
}