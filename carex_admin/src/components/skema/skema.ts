import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { ClipboardService } from 'ngx-clipboard';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';
import { HelperSerice } from '../../providers/utils/helperService';
import { saveAs } from 'file-saver';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';


declare var $: any;

@Component({
    selector: 'skema',
    templateUrl: 'skema.html'
})

export class Skema {
    private userrelations: any;
    private skemalist: any;
    private skemacontent: any;
    private modalReference: any;
    private Objekttype: any;
    private slctorg= null;
    private skema=null;
    private title;
    private teaser = '';
    private beskrivelse = '';
    private flettefelter = "[]";
    private body: any;
    private newbody: any;
    private slctlang = 'da';
    private loading = false;
    private authorisation;
    private skemaList;
    private sorteduserrelations;
    private slcOrgname: any;
    private slctbrugervendt: any;
    private orgcopyflag: boolean = false;
    private brugervendcopyflag: boolean = false;
    private textcopyflag: boolean = false;
    private jsoneditor;
    private issubmitted = false;
    private changedjsondata;
    private showupload = false;
    private slctdSkema = null;
    private ngbModalOptions: NgbModalOptions = {
        backdrop: 'static',
        keyboard: false
    };

    @ViewChild('successNotification', { static: false }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: false }) errorNotification: jqxNotificationComponent;
    public editorOptions: JsonEditorOptions;
    @ViewChild(JsonEditorComponent, { static: true }) editor: JsonEditorComponent;

    constructor(private baserestService: BaseRestService, private auth: AuthRestService, private ref: ChangeDetectorRef,
        private modalService: NgbModal,
        private clipboardService: ClipboardService, private helperService: HelperSerice) {
        this.editorOptions = new JsonEditorOptions();
        this.auth.userrelations.subscribe(
            (userrelations) => {this.userrelations = userrelations;});
    }

    ngOnInit() {
        console.log("in Skema");
        console.log(this.userrelations);
        let sortlist = this.helperService.sortList(this.userrelations);
        this.sorteduserrelations = sortlist;
        this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
    }

    ngAfterViewInit(): void {
        $('select').selectpicker();
    }
    onChange(e, value) {
        this.loading = true;
        console.log(e.target.value);
        this.skemacontent = null;
        let item = this.sorteduserrelations.filter(x => x.organisation_uuid == this.slctorg)[0];
        this.slcOrgname = item.org;
        this.getskemaslist();

    }
    getskemaslist(){  
        this.skemalist=null;
        this.skema=null;
        this.ref.detectChanges();
        this.baserestService.getskemaslist(this.slctorg).then(
            (skemalist: any) => {this.renderSkema(skemalist.data); },
            (error) => { console.log(error); this.loading = false; }
        )
}
    renderSkema(skemalist) {
        if (skemalist && skemalist.length > 0) {
            let sortedList = this.sortbyAlpha(skemalist);
            this.skemalist = sortedList;
            setTimeout(() => {
                $('select').selectpicker();
            });
        }
        this.slctdSkema = this.skemalist.filter(x => x.uuid == this.skema)[0];
        console.log(this.slctdSkema);
        this.loading = false;
    }
    sortbyAlpha(skemalist) {
        let sorted = skemalist.sort(function (a, b) {
            var nameA = a.brugervendtnoegle.toLowerCase(), nameB = b.brugervendtnoegle.toLowerCase();
            if (nameA < nameB)
                return -1;
            if (nameA > nameB)
                return 1;
            return 0;
        });
        return sorted;

    }
    onchangeSkema() {
        this.loading = true;
        this.slctdSkema = this.skemalist.filter(x => x.uuid == this.skema)[0];
        this.slctbrugervendt = this.slctdSkema.brugervendtnoegle;
        this.baserestService.getskemacontent(this.skema).then(
            (skemacontent: any) => { this.skemacontent = skemacontent; this.setData() },
            (error) => { console.log(error); this.loading = false; }
        )
    }
    setData() {
        this.loading = false;
        this.body = this.skemacontent;
        this.changedjsondata = this.skemacontent.body;
        setTimeout(() => {
            $('select').selectpicker();
        });
    }
    Copyorg(flag) {
        this.orgcopyflag = false; this.brugervendcopyflag = false; this.textcopyflag = false;
        switch (flag) {
            case "orgcopyflag":
                this.orgcopyflag = true;
                break;
            case "brugervendcopyflag":
                this.brugervendcopyflag = true;
                break;
            case "textcopyflag":
                this.textcopyflag = true;
                break;
            default:
                break;
        }
        //   this.clipboardService.copyFromContent(this.slcOrgname);
    }
    copied(e) {
        console.log(e);
    }
    Cancel(e) {
        e.preventDefault();
        this.skemalist = null;
        this.skemacontent = null;
        this.slctdSkema= null;
    }
    saveSkema(modal, event) {
        this.issubmitted = true;
        let bodydata: any;
            bodydata = JSON.stringify(this.changedjsondata);
        if (bodydata && this.showupload === false) {
            this.loading = true;
            this.baserestService.saveskemaoncontent(bodydata).then(
                (savedcontent) => {
                    this.successNotification.open();
                    this.successNotification.open(); this.loading = false;
                    this.getskemaslist();
                },
                (error) => {
                    console.log(error);
                    this.errorNotification.open();
                    this.errorNotification.open(); this.loading = false;
                })
        }
        else if ((!bodydata|| bodydata=="null") && this.showupload === true) {
            this.modalReference = this.modalService.open(modal, event);
            this.loading = false;
        }
    }
    createSkema(modal, event) {
        this.modalReference = this.modalService.open(modal, event);
    }
    DownloadJson(e) {
        e.preventDefault();
        const blob = new Blob([JSON.stringify(this.body)], { type: 'application/json' });
        saveAs(blob, this.slctbrugervendt + '_skema' + '.json');
    }
    Deleteconfig(e) {
        e.preventDefault();
        this.body = null;;
        this.changedjsondata = null;;
        this.skemacontent = null;;
        this.showupload = true;
        this.ref.detectChanges();
    }
    uploadfile(evt) {
        try {
            let files = evt.target.files;
            if (!files.length) {
                console.log('No file selected!');
                this.errorNotification.open();
                this.errorNotification.open();
                return;
            }
            let file = files[0];
            let reader = new FileReader();
            const self = this;
            reader.onload = (event: any) => {
                let data: any = JSON.parse(event.target.result);
                this.body = data;
                this.changedjsondata = data;
                this.skemacontent = data;

                this.showupload = false;
                this.ref.detectChanges();
            };
            reader.readAsText(file);
        } catch (err) {
            console.error(err);
        }
    }
    getData(e) {
        this.changedjsondata = e;
    }
}