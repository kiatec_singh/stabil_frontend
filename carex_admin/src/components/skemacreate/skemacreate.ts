import { ChangeDetectorRef, Component, Input, ViewChild, } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseRestService } from '../../providers/base.rest.service';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
import { JsonEditorOptions } from 'ang-jsoneditor';

@Component({
    selector: 'skemacreate-viewer',
    templateUrl: 'skemacreate.html'
})
export class skemacreateComponent {


    private loading = false;
    @ViewChild('successNotification', { static: true }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: true }) errorNotification: jqxNotificationComponent;
    private changedjsondata;
    private showupload = true;
    public editorOptions: JsonEditorOptions;
    private bodydata;
  


    constructor(public activeModal: NgbModal, private ref: ChangeDetectorRef, private baserestService: BaseRestService) {
        this.editorOptions = new JsonEditorOptions();
    }

    ngOnInit() {
        this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
    }

    cancel(e) {
        this.activeModal.dismissAll();
    }
    uploadNew(e) {
        e.preventDefault();
        this.bodydata = JSON.stringify(this.changedjsondata);
        this.changedjsondata = this.bodydata;
        this.ref.detectChanges();
    }
    getDataa(e) {
        this.changedjsondata = e;
    }
    uploadfilee(evt) {
        try {
            let files = evt.target.files;
            if (!files.length) {
                console.log('No file selected!');
                this.errorNotification.open();
                this.errorNotification.open();
                return;
            }
            let file = files[0];
            let reader = new FileReader();
            const self = this;
            reader.onload = (event: any) => {
                let data: any = JSON.parse(event.target.result);
                this.changedjsondata = data;
                this.bodydata = data;
                this.showupload = false;
                this.ref.detectChanges();
            };
            reader.readAsText(file);
        } catch (err) {
            console.error(err);
        }
    }
    skemadelete(e) {
        console.log(e);
        this.loading = true;
        let bodydata: any;
        bodydata = JSON.stringify(this.changedjsondata);
        console.log(bodydata);
    }
    skemacreate(){
        this.loading = true;
       this.baserestService.saveskemaoncontent( JSON.stringify(this.changedjsondata)).then(
        (savedcontent) => {
            this.successNotification.open();
            this.successNotification.open(); this.loading = false;
            this.activeModal.dismissAll();
        },
        (error) => {
            console.log(error);
            this.errorNotification.open();
            this.errorNotification.open(); this.loading = false;
            this.activeModal.dismissAll();
        })    
    }
 
}