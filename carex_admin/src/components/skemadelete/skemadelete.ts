import { Component, Input, ViewChild, } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseRestService } from '../../providers/base.rest.service';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';

@Component({
    selector: 'skemadelete-viewer',
    templateUrl: 'skemadelete.html'
})
export class skemadeleteComponent {

    @Input() slctdSkema: any;
    private loading=false;
    @ViewChild('successNotification', { static: true }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: true }) errorNotification: jqxNotificationComponent;


    constructor(public activeModal: NgbModal, private baserestService: BaseRestService) {
    }

    ngOnInit() {
    }

    cancel(e) {
        this.activeModal.dismissAll();
    }
    skemadelete(e) {
        console.log(e);
        this.loading = true;
        this.baserestService.deleteSkema(this.slctdSkema.uuid).then(
            (success) => {
                this.loading = false;
                this.successNotification.refresh();
                this.successNotification.open();
                this.activeModal.dismissAll();
            },
            (error) => {
                this.loading = false;
                this.errorNotification.refresh();
                this.errorNotification.open();
                this.activeModal.dismissAll();
            }
        )
    }
}