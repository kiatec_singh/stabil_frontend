import { Component, ElementRef, ViewChild } from '@angular/core';
import { BaseRestService } from '../../providers/base.rest.service';
import { AuthRestService } from '../../providers/auth.rest.service';
import { HelperSerice } from '../../providers/utils/helperService';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';
declare var $: any;

@Component({
    selector: 'uploads-viewer',
    templateUrl: 'uploads.html'
})
export class uploadsComponent {

    @ViewChild('fileUpload', { static: false }) fileUpload: any;
    @ViewChild('uploadhandler', { static: false }) uploadhandler: ElementRef;

    @ViewChild('successNotification', { static: true }) successNotification: jqxNotificationComponent;
    @ViewChild('errorNotification', { static: true }) errorNotification: jqxNotificationComponent;

    private slctpath=null;
    private pdfconfig: any;
    private picconfig = {
        multiple: true,
        formatsAllowed: ".jpg,.png,.pdf",
        maxSize: "25",
        uploadAPI: {
            url: "https//uat-admin.carex.dk/pictures/",
            headers: {
                "Content-Type": "text/plain;charset=UTF-8",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT",
                "Accept": "application/json",
                "Access-Control-Allow-Headers": "X-Requested-With"
            }
        },
        theme: "",
        hideProgressBar: true,
        hideResetBtn: true,
        hideSelectBtn: false,
        replaceTexts: {
            selectFileBtn: 'Upload files',
            resetBtn: 'Reset files',
            uploadBtn: 'Upload pictures',
            dragNDropBox: 'Drag N Drop',
            attachPinBtn: 'Attach Files...',
            afterUploadMsg_success: 'Successfully Uploaded !',
            afterUploadMsg_error: 'Upload Failed !'
        }

    };
    private userrelations;
    private sorteduserrelations;
    private loading = false;
    private modalReference: any;
    private fileToUpload: File = null;
    private overwrite = false;
    private error = false;
    private success=false;
    private failedfiles=[];
    private alloverwrite=false;
    private ngbModalOptions: NgbModalOptions = {
        backdrop: 'static',
        keyboard: false
    };
    private message = '';
    private slctOrg;
    private paths = [
        {
            uuid: 'gjensidige',
            path: 'Public'
        },
        {
            uuid: 'path_uploads',
            path: 'path_uploads'
        },
        {
            uuid: 'path_pictures',
            path: 'path_pictures'
        },
    ]
    constructor(private baserestService: BaseRestService,  private modalService: NgbModal,private auth: AuthRestService, private helperService: HelperSerice) {
        this.auth.userrelations.subscribe(
            (userrelations) => {
                this.userrelations = userrelations;
            }
        );
    }

    ngOnInit() {
        let sortlist = this.helperService.sortList(this.userrelations);
        this.sorteduserrelations = sortlist;
    }
    ngAfterViewInit(): void {
        $('select').selectpicker();
    }
    onChange(event, item) {
        if( this.fileUpload &&  this.fileUpload.selectedFiles){
            this.fileUpload.resetFileUpload();
        }
        setTimeout(() => {
            $('select').selectpicker();
        }, 50);
    }
    onchangeOrg(event, item) {
        console.log('');
    }
    Changeoverwrite(e) {
        console.log('');
    }
    savefiles() {
        this.loading = true;
        this.baserestService.uploadFiles(this.slctpath, this.fileUpload.selectedFiles, this.overwrite).then(
            (success) => {
                this.loading = false;
                this.handler(success)
            },
            (error) => {
                this.loading = false;
                this.error = true;
                this.handler(error)
                this.message = error.error.besked;
                console.log(error)
            }
        )
    }
    handler(staus) {
        let statusfile =[];
       if(this.modalReference && !staus.errors){
         this.modalReference.close();
          this.fileUpload.resetFileUpload();
          this.slctpath = null;
          $('select').selectpicker();
       } 
        if(staus  && staus.errors){
            statusfile = staus.errors;
            statusfile.map(item=>{item.ischecked=false});
             this.failedfiles=statusfile;
            this.modalReference = this.modalService.open(this.uploadhandler, this.ngbModalOptions);
            this.modalReference.result.then((result) => {
            }, (reason) => {
              console.log(reason);
            });
        }else{
            this.success= true;
            this.successNotification.open();
            this.message ='Success';
        }

  
    }
    Changeoverwritefile(e,item){
    item.ischecked =  e.target.checked;
    }
    Saveagain(){
        this.loading =true;
        let result=[];
        if(this.alloverwrite===true){
             result = this.fileUpload.selectedFiles.filter(o =>  this.failedfiles.some(({file}) => o.name === file));
        }
        else{
            result = this.fileUpload.selectedFiles.filter(o =>  this.failedfiles.some(({file,ischecked}) => o.name === file && ischecked===true));
        }
        this.baserestService.uploadFiles(this.slctpath, result, true).then(
            (success) => {
                this.loading = false;
                this.successNotification.open();
                this.handler(success);
            },
            (error) => {
                this.loading = false;
                this.error = true;
                this.handler(error);
                this.message = error.error.besked;
                console.log(error);
                this.errorNotification.open();this.errorNotification.open();
            }
        )
        this.loading =false;
    }
    cancel(){
        this.modalReference.close();
        this.loading = false;
    }
}