import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BaseRestService } from '../../providers/base.rest.service';
import { StorageService } from '../../providers/storageservice/storageservice';
import { AuthRestService } from '../../providers/auth.rest.service';



@Component({
    selector: 'username-viewer',
    templateUrl: 'username.html'
})
export class UsernameComponent {

    private username;
    private errormessage;
    private error = false;
    private loading = false;
    private noauthorisation=false;
    constructor(private router: Router, private baserestService: BaseRestService) {
        this.username = this.router.getCurrentNavigation().extras &&  this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.username ? this.router.getCurrentNavigation().extras.state.username:'';

    }
    ngOnInit(): void {
        //Called after the constructor, i
      
        //Add 'implements OnInit' to the class.

    }
    otp() {
        this.loading =true;
        this.baserestService.OTP(this.username).then(
            (success) => {
                this.loading =false;
                this.setuserData(success);
            },
            (error) => {
                this.error = true;
                this.loading =false;
                this.errormessage = error.error.text?error.error.text:error.error.error;
            }
        )
    }

    setuserData(success) {
        this.loading = false;
        if (success === true) {
            this.router.navigate(['otp'],{ state: { username: this.username  } });
            return;
        }
        if (success === false) {
            this.error = true;
            return;
        }
        else {
            this.noauthorisation = true;
            return;

        }
    }
}