import { Component, Input, Output, EventEmitter, } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Services } from '../../providers/services/services';


@Component({
    selector: 'warning-viewer',
    templateUrl: 'warning.html'
})
export class warningComponent {

     @Output() confirmreplace= new EventEmitter<string>();
     private replace;
     private modalref;

    constructor(public activeModal: NgbModal,  public services:Services) {
    }

    ngOnInit() {

    }

    cancel(e) {
        this.replace = false;
        this.confirmreplace.emit(this.replace);
        this.modalref =this.services.getmodalReference()
        this.modalref.close();
    }
    apply(e){
        this.replace = true;
        this.confirmreplace.emit(this.replace);
        this.modalref =this.services.getmodalReference()
        this.modalref.close();
    }
}