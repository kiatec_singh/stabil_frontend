// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import * as packagejson from './../../package.json';
export const environment = {
  production: true,
  baseURL :"https://api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://idp.carex.dk/endpoints/login_services.php",
  skabelonUrl:"https://idp.carex.dk/html/skabelonapi.php",
  idpservicesUrl:"https://idp.carex.dk/endpoints/idp_service.php",
  idpservicesUrl_v2:"https://idp.carex.dk/v2/endpoints/idp_service.php",
  picturesUrl:"https//demo-admin.carex.dk/pictures/",
  saveUrl:"https://admin.carex.dk/pictures/save-file.php",
  applikation_uuid:"48b00aae-5001-4b4f-9eea-b7bd61145686",
  context:"1c812baf-8189-4f32-ad50-658b29006f0d",
  version: packagejson.version,
  envi:"prod"
};
