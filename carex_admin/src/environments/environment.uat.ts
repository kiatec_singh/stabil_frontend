import * as packagejson from './../../package.json';
export const environment = {
  production: true,
  baseURL :"https://uat-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://uat-idp.carex.dk/endpoints/login_services.php",
  skabelonUrl:"https://uat-idp.carex.dk/html/skabelonapi.php",
  idpservicesUrl:"https://uat-idp.carex.dk/endpoints/idp_service.php",
  idpservicesUrl_v2:"https://uat-idp.carex.dk/v2/endpoints/idp_service.php",
  picturesUrl:"https//uat-admin.carex.dk/pictures/",
  saveUrl:"https://uat-admin.carex.dk/pictures/save-file.php",
  applikation_uuid:"48b00aae-5001-4b4f-9eea-b7bd61145686",
  context:"1c812baf-8189-4f32-ad50-658b29006f0d",
  version: packagejson.version,
  envi:"uat"
};
