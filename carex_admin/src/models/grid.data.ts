export class GridData {

    constructor(griddata) {
        this.uuid = griddata.uuid;
        this.fra = griddata.fra;
        this.til = griddata.til;
        this.note = griddata.note;
        this.facetbeskrivelse = griddata.facetbeskrivelse;
        this.brugervendtnoegle = griddata.brugervendtnoegle;
        this.facetophavsret = griddata.facetophavsret;
        this.facetopbygning = griddata.facetopbygning;
        this.facetplan = griddata.facetplan;
        this.note = griddata.note;
        this.facetophavsret = griddata.facetophavsret;
        this.retskilde = griddata.retskilde;
        this.publiceret = griddata.publiceret;
        this.redaktoerer = griddata.create_timestamp;
        this.facetsupplement = griddata.facetsupplement;
        this.ansvarlig_uuid = griddata.ansvarlig_uuid;
        this.ejer_uuid = griddata.ejer_uuid;
        this.facettilhoerer_uuid = griddata.facettilhoerer_uuid;
        this.redaktoerer_uuid = griddata.redaktoerer_uuid;
        this.ejer = griddata.ejer;

    }

    public uuid: string;
    public fra: string;
    public til: string;
    public note: string;
    public facetopbygning: string;
    public facetbeskrivelse: string;
    public brugervendtnoegle: number;
    public facetplan: string;
    public facetsupplement: string;
    public facetophavsret: string;
    public retskilde: string;
    public publiceret: string;
    public ansvarlig_uuid: string;
    public ejer_uuid: string;
    public facettilhoerer_uuid: string;
    public redaktoerer_uuid: string;
    public ansvarlig: string;
    public ejer: string;
    public facettilhoerer: string;
    public redaktoerer: string;
}

export class Columns {
    constructor(input) {
        const ColumnsList: GridData[] = input;
        return ColumnsList;
    }
}

export class RowsList {
    constructor(input) {
        const RowsList: GridData[] = input;
        return RowsList;
    }
}


export class ObjectType {
    constructor(hovedobjekt) {
        const ObjektType: String = hovedobjekt;
        return ObjektType;
    }
}

export class DataFields {
    constructor(input) {
        const DataFieldsList: GridData[] = input;
        return DataFieldsList;
    }
}