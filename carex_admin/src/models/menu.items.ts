export class MenuItems {

    constructor(menuItem) {
        this.id = menuItem.id;
        this.uuid = menuItem.uuid;
        this.virkning_id = menuItem.virkning_id;
        this.facetbeskrivelse = menuItem.facetbeskrivelse;
        this.brugervendtnoegle = menuItem.brugervendtnoegle;
        this.facetophavsret = menuItem.facetophavsret;
        this.personliste = menuItem.personliste;
        this.facetplan = menuItem.facetplan;
        this.note = menuItem.note;
        this.facetophavsret =menuItem.facetophavsret;
        this.retskilde= menuItem.retskilde;
        this.publiceret = menuItem.publiceret;
        this.create_timestamp = menuItem.create_timestamp;

    } 
    public id: number;
    public uuid: string;
    public virkning_id: string;
    public facetbeskrivelse: string;
    public brugervendtnoegle: number;
    public facetsupplement: string;
    public personliste: string;
    public facetplan: string;
    public note: string;
    public facetophavsret: string;
    public facetopbygning: string;
    public retskilde: string;
    public publiceret: string;
    public create_timestamp: string;

}

export class Menu{
    constructor(menu){
        this.objArray = menu;
    }

    public objArray: Array<MenuItems>[]
}

export class MenuListItems {
    constructor(input){
        const MenuList: Menu[] = input;
        return  MenuList;
    }
}