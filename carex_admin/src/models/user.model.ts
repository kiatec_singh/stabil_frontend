export class UserData {

    public uuid: string;
    public emailid: string;
    public bruger_group: string;
    public type: string;
    public gyldighed: string;
    public username: string;
    public brugernavn: string;
    constructor(input) {
        this.uuid = input.id;
        this.emailid = input.email;
        this.gyldighed = input.gyldighed;
        this.username = input.username;
        this.brugernavn = input.brugernavn;
    }
}
