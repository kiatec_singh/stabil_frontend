import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UserData } from '../models/user.model';


@Injectable()
export class AuthRestService {
    public userdata: UserData;
    public userrelations= new BehaviorSubject<any>([]);
    public menulist= new BehaviorSubject<any>([]);
    public client;
    public userToken = new BehaviorSubject<any>([]);
    public userobj = new BehaviorSubject<any>([]);

    constructor() {

    }

    setUserData(data): void {
        this.userdata = new UserData(data.result[0]);
    }
    setuserrelations(userrelations) {
        this.userrelations.next(userrelations);
    }

    getClient() {
        return this.client;
    }
    setClient(client) {
        this.client = client;
    }
    setuserToken(value: any) {
        this.userToken.next(value);
    }
    setuserobj(value: any) {
        this.userobj.next(value);
    }
    setmenulist(value: any) {
        this.menulist.next(value);
    }

}