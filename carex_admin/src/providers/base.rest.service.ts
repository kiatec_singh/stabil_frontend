import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Form } from '@angular/forms';
import { Subscriber,Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { UserData } from '../models/user.model';
import { DetailsData, Structure } from '../models/details.data';
import { environment } from '../environments/environment';
import { AuthRestService } from './auth.rest.service';

declare var $: any;

@Injectable()
export class BaseRestService {
    headers: Headers;
    private options;
    form: Form;
    private detailsdata: DetailsData;
    private structure: Structure;
    private userData: UserData;
    private authorisation;
    private subscriber: Subscriber<any>
    private idpservicesUrl;
    private baseUrl;
    private userToken;

    // URL to web api
    constructor(private http: HttpClient,private auth:AuthRestService,private authService: AuthRestService) {
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
            'Accept': 'application/json',
            "Access-Control-Allow-Headers": "X-Requested-With"
        });
        this.options = new RequestOptions({ headers: this.headers });
        this.authService.userobj.subscribe(
            (userobj)=>{
                if(userobj && userobj.id){
                this.userData = new UserData(userobj);
                }
            });
        this.baseUrl = environment.baseURL;
        this.idpservicesUrl = environment.idpservicesUrl_v2;
        this.auth.userToken.subscribe(
            (userToken: any) => {
                    this.userToken = userToken;
            }
        );
    }
    private formdata;

    getEnvironment() {
        return this.baseUrl;
    }

    getAllMenuList(uid): any {
        this.formdata = new FormData();
        let userid = uid?uid:this.userData.uuid;
        this.formdata.append('action', 'list_menuitems');
        this.formdata.append('bruger_uuid', userid);
        this.formdata.append('applikation_uuid', environment.applikation_uuid);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    getGriddata(uuid): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', uuid);
        this.formdata.append('pageSize', 10000);
        this.formdata.append('page', 1);
        this.formdata.append('context', environment.context);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    getDetailsdata(uuid, listId): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_objekt_detaljer');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('objekt_uuid', uuid);
        this.formdata.append('skema_uuid', listId);
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    getcreateDetailsdata(listId): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_objekt_detaljer');
        this.formdata.append('userToken',  this.userToken);
        this.formdata.append('skema_uuid', listId);
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    downloadcsvFile(listId, userToken, requestedfiletype): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_eksporter_objektdata');
        this.formdata.append('userToken',  this.userToken);
        this.formdata.append('skema_uuid', listId);
        this.formdata.append('requestedfiletype', requestedfiletype)
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();

    }
    uploadcsvFile(file, filename, listId): any {
        this.formdata = new FormData();
        let o: any = new Observable(obs => this.subscriber = obs);
        this.formdata.append('action', 'CALL_importer_objektdata');
        this.formdata.append('userToken', this.userToken );
        this.formdata.append('skema_uuid', listId);
        this.formdata.append('fil', file);
        this.formdata.append('filnavn', filename);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();

    }
    public cancelRequest() {
        this.subscriber.error();
    }
    updateDetailsdata(detailsuuid, listedefinition_uuid, update_timestamp, updateddata, allfiles): any {
        this.formdata = new FormData();
        let objectuuid = listedefinition_uuid ? listedefinition_uuid : '';
        this.formdata.append('action', 'CALL_update_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('objekt_uuid', objectuuid);
        this.formdata.append('update_objekt', updateddata);
        this.formdata.append('update_timestamp', update_timestamp);
        if (allfiles) {
            for (let i = 0; i < allfiles.length; i++) {
                let name = allfiles[i].name;
                let value = allfiles[i].value;
                this.formdata.append(name, value);
            }
            this
        }
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise();
    }
    createDetailsdata(detailsuuid, update_timestamp, updateddata, allfiles): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_update_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('update_objekt', updateddata);
        this.formdata.append('update_timestamp', update_timestamp);
        if (allfiles) {
            for (let i = 0; i < allfiles.length; i++) {
                let name = allfiles[i].name;
                let value = allfiles[i].value;
                this.formdata.append(name, value);
            }
            this
        }

        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise();
    }
    getbrandConfig(brand_uuid, auth) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getBrand');
        this.formdata.append('version', '1.2.92');
        this.formdata.append('userToken', auth);
        this.formdata.append('itsystem_uuid', environment.applikation_uuid);
        this.formdata.append('organisation_uuid', environment.applikation_uuid);
        this.formdata.append('brand_uuid', brand_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    }
    downloadFileDocument(detailsuuid, listedefinition_uuid): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_get_file');
        this.formdata.append('userToken', this.userData.uuid);
        this.formdata.append('dokument_uuid', listedefinition_uuid);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    }
    exportFileDocument(detailsuuid, filenavn, path, auth): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_get_exportfile');
        this.formdata.append('userToken', auth);
        this.formdata.append('filnavn', filenavn);
        this.formdata.append('filpath', path);
        this.formdata.append('itsystem_uuid', environment.applikation_uuid);
        return this.http.post(this.baseUrl, this.formdata).toPromise();
    }
    deleteDetailsdata(detailsuuid, listedefinition_uuid, update_timestamp): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_delete_objekt');
        this.formdata.append('skema_uuid', detailsuuid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('objekt_uuid', listedefinition_uuid);
        this.formdata.append('update_timestamp', update_timestamp);
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise();
    }
    getDokumentList() {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokument_list');
        this.formdata.append('dokument_type', 'brand');
        this.formdata.append('userToken', this.userToken);
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise();
    }
    getBrands(dokument_uuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokumentfil_read');
        this.formdata.append('objekt_uuid', dokument_uuid);
        this.formdata.append('userToken', authid);
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise();
    }
    savesConfiguration(dokument_uuid, body, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokumentfil_ajour');
        this.formdata.append('objekt_uuid', dokument_uuid);
        this.formdata.append('userToken', authid);
        this.formdata.append('json', body)
        return this.http.post<DetailsData[]>(this.baseUrl, this.formdata).toPromise()
    }

    setupdatedDetailsdata(data) {
        this.structure = new Structure(data.result.struktur);
        this.detailsdata = new DetailsData(data.result.bi_objekter);
    }
    SetDetailsData(data) {
        this.detailsdata = new DetailsData(data.result);
    }
    getuseranalytics(itsystem_uuid, fromDate, toDate) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getuseranalytics');
        this.formdata.append('applikation_uuid', JSON.stringify(itsystem_uuid));
        this.formdata.append('fromDate', fromDate);
        this.formdata.append('toDate', toDate);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).map(res => res).toPromise();
    }
    
    translateImport(file, filename, objekttype, organisation): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'importTranslations');
        this.formdata.append('userToken',  this.userToken);
        this.formdata.append('organisation', organisation);
        this.formdata.append('objekttype', objekttype);
        this.formdata.append('fil', file);
        this.formdata.append('filnavn', filename);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();

    }
    translateExport(objekttype, organisation): any {
        this.formdata = new FormData();
        this.formdata.append('action', 'exportTranslations');
        this.formdata.append('userToken',  this.userToken);
        this.formdata.append('organisation', organisation);
        this.formdata.append('objekttype', objekttype);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();

    }
    getskabelonlist(organisationsid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getskabelon');
        this.formdata.append('organisationsid', organisationsid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('bruger_uuid', this.userData.uuid);
        this.formdata.append('applikation_uuid', environment.applikation_uuid);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).map(res => res).toPromise();

    }
    getskemaslist(organisationsid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'skema_list');
        this.formdata.append('skema_tilhoerer_uuid', organisationsid);
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).map(res => res).toPromise();
    }
    getskemacontent(skema_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'skema_read');
        this.formdata.append('skema_uuid', skema_uuid);
         this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).map(res => res).toPromise();
    }
    saveskemaoncontent(skema_json) {
        this.formdata = new FormData();
        this.formdata.append('action', 'skema_ajour');
        this.formdata.append('skema_json', skema_json);
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).map(res => res).toPromise();
    }
    deleteSkema(skema_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'skema_delete');
        this.formdata.append('skema_uuid', skema_uuid);
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).map(res => res).toPromise();
    }
    getskabeloncontent(skabelonid, lang) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getskabeloncontent');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skabelonid', skabelonid);
        this.formdata.append('sprog', lang);
        return this.http.post(this.idpservicesUrl, this.formdata).map(res => res).toPromise();

    }
    saveskabeloncontent(skabelonid, title, teaser, beskrivelse,ikon, flettefelter, slctlang, body) {
        this.formdata = new FormData();
        this.formdata.append('action', 'saveskabeloncontent');
        this.formdata.append('skabelonid', skabelonid);
        this.formdata.append('titel', title);
        this.formdata.append('teaser', teaser);
        this.formdata.append('beskrivelse', beskrivelse);
        this.formdata.append('ikon', ikon);
        this.formdata.append('sprog', slctlang);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('flettefelter', flettefelter);
        this.formdata.append('tekst', body);
        return this.http.post(this.idpservicesUrl, this.formdata).map(res => res).toPromise();

    }
    getmenufilters(objekt_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokument_list');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('dokument_type', 'filtergui');
        this.formdata.append('indhold', objekt_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    }
    readfilterdata(dokument_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'dokumentfil_read');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('objekt_uuid', dokument_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    }
    savefilterdata(filterRequest) {
        this.formdata = new FormData();
        this.formdata.append('action', 'filterAjour');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('filterRequest', filterRequest);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();

    }
    getsearch(skema_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getObjektfilter');
        this.formdata.append('userToken',  this.userToken);
        this.formdata.append('skema_uuid', skema_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();

    }
    
    getEditors() {
        this.formdata = new FormData();
        this.formdata.append('action', 'getRedaktoer');
        this.formdata.append('userToken',  this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();

    }
    notificationBatch(action,selectedUuids, selecteddate) {
        this.formdata = new FormData();
        this.formdata.append('action','notificationBatch' );
        this.formdata.append('batchOperation',action );
        this.formdata.append('selectedUuids',selectedUuids );
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('dateTime', selecteddate);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();

    }
    notificationsDispatch(action) {
        this.formdata = new FormData();
        this.formdata.append('action',action );
        this.formdata.append('userToken', this.userToken);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();

    }
    uploadFiles(path,files,overwrite) {
        this.formdata = new FormData();
        for (let i in files) {
            this.formdata.append('files[]', files[i]);
        }
        this.formdata.append('action','uploadFileAppcontent' );
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('path', path);
        this.formdata.append('overwrite', overwrite);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();

    }
    login(username, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'login');
        this.formdata.append('id', username);
        this.formdata.append('password', password);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).toPromise();
    }
    OTP(username) {
        this.formdata = new FormData();
        this.formdata.append('action', 'OTP');
        this.formdata.append('username', username);
        this.formdata.append('applikation_uuid', environment.applikation_uuid);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).toPromise();
    }
    verifyOTP(username,otp) {
        this.formdata = new FormData();
        this.formdata.append('action', 'verifyOTP');
        this.formdata.append('username', username);
        this.formdata.append('otp', otp);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).toPromise();
    }
    searchgridData(skema_uuid,action, formdata)
    {
        this.formdata = new FormData();
        this.formdata.append('action', action);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', skema_uuid);
        if((Object.keys(formdata)).length>0){
        this.formdata.append('filter_json', JSON.stringify(formdata));
        }
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise() 
    }
    searchgridDatausinggemtFilter(filter_uuid,uuid){
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', uuid);
        this.formdata.append('filter_uuid', filter_uuid);
        this.formdata.append('pageSize', 10000);
        this.formdata.append('page', 1);
        this.formdata.append('context', environment.context);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    
    getFilterData(filter_uuid,uuid){
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('skema_uuid', uuid);
        this.formdata.append('filter_uuid', filter_uuid);
        return this.http.post(this.baseUrl, this.formdata).map(res => res).toPromise();
    }
    changePassword(id, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'changePassword');
        this.formdata.append('id', id);
        this.formdata.append('password', password);
        return this.http.post(this.idpservicesUrl, this.formdata, this.options).toPromise();
    }
    readsearchData(skema_uuid,filter_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'list_objekt_data');
        this.formdata.append('skema_uuid', skema_uuid);
        this.formdata.append('userToken', this.userToken);
        this.formdata.append('filter_uuid', filter_uuid);
        return this.http.post(this.baseUrl, this.formdata, this.options).toPromise();
    }
    
}
