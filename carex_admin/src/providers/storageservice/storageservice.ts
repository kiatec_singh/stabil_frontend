
import { Injectable } from '@angular/core';
@Injectable()
export class StorageService {
  constructor() {

  }
  set(name, value): any {
    try {
      localStorage.setItem(name, JSON.stringify(value));
    } catch (e) {
      console.error('Error saving to localStorage', e);
    }
  }

  get(name){
    return Promise.resolve().then(function () {
      return JSON.parse(localStorage.getItem(name));
    });
  }
  clear() {
    try {
      localStorage.clear();
    } catch (e) {
      console.error('Error getting data from localStorage', e);
      return null;
    }
  }
}
