
import { Injectable } from '@angular/core';
import * as moment from 'moment';


@Injectable()
export class HelperSerice {

    public mindate;
    constructor() {

    }

    formatDate(datetoformat) {
        moment.locale('da');
        let startat, starthour, day, dd, mm, yyyy, hh, mi: any;
        if (datetoformat) {
            startat = moment(datetoformat).format('lll');
            starthour = moment(datetoformat).format('LT');
            day = moment(datetoformat).format('L');

        } else {
            startat = moment().format('lll');
            starthour = moment().format('LT');
            day = moment().format('L');
        }
        yyyy = day.split('.')[2];
        mm = day.split('.')[1];
        dd = day.split('.')[0];
        hh = starthour.split(':')[0];
        mi = starthour.split(':')[1];
        return new Date(yyyy, mm - 1, dd, hh, mi);
    }
    analticsformatDate(datetoformat) {
        moment.locale('da');
        let startat, starthour, day, dd, mm, yyyy, hh, mi: any;
        if (datetoformat) {
            startat = moment(datetoformat).format('lll');
            starthour = moment(datetoformat).format('LT');
            day = moment(datetoformat).format('L');

        } else {
            startat = moment().format('lll');
            starthour = moment().format('LT');
            day = moment().format('L');
        }
        yyyy = day.split('.')[2];
        mm = day.split('.')[1];
        dd = day.split('.')[0];
        hh = starthour.split(':')[0];
        mi = starthour.split(':')[1];
        let startdate =  moment(new Date(yyyy, mm - 1, dd, 0, 0)).subtract(1, 'months').format('yyyy-MM-DD HH:mm');
        return [new Date(startdate),new Date(yyyy, mm - 1, dd, 0, 0)];
    }
    sortList(userrelations) {
        let sorted = userrelations.sort(function (a, b) {
            if (a.org && b.org) {
                var nameA = a.org.toLowerCase(), nameB = b.org.toLowerCase();
                if (nameA < nameB) { //sort string ascending
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                else {
                    return 0;
                }
            }
        });
        return sorted;
    }

    sortmenuList(userrelations) {
        let sorted = userrelations.sort(function (a, b) {
            if (a.placering && b.placering) {
                var nameA = +a.placering, nameB = +b.placering;
                if (nameA < nameB) //sort string ascending
                    return -1;
                if (nameA > nameB)
                    return 1;
                return 0;
            }
        });
        return sorted;
    }

}